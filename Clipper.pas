unit Clipper;
(******************************************************************************

	Program:    unit Clipper
	Author:     Denis A. Sarrazin
	Copyright   Denis A. Sarrazin
	Dialect:    Delphi/Object-Pascal
	Date:       September 1995
	Notes:      See below

	This source code is for the use of Aquarium subscribers, who may
	include it in their applications as they see fit.  Distribution
	and duplication by any means, including printed and electronic,
	is prohibited without the express written consent of Grumpfish,
	Inc.

 The following file contains a number of functions, procedures and classes which
 duplicate the functionality of certain Clipper functions.

 === Special Notes ===

 (1) Empty is implemented only to work with strings

 (2) The AddLine procedure is a simple replacement for the SET ALTERNATE TO <fn> ADDITIVE

 (3) The Clipper DIRECTORY functionality has been duplicated using a class
     rather than a function.

     Example in Clipper:
       #include "directry.ch"
       procedure Test
         local aDir := Directory("*.*","D")
         local i

         for i := 1 to LEN(aDir)
           ? "["+aDir[i][F_NAME]+"]"
         next i
       return

     Example in Delphi:
       procedure Test;
       var
         aDir: TDirectory;
         i: integer;
       begin
         aDir := TDirectory.Create;           {create an object of this type}
         try
           aDir.Directory('*.*', 'D');        {get the directory of files}
           for i := 0 to aDir.Count-1 do
             ShowMessage('['+aDir.FileName[i]+']'); {display all names retrieved}
         finally
           aDir.Free;
         end;
       end;
     }
 ******************************************************************************)

interface

uses
  DBConsts, DB, BDE, DBTables, DbiProcs, DbiTypes, SysUtils, Classes, Forms, WinProcs, Dialogs;

const
  SHOW_DELETED = True; // used by SetDeleted to show the deleted records
  Y2K_CUTOFF = 50;
  
type
  TDirectory = class
    private
      FInfo: TList; {of TSearchRec}
      function GetName(i: integer): string;
      function GetDateTime(i: integer): TDateTime;
      function GetAttribute(i: integer): string;
      function GetSize(i: integer): longint;
    public
      constructor Create;
      destructor Free;
      procedure Directory(Specs: string; Attrib: string);
      function Count: integer;
      property FileName[i: integer]: string read GetName;
      property FileDateTime[i: integer]: TDateTime read GetDateTime;
      property FileAttr[i: integer]: string read GetAttribute;
      property FileSize[i: integer]: longint read GetSize;
  end;

procedure AddLine(filename, line: string);

function ALLTRIM(cString: string): string;
function CDOW(dWork: TDateTime): string;
function CENTER(cString: string; nWidth: integer): string;
function CMONTH(dWork: TDateTime): string;
function CFMONTH(dWork: TDateTime): string;
function CURDIR(DriveSpec: string): string;
function DAY(dWork: TDateTime): integer;
function DTOS(dWork: TDateTime): string;
function EMPTY(cString: string): boolean;
function IIF(lCond: boolean; cTrue, cFalse: string): string;
function IIFN(lCond: boolean; cTrue, cFalse: integer): integer;
function ISDELETED(tbl: TTable): boolean;
function GETENV(VarName : string): string;
function GETSHORTNAME(cString: AnsiString): AnsiString;
function LEFTSTR(cString: string; nLen: integer): string;
function LTRIM(cString: string): string;
function MONTH(dWork: TDateTime): integer;
function PADC(cString: string; nLen: integer): string;
function PADL(cString: string; nLen: integer): string;
function PADR(cString: string; nLen: integer): string;
function RIGHTSTR(cString: string; nLen: integer): string;
function RECNO(table: TTable): longint;
function REPLICATE(c: char; nLen: integer): string;
function RTRIM(cString: string): string;
function RUN(cProgName: string; CmdShow: word): word;
function SPACE(nLen: integer): string;
procedure SetDeleted(oTable: TTable; Value: Boolean);
function YEAR(dWork: TDateTime): integer;
function StrToDateY2K(s: string): TDateTime;

implementation

procedure AddLine(filename, line: string);
var
  F: TextFile;
begin
  {add the line to the given file (create the file if necessary}
  AssignFile(F, filename);
  if FileExists(filename) then
    Append(F) {add data at the end}
  else
    Rewrite(F); {create new file}
  Writeln(F, line);
  CloseFile(F);
end;

function ALLTRIM(cString: string): string;
{trim off any spaces and return the string}
var
  startPos, endPos: integer;
begin
  startPos := 1;
  endPos   := Length(cString);
  while (startPos < Length(cString)) and (cString[startPos] = ' ') do
    Inc(startPos);
  while (endPos > startPos) and (cString[endPos] = ' ') do
    Dec(endPos);
  if (endPos = startPos) and (cString[endPos] = ' ') then
    Result := ''
  else
    Result := Copy(cString, startPos, endPos-startPos+1);
end;

function CDOW(dWork: TDateTime): string;
{returns the Day of the Week as a character string}
begin
  Result := FormatDateTime('dddd', dWork);
end;

function CENTER(cString: string; nWidth: integer): string;
begin
  Result := PADC(cString, nWidth);
end;

function CMONTH(dWork: TDateTime): string;
{returns the month as a character string}
begin
  Result := FormatDateTime('mmmm', dWork);
end;

function CFMONTH(dWork: TDateTime): string;
{returns the french name for the month as a character string}
begin
  case MONTH(dWork) of
     1: Result := 'Janvier';
     2: Result := 'F�vrier';
     3: Result := 'Mars';
     4: Result := 'Avril';
     5: Result := 'Mai';
     6: Result := 'Juin';
     7: Result := 'Juillet';
     8: Result := 'Aout';
     9: Result := 'Septembre';
    10: Result := 'Octobre';
    11: Result := 'Novembre';
    12: Result := 'D�cembre';
    else
      Result := '';
    end;  
end;

function CURDIR(DriveSpec: string): string;
{returns the current directory for the specified drive (current drive
 if empty string is passed)}
var
  D: Byte;
begin
  if (DriveSpec = '') then
    D := 0    {if not drive given, then get default drive's current directory}
  else
    D := Ord(LEFTSTR(LTRIM(DriveSpec),1)[1]) - Ord('A') + 1;
  GetDir(D, Result);
end;

function DAY(dWork: TDateTime): integer;
{returns the day of the month}
var Year, Month, Day: Word;
begin
  DecodeDate(dWork, Year, Month, Day);
  Result := Day;
end;

function DTOS(dWork: TDateTime): string;
begin
  result := FormatDateTime('yyyymmdd', dWork);
end;

function EMPTY(cString: string): boolean;
{returns true if the string is empty of non-spaces, false otherwise}
begin
  if AllTrim(cString) = '' then
    Result := true
  else
    Result := false;
end;

function GETENV(VarName : string): string;
{ NOTE: taken from Lloyd's Help File                          }
{=============================================================}
{= GetEnv - Get Dos Environment Variable Setting             =}
{=                                                           =}
{= This function is a modified version of the GetEnvVar      =}
{= function that appears in the WinDos unit that comes       =}
{= with Delphi.  This function's interface uses Pascal       =}
{= strings instead of null-terminated strings.               =}
{=============================================================}
var
  Len     : Word;
  EnvStz  : PChar;
  NameStz : array[ 0..180 ] of Char;
begin
  StrPCopy( NameStz, VarName );     { Covert VarName to PChar }
  Len := StrLen( NameStz );
  EnvStz := GetEnvironmentStrings;      { EnvStz holds entire env }

  while EnvStz^ <> #0 do
  begin                  { Pick off Variable Name and Compare }
    if ( StrLIComp( EnvStz, NameStz, Len ) = 0 ) and
       ( EnvStz[ Len ] = '=' ) then
    begin          { Convert to Pascal string before returing }
      Result := StrPas( EnvStz + Len + 1 );
      Exit;
    end;
    Inc( EnvStz, StrLen( EnvStz ) + 1 );   { Jump to Next Var }
  end;
  Result := '';
end;

function GETSHORTNAME(cString: AnsiString): AnsiString;
{returns the short DOS 8.3 name for the given filename}
var
  sTmp: pchar;
begin
  sTmp := StrAlloc(Length(cString)+1);
  GetShortPathName(PChar(string(cString)), sTmp, Length(cString)+1);
  Result := StrPas(sTmp);
  StrDispose(sTmp);
end;

function IIF(lCond: boolean; cTrue, cFalse: string): string;
begin
  if (lCond) then
    Result := cTrue
  else
    Result := cFalse;
end;

function IIFN(lCond: boolean; cTrue, cFalse: integer): integer;
begin
  if (lCond) then
    Result := cTrue
  else
    Result := cFalse;
end;

function ISDELETED( tbl: TTable ): boolean;
var
  CursorProps: CurProps;
  RecordProps: RECProps;
begin

  { Return 0 if dataset is not dBASE }
  Result := False;
  with tbl do 
    begin
    { Is the dataset active? }
    if (State = dsInactive) then
      DatabaseError(SDataSetClosed);

    { We need to make this call to grab the cursor's iSeqNums }
    Check(DbiGetCursorProps(Handle, CursorProps));

    { Synchronize the BDE cursor with the Dataset's cursor }
    UpdateCursorPos;

    { Fill RecordProps with the current record's properties }
    Check(DbiGetRecord(Handle, dbiNOLOCK, nil, @RecordProps));

    { What kind of dataset are we looking at? }
    if (CursorProps.iSeqNums = 0) then 
      Result := RecordProps.bDeleteFlag;  { dBASE   }
    end; { with }
    
end; { function }


function LEFTSTR(cString: string; nLen: integer): string;
{return the nLen left most substring}
begin
  if nLen = 0 then
    Result := ''
  else if (Length(cString) > nLen) then
    Result := Copy(cString,1,nLen)
  else
    Result := cString;
end;

function LTRIM(cString: string): string;
{trim off any spaces on the left and return the new string}
var pos: byte;
begin
  pos := 1;
  while (cString[pos] = ' ') and (pos < Length(cString)) do
    Inc(pos);
  if (cString[pos] = ' ') then
    Result := ''
  else
    Result := Copy(cString, pos, 255);
end;

function MONTH(dWork: TDateTime): integer;
{returns the month of the year}
var Year, Month, Day: Word;
begin
  DecodeDate(dWork, Year, Month, Day);
  Result := Month;
end;

function PADC(cString: string; nLen: integer): string;
{make certain the string is exactly nLen character}
var nSpace: byte;
begin
  if (Length(cString) <> nLen) then
    if Length(cString) > nLen then
      {length is smaller than string, so cut string to fit}
      Result := Copy(cString,1,nLen)
    else
      begin
        {length is bigger than string, so add spaces}
        nSpace := (nLen-Length(cString)) div 2;
        Result := (Space(nSpace))+cString;
        if (nLen-Length(cString)-nSpace > 0) then
          Result := Result + Space(nLen-Length(cString)-nSpace);
      end
  else
    Result := cString;
end;

function PADL(cString: string; nLen: integer): string;
{make certain the string is exactly nLen character}
begin
  if (Length(cString) <> nLen) then
    if Length(cString) > nLen then
      {length is smaller than string, so cut string to fit}
      Result := Copy(cString,1,nLen)
    else
      {length is bigger than string, so add spaces}
      Result := Space(nLen-Length(cString))+cString
  else
    Result := cString;
end;

function PADR(cString: string; nLen: integer): string;
{make certain the string is exactly nLen character}
begin
  if (Length(cString) <> nLen) then
    if Length(cString) > nLen then
      {length is smaller than string, so cut string to fit}
      Result := Copy(cString,1,nLen)
    else
      {length is bigger than string, so add spaces}
      Result := cString + Space(nLen-Length(cString))
  else
    Result := cString;
end;

function RECNO(table: TTable): longint;
{returns the physical record number for the given table}
var
  myrecprop: RECprops;
begin
  table.updatecursorpos;
  dbiGetRecord(table.handle, dbiNOLOCK, nil, @myrecprop);
  Result := myrecprop.iPhyRecNum;
end;

function REPLICATE(c: char; nLen: integer): string;
{return a string containing nLen times the character c}
var
  i: integer;
begin
  Result := '';
  for i := 1 to nLen do
    Result := Result + c;
end;

function RIGHTSTR(cString: string; nLen: integer): string;
{returns the nLen rightmost characters}
begin
  if nLen = 0 then
    Result := ''
  else if (Length(cString) > nLen) then
    Result := Copy(cString,Length(cString)-nLen+1, nLen)
  else
    Result := cString;
end;

function RTRIM(cString: string): string;
{trim off any spaces on the right and return the string}
var pos: byte;
begin
  pos := Length(cString);
  while (pos > 1) and (cString[pos] = ' ') do
    Dec(pos);
  if (cString[pos] = ' ') then
    Result := ''
  else
    Result := Copy(cString, 1, pos);
end;

function RUN(cProgName: string; CmdShow: word): word;
{runs the given program}
{note: CmdShow can be one of these, provided WinTypes is in the 'uses' section
         sw_Hide
         sw_ShowNormal
         sw_Normal
         sw_ShowMinimized
         sw_OtherMaximized
         sw_ShowMaximized
         sw_Maximize
         sw_ShowNoActivate
         sw_OtherRestored
         sw_Show
         sw_Minimize
         sw_ShowMinNoActive
         sw_ShowNA
         sw_Restore}
begin
  cProgName := cProgName + #0; {add the character #0 to make it a PChar}
  Result := WinExec((PAnsiChar(@cProgName)+1), CmdShow); {pass it, skipping length byte}
end;

procedure SetDeleted(oTable:TTable; Value: Boolean);
{This code came from Lloyd's help file!}
var
  rslt: DBIResult;
  szErrMsg: DBIMSG;
begin
  try
    oTable.DisableControls;
    try
      rslt := DbiSetProp(hDBIObj(oTable.Handle), curSOFTDELETEON, LongInt(Value));
      if rslt <> DBIERR_NONE then
      begin
      DbiGetErrorString(rslt, szErrMsg);
      raise Exception.Create(StrPas(szErrMsg));
      end;
    except
      on E: EDBEngineError do ShowMessage(E.Message);
      on E: Exception do ShowMessage(E.Message);
    end;
  finally
    oTable.Refresh;
    oTable.EnableControls;
  end;
end;


function SPACE(nLen: integer): string;
{return the number of spaces asked for}
begin
  Result := Replicate(' ', nLen);
end;

function YEAR(dWork: TDateTime): integer;
{returns the year}
var Year, Month, Day: Word;
begin
  DecodeDate(dWork, Year, Month, Day);
  Result := Year;
end;

function StrToDateY2K(s: string): TDateTime;
var
  y, m, d: word;
  nPos: integer;
  r: string;
  num: string;
begin
  nPos := Pos(DateSeparator, s);
  if (nPos > 0) then
    begin
    num := Trim(Copy(s,1,nPos-1));
    if (num = '') then
      result := 0.0
    else
      begin
      y := StrToInt(num);
      r := Copy(s, nPos+1, Length(s));
      nPos := Pos(DateSeparator, r);
      if (nPos > 0) then
        begin
        m := StrToInt(Trim(Copy(r, 1, nPos-1)));
        d := StrToInt(Trim(Copy(r, nPos+1, Length(r))));

        if (y < Y2K_CUTOFF) then
          y := 2000 + y
        else if (y < 100) then
          y := 1900 + y;  
                  
        result := EncodeDate(y,m,d);

        end
      else
        begin
        raise EConvertError.Create('Invalid date "'+s+'"');
        end
      end  
    end
  else
    begin
    raise EConvertError.Create('Invalid date "'+s+'"');
    end;
end;
  


{=== TDirectory methods ===}

constructor TDirectory.Create;
begin
  FInfo := TList.Create;
end;

destructor TDirectory.Free;
var
  i: integer;
begin
  {release all items in TList}
  for i := 0 to FInfo.Count-1 do
    FreeMem(FInfo.Items[i], SizeOf(TSearchRec));
  {release TList object}
  FInfo.Free;
end;

function TDirectory.Count: integer;
begin
  Result := FInfo.Count;
end;

procedure TDirectory.Directory(Specs: string; Attrib: string);
{retrieve given files}
{Note: The Attr variable below can take any of the following
       values:
         faReadOnly   Read-only files
         faHidden     Hidden files
         faSysFile    System files
         faVolumeID   Volume ID files
         faDirectory  Directory files
         faArchive    Archive files
         faAnyFile    Any file
       These values can be combined through addition, for example:
         (faHidden + faSysFile)
}
var
  FileInfo: TSearchRec;
  PFileInfo: ^TSearchRec;
  Attr: Word;
begin
  {convert Attrib to proper value}
  Attr := 0;
  if Pos('D', Attrib) <> 0 then Attr := Attr + faDirectory;
  if Pos('S', Attrib) <> 0 then Attr := Attr + faSysFile;
  if Pos('H', Attrib) <> 0 then Attr := Attr + faHidden;
  if Pos('V', Attrib) <> 0 then Attr := Attr + faVolumeID;
  {delete any existing entries in FInfo}
  FInfo.Clear;
  {load FInfo with all filenames}
  if (FindFirst(Specs, Attr, FileInfo) = 0) then
    repeat
      {create a TSearchRec and store data into it}
      GetMem(PFileInfo, SizeOf(TSearchRec));
      PFileInfo^.Name := FileInfo.Name;
      PFileInfo^.Attr := FileInfo.Attr;
      PFileInfo^.Time := FileInfo.Time;
      PFileInfo^.Size := FileInfo.Size;
      {add it to the list}
      FInfo.Add(PFileInfo);
      {get next entry}
    until FindNext(FileInfo) <> 0;
end;

function TDirectory.GetName(i: integer): string;
begin
  Result := TSearchRec(FInfo.Items[i]^).Name;
end;

function TDirectory.GetDateTime(i: integer): TDateTime;
begin
  Result := FileDateToDateTime(TSearchRec(FInfo.Items[i]^).Time);
end;

function TDirectory.GetAttribute(i: integer): string;
var
  Attrib: Word;
begin
  {convert the Attr word value into a special string value}
  Attrib := TSearchRec(FInfo.Items[i]^).Attr;
  Result := '';
  if (Attrib and faDirectory) = faDirectory then Result := Result + 'D';
  if (Attrib and faSysFile)   = faSysFile   then Result := Result + 'S';
  if (Attrib and faHidden)    = faHidden    then Result := Result + 'H';
  if (Attrib and faVolumeID)  = faVolumeID  then Result := Result + 'V';
end;

function TDirectory.GetSize(i: integer): longint;
begin
  Result := TSearchRec(FInfo.Items[i]^).Size;
end;

end.

