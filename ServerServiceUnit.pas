unit ServerServiceUnit;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.SvcMgr, Vcl.Dialogs;

type
  TTisDsServ1 = class(TService)
  private
  public
    function GetServiceController: TServiceController; override;
  end;

var
  TisDsServ1: TTisDsServ1;

implementation

{$R *.dfm}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  TisDsServ1.Controller(CtrlCode);
end;

function TTisDsServ1.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

end.

