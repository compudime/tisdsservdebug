object ServerMethods1: TServerMethods1
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 283
  Width = 540
  object FDConnection: TFDConnection
    Params.Strings = (
      'DriverID=ADS'
      'TableType=Default')
    ConnectedStoredUsage = []
    LoginPrompt = False
    BeforeConnect = FDConnectionBeforeConnect
    Left = 316
    Top = 7
  end
  object FDGUIxWaitCursor1: TFDGUIxWaitCursor
    Provider = 'Forms'
    Left = 466
    Top = 7
  end
  object FDQueryItems: TFDQuery
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'select * from items')
    Left = 466
    Top = 113
  end
  object FDStanStorageJSONLink1: TFDStanStorageJSONLink
    Left = 352
    Top = 166
  end
  object FDStanStorageBinLink1: TFDStanStorageBinLink
    Left = 314
    Top = 166
  end
  object FDQueryItem: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Select '
      'Cast(:Line1 AS SQL_CHAR) AS Line1, '
      'Cast(:Line2 AS SQL_CHAR) AS Line2, '
      'Cast(:Line3 AS SQL_CHAR) AS Line3, '
      'Cast(:Line4 AS SQL_CHAR) AS Line4,'
      'Cast(:CodeFound AS SQL_CHAR) AS CodeFound,'
      'Cast(:CodePricedItem AS SQL_CHAR) AS CodePricedItem'
      'From System.Iota;')
    Left = 390
    Top = 113
    ParamData = <
      item
        Name = 'LINE1'
        ParamType = ptInput
        Value = Null
      end
      item
        Name = 'LINE2'
        ParamType = ptInput
      end
      item
        Name = 'LINE3'
        ParamType = ptInput
      end
      item
        Name = 'LINE4'
        ParamType = ptInput
      end
      item
        Name = 'CODEFOUND'
        ParamType = ptInput
      end
      item
        Name = 'CODEPRICEDITEM'
        ParamType = ptInput
      end>
  end
  object tbPrdmstr: TFDTable
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    UpdateOptions.UpdateTableName = 'Prdmstr'
    TableName = 'Prdmstr'
    Left = 390
    Top = 166
  end
  object FDPhysADSDriverLink1: TFDPhysADSDriverLink
    ShowDeleted = False
    Left = 314
    Top = 60
  end
  object FDQueryPrdmstr: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Select Code, [Desc], Price'
      'From Prdmstr'
      'Where Code=:CodeFound;'
      '')
    Left = 352
    Top = 113
    ParamData = <
      item
        Name = 'CODEFOUND'
        ParamType = ptInput
      end>
  end
  object AdsConnection1: TAdsConnection
    AliasName = 'TisWin3DD'
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    Username = 'TisSrvDS'
    StoreConnected = False
    AfterConnect = AdsConnection1AfterConnect
    BeforeConnect = AdsConnection1BeforeConnect
    Left = 10
    Top = 10
  end
  object FDQueryItem2: TAdsQuery
    SQL.Strings = (
      'Select'
      'Cast(:Line1 AS SQL_CHAR) AS Line1,'
      'Cast(:Line2 AS SQL_CHAR) AS Line2,'
      'Cast(:Line3 AS SQL_CHAR) AS Line3,'
      'Cast(:Line4 AS SQL_CHAR) AS Line4,'
      'Cast(:CodeFound AS SQL_CHAR) AS CodeFound'
      'From System.Iota;')
    AdsConnection = AdsConnection1
    Left = 48
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'Line1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Line2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Line3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'Line4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'CodeFound'
        ParamType = ptUnknown
      end>
  end
  object qryRmLabel2: TAdsQuery
    StoreActive = False
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    SQL.Strings = (
      'Insert Into RmLabel ([Code], [Type]) Values(:BarCode, '#39'P'#39');')
    SourceTableType = ttAdsCDX
    AdsConnection = AdsConnection1
    Left = 124
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'BarCode'
        ParamType = ptUnknown
      end>
  end
  object qryRmLabel: TFDQuery
    Connection = FDConnectionCDX
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Insert Into RmLabel ([Code], [Type], [SheetName]) '
      'Values(:BarCode, :LabelType, :SheetName);'
      '')
    Left = 428
    Top = 113
    ParamData = <
      item
        Name = 'BARCODE'
        ParamType = ptInput
      end
      item
        Name = 'LABELTYPE'
        ParamType = ptInput
      end
      item
        Name = 'SHEETNAME'
        ParamType = ptInput
      end>
  end
  object qryDlvPickedBy2: TAdsQuery
    StoreActive = False
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    SQL.Strings = (
      'Update DlvCntrl Set PickedBy=:PickedBy Where InvID=:DlvID;')
    AdsConnection = AdsConnection1
    Left = 86
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'PickedBy'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'DlvID'
        ParamType = ptUnknown
      end>
  end
  object qryDlvPickedBy: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Update DlvCntrl Set PickedBy=:PickedBy Where InvID=:DlvID;')
    Left = 314
    Top = 113
    ParamData = <
      item
        Name = 'PICKEDBY'
        DataType = ftString
        ParamType = ptInput
      end
      item
        Name = 'DLVID'
        DataType = ftString
        ParamType = ptInput
      end>
  end
  object FDConnectionCDX: TFDConnection
    Params.Strings = (
      'Alias=TisWin3DD'
      'User_Name=TisSrvDS'
      'DriverID=ADS'
      'TableType=CDX'
      'Locking=Compatible'
      'CharacterSet=OEM')
    ConnectedStoredUsage = []
    LoginPrompt = False
    BeforeConnect = FDConnectionBeforeConnect
    Left = 390
    Top = 7
  end
  object FDQueryGG: TFDQuery
    AutoCalcFields = False
    Connection = FDConnectionGG
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    Left = 466
    Top = 60
  end
  object FDConnectionGG: TFDConnection
    Params.Strings = (
      'DriverID=ADS'
      'TableType=ADT'
      'Alias=GG'
      'ServerTypes=Remote'
      'Locking=Compatible'
      'Protocol=TCPIP')
    ConnectedStoredUsage = []
    LoginPrompt = False
    BeforeConnect = FDConnectionGGBeforeConnect
    Left = 352
    Top = 7
  end
  object qryRebatSearch: TFDQuery
    Connection = FDConnectionGG
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Select Sum(IfNull(r.Amnt,0)-IfNull(r.Used,0)) Value'
      'From Rebates r'
      'Where Upper(r.RefNum2)=:Scan'
      'And Not RefNum2 Is Null'
      'And Upper(r.[Type])='#39'C'#39
      'And r.Active=True'
      'And not r.UnActive=True'
      'And (r.Expires is Null or r.Expires>=CurDate());')
    Left = 428
    Top = 60
    ParamData = <
      item
        Name = 'SCAN'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
  object qryItemSearch: TFDQuery
    Connection = FDConnectionGG
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Select  TicCode, Price, Descrip,  ProStart, ProEnd, Discnt'
      'From Item'
      'where  Upper(ticcode)=:Scan')
    Left = 352
    Top = 60
    ParamData = <
      item
        Name = 'SCAN'
        DataType = ftString
        ParamType = ptInput
        Value = Null
      end>
  end
  object qryCustomer: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    SQL.Strings = (
      'Select Balance, CCustom1 Pin, Ovr120'
      'From Customer'
      'Where Tel=:TelNum')
    Left = 390
    Top = 60
    ParamData = <
      item
        Name = 'TELNUM'
        ParamType = ptInput
      end>
  end
  object qrySQLOpen: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    Left = 312
    Top = 216
  end
  object qrySQLExecute: TFDQuery
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    Left = 352
    Top = 216
  end
  object DataSetTableProducer1: TDataSetTableProducer
    Left = 88
    Top = 216
  end
  object qryHelper: TAdsQuery
    AdsConnection = AdsConnection1
    Left = 8
    Top = 58
    ParamData = <>
  end
  object tbPrdAlias: TFDTable
    Connection = FDConnection
    ResourceOptions.AssignedValues = [rvMacroCreate, rvMacroExpand]
    ResourceOptions.MacroCreate = False
    ResourceOptions.MacroExpand = False
    UpdateOptions.UpdateTableName = 'PrdAlias'
    TableName = 'PrdAlias'
    Left = 430
    Top = 166
  end
  object qryHelper2: TAdsQuery
    AdsConnection = AdsConnection1
    Left = 56
    Top = 58
    ParamData = <>
  end
end
