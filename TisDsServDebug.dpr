program TisDsServDebug;
{$APPTYPE GUI}

uses
  madExcept,
  madLinkDisAsm,
  madListHardware,
  madListProcesses,
  madListModules,
  Vcl.Forms,
  Vcl.SvcMgr,
  Web.WebReq,
  IdHTTPWebBrokerBridge,
  ServerUnit in 'ServerUnit.pas' {frmServiceSettings},
  ServerMethodsUnit1 in 'ServerMethodsUnit1.pas' {simpl: TDataModule},
  WebModuleUnit1 in 'WebModuleUnit1.pas' {WebModule1: TWebModule},
  ServerServiceUnit in 'ServerServiceUnit.pas' {TisDsServ1: TService},
  log in 'Modules\log.pas',
  optimize in 'Modules\optimize.pas',
  VendInvHelpersUnit in 'TisWinSource\VendInvHelpersUnit.pas',
  PrdHelpersIUnit in 'TisWinSource\PrdHelpersIUnit.pas',
  VendHelpersIUnit in 'TisWinSource\VendHelpersIUnit.pas',
  GeneralHelpersUnit in 'TisWinSource\GeneralHelpersUnit.pas',
  DataHelpersUnit in 'TisWinSource\DataHelpersUnit.pas',
  GuiNonGuiHelpersUnit in 'TisWinSource\GuiNonGuiHelpersUnit.pas',
  ProjHelpersIUnit in 'TisWinSource\ProjHelpersIUnit.pas',
  Tis_CreatDBObjects in 'TisWinSource\Tis_CreatDBObjects.pas',
  dmDataModulePrd in 'TisWinSource\dmDataModulePrd.pas' {dmDataPrd: TDataModule},
  ScanResolveUnit in 'TisWinSource\ScanResolveUnit.pas',
  dmDataModule in 'TisWinSource\dmDataModule.pas' {dmData: TDataModule};

{$R *.res}

begin
  if not IsDesktopMode then
  begin
    if not Application.DelayInitialize or Application.Installing then
      Vcl.SvcMgr.Application.Initialize;

    if WebRequestHandler <> nil then
      WebRequestHandler.WebModuleClass := WebModuleClass;

    Vcl.SvcMgr.Application.CreateForm(TTisDsServ1, TisDsServ1);
  if not Application.Installing then
    begin
      Vcl.Forms.Application.CreateForm(TfrmServiceSettings, frmServiceSettings);
      frmServiceSettings.StartServer;
    end;
    Vcl.SvcMgr.Application.Run;
  end
  else
  begin
    if WebRequestHandler <> nil then
      WebRequestHandler.WebModuleClass := WebModuleClass;
    Vcl.Forms.Application.Initialize;
    Vcl.Forms.Application.CreateForm(TfrmServiceSettings, frmServiceSettings);

    Vcl.Forms.Application.Run;
  end;

end.
