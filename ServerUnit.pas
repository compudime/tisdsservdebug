unit ServerUnit;

interface

uses
  Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.AppEvnts, Vcl.StdCtrls, IdHTTPWebBrokerBridge, Web.HTTPApp, adscnnct, adsdata, IdContext, Vcl.ExtCtrls;

type
  TfrmServiceSettings = class(TForm)
    ButtonStart: TButton;
    ButtonStop: TButton;
    EditPort: TEdit;
    Label1: TLabel;
    ApplicationEvents1: TApplicationEvents;
    ButtonOpenBrowser: TButton;
    chkALS: TCheckBox;
    lblVer: TLabel;
    edtCdk: TEdit;
    lblcdx: TLabel;
    btn1: TButton;
    chkVerboseLogging: TCheckBox;
    chkUseAuthentication: TCheckBox;
    chkPreloadTisLib3: TCheckBox;
    chkHTTPS: TCheckBox;
    HealthTimer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure ButtonStartClick(Sender: TObject);
    procedure ButtonStopClick(Sender: TObject);
    procedure ButtonOpenBrowserClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EditPortChange(Sender: TObject);
    procedure chkALSClick(Sender: TObject);
    procedure edtCdkChange(Sender: TObject);
    procedure btn1Click(Sender: TObject);
    procedure chkVerboseLoggingClick(Sender: TObject);
    procedure chkUseAuthenticationClick(Sender: TObject);
    procedure chkPreloadTisLib3Click(Sender: TObject);
    procedure chkHTTPSClick(Sender: TObject);
    procedure HealthTimerTimer(Sender: TObject);
  private
    FServer: TIdHTTPWebBrokerBridge;
    FTisWin3Loaded: Integer;
    FTisWin3Handle: THandle;
    FPreloadTisLib3: Boolean;
    FCdPayLibInst: THandle;
    FCdPayLibLoaded: Integer;
    FAdsConnection: TAdsConnection;
    FCloseCommandActive: Boolean;
    function GetAdsConnection: TAdsConnection;
    property AdsConnection: TAdsConnection read GetAdsConnection write FAdsConnection;

    procedure ReadIniSettings;
    procedure SetPreloadTisLib3(const Value: Boolean);
    procedure SetHttpsON;
    procedure OnGetSSLPassword(var APassword: String);
    procedure OnContextCreated(AContext: TIdContext);
    procedure UnloadTisLib3;
    function DatabasePath: string;
    procedure CheckForNewAllFiles;
    procedure CheckForPrevRestartAll;
  public
    procedure StartServer;
    function TisLib3Init(AdsCon: TAdsConnection): Boolean;
    function CdPayInit: Boolean;
    property PreloadTisLib3: Boolean read FPreloadTisLib3 write SetPreloadTisLib3;
    property TisWin3Loaded: Integer read FTisWin3Loaded write FTisWin3Loaded;
    property TisWin3Handle: THandle read FTisWin3Handle write FTisWin3Handle;
    property CdPayLibLoaded: Integer read FCdPayLibLoaded write FCdPayLibLoaded;
    property CdPayLibInst: THandle read FCdPayLibInst write FCdPayLibInst;
    property CloseCommandActive: Boolean read FCloseCommandActive;
  end;

var
  IniFile: String;
  frmServiceSettings: TfrmServiceSettings;

function IsDesktopMode: Boolean;

implementation

{$R *.dfm}

uses
  Winapi.Windows, Winapi.ShellApi, Datasnap.DSSession, IniFiles, adsset, IdSSLOpenSSL, JvVersionInfo;

const
  NullDate = -700000;

var
  FRestartAllTime: TDateTime;
  LIOHandleSSL: TIdServerIOHandlerSSLOpenSSL;

function IsDesktopMode: Boolean;
begin
  Result := (ParamCount > 0) and ((ParamStr(1) = '/auto') or (ParamStr(1) = '/gui'));
end;

procedure TfrmServiceSettings.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
  ButtonStart.Enabled := not FServer.Active;
  ButtonStop.Enabled := FServer.Active;
  EditPort.Enabled := not FServer.Active;
  chkALS.Enabled := not FServer.Active;
  edtCdk.Enabled := not FServer.Active;
end;

procedure TfrmServiceSettings.btn1Click(Sender: TObject);
begin
  //frmSetup1.Show;
end;

procedure TfrmServiceSettings.ButtonOpenBrowserClick(Sender: TObject);
var
  LURL: String;
begin
  StartServer;
  LURL := Format('http://localhost:%s', [EditPort.Text]);
  ShellExecute(0, nil, PChar(LURL), nil, nil, SW_SHOWNOACTIVATE);
end;

procedure TfrmServiceSettings.ButtonStartClick(Sender: TObject);
begin
  StartServer;
end;

procedure TerminateThreads;
begin
  if TDSSessionManager.Instance <> nil then
    TDSSessionManager.Instance.TerminateAllSessions;
end;

procedure TfrmServiceSettings.ButtonStopClick(Sender: TObject);
begin
  TerminateThreads;
  FServer.Active := False;
  FServer.Bindings.Clear;
end;

function TfrmServiceSettings.CdPayInit: Boolean;
type
  TDevExInit = procedure;
var
  DevExInit: TDevExInit;
  AOldDir: String;
begin
  if FCdPayLibLoaded = -1 then
  begin
    FCdPayLibInst := LoadLibrary(PChar('C:\RunPOS\CdPayLib.dll'));
    FCdPayLibLoaded := 0;

    if FCdPayLibInst > 0 then
    begin
      AOldDir := GetCurrentDir;
      try
        ChDir('C:\RunPOS\');
        @DevExInit := GetProcAddress(FCdPayLibInst, 'dxInitialize');
        if @DevExInit <> nil then
          DevExInit;
      finally
        ChDir(AOldDir);
      end;
    end;
  end;

  Result := FCdPayLibInst > 0;
end;

function FileTimeToLocalDateTime(AFileTime: TFileTime): TDateTime;
var
  SystemTime, LocalTime: TSystemTime;
begin
  if not FileTimeToSystemTime(AFileTime, SystemTime) then
    RaiseLastOSError;
  if not SystemTimeToTzSpecificLocalTime(nil, SystemTime, LocalTime) then
    RaiseLastOSError;
  Result := SystemTimeToDateTime(LocalTime);
end;

procedure TfrmServiceSettings.CheckForNewAllFiles;
var
  APath: string;
  fad: TWin32FileAttributeData;
  ADoDisconnect: Boolean;
begin
  APath := DatabasePath;

  ADoDisconnect := FileExists(APath + '\Restart.all');

  if ADoDisconnect then
  begin
    ADoDisconnect := FRestartAllTime = NullDate;
    GetFileAttributesEx(PChar(APath + '\Restart.all'), GetFileExInfoStandard, @fad);
    if not ADoDisconnect then
      ADoDisconnect := FRestartAllTime <> FileTimeToLocalDateTime(fad.ftLastWriteTime);
    FRestartAllTime := FileTimeToLocalDateTime(fad.ftLastWriteTime);
  end;

  if not ADoDisconnect then
    ADoDisconnect := FileExists(APath + '\Close.all') or FileExists(APath + '\Lock.all');

  if ADoDisconnect and AdsConnection.IsConnected then
  begin
    UnloadTisLib3;
    if AdsConnection.IsConnected then
      AdsConnection.Disconnect;
  end;

  if (not ADoDisconnect) and CloseCommandActive and chkPreloadTisLib3.Checked then
    TisLib3Init(nil);

  FCloseCommandActive := ADoDisconnect;
end;

procedure TfrmServiceSettings.CheckForPrevRestartAll;
var
  APath: string;
  fad: TWin32FileAttributeData;
begin
  APath := DatabasePath;
  FRestartAllTime := NullDate;

  if FileExists(APath + '\Restart.all') then
  begin
    GetFileAttributesEx(PChar(APath + '\Restart.all'), GetFileExInfoStandard, @fad);
    FRestartAllTime := FileTimeToLocalDateTime(fad.ftLastWriteTime);
  end;
end;

procedure TfrmServiceSettings.chkALSClick(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  FServer.DefaultPort := StrToInt(EditPort.Text);
  with TIniFile.Create(IniFile) do
    try
      WriteBool('Settings', 'AdsLocal', chkALS.Checked);
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.chkHTTPSClick(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  with TIniFile.Create(IniFile) do
    try
      WriteBool('Settings', 'UseHTTPS', chkHTTPS.Checked);
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.chkPreloadTisLib3Click(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  PreloadTisLib3 := chkPreloadTisLib3.Checked;
end;

procedure TfrmServiceSettings.chkUseAuthenticationClick(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  with TIniFile.Create(IniFile) do
    try
      WriteBool('Settings', 'UseAuthentication', chkUseAuthentication.Checked);
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.chkVerboseLoggingClick(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  with TIniFile.Create(IniFile) do
    try
      WriteBool('Settings', 'VerboseLogging', chkVerboseLogging.Checked);
    finally
      Free;
    end;
end;

function TfrmServiceSettings.DatabasePath: string;
var
  APos: Integer;
begin
  Result := AdsConnection.GetConnectionPath;
  APos := Pos(':6262', Result);
  if APos > 0 then
    Result := Copy(Result, 1, APos - 1) + Copy(Result, APos + 5, Length(Result));
end;

procedure TfrmServiceSettings.EditPortChange(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;

  if FServer.DefaultPort = StrToInt(EditPort.Text) then
    Exit;

  FServer.DefaultPort := StrToInt(EditPort.Text);
  with TIniFile.Create(IniFile) do
    try
      WriteString('Server', 'Port', IntToStr(FServer.DefaultPort));
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.edtCdkChange(Sender: TObject);
begin
  if not(FServer.Active or IsDesktopMode) then
    Exit;
  FServer.DefaultPort := StrToInt(EditPort.Text);
  with TIniFile.Create(IniFile) do
    try
      WriteString('Settings', 'Type', UpperCase(edtCdk.Text));
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.FormCreate(Sender: TObject);
var
  AVer: TJvVersionInfo;
begin
  FServer := TIdHTTPWebBrokerBridge.Create(Self);
  FServer.OnContextCreated := OnContextCreated;
  FServer.MaxConnections := 200;
  FTisWin3Loaded := -1;
  FCdPayLibLoaded := -1;

  with TIniFile.Create(GetAliasPathAndFileName) do
  begin
    if not ReadBool('SETTINGS', 'RETRY_ADS_CONNECTS', False) then
      WriteBool('SETTINGS', 'RETRY_ADS_CONNECTS', True);
    Free;
  end;

  with TIniFile.Create(IniFile) do
    try
      FPreloadTisLib3 := ReadBool('Settings', 'PreloadTisLib3', False);
    finally
      Free;
    end;

  CheckForPrevRestartAll;
  CheckForNewAllFiles;
  if PreloadTisLib3 and (not IsDesktopMode) and (not CloseCommandActive) then
    TisLib3Init(nil);
  AVer := TJvVersionInfo.Create(ParamStr(0));
  try
    lblVer.Caption := LongVersionToString(AVer.FileLongVersion) + ' - ' + DateTimeToStr(AVer.VerFileDate);
  finally
    AVer.Free;
  end;
end;

procedure TfrmServiceSettings.FormShow(Sender: TObject);
begin
  if PreloadTisLib3 and IsDesktopMode and (not CloseCommandActive) then
    TisLib3Init(nil);
  ButtonStart.Click;
end;

function DatabaseUser: String;
begin
  Result := 'TisSrvDS';
end;

function DatabasePass: String;
var
  I: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for I := 1 to 3 do
    Result := Result + IntToStr(I);
  Result := Result + '$5';
  Result := Result + Copy(Result, 9, 1) + Copy(DatabaseUser, 4, 3);
  // lezaM123$5$Srv
end;

function TfrmServiceSettings.GetAdsConnection: TAdsConnection;
begin
  if not Assigned(FAdsConnection) then
  begin
    FAdsConnection := TAdsConnection.Create(Application);
    FAdsConnection.AliasName := 'TisWin3DD';
    FAdsConnection.AdsServerTypes := [stADS_REMOTE, stADS_LOCAL];
    FAdsConnection.LoginPrompt := False;
    FAdsConnection.Username := DatabaseUser;
    FAdsConnection.Password := DatabasePass;
  end;

  Result := FAdsConnection;
end;

procedure TfrmServiceSettings.HealthTimerTimer(Sender: TObject);
begin
  CheckForNewAllFiles;
end;

procedure TfrmServiceSettings.OnContextCreated(AContext: TIdContext);
begin
  AContext.Connection.IOHandler.MaxLineLength := MaxInt;
end;

procedure TfrmServiceSettings.OnGetSSLPassword(var APassword: String);
begin
  APassword := 'L0ca$';
end;

procedure TfrmServiceSettings.ReadIniSettings;
begin
  with TIniFile.Create(IniFile) do
    try
      EditPort.Text := ReadString('Server', 'Port', EditPort.Text);
      edtCdk.Text := ReadString('Settings', 'Type', edtCdk.Text);
      chkVerboseLogging.Checked := ReadBool('Settings', 'VerboseLogging', False);
      chkUseAuthentication.Checked := ReadBool('Settings', 'UseAuthentication', False);
      chkALS.Checked := ReadBool('Settings', 'AdsLocal', False);
      chkHTTPS.Checked := ReadBool('Settings', 'UseHTTPS', False);
    finally
      Free;
    end;
  chkPreloadTisLib3.Checked := PreloadTisLib3;
end;

procedure TfrmServiceSettings.SetHttpsON;
begin
  if not Assigned(LIOHandleSSL) then
  begin
    if not FileExists('.\SslKeys\localhost.cert.pem') then
    begin
      if IsDesktopMode then
        raise Exception.Create('Missing file ".\SslKeys\localhost.cert.pem", can''t start SSL.');
      chkHTTPS.Checked := False;
      Exit;
    end;

    if not FileExists('.\SslKeys\ca.cert.pem') then
    begin
      if IsDesktopMode then
        raise Exception.Create('Missing file ".\SslKeys\ca.cert.pem", can''t start SSL.');
      chkHTTPS.Checked := False;
      Exit;
    end;

    if not FileExists('.\SslKeys\localhost.key.pem') then
    begin
      if IsDesktopMode then
        raise Exception.Create('Missing file ".\SslKeys\localhost.key.pem", can''t start SSL.');
      chkHTTPS.Checked := False;
      Exit;
    end;

    LIOHandleSSL := TIdServerIOHandlerSSLOpenSSL.Create(FServer);
    LIOHandleSSL.SSLOptions.CertFile := '.\SslKeys\localhost.cert.pem';
    LIOHandleSSL.SSLOptions.RootCertFile := '.\SslKeys\ca.cert.pem';
    LIOHandleSSL.SSLOptions.KeyFile := '.\SslKeys\localhost.key.pem';
    LIOHandleSSL.OnGetPassword := OnGetSSLPassword;
  end;

  if Assigned(LIOHandleSSL) then
    FServer.IOHandler := LIOHandleSSL;
end;

procedure TfrmServiceSettings.SetPreloadTisLib3(const Value: Boolean);
begin
  if FPreloadTisLib3 = Value then
    Exit;

  FPreloadTisLib3 := Value;
  with TIniFile.Create(IniFile) do
    try
      WriteBool('Settings', 'PreloadTisLib3', Value);
    finally
      Free;
    end;
end;

procedure TfrmServiceSettings.StartServer;
begin
  if not FServer.Active then
  begin
    ReadIniSettings;

    if chkHTTPS.Checked then
      SetHttpsON
    else
      FServer.IOHandler := nil;

    FServer.Bindings.Clear;
    FServer.DefaultPort := StrToInt(EditPort.Text);
    FServer.Active := True;
  end;
end;

function TfrmServiceSettings.TisLib3Init(AdsCon: TAdsConnection): Boolean;
type
  TDevExInit = procedure;
  TLib3Loading = procedure(AOwner: HWnd; AConn: ADSHANDLE); stdcall;
  TUserInfo = procedure(AUserName, APassword, ARecht: PAnsiChar; ARights: Integer); stdcall;
var
  DevExInit: TDevExInit;
  Loading: TLib3Loading;
  UserInfo: TUserInfo;
  lPassword, lRecht: PAnsiChar;
begin
  Result := False;

  if TisWin3Loaded = -1 then
  begin
    if not Assigned(AdsCon) then
    begin
      AdsCon := AdsConnection;
      if not AdsCon.IsConnected then
        AdsCon.Connect;
    end;
    TisWin3Handle := LoadLibrary('TisLib3.dll');
    if TisWin3Handle > 0 then
    begin
      TisWin3Loaded := 1;
      @DevExInit := GetProcAddress(TisWin3Handle, 'dxInitialize');
      if @DevExInit = nil then
      begin
        // DoVerboseLogging('Error Loading TisWin3 dxInitialize');
        Exit;
      end;
      DevExInit;

      @Loading := GetProcAddress(TisWin3Handle, 'Loading');
      if @Loading = nil then
      begin
        // DoVerboseLogging('Error Loading TisWin3 "Loading"');
        Exit;
      end;
      Loading(Application.Handle, AdsCon.ConnectionHandle);

      @UserInfo := GetProcAddress(TisWin3Handle, 'UserInfo');
      if @UserInfo = nil then
        Exit;

      lPassword := PAnsiChar(AnsiString('0' + FormatDateTime('ddmm', Date) + '5'));
      lRecht := PAnsiChar(AnsiString(StringOfChar('F', 300)));
      UserInfo('TECH', lPassword, lRecht, 9);
      // UserInfo('TECH', '222010', 'FFF', 9);
    end
    else
    begin
      TisWin3Loaded := 0;
      RaiseLastOSError;
    end;
  end;

  Result := TisWin3Handle > 0;
  if Result and (not PreloadTisLib3) then
    PreloadTisLib3 := True;
end;

procedure TfrmServiceSettings.UnloadTisLib3;
type
  TDevExUnInit = procedure;
  TLib3Unloading = procedure; stdcall;
var
  DevExUnInit: TDevExUnInit;
  Unloading: TLib3Unloading;
begin
  if TisWin3Handle > 0 then
  begin
    @Unloading := GetProcAddress(TisWin3Handle, 'Unloading');
    if @Unloading <> nil then
      Unloading;

    @DevExUnInit := GetProcAddress(TisWin3Handle, 'dxFinalize');
    if @DevExUnInit <> nil then
      DevExUnInit;
    FreeLibrary(TisWin3Handle);
    TisWin3Loaded := -1;
  end;
  if Assigned(AdsConnection) then
    AdsConnection.Disconnect;
end;

initialization

IniFile := ChangeFileExt(ParamStr(0), '.ini');

end.
