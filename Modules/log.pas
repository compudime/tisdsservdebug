unit log;

interface

uses Windows, Classes, SysUtils, Optimize;

var
  Log_Lock: TOptimizeCriticalSection;
  CurModuleFileName: array [0 .. 1024] of char;
  CurModulePath: string;
  flAddDateTime2Log: boolean;

procedure WriteToLog(const sData: string; LogFileName: String = ''; flNoLock: boolean = False; AAlwaysLog: Boolean = False);

implementation

procedure WriteToLog(const sData: string; LogFileName: String; flNoLock: Boolean; AAlwaysLog: Boolean);
var
  fsLog: TFileStream;
  sFullPath, sTemp: string;
  dtAddTime: TDateTime;
  ARawByteString: AnsiString;
begin
  if not (SameText(LogFileName, 'err.log') or AAlwaysLog) then
    Exit; ///////////////////////////////////////////Disable other logs, for size, speed and memory

  if (Length(sData) > 0) then
  begin
    if LogFileName='' then
      LogFileName := 'App-' + FormatDateTime('yyyy-mm-dd', Now()) + '.log';

    sTemp := '';
    if flAddDateTime2Log then
    begin
      dtAddTime := Now;
      sTemp := FormatDateTime('dd.mm.yyyy" "HH:NN:SS zzz', dtAddTime) + '; ';
    end;

    sTemp := sTemp + sData + #13#10;
    ARawByteString := AnsiString(sTemp);
    if not flNoLock then
      Log_Lock.Enter;
    try
      sFullPath := CurModulePath + LogFileName;
      fsLog := nil;
      if FileExists(sFullPath) then
      begin
        fsLog := TFileStream.Create(sFullPath, (fmOpenReadWrite or fmShareDenyWrite));
        {if fsLog.Size>250000 then
        begin
          if FileExists(ChangeFileExt(sFullPath, '.bak')) then
            DeleteFile(ChangeFileExt(sFullPath, '.bak'));
          fsLog.Free;
          fsLog := nil;
          RenameFile(sFullPath, ChangeFileExt(sFullPath, '.bak'));
        end;}
      end;
      if not Assigned(fsLog) then
        fsLog := TFileStream.Create(sFullPath, fmCreate);

      try
        fsLog.Seek(0, soFromEnd);
        fsLog.Write(ARawByteString[1], Length(ARawByteString));
      finally
        fsLog.Free;
      end;
    finally
      if not flNoLock then
        Log_Lock.Leave;
    end;
  end;
end;

initialization

flAddDateTime2Log := True;
Log_Lock := TOptimizeCriticalSection.Create('Log_Lock', False);
if GetModuleFileName(GetModuleHandle(nil), CurModuleFileName, 1024) = 0 then
  RaiseLastOSError;
CurModulePath := ExtractFilePath(CurModuleFileName);

finalization

Log_Lock.Free;

end.
