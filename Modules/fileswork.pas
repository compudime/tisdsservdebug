unit fileswork;

interface

uses Classes,SysUtils,windows,ShellApi, Forms, wininfo;

Const
     DprFun = 0;
     PasFun = 1;
     TSpecialSymbols = [char(VK_RETURN),char(VK_TAB),char(10),' '];


function TrimStr(Str : String) : String;
procedure GetFileList(Dir : String;Ext : String;var FileList : TStrings);
procedure GetFileListFromDir(Dir : String;Ext : String;var FileList : TStrings;flOnlyFileNames : boolean=false);
procedure GetDirListIncludeDir(Dir : String;IncludeDir : string;var FileList : TStrings);
procedure FGetFileList(Dir : String;Ext : String;var FileList : TStrings);
procedure FGetFileListFromDir(Dir : String;Ext : String;var FileList : TStrings; flOnlyFileNames : boolean=false);
procedure FGetDirListIncludeDir(Dir : String;IncludeDir : string;var FileList : TStrings);
function Replace(S, SChar, DChar : String) : String;
function ReplaceAll(S : String; const ReplArr : array of variant) : String;
function RPos(Substr: string; S: string; iFrom : integer=0): Integer;
function SCopy(S : String; iFrom : integer; iTo : integer=-1) : String;
function StrRPos(Substr: string; S: string) : String;
function CopyDir(sFrom,sTo : String) : integer;

function ReadTemplate(fName : String; const ReplArr : array of variant; TemplFolder : String='template') : string;
procedure Save2File(fName,Body : String);
function GetFileData(fName : String) : String;

implementation

uses Variants, StrUtils;

//uses FindText;
function SCopy(S : String; iFrom : integer; iTo : integer=-1) : String;
begin
  if iTo=-1 then
    iTo:=Length(S);
  Result:=Copy(S,iFrom,iTo-iFrom+1);
end;

function Replace(S, SChar, DChar : String) : String;
{var
  Ptr : Integer;}
begin
  Result:=StringReplace(S,SChar,DChar,[rfReplaceAll,rfIgnoreCase]);
(*  Ptr := Pos(SChar, S);
  while Ptr > 0 do
    begin
      Delete(S, Ptr, Length(SChar));
      Insert(DChar, S, Ptr);
      Ptr := PosEx(SChar, S, Ptr+1);
    end;
  Result := S;*)
end;

function ReplaceAll(S : String; const ReplArr : array of variant) : String;
var
  i,parCount : integer;
begin
  Result:=S;
  parCount:=(High(ReplArr)+1) div 2;
  if parCount<>0 then
    for i:=0 to parCount-1 do
      Result:=Replace(Result,VarToStr(ReplArr[i*2]),VarToStr(ReplArr[i*2+1]));
end;

function RPos(Substr: string; S: string; iFrom : integer=0): Integer;
var
  ind : integer;
begin
  Result:=0;
  if iFrom>0 then
    S:=SCopy(S,1,iFrom);
  ind:=0;
  repeat
    S:=SCopy(S,Ind+1);
    Result:=Result+ind;
    ind:=Pos(Substr,S);    
  until ind=0;
end;

function StrRPos(Substr: string; S: string) : String;
begin
  Result:=SCopy(S,RPos(Substr,S));
end;

function TrimStr(Str : String) : String;
var
   i : integer;
begin
Result:='';
for i:=1 to Length(Str) do
    if not (Str[i] in TSpecialSymbols) then Result:=Result+Str[i];
end;

procedure GetFileList(Dir : String;Ext : String;var FileList : TStrings);
begin
  if AnsiLastChar(Dir)<>'\' then
    Dir:=Dir+'\';
  FGetFileList(Dir,UpperCase(Ext),FileList);
end;

procedure FGetFileList(Dir : String;Ext : String;var FileList : TStrings);
var
   FileData : _Win32_Find_Data;
   FileHandle : THandle;
   FileName : String;
begin
FileHandle:=FindFirstFile(PChar(Dir+'*.*'),FileData);
if FileHandle <> INVALID_HANDLE_VALUE then
 begin
  repeat
    FileName:=FileData.cFileName;
    if (((FileData.dwFileAttributes And faDirectory)>0)
         and ((FileName<>'.') And (FileName<>'..'))) then
       GetFileList(Dir+FileName+'\',Ext,FileList)
    else
     if (Ext='*.*') or (UpperCase(ExtractFileExt(FileName)) =Ext) then
       FileList.Add(Dir+FileName);
  until not FindNextFile(FileHandle,FileData);
 end;
end;

procedure GetFileListFromDir(Dir : String;Ext : String;var FileList : TStrings; flOnlyFileNames : boolean=false);
begin
  FGetFileListFromDir(Dir,UpperCase(Ext),FileList, flOnlyFileNames);
end;

procedure FGetFileListFromDir(Dir : String;Ext : String;var FileList : TStrings; flOnlyFileNames : boolean=false);
var
   FileData : TWin32FindData;
   FileHandle : THandle;
   FileName : String;
begin
  FileHandle:=FindFirstFile(PChar(Dir+'*.*'),FileData);
  if FileHandle <> INVALID_HANDLE_VALUE then
  repeat
    FileName:=FileData.cFileName;
    if (FileData.dwFileAttributes And faDirectory)=0 then
      if (Ext='*.*') or (UpperCase(ExtractFileExt(FileName))=Ext) then
        begin
          if flOnlyFileNames then
            FileList.Add(FileName)            
          else
            FileList.Add(Dir+FileName);
        end;
  until not FindNextFile(FileHandle,FileData);
end;

procedure GetDirListIncludeDir(Dir : String;IncludeDir : string;var FileList : TStrings);
begin
FGetDirListIncludeDir(Dir,UpperCase(IncludeDir),FileList);
end;

procedure FGetDirListIncludeDir(Dir : String;IncludeDir : string;var FileList : TStrings);
var
   FileData : TWin32FindData;
   FileHandle : THandle;
   flDeep : boolean;
   FileName : String;
begin
FileHandle:=FindFirstFile(PChar(Dir+'*.*'),FileData);
flDeep:=true;
if FileHandle <> INVALID_HANDLE_VALUE then
 begin
  repeat
    FileName:=FileData.cFileName;
    if (((FileData.dwFileAttributes And faDirectory)>0) and
       ((FileName<>'.') And ((FileName<>'..') and
         (IncludeDir=UpperCase(FileName))))) then
         begin
          FileList.Add(Dir);
          flDeep:=false;
         end;
  until not FindNextFile(FileHandle,FileData);
  if flDeep then
     begin
       FileHandle:=FindFirstFile(PChar(Dir+'*.*'),FileData);
       repeat
        FileName:=FileData.cFileName;
        if (((FileData.dwFileAttributes And faDirectory)>0) and
            ((FileName<>'.') And (FileName<>'..')))
          then GetDirListIncludeDir(Dir+FileName+'\',IncludeDir,FileList);
       until not FindNextFile(FileHandle,FileData);
     end;
 end;
end;

function CopyDir(sFrom,sTo : String) : integer;
var
  lpFileOp: TSHFileOpStruct;
begin
  lpFileOp.Wnd:=0;
  lpFileOp.pFrom:=PChar(sFrom);
  lpFileOp.pTo:=PChar(sTo);
  lpFileOp.wFunc:=FO_COPY;
  lpFileOp.fFlags:=FOF_NOCONFIRMMKDIR or FOF_NOCONFIRMATION or FOF_SIMPLEPROGRESS or FOF_SILENT;
  lpFileOp.fAnyOperationsAborted:=false;  
  lpFileOp.hNameMappings:=nil;  
  lpFileOp.lpszProgressTitle:=nil;  
  Result:=SHFileOperation(lpFileOp);
//FOF_MULTIDESTFILES 
end;

function ReadTemplate(fName : String; const ReplArr : array of variant; TemplFolder : String) : string;
begin
  with TStringList.Create do
    try
      TemplFolder:=IncludeTrailingBackslash(IncludeTrailingBackslash(ExtractFilePath(Application.ExeName))+TemplFolder);
      LoadFromFile(TemplFolder+FName);
      Result:=ReplaceAll(Text,ReplArr);
    finally
      Free;
    end;
end;

procedure Save2File(fName,Body : String);
var
  fs : TFileStream;
begin
  if ExtractFilePath(fName)='' then
    fName:=AbsPath(fName);
  fs:=TFileStream.Create(fName,fmCreate);
  try
    if fs.Write(Body[1],Length(Body))<>Length(Body) then raise Exception.Create('Wrong File Size');
  finally
    fs.Free;
  end;

{  with TStringList.Create do
    try
      Text:=Body;
      SaveToFile(fName);
    finally
      Free;
    end;}
end;

function GetFileData(fName : String) : String;
var
  fs : TFileStream;
  buf : PChar;
  rLen : integer;
begin
  if ExtractFilePath(fName)='' then
    fName:=AbsPath(fName);
  fs:=TFileStream.Create(fName,fmOpenRead);
  try
    buf:=AllocMem(fs.Size);
    rLen:=fs.Read(buf^,fs.Size);
    if rLen<>fs.Size then raise Exception.Create('Wrong Read Size');
    SetString(Result,buf,fs.Size);
//    Result:=buf;//Copy(Strpas(buf),1,fs.Size);
//    if fs.Write(Body[1],Length(Body))<>Length(Body) then raise Exception.Create('Wrong File Size');
  finally
    FreeMem(buf);
    fs.Free;
  end;

{  with TStringList.Create do
    try
      Text:=Body;
      SaveToFile(fName);
    finally
      Free;
    end;}
end;

end.
