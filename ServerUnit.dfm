object frmServiceSettings: TfrmServiceSettings
  Left = 271
  Top = 114
  Caption = 'TS Service Settings'
  ClientHeight = 194
  ClientWidth = 314
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 36
    Width = 20
    Height = 13
    Caption = 'Port'
  end
  object lblVer: TLabel
    Left = 8
    Top = 173
    Width = 97
    Height = 13
    Caption = 'Version Place Holder'
  end
  object lblcdx: TLabel
    Left = 8
    Top = 129
    Width = 44
    Height = 13
    Caption = 'ADT/CDX'
  end
  object ButtonStart: TButton
    Left = 8
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = ButtonStartClick
  end
  object ButtonStop: TButton
    Left = 89
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 1
    OnClick = ButtonStopClick
  end
  object EditPort: TEdit
    Left = 8
    Top = 52
    Width = 75
    Height = 21
    TabOrder = 2
    Text = '8080'
    OnChange = EditPortChange
  end
  object ButtonOpenBrowser: TButton
    Left = 199
    Top = 8
    Width = 107
    Height = 25
    Caption = 'Open Browser'
    TabOrder = 3
    OnClick = ButtonOpenBrowserClick
  end
  object chkALS: TCheckBox
    Left = 55
    Top = 149
    Width = 97
    Height = 17
    Caption = 'Local (ALS)'
    TabOrder = 4
    OnClick = chkALSClick
  end
  object edtCdk: TEdit
    Left = 8
    Top = 145
    Width = 41
    Height = 21
    TabOrder = 5
    Text = 'ADT'
    OnChange = edtCdkChange
  end
  object btn1: TButton
    Left = 214
    Top = 161
    Width = 92
    Height = 25
    Caption = 'Sheet Setup '
    TabOrder = 6
    OnClick = btn1Click
  end
  object chkVerboseLogging: TCheckBox
    Left = 89
    Top = 56
    Width = 97
    Height = 17
    Caption = 'Verbose Logging'
    TabOrder = 7
    OnClick = chkVerboseLoggingClick
  end
  object chkUseAuthentication: TCheckBox
    Left = 89
    Top = 79
    Width = 112
    Height = 17
    Caption = 'Use Authentication'
    TabOrder = 8
    OnClick = chkUseAuthenticationClick
  end
  object chkPreloadTisLib3: TCheckBox
    Left = 89
    Top = 125
    Width = 97
    Height = 17
    Caption = 'Preload TisLib3'
    TabOrder = 9
    OnClick = chkPreloadTisLib3Click
  end
  object chkHTTPS: TCheckBox
    Left = 89
    Top = 102
    Width = 97
    Height = 17
    Caption = 'Use HTTPS'
    TabOrder = 10
    OnClick = chkHTTPSClick
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 264
    Top = 40
  end
  object HealthTimer: TTimer
    Interval = 120000
    OnTimer = HealthTimerTimer
    Left = 272
    Top = 96
  end
end
