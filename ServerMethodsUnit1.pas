unit ServerMethodsUnit1;

interface

uses System.SysUtils, System.Classes, System.Json, Datasnap.DSServer, Datasnap.DSAuth, Data.DBXPlatform,
  Datasnap.DSProviderDataModuleAdapter, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf,
  FireDAC.Phys.Intf, FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys, FireDAC.Phys.ADS,
  FireDAC.Phys.ADSDef, Data.DB, FireDAC.Comp.Client, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Stan.StorageBin, FireDAC.Stan.StorageJSON, FireDAC.Comp.DataSet,
  FireDAC.Comp.UI, Data.FireDACJSONReflect, adstable, adsdata, adsfunc, adscnnct, ScanResolveUnit,
  Web.HTTPApp, Web.DBWeb, Winapi.Windows, JvSimpleXml, {MVCFramework.DataSet.Utils,}
  VendInvHelpersUnit, GeneralHelpersUnit, DataHelpersUnit, dmDataModule, dmDataModulePrd,
  GuiNonGuiHelpersUnit, PrdHelpersIUnit, ProjHelpersIUnit, VendHelpersIUnit;

{$METHODINFO ON}

type
  TServerMethods1 = class(TDataModule)
    FDConnection: TFDConnection;
    FDGUIxWaitCursor1: TFDGUIxWaitCursor;
    FDQueryItems: TFDQuery;
    FDStanStorageJSONLink1: TFDStanStorageJSONLink;
    FDStanStorageBinLink1: TFDStanStorageBinLink;
    FDQueryItem: TFDQuery;
    tbPrdmstr: TFDTable;
    FDPhysADSDriverLink1: TFDPhysADSDriverLink;
    FDQueryPrdmstr: TFDQuery;
    AdsConnection1: TAdsConnection;
    FDQueryItem2: TAdsQuery;
    qryRmLabel2: TAdsQuery;
    qryRmLabel: TFDQuery;
    qryDlvPickedBy2: TAdsQuery;
    qryDlvPickedBy: TFDQuery;
    FDConnectionCDX: TFDConnection;
    FDQueryGG: TFDQuery;
    FDConnectionGG: TFDConnection;
    qryRebatSearch: TFDQuery;
    qryItemSearch: TFDQuery;
    qryCustomer: TFDQuery;
    qrySQLOpen: TFDQuery;
    qrySQLExecute: TFDQuery;
    DataSetTableProducer1: TDataSetTableProducer;
    qryHelper: TAdsQuery;
    tbPrdAlias: TFDTable;
    qryHelper2: TAdsQuery;
    procedure FDConnectionBeforeConnect(Sender: TObject);
    procedure AdsConnection1BeforeConnect(Sender: TObject);
    procedure FDConnectionGGBeforeConnect(Sender: TObject);
    procedure DataModuleCreate(Sender: TObject);
    procedure AdsConnection1AfterConnect(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    FDataAlias: string;
    AScanResolve: TScanResolve;
    FFindOpt: Integer;
    RmLabelMode: Integer;
    FVerboseLogging: Integer;
    FImagesPath: string;
    FSessionID: string;
    // FCdPayLibInst: THandle;
    // FCdPayLibLoaded: Integer;

    procedure DoNewVendorInvoiceX(JSONRequest, JSONResponse: TJSONObject);
    procedure CheckPrice(BarCode: string);
    function FindCodePrice(UpcCode: string; var nPrice: Currency): Boolean;
    function FindProduct(UpcCode: string): Boolean;
    procedure DoRmLabel(BarCode: string; ASheetName: string; Qty: Integer = 1);
    procedure DisplayProduct;
    procedure DisplayCodePrice(nPrice: Currency);
    function DatabaseUser: string;
    function DatabasePass: string;
    function OpenPrdTable: Boolean;
    function SearchRebate: Boolean;
    procedure AssignCodeParamToQuery(AQuery: TFDQuery);
    procedure FixUpSqlText(var ASqlText: string);
    function SecureSqlText(var ASqlText: string): Boolean;
    function DBPrdCode(const ASearchCode: string): string;
    function DBPrdCodeSearch(AValue: string): string;
    procedure AdjustCstmPrdPrice(var ARegPrice, APkgPrice, ACasePrice, ASalePrice, ASPkgPrice, ASCasePrice: Currency;
      var AUPP, ASUPP: Integer);
    function GetVerboseLogging: Boolean;
    function VerifyWebOrderPost(const AXmlString: string; var AOrderNum, AProcessor: string): Boolean;
    procedure DoVerboseLogging(const AText: string);
    procedure AddVndRtrn(JSONRequest, JSONResponse: TJSONObject);
    procedure DeliveryStatus(JSONRequest, JSONResponse: TJSONObject);
    procedure DoAddBatchItem(JSONRequest, JSONResponse: TJSONObject);
    procedure DoNewVendorInvoice(JSONRequest, JSONResponse: TJSONObject);
    procedure DoRmLabelJson(JSONRequest, JSONResponse: TJSONObject);
    procedure DoPrdGlobalChangesJson(JSONRequest, JSONResponse: TJSONObject);
    // procedure CallTisLib3JsonAction(JSONRequest, JSONResponse: TJSONObject);
    procedure DoIsCustomerInDB(JSONRequest, JSONResponse: TJSONObject);
    procedure DoPostPrintJob(JSONRequest, JSONResponse: TJSONObject);
    procedure DoCustomerCardPayment(JSONRequest, JSONResponse: TJSONObject);
    procedure DoLocationInventoryTransfer(JSONRequest, JSONResponse: TJSONObject);
    procedure DoProductInfo(JSONRequest, JSONResponse: TJSONObject);
    procedure DoAddLineBusterBatch(JSONRequest, JSONResponse: TJSONObject);
    procedure DoCustAcntAvailLineBuster(JSONRequest, JSONResponse: TJSONObject);
    procedure DoLineBusterClear(JSONRequest, JSONResponse: TJSONObject);
    procedure DoAddNewProduct(JSONRequest, JSONResponse: TJSONObject);
    procedure DoUpdateProduct(JSONRequest, JSONResponse: TJSONObject);
    procedure DoSelfCheckoutStatus(JSONRequest, JSONResponse: TJSONObject);

    function QueryCustomerInDB(const ATel, AFilter: string): Boolean;
    function ProcessCardKnoxPay(ACardNo, AExpire, ACVV2, AToken, ACardKnoxTID, APayDescrip, APayCode,
      AProcessor: string; AAmount: Currency; AXmlResponse: TJvSimpleXML; JSONResponse: TJSONObject): Boolean;
    function XMLNamedNodeValue(ANode: TJvSimpleXMLElem; const AName: string): string;
    // procedure ScheduleRunOnceNow(const ATaskName: string; const AFileName: string; const AUserAccount: string);
    function GetImagesPath: string;
    // function CdPayInit: Boolean;
  protected
    FLine1, FLine2, FLine3, FLine4, FCodeFound: string;
    FCodePricedItem: Boolean;
    property VerboseLogging: Boolean read GetVerboseLogging;
    property ImagesPath: string read GetImagesPath;
    property SessionID: string read FSessionID;

    function CloseCommandActive: Boolean;
    // property CdPayLibLoaded: Integer read FCdPayLibLoaded write FCdPayLibLoaded;
    // property CdPayLibInst: THandle read FCdPayLibInst write FCdPayLibInst;
    // function TisLib3Init: Boolean;
  public
    function EchoString(Value: string): string;
    function DoGarmentReadyTel(AKeyValue: string): string;
    function GetCustomerBal(const sTel: string): string;
    function ExecuteSQL(ASqlText, ADataAlias: string): Integer;
    function OpenSqlJson(ASqlText, ADataAlias: string): TJSONValue;
    function OpenSqlXML(ASqlText, ADataAlias: string): string;
    function UpdatePostWebOrder(AInfo: TJSONObject): TJSONValue;
    function LineBusterAdd(const ABatchID, AItemCode: string; const AQuan: Double; const AIsCase: Boolean): TJSONValue;
    function LineBusterAddBatch(ABatchID: string; AInfo: string): TJSONValue;
    // function DlvCntrlScan(const sScan: string): TDataSet;
    function DlvCntrlAddBatch(sDriverName: string; sBatchData: string): TJSONValue;
    // function DataSetJSON(const sTable, sFilter: string): TDataSet;

    function DoCmd(ACmdType: string = ''): TJSONValue;
    //function DoDataCmd: TJSONValue;
    function UpdDlvStatus: TJSONValue;
    function AddBatchItem: TJSONValue;
    function NewVendorInvoice: TJSONValue;
    function PrintLabelRm: TJSONValue;
    function PrdGlobalChanges: TJSONValue;
    function PostPrintJob: TJSONValue;
    function IsCustomerInDB: TJSONValue;
    function AddCustomerToDB: TJSONValue;
    function AddUpdateCustomer: TJSONValue;
    function CustomerCardPayment: TJSONValue;
    function LocationInventoryTransfer: TJSONValue;
    function ProductInfo: TJSONValue;
    function AddLineBusterBatch: TJSONValue;
    function CustAcntAvailLineBuster: TJSONValue;
    function LineBusterClear: TJSONValue;
    function AddNewProduct: TJSONValue;
    function UpdateProduct: TJSONValue;
    function SelfCheckoutStatus: TJSONValue;
    // add new methods to TWebModule1.DSHTTPWebDispatcher1FormatResult
  end;
{$METHODINFO OFF}

function GenHelpers: TGenHelpers;
function DataHelpers: TDataHelpers;
function dmData: TdmData; overload;
function dmData(AConn: adscnnct.TAdsConnection): TdmData; overload;
function dmDataPrd: TdmDataPrd;
function GuiNonGuiHelpers: TGuiNonGuiHelpers;
function PrdHelpersI: TPrdHelpersI;
function ProjectHelpersI: TProjectHelpersI;
function VendHelpersI: TVendHelpersI;
function VendInvHelpers: TVendInvHelpers;

implementation

{$R *.dfm}

uses System.StrUtils, IniFiles, ServerUnit, FireDAC.Phys.ADSWrapper, Utils, System.Variants, IdURI, log, Forms,
  REST.Response.Adapter, System.Generics.Collections, DataSetToJSON, {MVCFramework.SystemJSONUtils,} ShellAPI,
  DateUtils, System.Masks, Data.DBXJSONCommon, System.RegularExpressions, Datasnap.DSSession;

var
  __VendInvHelpers: TDictionary<string, TVendInvHelpers>;
  __VendHelpersI: TDictionary<string, TVendHelpersI>;
  __ProjectHelpersI: TDictionary<string, TProjectHelpersI>;
  __PrdHelpersI: TDictionary<string, TPrdHelpersI>;
  __GuiNonGuiHelpers: TDictionary<string, TGuiNonGuiHelpers>;
  __dmDataPrd: TDictionary<string, TdmDataPrd>;
  __dmData: TDictionary<string, TdmData>;
  __DataHelpers: TDictionary<string, TDataHelpers>;
  __GenHelpers: TDictionary<string, TGenHelpers>;

function GenHelpers: TGenHelpers;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__GenHelpers) then
    __GenHelpers := TDictionary<string, TGenHelpers>.Create;
  if not __GenHelpers.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TGenHelpers.Create;
    __GenHelpers.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TGenHelpers', '', False, True);
  end;
end;

function DataHelpers: TDataHelpers;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__DataHelpers) then
    __DataHelpers := TDictionary<string, TDataHelpers>.Create;
  if not __DataHelpers.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TDataHelpers.Create;
    __DataHelpers.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TDataHelpers', '', False, True);
  end;
end;

function dmData: TdmData; overload;
begin
  Result := dmData(nil);
end;

function dmData(AConn: adscnnct.TAdsConnection): TdmData; overload;
var
  Session: TDSSession;
  AAppID: string;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__dmData) then
    __dmData := TDictionary<string, TdmData>.Create;
  if not __dmData.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TdmData.Create(nil);
    Result.AssignConnectionHandle(AConn.Handle);
    __dmData.Add(Session.SessionName, Result);
    AAppID := QuotedStr(ExtractFileName(Application.ExeName) + ' ' + Session.SessionName);
    // Result.POSConnection.Execute('Execute Procedure sp_SetApplicationID(' + AAppID + ');');
    // WriteToLog(Session.SessionName + ' - TdmData', '', False, True);
  end;

  // WriteToLog(Session.SessionName + ' - ' + Result.Name + ' - ' + IntToStr(Result.POSConnection.Handle) , '', False, True);
end;

function dmDataPrd: TdmDataPrd;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__dmDataPrd) then
    __dmDataPrd := TDictionary<string, TdmDataPrd>.Create;
  if not __dmDataPrd.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TdmDataPrd.Create(nil);
    __dmDataPrd.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - ' + Result.Name, '', False, True);
  end;
  // WriteToLog(Session.SessionName + ' - ' + Result.Name, '', False, True);
end;

function GuiNonGuiHelpers: TGuiNonGuiHelpers;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__GuiNonGuiHelpers) then
    __GuiNonGuiHelpers := TDictionary<string, TGuiNonGuiHelpers>.Create;
  if not __GuiNonGuiHelpers.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TGuiNonGuiHelpers.Create;
    __GuiNonGuiHelpers.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TGuiNonGuiHelpers', '', False, True);
  end;
end;

function PrdHelpersI: TPrdHelpersI;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__PrdHelpersI) then
    __PrdHelpersI := TDictionary<string, TPrdHelpersI>.Create;
  if not __PrdHelpersI.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TPrdHelpersI.Create;
    __PrdHelpersI.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TPrdHelpersI', '', False, True);
  end;
end;

function ProjectHelpersI: TProjectHelpersI;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__ProjectHelpersI) then
    __ProjectHelpersI := TDictionary<string, TProjectHelpersI>.Create;
  if not __ProjectHelpersI.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TProjectHelpersI.Create;
    __ProjectHelpersI.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TProjectHelpersI', '', False, True);
  end;
end;

function VendHelpersI: TVendHelpersI;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__VendHelpersI) then
    __VendHelpersI := TDictionary<string, TVendHelpersI>.Create;
  if not __VendHelpersI.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TVendHelpersI.Create;
    __VendHelpersI.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TVendHelpersI', '', False, True);
  end;
end;

function VendInvHelpers: TVendInvHelpers;
var
  Session: TDSSession;
begin
  Session := TDSSessionManager.GetThreadSession;
  if not Assigned(__VendInvHelpers) then
    __VendInvHelpers := TDictionary<string, TVendInvHelpers>.Create;
  if not __VendInvHelpers.TryGetValue(Session.SessionName, Result) then
  begin
    Result := TVendInvHelpers.Create;
    __VendInvHelpers.Add(Session.SessionName, Result);
    // WriteToLog(Session.SessionName + ' - TVendInvHelpers', '', False, True);
  end;
end;

function Replicate(c: string; nLen: Integer): string;
var
  I: Integer;
begin
  Result := '';
  for I := 1 to nLen do
    Result := Result + c;
end;

function PadL(strStr: string; intLen: Integer; strFill: string = ' '): string;
begin
  Result := Copy(strStr, 1, intLen);
  Result := Replicate(strFill, intLen - Length(Result)) + Result;
end;

function PadR(strStr: string; intLen: Integer; strFill: string = ' '): string;
begin
  Result := Copy(strStr, 1, intLen);
  Result := Result + Replicate(strFill, intLen - Length(Result));
end;

function GetUniqueFileName(const Path, Prefix, Extension: string): string;
var
  Buffer: array [0 .. MAX_PATH] of Char;
begin
  repeat
    GetTempFileName(PChar(Path), PChar(Prefix), 0, Buffer);
    DeleteFile(Buffer);
    Result := ChangeFileExt(Buffer, Extension);
  until not FileExists(Result);
end;

function TServerMethods1.AddBatchItem: TJSONValue;
begin
  Result := DoCmd('AddBatchItem');
end;

function TServerMethods1.AddCustomerToDB: TJSONValue;
begin
  Result := DoCmd('AddCustomerToDB');
end;

function TServerMethods1.AddLineBusterBatch: TJSONValue;
begin
  Result := DoCmd('AddLineBusterBatch');
end;

function TServerMethods1.AddNewProduct: TJSONValue;
begin
  Result := DoCmd('AddNewProduct');
end;

function TServerMethods1.AddUpdateCustomer: TJSONValue;
begin
  Result := DoCmd('AddUpdateCustomer');
end;

procedure TServerMethods1.AddVndRtrn(JSONRequest, JSONResponse: TJSONObject);
var
  AUpcCode, AAuthBy, AVendID, AQuan, ARowS: string;
  APos: Integer;
  APair: TJSONPair;
  AEnum: TJSONPairEnumerator;
  procedure AddVndRtrnRow;
  begin
    qryHelper.SQL.Add('Insert Into VndRtrns (UpcCode, Quan, AuthBy');
    if AVendID <> '' then
      qryHelper.SQL.Add(', VendID');
    qryHelper.SQL.Add(') Values(');
    qryHelper.SQL.Add(Format('%s, %s, %s', [QuotedStr(AUpcCode), AQuan, QuotedStr(AAuthBy)]));
    if AVendID <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(AVendID)]));
    qryHelper.SQL.Add(');');
  end;

begin
  JSONRequest.TryGetValue<string>('UpcCode', AUpcCode);
  JSONRequest.TryGetValue<string>('Quan', AQuan);
  JSONRequest.TryGetValue<string>('AuthBy', AAuthBy);
  JSONRequest.TryGetValue<string>('VendID', AVendID);
  if (AUpcCode <> '') and (AQuan <> '') then
  begin
    qryHelper.SQL.Clear;
    AddVndRtrnRow;
    try
      qryHelper.ExecSQL;
      JSONResponse.AddPair('Status', 'Success');
      JSONResponse.AddPair('Result', Format('Vendor Return Added for %s', [AUpcCode]));
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('AddVndRtrn %s', [E.Message]));
      end;
    end;
  end
  else if JSONRequest.TryGetValue<string>('Row', ARowS) then
  begin
    qryHelper.SQL.Clear;
    AEnum := JSONRequest.GetEnumerator;
    while AEnum.MoveNext do
    begin
      APair := AEnum.Current;
      if SameText(APair.JsonString.Value, 'Row') then
      begin
        ARowS := APair.JsonValue.Value;
        APos := Pos('|', ARowS);
        if APos > 0 then
        begin
          AUpcCode := Copy(ARowS, 1, APos - 1);
          ARowS := Copy(ARowS, APos + 1, MaxInt);
          APos := Pos('|', ARowS);
          if APos > 0 then
          begin
            AQuan := Copy(ARowS, 1, APos - 1);
            ARowS := Copy(ARowS, APos + 1, MaxInt);
          end;
          if (AUpcCode <> '') and (AQuan <> '') then
            AddVndRtrnRow;
        end;
      end;
    end;
    if qryHelper.SQL.Count > 0 then
    begin
      try
        qryHelper.ExecSQL;
        JSONResponse.AddPair('Status', 'Success');
        JSONResponse.AddPair('Result', Format('%d Vendor Returns Added', [qryHelper.RowsAffected]));
      except
        on E: Exception do
        begin
          JSONResponse.AddPair('Status', 'Error');
          JSONResponse.AddPair('Error', Format('AddVndRtrn raised %s', [E.Message]));
        end;
      end;
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'AddVndRtrn No Valid Row submitted');
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'AddVndRtrn required parameter missing');
  end;
end;

procedure TServerMethods1.AdjustCstmPrdPrice(var ARegPrice, APkgPrice, ACasePrice, ASalePrice, ASPkgPrice,
  ASCasePrice: Currency; var AUPP, ASUPP: Integer);
var
  ACstmInList: string;
  function IsInTimeFrame: Boolean;
  var
    STime, ETime: string;
    SDate, EDate: TDate;
  begin
    try
      SDate := qryHelper.FieldByName('Begins').AsDateTime;
      EDate := qryHelper.FieldByName('Expires').AsDateTime;
      STime := qryHelper.FieldByName('TBegins').AsString;
      ETime := qryHelper.FieldByName('TExpires').AsString;
      if STime = '' then
        STime := '0000';
      if ETime = '' then
        ETime := '2359';
      STime := Copy(STime, 1, 2) + ':' + Copy(STime, 3, 2);
      ETime := Copy(ETime, 1, 2) + ':' + Copy(ETime, 3, 2);
      Result := ((SDate = 0) or ((SDate < Date) or ((SDate = Date) and ((Time > StrToTime(STime)))))) and
        ((EDate = 0) or ((EDate > Date) or ((Date = EDate) and ((Time < StrToTime(ETime))))));
    except
      Result := False;
    end;
  end;

begin
  // Use Only empty RuleFile Lines except if set to include Rule - ONLY DOING PRiceMode $
  ACstmInList := ''; // Use Setting to assign values
  qryHelper.Close;
  qryHelper.SQL.Add('Select UpcCode, PriceField, PriceMode, Price, Upp, Begins, Expires, TBegins, TExpires');
  qryHelper.SQL.Add(Format('From PrdPrice Where Upper(UpcCode)=%s And Upper(Cat)=''PRDMSTR''',
    [QuotedStr(FCodeFound)]));
  qryHelper.SQL.Add('And Not UnActive And Price>0 And PriceMode=''$''');
  qryHelper.SQL.Add('And (Begins Is Null or Begins<=CurDate()) And (Expires Is Null or Expires>=CurDate())');
  qryHelper.SQL.Add('And (IfNull(RuleFile,'''')=''''');
  if ACstmInList <> '' then
    qryHelper.SQL.Add('or RuleFile IN (' + ACstmInList + ')');
  qryHelper.SQL.Add(')');
  qryHelper.SQL.Add('Order By PriceField');
  qryHelper.Open;

  if qryHelper.IsEmpty then
    Exit;

  qryHelper.First;
  if qryHelper.Locate('PriceField', 'PRICE', [loCaseInsensitive]) then
    ARegPrice := qryHelper.FieldByName('Price').AsCurrency;

  if qryHelper.Locate('PriceField', 'PKGPRC', [loCaseInsensitive]) and (qryHelper.FieldByName('UPP').AsInteger > 1) then
  begin
    APkgPrice := qryHelper.FieldByName('Price').AsCurrency;
    AUPP := qryHelper.FieldByName('UPP').AsInteger;
  end;

  if qryHelper.Locate('PriceField', 'CASEPRICE', [loCaseInsensitive]) then
    ACasePrice := qryHelper.FieldByName('Price').AsCurrency;

  if qryHelper.Locate('PriceField', 'SALE', [loCaseInsensitive]) and IsInTimeFrame then
    ASalePrice := qryHelper.FieldByName('Price').AsCurrency;

  if qryHelper.Locate('PriceField', 'SPKGPRC', [loCaseInsensitive]) and (qryHelper.FieldByName('UPP').AsInteger > 1) and IsInTimeFrame
  then
  begin
    ASPkgPrice := qryHelper.FieldByName('Price').AsCurrency;
    ASUPP := qryHelper.FieldByName('UPP').AsInteger;
  end;

  if qryHelper.Locate('PriceField', 'CASEPRICE', [loCaseInsensitive]) and IsInTimeFrame then
    ASCasePrice := qryHelper.FieldByName('Price').AsCurrency;
end;

procedure TServerMethods1.AdsConnection1AfterConnect(Sender: TObject);
begin
  try
    AdsConnection1.Execute('Try EXECUTE PROCEDURE cdUpdateCrUseData(); Catch All End;');
  except
  end;
end;

procedure TServerMethods1.AdsConnection1BeforeConnect(Sender: TObject);
begin
  AdsConnection1.Password := DatabasePass;
end;

function JSONToByteString(obj: TJSONAncestor): string;
var
  bytes: TBytes;
  len: Integer;
begin
  SetLength(bytes, obj.EstimatedByteSize);
  len := obj.ToBytes(bytes, 0);
  Result := TEncoding.ANSI.GetString(bytes, 0, len);
end;

{ procedure TServerMethods1.CallTisLib3JsonAction(JSONRequest, JSONResponse: TJSONObject);
  type
  TActionJSON = function(AJson: PAnsiChar): PAnsiChar; stdcall;
  var
  AActionJSON: TActionJSON;
  AResult: PAnsiChar;
  AJson: string;
  JSONResponseC: TJSONObject;
  AEnumerator: TJSONPairEnumerator;
  APair: TJSONPair;
  begin
  AJson := JSONToByteString(JSONRequest);
  // using JSONRequest.ToString; causes Endcoded values to be decoded, and we need to still pass as proper JSON string

  try
  // if not frmServiceSettings.TisLib3Init(AdsConnection1) then
  if not frmServiceSettings.TisLib3Init(nil) then
  raise Exception.Create('TisLib3 Not Loaded - CallTisLib3JsonAction');

  @AActionJSON := GetProcAddress(frmServiceSettings.TisWin3Handle, 'ActionJSON');
  if @AActionJSON = nil then
  raise Exception.Create('Error Loading TisLib3 "ActionJSON"');

  AResult := AActionJSON(PAnsiChar(AnsiString(AJson)));

  JSONResponseC := TJSONObject.ParseJSONValue(string(AResult)) as TJSONObject;
  AEnumerator := JSONResponseC.GetEnumerator;
  while AEnumerator.MoveNext do
  begin
  APair := AEnumerator.Current;
  JSONResponse.AddPair(APair.JsonString.Value, APair.JsonValue.Value);
  end;
  except
  on E: Exception do
  begin
  JSONResponse.AddPair('Status', 'Error');
  JSONResponse.AddPair('Error', Format('CallTisLib3JsonAction %s', [E.Message]));
  end;
  end;
  end; }

{ function TServerMethods1.CdPayInit: Boolean;
  type
  TDevExInit = procedure;
  var
  DevExInit: TDevExInit;
  AOldDir: string;
  begin
  Result := False;
  if FCdPayLibLoaded = -1 then
  begin
  FCdPayLibInst := LoadLibrary(PChar('C:\RunPOS\CdPayLib.dll'));
  FCdPayLibLoaded := 0;

  if FCdPayLibInst > 0 then
  begin
  AOldDir := GetCurrentDir;
  try
  ChDir('C:\RunPOS\');
  @DevExInit := GetProcAddress(FCdPayLibInst, 'dxInitialize');
  if @DevExInit = nil then
  Exit;
  DevExInit;
  finally
  ChDir(AOldDir);
  end;
  end;
  end;

  Result := FCdPayLibInst > 0;
  end; }

procedure TServerMethods1.CheckPrice(BarCode: string);
var
  CodePrice: Currency;
  APriceEncoded: Boolean;
  // AMask: TMask;
begin
  FFindOpt := 0;

  if not OpenPrdTable then
  begin
    FLine1 := 'Database Unavailable';
    FLine2 := '';
    if FLine2 = '' then
      FLine2 := 'Table not Active.';
  end
  else if (AdsConnection1.DDVersionMajor > 1) or
    ((AdsConnection1.DDVersionMajor = 1) and (AdsConnection1.DDVersionMinor >= 221)) then
  begin
    qryHelper.SQL.Clear;
    qryHelper.SQL.Text := Format('Select Code, CodePrice From Prdmstr Where Code=cdScanResolveP(%s);',
      [QuotedStr(BarCode)]);
    qryHelper.Open;
    if qryHelper.IsEmpty then
    begin
      APriceEncoded := qryHelper.FieldByName('CodePrice').AsBoolean;
      if not APriceEncoded then
      begin
        APriceEncoded := TRegEx.IsMatch(qryHelper.FieldByName('Code').AsString, '^2[0-9]{5}[0]{5}$');
        { AMask := TMask.Create('2?????00000');
          APriceEncoded := AMask.Matches(qryHelper.FieldByName('Code').AsString);
          AMask.Free; }
      end;
      if APriceEncoded then
        CodePrice := StrToCurrDef(Copy(BarCode, 8, 4), 0) / 100;
      if not APriceEncoded then
      begin
        APriceEncoded := TRegEx.IsMatch(qryHelper.FieldByName('Code').AsString, '^2[0-9]{5}[9]{5}$');
        { AMask := TMask.Create('2?????99999');
          APriceEncoded := AMask.Matches(qryHelper.FieldByName('Code').AsString);
          AMask.Free; }
        if APriceEncoded then
          CodePrice := StrToCurrDef(Copy(BarCode, 7, 5), 0) / 100;
      end;
      tbPrdmstr.Locate('Code', BarCode, [loCaseInsensitive]);
      if APriceEncoded then
        DisplayCodePrice(CodePrice)
      else
        DisplayProduct;
    end
    else
    begin
      FLine1 := BarCode;
      FLine2 := 'Unlisted Item.';
    end;
  end
  else if FindProduct(BarCode) then
    DisplayProduct
  else if FindCodePrice(BarCode, CodePrice) and (CodePrice > 0) then
    DisplayCodePrice(CodePrice)
  else
  begin
    FLine1 := BarCode;
    FLine2 := 'Unlisted Item.';
  end
end;

function TServerMethods1.CloseCommandActive: Boolean;
begin
  Result := frmServiceSettings.CloseCommandActive;
end;

function TServerMethods1.CustAcntAvailLineBuster: TJSONValue;
begin
  Result := DoCmd('CustAcntAvailLineBuster');
end;

function TServerMethods1.CustomerCardPayment: TJSONValue;
begin
  Result := DoCmd('CustomerCardPayment');
end;

function TServerMethods1.QueryCustomerInDB(const ATel, AFilter: string): Boolean;
begin
  qryHelper.SQL.Clear;
  qryHelper.SQL.Add(Format('Select Count(*) Cnt From Customer Where Tel=%s', [QuotedStr(ATel)]));
  if AFilter <> '' then
    qryHelper.SQL.Add('And ' + AFilter);
  qryHelper.SQL.Add(';');
  qryHelper.Open;
  Result := qryHelper.Fields[0].AsInteger > 0;
end;

procedure TServerMethods1.DoSelfCheckoutStatus(JSONRequest, JSONResponse: TJSONObject);
var
  AStatus, ADeviceID, ASecureCardID: string;
begin
  JSONRequest.TryGetValue<string>('Status', AStatus);
  JSONRequest.TryGetValue<string>('DeviceID', ADeviceID);
  JSONRequest.TryGetValue<string>('SecureCardID', ASecureCardID);

  if AStatus.IsEmpty or ADeviceID.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'SelfCheckoutStatus required parameter missing');
    Exit;
  end;

  try
    qryHelper.Close;
    qryHelper.SQL.Clear;
    if SameText(AStatus, 'Void') then
      qryHelper.SQL.Text :=
        'Update SelfCheckoutStatus Set EndTime=Now(), EndAction=''VOID'' Where DeviceID=:DeviceID And EndTime Is Null;'
    else if SameText(AStatus, 'Security') and (not ASecureCardID.IsEmpty) then
    begin
      qryHelper.SQL.Text :=
        'Update SelfCheckoutStatus Set SecureStartTime=Now(), SecureCardID=:SecureCardID Where DeviceID=:DeviceID And EndTime Is Null;';
    end;

    if qryHelper.SQL.Text = '' then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'SelfCheckoutStatus no proper Status passed');
      Exit;
    end;
    qryHelper.Prepare;
    qryHelper.ParamByName('DeviceID').AsString := ADeviceID;
    if qryHelper.Params.FindParam('SecureCardID') <> nil then
      qryHelper.ParamByName('SecureCardID').AsString := ASecureCardID;
    qryHelper.ExecSQL;
    JSONResponse.AddPair('Status', 'Success');
  except
    on E: Exception do
      JSONResponse.AddPair('Report SelfCheckoutStatus Error', E.Message);
  end;
end;

procedure TServerMethods1.DoUpdateProduct(JSONRequest, JSONResponse: TJSONObject);
begin
  try
  AdsConnection1.Connect;
  dmData(AdsConnection1);
  PrdHelpersI.UpdatePrd(JSONRequest, JSONResponse);
  except
    on E: Exception do
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', Format('DoUpdateProduct %s', [E.Message]));
    end;
  end;
end;

procedure TServerMethods1.DoVerboseLogging(const AText: string);
begin
  if VerboseLogging then
    WriteToLog(AText, '', False, True);
end;

procedure TServerMethods1.DoPostPrintJob(JSONRequest, JSONResponse: TJSONObject);
const
  PostScript =
    'Insert Into PrintJobs (UpcCode, Quan, OutputName, PrinterName, Action) Values(:UpcCode, :Quan, :OutputName, :PrinterName, ''Print'')';
var
  ABarCode, AOutputName, APrinterName: string;
  AQty: Integer;
begin
  JSONRequest.TryGetValue<string>('BarCode', ABarCode);
  JSONRequest.TryGetValue<string>('OutputName', AOutputName);
  JSONRequest.TryGetValue<string>('PrinterName', APrinterName);
  JSONRequest.TryGetValue<Integer>('Qty', AQty);
  if AQty = 0 then
    AQty := 1;
  if (ABarCode <> '') and (AOutputName <> '') then
  begin
    AOutputName := 'Test1'; //SheetNameLookup(AOutputName);
    try
      qryHelper.Close;
      qryHelper.SQL.Text := PostScript;
      qryHelper.Prepare;
      qryHelper.ParamByName('UpcCode').AsString := ABarCode;
      qryHelper.ParamByName('OutputName').AsString := AOutputName;
      qryHelper.ParamByName('PrinterName').AsString := APrinterName;
      qryHelper.ParamByName('Quan').AsInteger := AQty;
      qryHelper.ExecSQL;
      JSONResponse.AddPair('Status', 'Success');
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('DoPostPrintJob %s', [E.Message]));
      end;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'DoPostPrintJob required parameter missing');
  end;
end;

procedure TServerMethods1.DoPrdGlobalChangesJson(JSONRequest, JSONResponse: TJSONObject);
var
  JSONRows: TJSONArray;
  JSONRowA: TJSONObject;
  AUpcCode, AUpdateField, AUpdateValue: string;
  AUpcCodeList: TStringList;
  I: Integer;
begin
  qryHelper.SQL.Clear;
  JSONRequest.TryGetValue<string>('UpdateField', AUpdateField);
  JSONRequest.TryGetValue<string>('UpdateValue', AUpdateValue);
  if (AUpdateField <> '') and (AUpdateValue <> '') then
  begin
    qryHelper.SQL.Add('Update Prdmstr Set');
    qryHelper.SQL.Add(Format('[%s]=%s', [AUpdateField, AUpdateValue]));
  end
  else
  begin
    JSONRows := JSONRequest.GetValue('Updates') as TJSONArray;
    if JSONRows <> nil then
      for I := 0 to JSONRows.Count - 1 do
      begin
        JSONRowA := JSONRows.Items[I] as TJSONObject;
        JSONRowA.TryGetValue<string>('UpdateField', AUpdateField);
        JSONRowA.TryGetValue<string>('UpdateValue', AUpdateValue);
        if (AUpdateField <> '') and (AUpdateValue <> '') then
        begin
          if qryHelper.SQL.Count = 0 then
            qryHelper.SQL.Add('Update Prdmstr Set');
          qryHelper.SQL.Add(Format('[%s]=%s', [AUpdateField, AUpdateValue]));
        end;
      end;
  end;

  if qryHelper.SQL.Count > 0 then
  begin
    AUpcCodeList := TStringList.Create;
    try
      JSONRows := JSONRequest.GetValue('Rows') as TJSONArray;
      if JSONRows <> nil then
        for I := 0 to JSONRows.Count - 1 do
        begin
          JSONRowA := JSONRows.Items[I] as TJSONObject;
          JSONRowA.TryGetValue<string>('UpcCode', AUpcCode);
          if (AUpcCode <> '') then
            AUpcCodeList.Add(AUpcCode.QuotedString);
        end;
      if AUpcCodeList.Count > 0 then
      begin
        qryHelper.SQL.Add(Format('Where Code IN (%s);', [AUpcCodeList.CommaText]));

        qryHelper.SQL.Insert(0, 'Begin Transaction; Try');
        qryHelper.SQL.Add('Commit Work; Catch All Rollback Work; Raise; End;');
        try
          qryHelper.ExecSQL;
          JSONResponse.AddPair('Status', 'Success');
          JSONResponse.AddPair('Result', Format('%d product Global Changes Complete', [qryHelper.RowsAffected]));
        except
          on E: Exception do
          begin
            JSONResponse.AddPair('Status', 'Error');
            JSONResponse.AddPair('Error', Format('DoPrdGlobalChangesJson raised %s', [E.Message]));
          end;
        end;
      end
      else
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', 'DoPrdGlobalChangesJson No Valid Rows submitted');
      end;
    finally
      AUpcCodeList.Free;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'DoPrdGlobalChangesJson No Valid Updates submitted');
  end;
end;

procedure TServerMethods1.DoProductInfo(JSONRequest, JSONResponse: TJSONObject);
var
  AUpcCode, AUpcCodeO, AImageFile, AWghCdPre, AExtPrice, ADeviceID, AScanMode: string;
  APriceEncoded, AGetImage, AHaveImage, AIsCase: Boolean;
  AStream: TFileStream;
  AJson: TJSONArray;
  ASqlString: TStringList;
  ABarCodeQty: Double;
begin
  try
    JSONRequest.TryGetValue<string>('UpcCode', AUpcCode);
    JSONRequest.TryGetValue<Boolean>('GetImage', AGetImage);
    JSONRequest.TryGetValue<string>('DeviceID', ADeviceID);
    JSONRequest.TryGetValue<string>('ScanMode', AScanMode);

    if AUpcCode.IsEmpty then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'ProductInfo required parameter missing');
      Exit;
    end;

    AUpcCodeO := AUpcCode;
    qryHelper.Close;
    qryHelper.SQL.Clear;
    qryHelper.SQL.Add('Select Code, [Desc], SmlDesc, UPC, Price, Scale, CasePrice, CodePrice, ');

    qryHelper.SQL.Add('IIF(Cast(Price AS SQL_NUMERIC)>0 And ');
    qryHelper.SQL.Add('(Cast(IfNull(PkgPrc, 0) AS SQL_NUMERIC)=0 or Cast(IfNull(Upp, 0) AS SQL_NUMERIC)<=1),');
    qryHelper.SQL.Add('''1 @ '' + Trim(Cast(Price As SQL_CHAR)) + '', '', '''') +');

    qryHelper.SQL.Add('IIF(Cast(PkgPrc AS SQL_NUMERIC)>0 And Cast(Upp AS SQL_NUMERIC)>1,');
    qryHelper.SQL.Add('Trim(Cast(Upp As SQL_CHAR)) + '' @ '' + Trim(Cast(PkgPrc As SQL_CHAR))');
    qryHelper.SQL.Add('+ '', '', '''') +');

    qryHelper.SQL.Add('IIF(Cast(Sale AS SQL_NUMERIC)>0 And (Begins Is Null Or Begins<=CurDate())');
    qryHelper.SQL.Add('And (Expires Is Null Or Expires>=CurDate()),');
    qryHelper.SQL.Add('''1 @ '' + Trim(Cast(Sale As SQL_CHAR)) + '', '', '''') +');

    qryHelper.SQL.Add('IIF(Cast(SPkgPrc AS SQL_NUMERIC)>0 And SUpp>1');
    qryHelper.SQL.Add('And (PBegins Is Null Or PBegins<=CurDate())');
    qryHelper.SQL.Add('And (PExpires Is Null Or PExpires>=CurDate()),');
    qryHelper.SQL.Add('Trim(Cast(SUpp As SQL_CHAR)) + '' @ '' + Trim(Cast(SPkgPrc As SQL_CHAR))');
    qryHelper.SQL.Add('+ '', '', '''')  [ExtPrice],');

    qryHelper.SQL.Add('IIF(Cast(CasePrice AS SQL_NUMERIC)>0,');
    qryHelper.SQL.Add('Trim(Cast(UPC As SQL_CHAR)) + '' @ '' + Trim(Cast(CasePrice As SQL_CHAR)), '''') +');

    qryHelper.SQL.Add('IIF(Cast(CaseSale AS SQL_NUMERIC)>0');
    qryHelper.SQL.Add('And (CBegins Is Null Or CBegins<=CurDate())');
    qryHelper.SQL.Add('And (CExpires Is Null Or CExpires>=CurDate()),');
    qryHelper.SQL.Add('Trim(Cast(UPC As SQL_CHAR)) + '' @ '' +');
    qryHelper.SQL.Add('Trim(Cast(CaseSale As SQL_CHAR)), '''') [ExtCasePrice]');

    qryHelper.SQL.Add(Format('From Prdmstr Where Code=cdScanResolveP(%s);', [QuotedStr(AUpcCode)]));
    qryHelper.Open;

    if qryHelper.IsEmpty then
    begin
      qryHelper.Close;
      ASqlString := TStringList.Create;
      try
        ASqlString.Assign(qryHelper.SQL);
        qryHelper.SQL.Clear;
        qryHelper.SQL.Add('Select PrdLineID, AliasCode From PrdAlias Where');
        qryHelper.SQL.Add(Format('AliasCode=cdScanResolve(%s, ''PrdAlias'', ''AliasCode'');', [QuotedStr(AUpcCode)]));
        qryHelper.Open;
        if not qryHelper.IsEmpty then
        begin
          AUpcCode := qryHelper.FieldByName('PrdLineID').AsString;
          qryHelper.Close;
          qryHelper.SQL.Assign(ASqlString);
          qryHelper.SQL.Delete(qryHelper.SQL.Count - 1);
          qryHelper.SQL.Add(Format('From Prdmstr Where LineID=%s;', [QuotedStr(AUpcCode)]));
          qryHelper.Open;
        end
        else
        begin
          qryHelper.Close;
          qryHelper.SQL.Assign(ASqlString);
        end;
      finally
        ASqlString.Free;
      end;
    end;

    if qryHelper.IsEmpty then
    begin
      qryHelper.Close;
      qryHelper.SQL.Delete(qryHelper.SQL.Count - 1);
      qryHelper.SQL.Add
        (Format('From Prdmstr Where Upper(CaseCode)<>'''' And CaseCode=cdScanResolve(%s, ''Prdmstr'', ''CaseCode'');',
        [QuotedStr(AUpcCode)]));
      qryHelper.Open;
      AIsCase := not qryHelper.IsEmpty;
    end;

    if qryHelper.IsEmpty then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'Item Not Found');
      if not ADeviceID.IsEmpty then
        try
          qryHelper.Close;
          qryHelper.SQL.Text :=
            'Update SelfCheckoutStatus Set ItemsNotFound=ItemsNotFound+1 Where DeviceID=:DeviceID And EndTime Is Null;';
          qryHelper.Prepare;
          qryHelper.ParamByName('DeviceID').AsString := ADeviceID;
          qryHelper.ExecSQL;
        except
          on E: Exception do
            JSONResponse.AddPair('Report SelfCheckoutStatus Error', E.Message);
        end;
      Exit;
    end;

    ABarCodeQty := 1;
    JSONResponse.AddPair('Status', 'Success');
    JSONResponse.AddPair('Code', qryHelper.FieldByName('Code').AsString);
    JSONResponse.AddPair('Desc', qryHelper.FieldByName('Desc').AsString);
    JSONResponse.AddPair('SmlDesc', qryHelper.FieldByName('SmlDesc').AsString);
    JSONResponse.AddPair('UPC', qryHelper.FieldByName('UPC').AsInteger.ToString);
    JSONResponse.AddPair('Scale', qryHelper.FieldByName('Scale').AsBoolean.ToString(TUseBoolStrs.True));

    APriceEncoded := qryHelper.FieldByName('CodePrice').AsBoolean;
    if not APriceEncoded then
    begin
      APriceEncoded := TRegEx.IsMatch(qryHelper.FieldByName('Code').AsString, '^2[0-9]{5}[0]{5}$');
      { AMask := TMask.Create('2?????00000');
        APriceEncoded := AMask.Matches(qryHelper.FieldByName('Code').AsString);
        AMask.Free; }
    end;
    if APriceEncoded then
      JSONResponse.AddPair('Price', (StrToIntDef(Copy(AUpcCode, 8, 4), 0) / 100).ToString);

    if not APriceEncoded then
    begin
      APriceEncoded := TRegEx.IsMatch(qryHelper.FieldByName('Code').AsString, '^2[0-9]{5}[9]{5}$');
      { AMask := TMask.Create('2?????99999');
        APriceEncoded := AMask.Matches(qryHelper.FieldByName('Code').AsString);
        AMask.Free; }
      if not APriceEncoded then // XBrowse2 Labels
      begin
        APriceEncoded := qryHelper.FieldByName('Scale').AsBoolean and AUpcCodeO.StartsWith('2') and
          (AUpcCodeO.Length >= 11) and (AUpcCodeO.Length <= 12) and
          (qryHelper.FieldByName('Code').AsString.Length <= 5);
      end;
      if APriceEncoded then
        JSONResponse.AddPair('Price', (StrToIntDef(Copy(AUpcCode, 7, 5), 0) / 100).ToString);
    end;

    JSONResponse.AddPair('PriceEncoded', APriceEncoded.ToString(TUseBoolStrs.True));

    if not APriceEncoded then
    begin
      JSONResponse.AddPair('Price', qryHelper.FieldByName('Price').AsFloat.ToString);
      JSONResponse.AddPair('CasePrice', qryHelper.FieldByName('CasePrice').AsFloat.ToString);
    end;
    if APriceEncoded then
      AUpcCode := Copy(AUpcCode, 1, 11);

    if (not APriceEncoded) and (Length(qryHelper.FieldByName('Code').AsString) <= 5) and
      (Length(qryHelper.FieldByName('Code').AsString) >= 3) then
    begin
      qrySQLOpen.Close;
      qrySQLOpen.SQL.Text := 'Select WghCodPre From FsData;';
      qrySQLOpen.Open;
      AWghCdPre := qrySQLOpen.Fields[0].AsString;
      qrySQLOpen.Close;

      if (not AWghCdPre.IsEmpty) and (Copy(AUpcCode, 1, 1) = AWghCdPre) then
        ABarCodeQty := StrToIntDef(Copy(AUpcCode, 7, 5), 0) / 100;
    end;

    JSONResponse.AddPair('OrigCode', AUpcCodeO);
    JSONResponse.AddPair('BarCodeQty', ABarCodeQty.ToString);

    AExtPrice := qryHelper.FieldByName('ExtPrice').AsString;
    if AIsCase then
    begin
      if not AExtPrice.EndsWith(',') then
        AExtPrice := AExtPrice + ',';
      AExtPrice := AExtPrice + qryHelper.FieldByName('ExtCasePrice').AsString;
    end;

    if AExtPrice.EndsWith(',') then
      Delete(AExtPrice, AExtPrice.Length, 1);

    JSONResponse.AddPair('ExtPrice', AExtPrice);
    JSONResponse.AddPair('IsCase', AIsCase.ToString(TUseBoolStrs.True));
    qryHelper.Close;

    if AGetImage then
    begin
      AImageFile := ImagesPath + AUpcCode + '.png';
      AHaveImage := FileExists(AImageFile);
      if not AHaveImage then
      begin
        AImageFile := ImagesPath + AUpcCode + '.gif';
        AHaveImage := FileExists(AImageFile);
      end;
      if AHaveImage then
      begin
        try
          AStream := nil;
          AJson := nil;
          try
            AStream := TFileStream.Create(AImageFile, fmOpenRead + fmShareDenyNone);
            AStream.Position := 0;
            AJson := TDBXJSONTools.StreamToJSON(AStream, 0, AStream.Size);
            JSONResponse.AddPair('Image', AJson);
          finally
            FreeAndNil(AStream);
            FreeAndNil(AJson);
          end;
        except
          on E: Exception do
            JSONResponse.AddPair('ImageError', E.Message);
        end;
      end;
    end;
    if not ADeviceID.IsEmpty then
      try
        qryHelper.Close;
        if SameText(AScanMode, 'Security') then
          qryHelper.SQL.Text :=
            'Update SelfCheckoutStatus Set SecureCount=IfNull(SecureCount,0)+1 Where DeviceID=:DeviceID And EndTime Is Null;'
        else
          qryHelper.SQL.Text :=
            'Update SelfCheckoutStatus Set ScanCount=ScanCount+1 Where DeviceID=:DeviceID And EndTime Is Null;';
        qryHelper.Prepare;
        qryHelper.ParamByName('DeviceID').AsString := ADeviceID;
        qryHelper.ExecSQL;
      except
        on E: Exception do
          JSONResponse.AddPair('Report SelfCheckoutStatus Error', E.Message);
      end;
  except
    on E: Exception do
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', Format('DoProductInfo raised %s', [E.Message]));
    end;
  end;
end;

function TServerMethods1.EchoString(Value: string): string;
begin
  Result := Value;
end;

function TServerMethods1.ExecuteSQL(ASqlText, ADataAlias: string): Integer;
begin
  if CloseCommandActive then
  begin
    Result := 0; // 'Database Not Available (Close Command)';
    Exit;
  end;

  if ADataAlias <> '' then
    FDataAlias := ADataAlias;

  FixUpSqlText(ASqlText);
  qrySQLExecute.ExecSQL(ASqlText);
  Result := qrySQLExecute.RowsAffected;
end;

procedure TServerMethods1.DoRmLabel(BarCode: string; ASheetName: string; Qty: Integer);
var
  I: Integer;
begin
  if not qryRmLabel.Prepared then
  begin
    qryRmLabel.ParamByName('BarCode').AsString := 'PREPARECODE';
    qryRmLabel.ParamByName('LabelType').AsString := 'P';
    qryRmLabel.ParamByName('SheetName').AsString := 'PREPARECODE';
    qryRmLabel.Prepare;
  end;
  qryRmLabel.ParamByName('BarCode').AsString := BarCode;
  qryRmLabel.ParamByName('LabelType').AsString := IIF(ASheetName = '', 'P', 'S');
  qryRmLabel.ParamByName('SheetName').AsString := ASheetName;
  try
    for I := 1 to Qty do
      qryRmLabel.ExecSQL;
    FLine1 := 'Storing Label for';
    FLine2 := BarCode;
  except
    on E: Exception do
    begin
      FLine1 := 'Error Storing Label';
      FLine2 := BarCode;
      FLine3 := E.Message;
    end;
  end;

  if RmLabelMode = 1 then
    RmLabelMode := 0;
end;

procedure TServerMethods1.DoRmLabelJson(JSONRequest, JSONResponse: TJSONObject);
var
  ABarCode, ASheetName: string;
  AQty: Integer;
begin
  JSONRequest.TryGetValue<string>('BarCode', ABarCode);
  JSONRequest.TryGetValue<string>('SheetName', ASheetName);
  JSONRequest.TryGetValue<Integer>('Qty', AQty);
  if AQty = 0 then
    AQty := 1;
  if (ABarCode <> '') then
  begin
    try
      DoRmLabel(ABarCode, ASheetName, AQty);
      JSONResponse.AddPair('Status', 'Success');
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('DoRmLabelCmd %s', [E.Message]));
      end;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'DoRmLabelCmd required parameter missing');
  end;
end;

function TServerMethods1.DatabasePass: string;
var
  I: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for I := 1 to 3 do
    Result := Result + IntToStr(I);
  Result := Result + '$5';
  Result := Result + Copy(Result, 9, 1) + Copy(DatabaseUser, 4, 3);
  // lezaM123$5$Srv
end;

function TServerMethods1.DatabaseUser: string;
begin
  Result := 'TisSrvDS';
end;

procedure TServerMethods1.DataModuleCreate(Sender: TObject);
var
  ASession: TDSSession;
begin
  ASession := TDSSessionManager.GetThreadSession;
  FSessionID := ASession.SessionName;
  // DoVerboseLogging('DataModuleCreate');
end;

procedure TServerMethods1.DataModuleDestroy(Sender: TObject);
var
  AGenHelpers: TGenHelpers;
  ADataHelpers: TDataHelpers;
  AdmData: TdmData;
  AdmDataPrd: TdmDataPrd;
  AGuiNonGuiHelpers: TGuiNonGuiHelpers;
  APrdHelpersI: TPrdHelpersI;
  AProjectHelpersI: TProjectHelpersI;
  AVendHelpersI: TVendHelpersI;
  AVendInvHelpers: TVendInvHelpers;
  AStartSec: Word;
begin
  // WriteToLog('Destory ' + SessionID, '', False, True);
  if Assigned(__VendInvHelpers) then
  begin
    if __VendInvHelpers.TryGetValue(SessionID, AVendInvHelpers) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - AVendInvHelpers', '', False, True);
      __VendInvHelpers.Remove(SessionID);
      FreeAndNil(AVendInvHelpers);
    end;
  end;
  if Assigned(__VendHelpersI) then
  begin
    if __VendHelpersI.TryGetValue(SessionID, AVendHelpersI) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - AVendHelpersI', '', False, True);
      __VendHelpersI.Remove(SessionID);
      FreeAndNil(AVendHelpersI);
    end;
  end;
  if Assigned(__ProjectHelpersI) then
  begin
    if __ProjectHelpersI.TryGetValue(SessionID, AProjectHelpersI) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - AProjectHelpersI', '', False, True);
      __ProjectHelpersI.Remove(SessionID);
      FreeAndNil(AProjectHelpersI);
    end;
  end;
  if Assigned(__PrdHelpersI) then
  begin
    if __PrdHelpersI.TryGetValue(SessionID, APrdHelpersI) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - APrdHelpersI', '', False, True);
      __PrdHelpersI.Remove(SessionID);
      FreeAndNil(APrdHelpersI);
    end;
  end;
  if Assigned(__GuiNonGuiHelpers) then
  begin
    if __GuiNonGuiHelpers.TryGetValue(SessionID, AGuiNonGuiHelpers) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - AGuiNonGuiHelpers', '', False, True);
      __GuiNonGuiHelpers.Remove(SessionID);
      FreeAndNil(AGuiNonGuiHelpers);
    end;
  end;
  if Assigned(__dmDataPrd) then
  begin
    if __dmDataPrd.TryGetValue(SessionID, AdmDataPrd) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - ' + AdmDataPrd.Name, '', False, True);
      __dmDataPrd.Remove(SessionID);
      FreeAndNil(AdmDataPrd);
    end;
  end;
  if Assigned(__DataHelpers) then
  begin
    if __DataHelpers.TryGetValue(SessionID, ADataHelpers) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - ADataHelpers', '', False, True);
      __DataHelpers.Remove(SessionID);
      FreeAndNil(ADataHelpers);
    end;
  end;
  if Assigned(__GenHelpers) then
  begin
    if __GenHelpers.TryGetValue(SessionID, AGenHelpers) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - AGenHelpers', '', False, True);
      __GenHelpers.Remove(SessionID);
      FreeAndNil(AGenHelpers);
    end;
  end;
  {
    AStartSec := SecondOf(Now) + 5;
    repeat
    Application.ProcessMessages;
    until SecondOf(Now)>AStartSec;
  }
  if Assigned(__dmData) then
  begin
    if __dmData.TryGetValue(SessionID, AdmData) then
    begin
      // WriteToLog('Destory ' + SessionID + ' - TdmData', '', False, True);
      __dmData.Remove(SessionID);
      FreeAndNil(AdmData);
    end;
  end;
  // DoVerboseLogging('DataModuleDestroy');
end;

function TServerMethods1.DBPrdCode(const ASearchCode: string): string;
var
  APrcEncd2: Boolean;
  AWghCodPre: string;
  AScanOpts: TDataSet;
begin
  if not Assigned(AScanResolve) then
  begin
    qrySQLOpen.Close;
    qrySQLOpen.SQL.Text := 'Select PrcEncd2, WghCodPre From FsData;';
    qrySQLOpen.Open;
    APrcEncd2 := qrySQLOpen.Fields[0].AsBoolean;
    AWghCodPre := qrySQLOpen.Fields[1].AsString;

    qrySQLOpen.Close;
    qrySQLOpen.SQL.Text := 'Select * From BcHandle Order By Indx;';
    qrySQLOpen.Open;
    AScanResolve := TScanResolve.Create(qrySQLOpen, APrcEncd2, AWghCodPre);
  end;

  // CDRepSnapService.LogMessage('DBPrdCode SearchCode: '+ASearchCode);
  Result := DBPrdCodeSearch(ASearchCode);

  if Result = '' then
  begin
    AScanOpts := AScanResolve.Execute(ASearchCode);
    AScanOpts.First;
    // CDRepSnapService.LogMessage('DBPrdCode ScanOpts RecCount: '+IntToStr(AScanOpts.RecordCount));
    while not AScanOpts.Eof do
    begin
      // CDRepSnapContainer.LogMessage('DBPrdCode SearchCode: '+AScanOpts.FieldByName('Scan').AsString+', Indx: '+AScanOpts.FieldByName('Indx').AsString);
      Result := DBPrdCodeSearch(AScanOpts.FieldByName('Scan').AsString);
      if Result <> '' then
        Break;

      AScanOpts.Next;
    end;
  end;
end;

function TServerMethods1.DBPrdCodeSearch(AValue: string): string;
const
  ATestSql = 'Select Code From Prdmstr Where Code=:Code';
begin
  if not SameText(qrySQLOpen.SQL.Text, ATestSql) then
  begin
    qrySQLOpen.Close;
    qrySQLOpen.SQL.Text := ATestSql;
    qrySQLOpen.ParamByName('Code').AsString := 'TEXT';
    qrySQLOpen.Prepare;
  end
  else
    qrySQLOpen.Close;

  qrySQLOpen.ParamByName('Code').AsString := AValue;
  qrySQLOpen.Open;
  if not qrySQLOpen.IsEmpty then
    Result := qrySQLOpen.Fields[0].AsString
  else
    Result := '';
end;

procedure TServerMethods1.DeliveryStatus(JSONRequest, JSONResponse: TJSONObject);
var
  AOrderNum, AEvent, ADateTime, ADriverName, AOrderList, AEta, ADelivered, ABody: string;
  AMissingParam: Boolean;
  AResultA: TJSONArray;
  AEnumA: TJSONArrayEnumerator;
  AValues: Integer;
begin
  JSONRequest.TryGetValue<string>('Body', ABody);
  if not ABody.IsEmpty then
    JSONRequest := TJSONObject.ParseJSONValue(ABody) as TJSONObject;

  JSONRequest.TryGetValue<string>('OrdNum', AOrderNum);
  JSONRequest.TryGetValue<string>('Event', AEvent);
  JSONRequest.TryGetValue<string>('DateTime', ADateTime);
  JSONRequest.TryGetValue<string>('DriverName', ADriverName);
  JSONRequest.TryGetValue<string>('OrderList', AOrderList);
  JSONRequest.TryGetValue<TJSONArray>('result', AResultA);
  if AOrderNum.IsEmpty then
    JSONRequest.TryGetValue<string>('order_id', AOrderNum);
  if ADriverName.IsEmpty then
    JSONRequest.TryGetValue<string>('driver_name', ADriverName);
  JSONRequest.TryGetValue<string>('eta', AEta);
  JSONRequest.TryGetValue<string>('actually_delivered', ADelivered);
  if ADateTime = '' then
    ADateTime := DateTimeToStr(Now);
  AMissingParam := True;

  qryHelper.SQL.Clear;
  try
    if Assigned(AResultA) and (AResultA.Count > 0) then
    begin
      AMissingParam := False;
      AEnumA := AResultA.GetEnumerator;
      while AEnumA.MoveNext do
      begin
        AEnumA.Current.TryGetValue<string>('order_id', AOrderNum);
        AEnumA.Current.TryGetValue<string>('driver_name', ADriverName);
        AEnumA.Current.TryGetValue<string>('eta', AEta);
        AEnumA.Current.TryGetValue<string>('actually_delivered', ADelivered);
        if (not AOrderNum.IsEmpty) and ((not ADriverName.IsEmpty) or (not AEta.IsEmpty) or (not ADelivered.IsEmpty))
        then
        begin
          AEvent := 'Array Values';
          AValues := 0;
          qryHelper.SQL.Add('Update DlvCntrl Set');
          if not ADriverName.IsEmpty then
          begin
            qryHelper.SQL.Add(Format('DlvPrsn=%s', [QuotedStr(ADriverName)]));
            Inc(AValues);
          end;
          if not AEta.IsEmpty then
          begin
            qryHelper.SQL.Add(Format('%sETA=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_TIME)',
              [IfThen(AValues > 0, ', ', ''), QuotedStr(AEta)]));
            Inc(AValues);
          end;
          if not ADelivered.IsEmpty then
          begin
            qryHelper.SQL.Add(Format('%sDtTmDeliverDone=Cast(%s AS SQL_TIMESTAMP)', [IfThen(AValues > 0, ', ', ''),
              QuotedStr(ADelivered)]));
            // qryHelper.SQL.Add(Format('%sLine12=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_CHAR)', [IfThen(AValues > 0, ', ', ''), QuotedStr(ADelivered)]));
            Inc(AValues);
          end;
          qryHelper.SQL.Add(Format('Where Code=%s;', [AOrderNum]));
          qryHelper.SQL.Add
            (Format('Update DlvCntrl Set DateD=Date(), TimeD=SubString(Time(),1,5) Where Code=%s And DateD Is Null;',
            [AOrderNum]));
        end;
      end;
    end
    else if (not AOrderNum.IsEmpty) and ((not ADriverName.IsEmpty) or (not AEta.IsEmpty) or (not ADelivered.IsEmpty))
    then
    begin
      AMissingParam := False;
      AEvent := 'Order Values';
      AValues := 0;
      qryHelper.SQL.Add('Update DlvCntrl Set');
      if not ADriverName.IsEmpty then
      begin
        qryHelper.SQL.Add(Format('DlvPrsn=%s', [QuotedStr(ADriverName)]));
        Inc(AValues);
      end;
      if not AEta.IsEmpty then
      begin
        qryHelper.SQL.Add(Format('%sETA=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_TIME)', [IfThen(AValues > 0, ', ', ''),
          QuotedStr(AEta)]));
        Inc(AValues);
      end;
      if not ADelivered.IsEmpty then
      begin
        qryHelper.SQL.Add
          (Format('%sDateD=Cast(Cast(%s as SQL_TIMESTAMP) as SQL_DATE), TimeD=Substring(Cast(Cast(Cast(%s as SQL_TIMESTAMP) as SQL_TIME) as SQL_CHAR),1,5)',
          [IfThen(AValues > 0, ', ', ''), QuotedStr(ADelivered), QuotedStr(ADelivered)]));
        // qryHelper.SQL.Add(Format('%sDtTmDeliverDone=Cast(%s AS SQL_TIMESTAMP)', [IfThen(AValues > 0, ', ', ''), QuotedStr(ADelivered)]));
        // qryHelper.SQL.Add(Format('%sLine12=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_CHAR)', [IfThen(AValues > 0, ', ', ''), QuotedStr(ADelivered)]));
        Inc(AValues);
      end;
      qryHelper.SQL.Add(Format('Where Code=%s;', [AOrderNum]));
      // qryHelper.SQL.Add(Format('Update DlvCntrl Set DateD=Date(), TimeD=SubString(Time(),1,5) Where Code=%s And DateD Is Null;', [AOrderNum]));
    end
    else if SameText(AEvent, 'OutForDelivery') then
    begin
      if (ADriverName <> '') and (AOrderList <> '') then
      begin
        AMissingParam := False;
        qryHelper.SQL.Add(Format('EXECUTE PROCEDURE cdAddToDlvBatchD(%s, %s, 0, %s);',
          [QuotedStr(ADriverName), QuotedStr(AOrderList), QuotedStr(ADateTime)]));
      end;
    end
    else if SameText(AEvent, 'DriverComplete') then
    begin
      if (ADriverName <> '') then
      begin
        AMissingParam := False;
        qryHelper.SQL.Add(Format('Update DlvBatchHD Set DateD=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_DATE),',
          [QuotedStr(ADateTime)]));
        qryHelper.SQL.Add(Format('TimeD=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_TIME)', [QuotedStr(ADateTime)]));
        qryHelper.SQL.Add(Format('Where Upper(DlvPrsn)=%s And (DateD Is Null and TimeD Is Null);',
          [QuotedStr(ADriverName)]));

        qryHelper.SQL.Add('Update DlvCntrl Set');
        qryHelper.SQL.Add(Format('Line12=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_CHAR)', [QuotedStr(ADateTime)]));
        qryHelper.SQL.Add(Format('Where Upper(DlvPrsn)=%s And (Line12 Is Null or Line12='''');',
          [QuotedStr(ADriverName)]));
      end;
    end
    else if (AOrderNum <> '') and (AEvent <> '') and (ADateTime <> '') then
    begin
      AMissingParam := False;
      qryHelper.SQL.Add('Update DlvCntrl Set');
      if SameText(AEvent, 'ETA') then
        qryHelper.SQL.Add(Format('ETA=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_TIME)', [QuotedStr(ADateTime)]))
      else if SameText(AEvent, 'Delivered') then
        qryHelper.SQL.Add(Format('DtTmDeliverDone=Cast(%s AS SQL_TIMESTAMP)', [QuotedStr(ADateTime)]))
      else if SameText(AEvent, 'Complete') then
        qryHelper.SQL.Add(Format('Line12=Cast(Cast(%s AS SQL_TIMESTAMP) AS SQL_CHAR)', [QuotedStr(ADateTime)]))
      else
        raise Exception.CreateFmt('Event %s not supported', [AEvent]);

      qryHelper.SQL.Add(Format('Where Code=%s', [AOrderNum]));
    end;
    if (not AMissingParam) and (qryHelper.SQL.Count > 0) then
    begin
      qryHelper.ExecSQL;
      if qryHelper.RowsAffected > 0 then
      begin
        if (AOrderNum = '') and (AOrderList <> '') then
          AOrderNum := AOrderList;
        JSONResponse.AddPair('Status', 'Success');
        JSONResponse.AddPair('Result', Format('DeliveryStatus %s Recorded for Order #%s at %s',
          [AEvent, AOrderNum, ADateTime]))
      end
      else if AOrderNum <> '' then
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('DeliveryStatus Order #%s not found', [AOrderNum]))
      end
      else
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', 'DeliveryStatus No Orders Updated')
      end;
    end;
  except
    on E: Exception do
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', Format('DeliveryStatus %s', [E.Message]));
    end;
  end;
  if AMissingParam then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'DeliveryStatus required parameter missing');
  end;
end;

procedure TServerMethods1.DisplayCodePrice(nPrice: Currency);
begin
  FCodeFound := tbPrdmstr.FieldByName('Code').AsString;
  FLine1 := Copy(tbPrdmstr.FieldByName('Desc').AsString, 1, 20);
  FCodePricedItem := True;

  if nPrice > 0 then
    FLine2 := 'Price' + PadL(FormatCurr('$##0.00', nPrice), 11);
end;

procedure TServerMethods1.DisplayProduct;
var
  sDisplay: string;
  RegPrice, PkgPrice, CasePrice, SalePrice, SPkgPrice, SCasePrice: Currency;
  UPP, SUPP: Integer;
  SDate, EDate: TDate;
  STime, ETime: string;
begin
  FCodeFound := tbPrdmstr.FieldByName('Code').AsString;
  RegPrice := tbPrdmstr.FieldByName('Price').AsCurrency;
  PkgPrice := tbPrdmstr.FieldByName('PkgPrc').AsCurrency;
  CasePrice := tbPrdmstr.FieldByName('CasePrice').AsCurrency;
  SalePrice := tbPrdmstr.FieldByName('Sale').AsCurrency;
  SPkgPrice := tbPrdmstr.FieldByName('SPkgPrc').AsCurrency;
  SCasePrice := tbPrdmstr.FieldByName('CaseSale').AsCurrency;
  UPP := tbPrdmstr.FieldByName('UPP').AsInteger;
  SUPP := tbPrdmstr.FieldByName('SUPP').AsInteger;

  if (SalePrice + SPkgPrice + SCasePrice) > 0 then
  begin
    if tbPrdmstr.FindField('SaleMode') <> nil then
    begin
      if (SalePrice > 0) and (tbPrdmstr.FieldByName('SaleMode').AsString = '%') then
        SalePrice := RegPrice * (1 - (SalePrice / 100));
      if (SCasePrice > 0) and (tbPrdmstr.FieldByName('CsSaleMode').AsString = '%') then
        SCasePrice := CasePrice * (1 - (SCasePrice / 100));
      if (SPkgPrice > 0) and (tbPrdmstr.FieldByName('PkSaleMode').AsString = '%') then
        SPkgPrice := PkgPrice * (1 - (SPkgPrice / 100));
    end;
    try
      SDate := tbPrdmstr.FieldByName('Begins').AsDateTime;
      EDate := tbPrdmstr.FieldByName('Expires').AsDateTime;
      STime := tbPrdmstr.FieldByName('TBegins').AsString;
      ETime := tbPrdmstr.FieldByName('TExpires').AsString;
      if STime = '' then
        STime := '0000';
      if ETime = '' then
        ETime := '2359';
      STime := Copy(STime, 1, 2) + ':' + Copy(STime, 3, 2);
      ETime := Copy(ETime, 1, 2) + ':' + Copy(ETime, 3, 2);
      if ((SDate > 0) and ((SDate > Date) or ((SDate = Date) and (((STime <> '') and (Time < StrToTime(STime))))))) or
        ((EDate > 0) and ((Date > EDate) or ((Date = EDate) and (((ETime <> '') and (Time > StrToTime(ETime))))))) then
      begin
        SalePrice := 0;
      end;
    except
      SalePrice := 0;
    end;

    try
      SDate := tbPrdmstr.FieldByName('PBegins').AsDateTime;
      EDate := tbPrdmstr.FieldByName('PExpires').AsDateTime;
      STime := tbPrdmstr.FieldByName('PTBegins').AsString;
      ETime := tbPrdmstr.FieldByName('PTExpires').AsString;
      if STime = '' then
        STime := '0000';
      if ETime = '' then
        ETime := '2359';
      STime := Copy(STime, 1, 2) + ':' + Copy(STime, 3, 2);
      ETime := Copy(ETime, 1, 2) + ':' + Copy(ETime, 3, 2);
      if ((SDate > 0) and ((SDate > Date) or ((SDate = Date) and (((STime <> '') and (Time < StrToTime(STime))))))) or
        ((EDate > 0) and ((Date > EDate) or ((Date = EDate) and (((ETime <> '') and (Time > StrToTime(ETime))))))) then
      begin
        SPkgPrice := 0;
      end;
    except
      SPkgPrice := 0;
    end;

    try
      SDate := tbPrdmstr.FieldByName('CBegins').AsDateTime;
      EDate := tbPrdmstr.FieldByName('CExpires').AsDateTime;
      STime := tbPrdmstr.FieldByName('CTBegins').AsString;
      ETime := tbPrdmstr.FieldByName('CTExpires').AsString;
      if STime = '' then
        STime := '0000';
      if ETime = '' then
        ETime := '2359';
      STime := Copy(STime, 1, 2) + ':' + Copy(STime, 3, 2);
      ETime := Copy(ETime, 1, 2) + ':' + Copy(ETime, 3, 2);
      if ((SDate > 0) and ((SDate > Date) or ((SDate = Date) and (((STime <> '') and (Time < StrToTime(STime))))))) or
        ((EDate > 0) and ((Date > EDate) or ((Date = EDate) and (((ETime <> '') and (Time > StrToTime(ETime))))))) then
      begin
        SCasePrice := 0;
      end;
    except
      SCasePrice := 0;
    end;
  end;

  AdjustCstmPrdPrice(RegPrice, PkgPrice, CasePrice, SalePrice, SPkgPrice, SCasePrice, UPP, SUPP);

  FLine1 := Copy(tbPrdmstr.FieldByName('Desc').AsString, 1, 20);
  if RegPrice > 0 then
  begin
    if SalePrice > 0 then
      FLine2 := 'Reg ' + PadL(FormatCurr('#0.00', RegPrice), 5) + '/Sale ' + PadL(FormatCurr('#0.00', SalePrice), 5)
    else
      FLine2 := 'Price' + PadL(FormatCurr('$##0.00', RegPrice), 11);
  end;

  sDisplay := '';
  if (PkgPrice > 0) and (UPP > 1) then
    sDisplay := PadR(IntToStr(UPP), 2) + ' for ' + PadL(FormatCurr('$##0.00', PkgPrice), 13);
  if (SPkgPrice > 0) and (SUPP > 1) then
    sDisplay := sDisplay + 'Sale ' + PadR(IntToStr(SUPP), 2) + ' for ' + PadL(FormatCurr('$##0.00', SPkgPrice), 8);
  if sDisplay <> '' then
  begin
    if FLine2 = '' then
      FLine2 := sDisplay
    else
      FLine3 := sDisplay;
  end;

  sDisplay := '';
  if CasePrice > 0 then
  begin
    if SCasePrice > 0 then
      sDisplay := 'Case' + PadL(FormatCurr('#0.00', CasePrice), 5) + '/Sale ' + PadL(FormatCurr('#0.00', SCasePrice), 5)
    else
      sDisplay := 'Case Price' + PadL(FormatCurr('$##0.00', CasePrice), 10);
  end
  else if SCasePrice > 0 then
    sDisplay := 'Case Sale' + PadL(FormatCurr('#0.00', SCasePrice), 11);
  if sDisplay <> '' then
  begin
    if FLine2 = '' then
      FLine2 := sDisplay
    else if FLine3 = '' then
      FLine3 := sDisplay
    else
      FLine4 := sDisplay;
  end;

  if (RegPrice + PkgPrice + CasePrice + SalePrice + SPkgPrice + SCasePrice) = 0 then
    FLine1 := 'Unpriced Item.';
end;

function TServerMethods1.DlvCntrlAddBatch(sDriverName, sBatchData: string): TJSONValue;
var
  JSONObject: TJSONObject;
  ADlvID: string;
  ASource, AString: TStringList;
  I, APos: Integer;
  ARowS: string;
  ASecureSQL: string;
begin
  DoVerboseLogging(Format('DlvCntrlAddBatch: Values(%s, %s)', [sDriverName, sBatchData]));
  JSONObject := TJSONObject.Create;
  try
    if CloseCommandActive then
      raise Exception.Create('Database Not Available (Close Command)');

    ASecureSQL := sDriverName + sBatchData;
    if not SecureSqlText(ASecureSQL) then
      raise Exception.Create('Security Alert ' + ASecureSQL);
    ASource := TStringList.Create;
    AString := TStringList.Create;
    try
      ASource.Delimiter := 'R';
      ASource.DelimitedText := Copy(sBatchData, 2, MaxInt);
      for I := 0 to ASource.Count - 1 do
      begin
        ARowS := ASource[I];
        APos := Pos('Q', ARowS);
        if APos > 0 then
        begin
          ADlvID := Copy(ARowS, 1, APos - 1);
        end
        else
          ADlvID := ARowS;
        if ADlvID <> '' then
          AString.Add(ADlvID);
      end;
      if AString.Text <> '' then
      begin
        qrySQLExecute.ExecSQL(Format('EXECUTE PROCEDURE cdAddToDlvBatch(%s, %s, 0);',
          [QuotedStr(sDriverName), QuotedStr(AString.CommaText)]));
        JSONObject.AddPair('Result', 'Success');
        JSONObject.AddPair('Records Updated', IntToStr(qrySQLExecute.RowsAffected));
      end;
    finally
      AString.Free;
      ASource.Free;
    end;
  except
    on E: Exception do
    begin
      JSONObject.AddPair('Result', 'Error');
      JSONObject.AddPair('ErrorDescription', E.Message);
      DoVerboseLogging(Format('DlvCntrlAddBatch: Error %s', [E.Message]));
    end;
  end;
  Result := JSONObject;
end;

function TServerMethods1.DoGarmentReadyTel(AKeyValue: string): string;
const
  SearchTel =
    'Select IfNull(Alteration, False) Alteration, Count(*) CntAll  From InvItem Where Upper(Accss)=:TelNum And IfNull(Pickup, False) Group By 1;';
var
  ASqlStr: string;
  metaData: TDSInvocationMetadata;
  I: Integer;
  ASecureSQL: string;
begin
  if CloseCommandActive then
  begin
    Result := 'Database Not Available (Close Command)';
    Exit;
  end;

  ASecureSQL := AKeyValue;
  if not SecureSqlText(ASecureSQL) then
  begin
    Result := 'Security Alert ' + ASecureSQL;
    Exit;
  end;

  ASqlStr := SearchTel;
  ASqlStr := StringReplace(ASqlStr, ':TelNum', QuotedStr(AKeyValue), [rfReplaceAll]);
  FDQueryGG.SQL.Text := ASqlStr;
  FDQueryGG.Open();
  FDQueryGG.First;
  if FDQueryGG.IsEmpty then
    Result := 'TelEmty'
  else if FDQueryGG.RecordCount = 2 then
    Result := 'TelSmRdy'
  else if FDQueryGG.FieldByName('Alteration').AsBoolean then
    Result := 'TelNoRdy'
  else
    // if not ClientDataSet1.FieldByName('Alteration').AsBoolean then
    Result := 'TelAlRdy';

  // this code is for returning xml not JSON
  metaData := GetInvocationMetadata;
  for I := 0 to Pred(metaData.QueryParams.Count) do
  begin
    Result := Result + '<param>' + metaData.QueryParams[I] + '</param>';
  end;
  if Result = '' then
    Result := 'EmptyResultString';
  metaData.ResponseContent := '<xml><param><telstatus>' + Result + '</telstatus></param></xml>';
end;

procedure TServerMethods1.DoIsCustomerInDB(JSONRequest, JSONResponse: TJSONObject);
var
  ATel, AFilter: string;
begin
  JSONRequest.TryGetValue<string>('Tel', ATel);
  JSONRequest.TryGetValue<string>('Filter', AFilter);
  if ATel <> '' then
  begin
    JSONResponse.AddPair('Status', 'Success');
    if QueryCustomerInDB(ATel, AFilter) then
      JSONResponse.AddPair('Result', 'Found')
    else
      JSONResponse.AddPair('Result', 'NotFound');
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'No Tel Value Supplied');
  end;
end;

procedure TServerMethods1.DoLineBusterClear(JSONRequest, JSONResponse: TJSONObject);
var
  ABatchID: string;
begin
  JSONRequest.TryGetValue<string>('BatchID', ABatchID);
  if ABatchID.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'LineBusterClear required parameter missing');
    Exit;
  end;

  try
    qrySQLExecute.ExecSQL(Format('Delete From DeliItms Where ID=%s', [QuotedStr(ABatchID)]));
    JSONResponse.AddPair('Status', 'Success');
  except
    on E: Exception do
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', E.Message);
    end;
  end;
end;

procedure TServerMethods1.DoLocationInventoryTransfer(JSONRequest, JSONResponse: TJSONObject);
var
  AFrom, ATo, ABatchID, ABatchName, AUpcCode: string;
  ARowS: TJSONArray;
  AQuan: Double;
  ACase: Boolean;

  AFromAddress, AToAddress: string;
  ARemote: Boolean;
  AJson, AJsonFrom, AJsonTo: TJSONObject;
  AJsonA: TJSONArray;
  AEnumA: TJSONArrayEnumerator;
  AQuerySQL: TStringList;
  PreviousTransaction: Boolean;
begin
  JSONRequest.TryGetValue<string>('From', AFrom);
  JSONRequest.TryGetValue<string>('To', ATo);
  JSONRequest.TryGetValue<string>('BatchID', ABatchID);
  JSONRequest.TryGetValue<string>('BatchName', ABatchName);
  JSONRequest.TryGetValue<TJSONArray>('Rows', ARowS);

  if (AFrom.IsEmpty and ATo.IsEmpty) or (ARowS.Count = 0) then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'LocationInventoryTransfer required parameter missing');
    Exit;
  end;

  qryHelper.Close;
  qryHelper.SQL.Text := 'Select * From InvLocList Where Location IN (' + QuotedStr(AFrom) + ',' + QuotedStr(ATo) + ')';
  qryHelper.Open;

  ARemote := False;
  if qryHelper.Locate('Location', AFrom, [loCaseInsensitive]) then
  begin
    if qryHelper.FieldByName('Remote').AsBoolean then
    begin
      ARemote := True;
      AFromAddress := qryHelper.FieldByName('RemoteAddress').AsString;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'LocationInventoryTransfer "From" Address not in InvLocList Table');
    Exit;
  end;

  if qryHelper.Locate('Location', ATo, [loCaseInsensitive]) then
  begin
    if qryHelper.FieldByName('Remote').AsBoolean then
    begin
      ARemote := True;
      AToAddress := qryHelper.FieldByName('RemoteAddress').AsString;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'LocationInventoryTransfer "To" Address not in InvLocList Table');
    Exit;
  end;
  qryHelper.Close;

  AJsonA := TJSONArray.Create;
  AQuerySQL := TStringList.Create;
  try
    AEnumA := ARowS.GetEnumerator;
    qryHelper.SQL.Text := 'Select Code, [Desc], UPC From Prdmstr Where Code=:Code';
    qryHelper.Prepare;
    while AEnumA.MoveNext do
    begin
      AEnumA.Current.TryGetValue<string>('UpcCode', AUpcCode);
      AEnumA.Current.TryGetValue<Double>('Quan', AQuan);
      AEnumA.Current.TryGetValue<Boolean>('Case', ACase);

      if (not AUpcCode.IsEmpty) and (AQuan > 0) then
      begin
        qryHelper.Close;
        qryHelper.ParamByName('Code').AsString := AUpcCode;
        qryHelper.Open;
        if not qryHelper.Eof then
        begin
          if not ARemote then
            AQuerySQL.Add(Format('EXECUTE PROCEDURE cdTransferOnHandLoc(%s, %s, %s, %d);',
              [QuotedStr(qryHelper.FieldByName('Code').AsString), QuotedStr(AFrom), QuotedStr(ATo),
              IIF(ACase, AQuan * qryHelper.FieldByName('UPC').AsInteger, AQuan)]))
          else
          begin
            AJson := TJSONObject.Create;
            AJson.AddPair('UpcCode', qryHelper.FieldByName('Code').AsString);
            AJson.AddPair('Descrip', qryHelper.FieldByName('Desc').AsString);
            if ACase then
              AJson.AddPair('UPCC', IntToStr(qryHelper.FieldByName('UPC').AsInteger))
            else
              AJson.AddPair('UPCC', '1');
            AJson.AddPair('AmntDel', FloatToStr(AQuan));
            AJsonA.AddElement(AJson);
          end;
        end;
      end;
    end;

    if ARemote and (AJsonA.Count > 0) then
    begin
      AJsonFrom := TJSONObject.Create;
      AJsonFrom.AddPair('VendorName', ATo);
      AJsonFrom.AddPair('Date', DateToStr(Date));
      AJsonFrom.AddPair('Type', '1');
      AJsonFrom.AddPair('AutoEmailToVendor', 'True');
      AJsonFrom.AddPair('Items', AJsonA);

      AJsonTo := TJSONObject.Create;
      AJsonTo.AddPair('VendorName', AFrom);
      AJsonTo.AddPair('Date', DateToStr(Date));
      AJsonTo.AddPair('Type', '0');
      AJsonTo.AddPair('AutoEmailToVendor', 'True');
      AJsonTo.AddPair('Items', AJsonA);

      // do diff, call NewVendorInvoice in service or thru Notify
      if AFromAddress <> '' then
      begin
        AQuerySQL.Add('Insert Into Notify (Action, Data1, Data2, Data3)');
        AQuerySQL.Add(Format('Values(''RestRequestJSON'', ''%s'', ''NewVendorInvoice'', %s);',
          [AFromAddress, QuotedStr(AJsonFrom.ToString)]));
      end
      else
      begin
        AJson := TJSONObject.Create;
        DoNewVendorInvoice(AJsonFrom, AJson);
      end;

      if AToAddress <> '' then
      begin
        AQuerySQL.Add('Insert Into Notify (Action, Data1, Data2, Data3)');
        AQuerySQL.Add(Format('Values(''RestRequestJSON'', ''%s'', ''NewVendorInvoice'', %s);',
          [AToAddress, QuotedStr(AJsonTo.ToString)]));
      end
      else
      begin
        AJson := TJSONObject.Create;
        DoNewVendorInvoice(AJsonTo, AJson);
      end;
    end;

    if AQuerySQL.Count > 0 then
    begin
      qryHelper.SQL.Assign(AQuerySQL);
      PreviousTransaction := qryHelper.AdsConnection.TransactionActive;
      if not PreviousTransaction then
        qryHelper.AdsConnection.BeginTransaction;
      try
        qryHelper.ExecSQL;
      except
        on E: Exception do
        begin
          if not PreviousTransaction then
            qryHelper.AdsConnection.Rollback;
          PreviousTransaction := True; // prevent calling Commit
          JSONResponse.AddPair('Status', 'Error');
          JSONResponse.AddPair('Error', Format('LocationInventoryTransfer %s', [E.Message]));
        end;
      end;
      if not PreviousTransaction then
        qryHelper.AdsConnection.Commit;
    end;
    JSONResponse.AddPair('Status', 'Success');
  finally
    AJsonA.Free;
    AQuerySQL.Free;
  end;
end;

procedure TServerMethods1.DoNewVendorInvoice(JSONRequest, JSONResponse: TJSONObject);
var
  AVendID, AVendInvoiceNum, AVendorName: string; // , ADocFile, ADocDescription: string;
  AItems: TJSONArray;
begin
  JSONRequest.TryGetValue<string>('VendID', AVendID);
  JSONRequest.TryGetValue<string>('VendInvoiceNum', AVendInvoiceNum);
  JSONRequest.TryGetValue<string>('VendorName', AVendorName);
  JSONRequest.TryGetValue<TJSONArray>('Items', AItems);

  // JSONRequest.TryGetValue<string>('DocFile', ADocFile);
  // JSONRequest.TryGetValue<string>('DocDescription', ADocDescription);
  if ((not AVendID.IsEmpty) and (not AVendInvoiceNum.IsEmpty) and (AVendInvoiceNum <> '911')) or
    ((not AVendorName.IsEmpty) and (AItems.Count > 0)) then
  begin
    try
      DoNewVendorInvoiceX(JSONRequest, JSONResponse);
      // JSONRequest.AddPair('CmdType', 'NewVendorInvoice');
      // CallTisLib3JsonAction(JSONRequest, JSONResponse);
      // JSONResponse.AddPair('Status', 'Success');
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('NewVendorInvoice %s', [E.Message]));
      end;
    end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'NewVendorInvoice required parameter missing');
  end;
end;

procedure TServerMethods1.DoNewVendorInvoiceX(JSONRequest, JSONResponse: TJSONObject);
var
  AVendID, AVendInvoiceNum, ADocFile, ADocDescription, AGenericTextPrinter, AError, AVendorName: string;
  ANewInvNum: Integer;
  AItems: TJSONArray;
begin
  JSONRequest.TryGetValue<string>('VendID', AVendID);
  JSONRequest.TryGetValue<string>('VendInvoiceNum', AVendInvoiceNum);
  JSONRequest.TryGetValue<string>('DocFile', ADocFile);
  JSONRequest.TryGetValue<string>('DocDescription', ADocDescription);
  JSONRequest.TryGetValue<string>('GenericTextPrinter', AGenericTextPrinter);
  JSONRequest.TryGetValue<string>('VendorName', AVendorName);
  JSONRequest.TryGetValue<TJSONArray>('Items', AItems);
  if (not AVendorName.IsEmpty) and (AItems.Count > 0) then
  begin
    try
      AdsConnection1.Connect;
      dmData(AdsConnection1);
      VendInvHelpers.NewVendorInvoice(JSONRequest, JSONResponse);
    except
      on E: Exception do
        JSONResponse.AddPair('Error', Format('NewVendorInvoice %s', [E.Message]));
    end;
  end
  else if (AVendID <> '') and (AVendInvoiceNum <> '') and (AVendInvoiceNum <> '911') then
  begin
    try
      ANewInvNum := VendInvHelpers.NewVendorInvoiceWithAttach(AVendID, AVendInvoiceNum, ADocFile,
        ADocDescription, AError);
      if ANewInvNum > 0 then
      begin
        JSONResponse.AddPair('Succes', 'New Vendor Invoice Added');
        JSONResponse.AddPair('InvoiceNum', IntToStr(ANewInvNum));
        JSONResponse.AddPair('Status', 'Success');
        if AGenericTextPrinter <> '' then
          try
            if not GenHelpers.LocateReportByName('ScannerVendInvLabel') then
              JSONResponse.AddPair('PrintingError', 'Report ScannerVendInvLabel not found')
            else
            begin
              //removed REPORT stuff
            end;
          except
            on E: Exception do
              JSONResponse.AddPair('PrintingError', E.Message);
          end;
      end
      else
      begin
        if AError <> '' then
          JSONResponse.AddPair('Error', 'NewVendorInvoice Invoice not created ' + AError)
        else
          JSONResponse.AddPair('Error', 'NewVendorInvoice Invoice not created (internal unknown)');
      end;
    except
      on E: Exception do
        JSONResponse.AddPair('Error', Format('NewVendorInvoice %s', [E.Message]));
    end;
  end
  else
    JSONResponse.AddPair('Error', 'NewVendorInvoice required parameter missing');
end;

procedure TServerMethods1.FDConnectionBeforeConnect(Sender: TObject);
begin
  if FDataAlias = '' then
    FDataAlias := 'TisWin3DD';
  // FDataAlias := 'RepData';
  TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).Alias := FDataAlias;
  TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).username := DatabaseUser;
  TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).Password := DatabasePass;

  with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
  begin
    if ReadBool('Settings', 'AdsLocal', False) then
      TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).ServerTypes := stLocal;

    if not SameText((Sender as TFDConnection).Name, FDConnectionCDX.Name) then
    begin
      if (ReadString('Settings', 'Type', 'ADT') = 'CDX') then
        TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).TableType := ttCDX;
    end;
    Free;
  end;
end;

procedure TServerMethods1.FDConnectionGGBeforeConnect(Sender: TObject);
begin
  TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).username := DatabaseUser;
  TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).Password := DatabasePass;

  with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
  begin
    if ReadBool('Settings', 'AdsLocal', False) then
      TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).ServerTypes := stLocal;

    if (ReadString('Settings', 'Type', 'ADT') = 'CDX') then
      TFDPhysADSConnectionDefParams((Sender as TFDConnection).Params).TableType := ttCDX;
    Free;
  end;
end;


function TServerMethods1.FindCodePrice(UpcCode: string; var nPrice: Currency): Boolean;
var
  liLen: Integer;
  sTmpCode: string;
begin
  Result := False;
  if Length(UpcCode) < 4 then
    Exit;

  if Length(UpcCode) = 12 then
    sTmpCode := Copy(UpcCode, 1, 11)
  else
    sTmpCode := UpcCode;
  liLen := Length(sTmpCode);

  if (Copy(UpcCode, 1, 1) = '2') and (liLen = 11) and
    (tbPrdmstr.Locate('Code', Copy(sTmpCode, 1, 6) + '00000', [loCaseInsensitive]) or tbPrdmstr.Locate('Code',
    Copy(sTmpCode, 1, 6) + '99999', [loCaseInsensitive])) then
  begin
    // 2?????00000 or 2?????99999
    Result := True;
    if SameText(Copy(tbPrdmstr.FieldByName('Code').AsString, 7, 5), '99999') then
      nPrice := StrToCurr(Copy(sTmpCode, 7, 5))
    else
      nPrice := StrToCurr(Copy(sTmpCode, 8, 4));
    FFindOpt := 8192;
  end
  else if (liLen > 5) and tbPrdmstr.Locate('Code', Copy(sTmpCode, 1, 5), [loCaseInsensitive]) and
    tbPrdmstr.FieldByName('CodePrice').AsBoolean then
  begin
    // first five digits
    Result := True;
    nPrice := StrToCurr(Copy(sTmpCode, 7, 4));
    FFindOpt := 64;
  end
  else if tbPrdmstr.Locate('Code', Copy(sTmpCode, 1, 3), [loCaseInsensitive]) and tbPrdmstr.FieldByName('CodePrice').AsBoolean
  then
  begin
    // first three digits
    Result := True;
    nPrice := StrToCurr(Copy(sTmpCode, 4, 4));
    FFindOpt := 128;
  end
  else if (sTmpCode <> UpcCode) and tbPrdmstr.Locate('Code', Copy(UpcCode, 1, 6), [loCaseInsensitive]) and
    tbPrdmstr.FieldByName('CodePrice').AsBoolean then
  begin
    // first six digits inclding leading digit
    Result := True;
    nPrice := StrToCurr(Copy(UpcCode, 8, 4));
    FFindOpt := 256;
  end
  else if tbPrdmstr.Locate('Code', Copy(sTmpCode, 2, 4), [loCaseInsensitive]) and tbPrdmstr.FieldByName('CodePrice').AsBoolean
  then
  begin
    // four digit code starting from position 2, pre digit already striped
    Result := True;
    nPrice := StrToCurr(Copy(sTmpCode, 7, 4));
    FFindOpt := 512;
  end
  else if (Copy(sTmpCode, 1, 2) = '00') and tbPrdmstr.Locate('Code', Copy(sTmpCode, 3, 3), [loCaseInsensitive]) and
    tbPrdmstr.FieldByName('CodePrice').AsBoolean then
  begin
    // three digits with two leading zeros
    Result := True;
    nPrice := StrToCurr(Copy(sTmpCode, 7, 4));
    FFindOpt := 1024;
  end;
  if nPrice > 0 then
    nPrice := nPrice / 100;
end;

function TServerMethods1.FindProduct(UpcCode: string): Boolean;
  function UpcE2A(sStr: string): string;
  begin
    Result := sStr;
    if Pos(Copy(sStr, 6, 1), '012') > 0 then
      Result := Copy(sStr, 1, 2) + Copy(sStr, 6, 1) + '0000' + Copy(sStr, 3, 3)
    else if Pos(Copy(sStr, 6, 1), '3') > 0 then
      Result := Copy(sStr, 1, 3) + '00000' + Copy(sStr, 4, 2)
    else if Pos(Copy(sStr, 6, 1), '4') > 0 then
      Result := Copy(sStr, 1, 4) + '00000' + Copy(sStr, 5, 1)
    else if Pos(Copy(sStr, 6, 1), '5678') > 0 then
      Result := Copy(sStr, 1, 5) + '0000' + Copy(sStr, 6, 1);
  end;
  function GetChkDgt(sStr: string): string;
  var
    x, clc1, clc2, chkDgt: Integer;
  begin
    clc1 := 0;
    clc2 := 0;

    while Length(sStr) < 12 do
      sStr := '0' + sStr;

    for x := 1 to Length(sStr) do
      if CharInSet(sStr[x], ['0' .. '9']) and (x mod 2 > 0) then
        clc1 := clc1 + StrToInt(Copy(sStr, x, 1));

    for x := 1 to Length(sStr) do
      if CharInSet(sStr[x], ['0' .. '9']) and (x mod 2 = 0) then
        clc2 := clc2 + StrToInt(Copy(sStr, x, 1));

    clc2 := clc2 * 3;

    clc1 := clc1 + clc2;
    try
      clc2 := Trunc(Int(clc1 / 10) + 1) * 10; // goto nearest 10
    except
    end;

    chkDgt := clc2 - clc1;
    if chkDgt = 10 then
      chkDgt := 0;
    Result := Trim(IntToStr(chkDgt));
  end;
  function SeekIt(ADataSet: TDataSet; const AFieldName: string): Boolean;
  var
    sTempCode: string;
  begin
    Result := False;
    FFindOpt := 0;
    if ADataSet.Locate(AFieldName, UpcCode, [loCaseInsensitive]) then
    // full code
    begin
      FFindOpt := 1;
      Result := True
    end
    else if (Length(UpcCode) > 2) and ADataSet.Locate(AFieldName, Copy(UpcCode, 2, Length(UpcCode) - 2),
      [loCaseInsensitive]) then
    // remove leading & check digit
    begin
      FFindOpt := 2;
      Result := True
    end
    else if (Length(UpcCode) > 1) and ADataSet.Locate(AFieldName, Copy(UpcCode, 1, Length(UpcCode) - 1),
      [loCaseInsensitive]) then
    // remove check digit only
    begin
      FFindOpt := 4;
      Result := True
    end
    else if (Length(UpcCode) > 1) and ADataSet.Locate(AFieldName, Copy(UpcCode, 2, Length(UpcCode) - 1),
      [loCaseInsensitive]) then
    // remove leading digit only
    begin
      FFindOpt := 8;
      Result := True
    end
    else
    begin
      sTempCode := '';
      if Length(UpcCode) = 8 then
        sTempCode := UpcE2A(Copy(UpcCode, 2, 6));
      if Length(UpcCode) = 6 then
        sTempCode := UpcE2A(UpcCode);
      if (sTempCode <> '') and ADataSet.Locate(AFieldName, sTempCode, [loCaseInsensitive]) then
      begin
        FFindOpt := 2048;
        Result := True;
      end
      else if (sTempCode <> '') then
      begin
        sTempCode := sTempCode + GetChkDgt(sTempCode);
        if ADataSet.Locate(AFieldName, sTempCode, [loCaseInsensitive]) then
        begin
          FFindOpt := 4096;
          Result := True;
        end;
      end;
    end;
    if not Result then
      while (Copy(UpcCode, 1, 1) = '0') and (UpcCode <> '0') do
      begin
        UpcCode := Copy(UpcCode, 2, 12);
        if ADataSet.Locate(AFieldName, UpcCode, [loCaseInsensitive]) then
        begin
          Result := True;
          FFindOpt := 1;
          Break;
        end;
      end;
  end;

begin
  UpcCode := Trim(UpperCase(UpcCode));
  if UpcCode = '' then
    UpcCode := 'CHAIM_Error_Code_Required';
  Result := SeekIt(tbPrdmstr, 'Code');
  if (not Result) and tbPrdAlias.Active then
  begin
    Result := SeekIt(tbPrdAlias, 'AliasCode') and tbPrdmstr.Locate('LineID', tbPrdAlias.FieldByName('PrdLineID')
      .AsString, [loCaseInsensitive]);
    if Result then
    begin
      UpcCode := tbPrdmstr.FieldByName('Code').AsString;
      FFindOpt := 64;
    end;
  end;

  if not Result then
  begin
    Result := SeekIt(tbPrdmstr, 'CaseCode');
    if Result then
      FFindOpt := FFindOpt + 16;
  end;
  DoVerboseLogging(Format('FindProduct: %s, FindOpt: %d', [UpcCode, FFindOpt]));
end;

function XMLToStr(XML: string): string;
begin
  Result := StringReplace(XML, '<br/>', #13#10, [rfReplaceAll]);
  Result := StringReplace(Result, '&lt;', '<', [rfReplaceAll]);
  Result := StringReplace(Result, '&gt;', '>', [rfReplaceAll]);
  Result := StringReplace(Result, '&quot;', '"', [rfReplaceAll]);
  Result := StringReplace(Result, '&amp;', '&', [rfReplaceAll]);
  Result := StringReplace(Result, '&apos;', '''', [rfReplaceAll]);
end;

procedure TServerMethods1.FixUpSqlText(var ASqlText: string);
var
  AParamStr, ADataCode, ASeacrhCode, SearchString: string;
  PosStart, nPos1, nPos2: Integer;
  AQuote: Boolean;
begin
  ASqlText := TIdURI.URLDecode(ASqlText);
  ASqlText := XMLToStr(ASqlText);
  SearchString := '<PRDMSTRCODE-';
  PosStart := Length(SearchString) + 1;
  repeat
    nPos1 := Pos(SearchString, ASqlText);
    nPos2 := 0;
    if nPos1 > 0 then
      nPos2 := Pos('>', Copy(ASqlText, nPos1, Length(ASqlText))) + nPos1;
    if nPos2 > nPos1 then
    begin
      AParamStr := Copy(ASqlText, nPos1, nPos2 - nPos1);
      ASeacrhCode := Copy(AParamStr, PosStart, Length(AParamStr) - PosStart);
      AQuote := (LeftStr(ASeacrhCode, 1) = '''') and (RightStr(ASeacrhCode, 1) = '''');
      if AQuote then
        ASeacrhCode := ASeacrhCode.DeQuotedString;

      ADataCode := DBPrdCode(ASeacrhCode);
      if AQuote then
        ADataCode := ADataCode.QuotedString;
      ASqlText := StringReplace(ASqlText, AParamStr, ADataCode, [rfReplaceAll, rfIgnoreCase]);
    end;
  until nPos2 = 0;

  SearchString := 'Alter Table';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
    ASqlText := 'Select ''Alter Table Commands Restricted'' [Error Message] from system.iota;';

  SearchString := 'Drop Table';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
    ASqlText := 'Select ''Drop Table Commands Restricted'' [Error Message] from system.iota;';

  SearchString := 'Update';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
  begin
    SearchString := 'Set';
    nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
    if nPos1 > 0 then
      ASqlText := 'Select ''Update Commands Restricted'' [Error Message] from system.iota;';
  end;
end;

function TServerMethods1.GetImagesPath: string;
begin
  if FImagesPath = '' then
  begin
    with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
    begin
      FImagesPath := ReadString('Settings', 'ImagesPath', '');
      Free;
    end;
    if FImagesPath = '' then
      FImagesPath := IncludeTrailingPathDelimiter(AdsConnection1.GetConnectionPath) + 'Images\'
    else
      FImagesPath := IncludeTrailingPathDelimiter(FImagesPath);
  end;
  Result := FImagesPath;
end;

function TServerMethods1.GetCustomerBal(const sTel: string): string;
var
  metaData: TDSInvocationMetadata;
  I: Integer;
  ASecureSQL: string;
begin
  if CloseCommandActive then
  begin
    Result := 'Database Not Available (Close Command)';
    Exit;
  end;

  ASecureSQL := sTel;
  if not SecureSqlText(ASecureSQL) then
  begin
    Result := 'Security Alert ' + ASecureSQL;
    Exit;
  end;

  qryCustomer.Active := False;
  qryCustomer.Params[0].Value := sTel; //
  qryCustomer.Open();
  Result := qryCustomer.FieldByName('balance').AsString;

  // this code is for returning xml not jsonn
  metaData := GetInvocationMetadata;
  for I := 0 to Pred(metaData.QueryParams.Count) do
  begin
    Result := Result + '<param>' + metaData.QueryParams[I] + '</param>';
  end;
  if Result = '' then
    Result := 'empty';
  metaData.ResponseContent := '<xml><param><balance>' + Result + '</balance></param></xml>';
end;

function TServerMethods1.GetVerboseLogging: Boolean;
begin
  if FVerboseLogging = 0 then
  begin
    with TIniFile.Create(ChangeFileExt(ParamStr(0), '.ini')) do
    begin
      if ReadBool('Settings', 'VerboseLogging', False) then
        FVerboseLogging := 1
      else
        FVerboseLogging := -1;
      Free;
    end;
  end;
  Result := FVerboseLogging = 1;
end;

function TServerMethods1.IsCustomerInDB: TJSONValue;
begin
  Result := DoCmd('IsCustomerInDB');
end;

function TServerMethods1.LineBusterAdd(const ABatchID, AItemCode: string; const AQuan: Double; const AIsCase: Boolean)
  : TJSONValue;
var
  JSONObject: TJSONObject;
  ASecureSQL: string;
begin
  DoVerboseLogging(Format('LineBusterAdd: Values(%s, %s, %f)', [ABatchID, AItemCode, AQuan]));
  JSONObject := TJSONObject.Create;
  if CloseCommandActive then
  begin
    JSONObject.AddPair('Status', 'Error');
    JSONObject.AddPair('Error', 'Database Not Available (Close Command)');
    Exit(JSONObject);
  end;

  ASecureSQL := ABatchID + AItemCode;
  if not SecureSqlText(ASecureSQL) then
  begin
    JSONObject.AddPair('Result', 'Error');
    JSONObject.AddPair('ErrorDescription', 'Security Alert ' + ASecureSQL);
    Exit(JSONObject);
  end;

  try
    qrySQLExecute.ExecSQL(Format('Insert Into DeliItms (ID, Code, [Count], IsCase) Values(%s, %s, %f, %s);',
      [QuotedStr(ABatchID), QuotedStr(AItemCode), AQuan, BoolToStr(AIsCase, True)]));
    JSONObject.AddPair('Result', 'Success');
  except
    on E: Exception do
    begin
      JSONObject.AddPair('Result', 'Error');
      JSONObject.AddPair('ErrorDescription', E.Message);
    end;
  end;
  Result := JSONObject;
end;

function TServerMethods1.LineBusterAddBatch(ABatchID, AInfo: string): TJSONValue;
var
  JSONObject: TJSONObject;
  AItemCode, AQuan, AIsCase: string;
  ASource, AString: TStringList;
  I, APos: Integer;
  ARowS: string;
  ASecureSQL: string;
begin
  try
    DoVerboseLogging(Format('LineBusterAdd: Values(%s, %s)', [ABatchID, AInfo]));
  except
    DoVerboseLogging(Format('LineBusterAdd: Values(%s) - Can''t ToString passed Json', [ABatchID]));
  end;
  JSONObject := TJSONObject.Create;
  try
    if CloseCommandActive then
      raise Exception.Create('Database Not Available (Close Command)');

    ASecureSQL := ABatchID + AInfo;
    if not SecureSqlText(ASecureSQL) then
      raise Exception.Create('Security Alert ' + ASecureSQL);

    ASource := TStringList.Create;
    AString := TStringList.Create;
    try
      ASource.Delimiter := 'R';
      ASource.DelimitedText := Copy(AInfo, 2, MaxInt);
      for I := 0 to ASource.Count - 1 do
      begin
        ARowS := ASource[I];
        APos := Pos('Q', ARowS);
        if APos > 0 then
        begin
          AItemCode := Copy(ARowS, 1, APos - 1);
          ARowS := Copy(ARowS, APos + 1, MaxInt);
          APos := Pos('Q', ARowS);
          if APos > 0 then
          begin
            AQuan := Copy(ARowS, 1, APos - 1);
            ARowS := Copy(ARowS, APos + 1, MaxInt);
          end;
          if ARowS = 'Y' then
            AIsCase := 'Y'
          else
            AIsCase := 'N';
          if (AItemCode <> '') and (AQuan <> '') then
            AString.Add(Format('Insert Into DeliItms (ID, Code, [Count], IsCase) Values(%s, %s, %s, %s);',
              [QuotedStr(ABatchID), QuotedStr(AItemCode), AQuan, BoolToStr(AIsCase.Equals('Y'), True)]));
        end;
      end;
      if AString.Text <> '' then
      begin
        qrySQLExecute.ExecSQL(AString.Text);
        JSONObject.AddPair('Result', 'Success');
        JSONObject.AddPair('Records Added', IntToStr(qrySQLExecute.RowsAffected));
      end;
    finally
      AString.Free;
      ASource.Free;
    end;
  except
    on E: Exception do
    begin
      JSONObject.AddPair('Result', 'Error');
      JSONObject.AddPair('ErrorDescription', E.Message);
      DoVerboseLogging(Format('LineBusterAdd: Error %s', [E.Message]));
    end;
  end;
  Result := JSONObject;
end;

function TServerMethods1.LineBusterClear: TJSONValue;
begin
  Result := DoCmd('LineBusterClear');
end;

function TServerMethods1.LocationInventoryTransfer: TJSONValue;
begin
  Result := DoCmd('LocationInventoryTransfer');
end;

function TServerMethods1.NewVendorInvoice: TJSONValue;
begin
  Result := DoCmd('NewVendorInvoice');
end;

function TServerMethods1.OpenPrdTable: Boolean;
begin
  tbPrdmstr.Open;
  Result := tbPrdmstr.Active;
  try
    tbPrdAlias.Open;
  except
  end;
end;

function TServerMethods1.OpenSqlJson(ASqlText, ADataAlias: string): TJSONValue;
var
  JSONObject: TJSONObject;
  I: Integer;
begin
  if ADataAlias <> '' then
    FDataAlias := ADataAlias;

  JSONObject := TJSONObject.Create;
  try
    if CloseCommandActive then
    begin
      JSONObject.AddPair('Status', 'Error');
      JSONObject.AddPair('Error', 'Database Not Available (Close Command)');
      Exit(JSONObject);
    end;

    FixUpSqlText(ASqlText);
    DoVerboseLogging('OpenSqlJson: ' + ASqlText);
    qrySQLOpen.Open(ASqlText);
    try
      qrySQLOpen.First;
      while not qrySQLOpen.Eof do
      begin
        for I := 0 to qrySQLOpen.FieldCount - 1 do
        begin
          if string.IsNullOrEmpty(qrySQLOpen.Fields[I].FullName) then
            Continue;
          JSONObject.AddPair(qrySQLOpen.Fields[I].FullName,
            VarToStr(qrySQLOpen.FieldValues[qrySQLOpen.Fields[I].FullName]));
        end;
        qrySQLOpen.Next;
      end;
    finally
      qrySQLOpen.Close;
    end;
  except
    on E: Exception do
    begin
      DoVerboseLogging('OpenSqlJson Error: ' + E.Message);
      Writeln(E.Message);
    end;
  end;
  Result := JSONObject;
end;

function TagIt(const ATag, AValue: string): string;
begin
  Result := Format('<%s>%s</%s>', [ATag, AValue, ATag]);
end;

function TServerMethods1.OpenSqlXML(ASqlText, ADataAlias: string): string;
var
  metaData: TDSInvocationMetadata;
  I: Integer;
  AData: string;
begin
  if CloseCommandActive then
  begin
    Result := 'Database Not Available (Close Command)';
    Exit;
  end;

  if ADataAlias <> '' then
    FDataAlias := ADataAlias;

  try
    FixUpSqlText(ASqlText);
    if VerboseLogging then
      WriteToLog('OpenSqlXML: ' + ASqlText, 'err.log');
    qrySQLOpen.Open(ASqlText);
  except
    on E: Exception do
    begin
      DoVerboseLogging('OpenSqlXML Error: ' + E.Message);
      Writeln(E.Message);
    end;
  end;

  AData := '';
  for I := 0 to qrySQLOpen.FieldCount - 1 do
    AData := AData + TagIt(qrySQLOpen.Fields[I].FieldName, qrySQLOpen.Fields[I].AsString);

  // Return XML not JSON
  metaData := GetInvocationMetadata;
  metaData.ResponseContent := TagIt('xml', TagIt('params', AData));
end;

function TServerMethods1.PostPrintJob: TJSONValue;
begin
  Result := DoCmd('PostPrintJob');
end;

function TServerMethods1.PrdGlobalChanges: TJSONValue;
begin
  Result := DoCmd('PrdGlobalChanges');
end;

function TServerMethods1.PrintLabelRm: TJSONValue;
begin
  Result := DoCmd('PrintLabelRm');
end;

function TServerMethods1.ProcessCardKnoxPay(ACardNo, AExpire, ACVV2, AToken, ACardKnoxTID, APayDescrip, APayCode,
  AProcessor: string; AAmount: Currency; AXmlResponse: TJvSimpleXML; JSONResponse: TJSONObject): Boolean;
type
  TProcessPayment = function(ACommand: PAnsiChar): PAnsiChar; stdcall;
var
  ProcessPayment: TProcessPayment;
  AResponse, ACommand: PAnsiChar;
  AOldDir: string;
  AXmlReq: TJvSimpleXML;
  AXmlElem: TJvSimpleXMLElem;
  I: Integer;
begin
  Result := False;

  { qryHelper.Close;
    qryHelper.SQL.Text :=
    'Select Descrip PayDescrip, PayCode, Processor, TID From PayList Where PayCode<>'''' And PayGroup=''R'' And TID<>'''' '
    + 'And (Processor=''FDLO'' or Processor=''FDLE'' or Processor=''FDLI'') Order By UseInReg;';
    qryHelper.Open;
    qryHelper.First;
    ACardKnoxTID := '';
    while (not qryHelper.Eof) and ACardKnoxTID.IsEmpty do
    begin
    ACardKnoxTID := qryHelper.FieldByName('TID').AsString;
    if not ACardKnoxTID.IsEmpty then
    for I := 0 to qryHelper.FieldCount - 1 do
    JSONResponse.AddPair(qryHelper.Fields[I].FieldName, qryHelper.Fields[I].AsString);
    qryHelper.Next;
    end;
    qryHelper.Close;
    qryHelper.AdsCloseSQLStatement; }

  if ACardKnoxTID.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'No Valid CardKnox TID Found');
    Exit(False);
  end;

  AOldDir := GetCurrentDir;
  AXmlReq := TJvSimpleXML.Create(nil);
  try
    ChDir('C:\RunPOS\');

    @ProcessPayment := GetProcAddress(frmServiceSettings.CdPayLibInst, 'ProcessPayment');
    if @ProcessPayment = nil then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'CDPayLib ProcessPayment procedure not found');
      Exit(False);
    end;

    AExpire := AExpire.Replace('/', '');

    AXmlReq.Root.Clear;
    AXmlReq.Root.Name := 'Trans';
    AXmlReq.Root.Items.Add('Key').Value := ACardKnoxTID;
    AXmlReq.Root.Items.Add('Command').Value := 'cc:sale';
    AXmlReq.Root.Items.Add('Amount').Value := CurrToStr(AAmount);
    AXmlReq.Root.Items.Add('CardNum').Value := ACardNo;
    AXmlReq.Root.Items.Add('Exp').Value := AExpire;
    AXmlReq.Root.Items.Add('Token').Value := AToken;
    AXmlReq.Root.Items.Add('CVV2').Value := ACVV2;
    AXmlReq.Root.Items.Add('Cardpresent').Value := 'Yes';
    AXmlReq.Root.Items.Add('TimeOut').Value := '120';
    AXmlReq.Root.Items.Add('EnableSilentMode').Value := 'Yes';
    AXmlReq.Root.Items.Add('OOS', 'Yes');

    ACommand := PAnsiChar(AnsiString(AXmlReq.XMLData));

    try
      AResponse := ProcessPayment(ACommand);
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', 'CDPayLib ProcessPayment error ' + E.Message);
        Exit(False);
      end;
    end;
    if AResponse <> '' then
    begin
      JSONResponse.AddPair('CardKnoxTID', ACardKnoxTID);
      JSONResponse.AddPair('PayDescrip', APayDescrip);
      JSONResponse.AddPair('PayCode', APayCode);
      JSONResponse.AddPair('Processor', AProcessor);

      AXmlReq.XMLData := '<Data>' + string(AnsiString(AResponse)) + '</Data>';

      AXmlResponse.Root.Clear;
      AXmlResponse.Root.Name := 'Result';
      AXmlElem := AXmlResponse.Root.Items.Add('Data');

      for I := 0 to AXmlReq.Root.ItemCount - 1 do
        AXmlElem.Items.Add(AXmlReq.Root.Items[I].Name).Value := AXmlReq.Root.Items[I].Value;
      Result := XMLNamedNodeValue(AXmlResponse.Root.Items[0], 'Result') = 'A';
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'No Response from CDPayLib ProcessPayment');
    end;
  finally
    ChDir(AOldDir);
    AXmlReq.Free;
  end;
end;

function TServerMethods1.ProductInfo: TJSONValue;
begin
  Result := DoCmd('ProductInfo');
end;

procedure TServerMethods1.AssignCodeParamToQuery(AQuery: TFDQuery);
begin
  AQuery.Close;

  if not AQuery.Prepared then
    AQuery.Params[0].DataType := ftString;
  AQuery.Prepare;
  AQuery.Params[0].AsString := FCodeFound;
  AQuery.Open;
end;

function TServerMethods1.SearchRebate: Boolean;
begin
  Result := False;
  AssignCodeParamToQuery(qryRebatSearch);
  if qryRebatSearch.IsEmpty or qryRebatSearch.FieldByName('Value').IsNull then
    Exit;

  FLine1 := 'Rebate'; // return type found
  // if qryRebatSearch.FieldByName('Value').AsCurrency > 0 then
  // Client side would decide
  FLine2 := qryRebatSearch.FieldByName('Value').AsString;
  Result := True;

  { if AdsQuery1.FieldByName('Value').AsCurrency <= 0 then
    begin
    lblErrorMes.Caption := 'No Points Availible';
    SetVisible(2);
    end; }
end;

function TServerMethods1.SecureSqlText(var ASqlText: string): Boolean;
var
  SearchString: string;
  nPos1: Integer;
begin
  Result := True;
  ASqlText := TIdURI.URLDecode(ASqlText);
  ASqlText := XMLToStr(ASqlText);

  SearchString := 'Alter Table';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
  begin
    Result := False;
    ASqlText := 'Alter Table Commands Restricted';
  end;

  SearchString := 'Drop Table';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
  begin
    Result := False;
    ASqlText := 'Drop Table Commands Restricted';
  end;

  SearchString := 'Update';
  nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
  if nPos1 > 0 then
  begin
    SearchString := 'Set';
    nPos1 := Pos(UpperCase(SearchString), UpperCase(ASqlText));
    if nPos1 > 0 then
    begin
      Result := False;
      ASqlText := 'Update Table Commands Restricted';
    end;
  end;
end;

function TServerMethods1.SelfCheckoutStatus: TJSONValue;
begin
  Result := DoCmd('SelfCheckoutStatus');
end;

procedure TServerMethods1.DoAddBatchItem(JSONRequest, JSONResponse: TJSONObject);
var
  JSONRows: TJSONArray;
  JSONRowA: TJSONObject;
  ABatchID, ABatchType, ABatchName, AUpcCode, AQuan, ACCustom1, ACCustom2, ACCustom3, ANCustom1, ANCustom2: string;
  I: Integer;
  procedure DoAddBatchItemRow;
  begin
    qryHelper.SQL.Add('Insert Into BatchItems (BatchID, BatchType, BatchName, UpcCode');
    if AQuan <> '' then
      qryHelper.SQL.Add(', Quan');
    if ACCustom1 <> '' then
      qryHelper.SQL.Add(', CCustom1');
    if ACCustom2 <> '' then
      qryHelper.SQL.Add(', CCustom2');
    if ACCustom3 <> '' then
      qryHelper.SQL.Add(', CCustom3');
    if ANCustom1 <> '' then
      qryHelper.SQL.Add(', NCustom1');
    if ANCustom2 <> '' then
      qryHelper.SQL.Add(', NCustom2');
    qryHelper.SQL.Add(') Values(');
    qryHelper.SQL.Add(Format('%s, %s, %s, %s', [QuotedStr(ABatchID), QuotedStr(ABatchType), QuotedStr(ABatchName),
      QuotedStr(AUpcCode)]));
    if AQuan <> '' then
      qryHelper.SQL.Add(Format(', %s', [AQuan]));
    if ACCustom1 <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(ACCustom1)]));
    if ACCustom2 <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(ACCustom2)]));
    if ACCustom3 <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(ACCustom3)]));
    if ANCustom1 <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(ANCustom1)]));
    if ANCustom2 <> '' then
      qryHelper.SQL.Add(Format(', %s', [QuotedStr(ANCustom2)]));
    qryHelper.SQL.Add(');');
  end;

begin
  JSONRequest.TryGetValue<string>('BatchID', ABatchID);
  JSONRequest.TryGetValue<string>('BatchType', ABatchType);
  JSONRequest.TryGetValue<string>('BatchName', ABatchName);
  JSONRequest.TryGetValue<string>('UpcCode', AUpcCode);
  JSONRequest.TryGetValue<string>('Quan', AQuan);
  JSONRequest.TryGetValue<string>('CCustom1', ACCustom1);
  JSONRequest.TryGetValue<string>('CCustom2', ACCustom2);
  JSONRequest.TryGetValue<string>('CCustom3', ACCustom3);
  JSONRequest.TryGetValue<string>('NCustom1', ANCustom1);
  JSONRequest.TryGetValue<string>('NCustom2', ANCustom2);
  if (ABatchID <> '') and (AUpcCode <> '') then
  begin
    qryHelper.SQL.Clear;
    DoAddBatchItemRow;
    try
      qryHelper.ExecSQL;
      JSONResponse.AddPair('Status', 'Success');
      JSONResponse.AddPair('Result', Format('Batch Item Added for %s', [AUpcCode]));
    except
      on E: Exception do
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('AddBatchItem %s', [E.Message]));
      end;
    end
  end
  else if ABatchID <> '' then
  begin
    qryHelper.SQL.Clear;
    JSONRows := JSONRequest.GetValue('Rows') as TJSONArray;
    if JSONRows <> nil then
      for I := 0 to JSONRows.Count - 1 do
      begin
        JSONRowA := JSONRows.Items[I] as TJSONObject;
        JSONRowA.TryGetValue<string>('UpcCode', AUpcCode);
        JSONRowA.TryGetValue<string>('Quan', AQuan);
        JSONRowA.TryGetValue<string>('CCustom1', ACCustom1);
        JSONRowA.TryGetValue<string>('CCustom2', ACCustom2);
        JSONRowA.TryGetValue<string>('CCustom3', ACCustom3);
        JSONRowA.TryGetValue<string>('NCustom1', ANCustom1);
        JSONRowA.TryGetValue<string>('NCustom2', ANCustom2);
        if (AUpcCode <> '') and (AQuan <> '') then
          DoAddBatchItemRow;
      end;
    if qryHelper.SQL.Count > 0 then
    begin
      qryHelper.SQL.Insert(0, 'Begin Transaction; Try');
      qryHelper.SQL.Add('Commit Work; Catch All Rollback Work; Raise; End;');
      try
        qryHelper.ExecSQL;
        JSONResponse.AddPair('Status', 'Success');
        JSONResponse.AddPair('Result', Format('%d Batch Items Added', [qryHelper.RowsAffected]));
      except
        on E: Exception do
        begin
          JSONResponse.AddPair('Status', 'Error');
          JSONResponse.AddPair('Error', Format('AddBatchItem raised %s', [E.Message]));
        end;
      end;
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'AddBatchItem No Valid Row submitted');
    end
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'AddBatchItem required parameter missing');
  end;
end;

procedure TServerMethods1.DoAddLineBusterBatch(JSONRequest, JSONResponse: TJSONObject);
const
  APrintJobConst =
    '<JobFile><JobName Description="LineBuster Label">LineBusterLabel1<RunLevel DataType="N">3</RunLevel>' +
    '<Device>Printer</Device><HideDialogs>True</HideDialogs><ExecuteOnce>True</ExecuteOnce><PrinterName>##PRINTER</PrinterName>'
    + '<Output>LineBuster1.btw</Output><NoDataAction>1</NoDataAction><ParamCCP0BD16>##BATCHID</ParamCCP0BD16></JobName></JobFile>';
var
  ABatchID, AUpcCode, AExistMethod, APrinterName, APrintJob, ADeviceID: string;
  AValue: Integer;
  AQuan: Double;
  ACase: Boolean;
  ARowS: TJSONArray;
  AEnumA: TJSONArrayEnumerator;
  AQuerySQL: TStringList;
begin
  JSONRequest.TryGetValue<string>('BatchID', ABatchID);
  JSONRequest.TryGetValue<string>('PrinterName', APrinterName);
  JSONRequest.TryGetValue<string>('ExistMethod', AExistMethod);
  JSONRequest.TryGetValue<string>('DeviceID', ADeviceID);
  // JSONRequest.TryGetValue<string>('Info', AInfo);
  JSONRequest.TryGetValue<TJSONArray>('Rows', ARowS);

  if (ABatchID.IsEmpty and APrinterName.IsEmpty) or (ARowS.Count = 0) then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'AddLineBusterBatch required parameter missing');
    Exit;
  end;

  if ABatchID.IsEmpty then
  begin
    qryHelper.SQL.Text := 'Select Max(ID) BatchID From DeliItms Where Length(Trim(ID))=5 And ID<=''99999''';
    qryHelper.Open;
    ABatchID := qryHelper.Fields[0].AsString;
    qryHelper.Close;
    ABatchID := Trim(IntToStr(StrToIntDef(ABatchID, 10000) + 1));
    AExistMethod := 'Notify';

    if ABatchID = '100000' then
    begin
      AValue := 10001;
      ABatchID := '';
      qryHelper.SQL.Text := 'Select ID BatchID From DeliItms Where Length(Trim(ID))=5 And ID<=''99999''';
      qryHelper.Open;
      qryHelper.First;
      while qryHelper.Locate('BatchID', AValue, []) do
      begin
        AValue := Incr(AValue);
        qryHelper.Next;
      end;
      qryHelper.Close;
      if AValue <= 99999 then
        ABatchID := Trim(IntToStr(AValue));
    end;
  end;

  if ABatchID.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'Error creating a proper BatchID');
    Exit;
  end;

  if AExistMethod.IsEmpty then
    AExistMethod := 'Notify';

  if SameText(AExistMethod, 'New') then
    qrySQLExecute.ExecSQL(Format('Delete From DeliItms Where ID=%s', [QuotedStr(ABatchID)]))
  else if SameText(AExistMethod, 'Notify') then
  begin
    qryHelper.SQL.Text := Format('Select Count(*) From DeliItms Where ID=%s', [QuotedStr(ABatchID)]);
    qryHelper.Open;
    if qryHelper.Fields[0].AsInteger > 0 then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', Format('ID in use (%d items)', [qryHelper.Fields[0].AsInteger]));
      qryHelper.Close;
      Exit;
    end;
  end;

  AQuerySQL := TStringList.Create;
  try
    AEnumA := ARowS.GetEnumerator;
    qryHelper.Close;
    qryHelper.SQL.Text := 'Select Code, [Desc], UPC From Prdmstr Where Code=:Code';
    qryHelper.Prepare;
    while AEnumA.MoveNext do
    begin
      AEnumA.Current.TryGetValue<string>('UpcCode', AUpcCode);
      AEnumA.Current.TryGetValue<Double>('Quan', AQuan);
      AEnumA.Current.TryGetValue<Boolean>('Case', ACase);
      if (not AUpcCode.IsEmpty) and (AQuan > 0) then
        AQuerySQL.Add(Format('Insert Into DeliItms (ID, Code, [Count], IsCase) Values(%s, %s, %f, %s);',
          [QuotedStr(ABatchID), QuotedStr(AUpcCode), AQuan, BoolToStr(ACase, True)]));
    end;
    if AQuerySQL.Count > 0 then
    begin
      qrySQLExecute.ExecSQL(AQuerySQL.Text);
      JSONResponse.AddPair('Status', 'Success');
      JSONResponse.AddPair('Records Added', IntToStr(qrySQLExecute.RowsAffected));
      if not APrinterName.IsEmpty then
      begin
        APrintJob := ReplaceStr(APrintJobConst, '##PRINTER', APrinterName);
        APrintJob := ReplaceStr(APrintJob, '##BATCHID', QuotedStr(ABatchID));
        qryHelper.SQL.Text :=
          Format('Insert Into Notify (AppID, Action, Data3) Values(ApplicationID(), ''DoReport'', %s);',
          [QuotedStr(APrintJob)]);
        qryHelper.ExecSQLScript;
      end;
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'No Valid Items with Quantity passed');
    end;
    if not ADeviceID.IsEmpty then
      try
        qryHelper.Close;
        qryHelper.SQL.Text :=
          'Update SelfCheckoutStatus Set EndTime=Now(), EndAction=''PushToPOS'' Where DeviceID=:DeviceID And EndTime Is Null;';
        qryHelper.Prepare;
        qryHelper.ParamByName('DeviceID').AsString := ADeviceID;
        qryHelper.ExecSQL;
      except
        on E: Exception do
          JSONResponse.AddPair('Report SelfCheckoutStatus Error', E.Message);
      end;
  finally
    AQuerySQL.Free;
  end;
end;

procedure TServerMethods1.DoAddNewProduct(JSONRequest, JSONResponse: TJSONObject);
begin
  try
    AdsConnection1.Connect;
    dmData(AdsConnection1);
    PrdHelpersI.AddNewPrd(JSONRequest, JSONResponse);
  except
    on E: Exception do
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', Format('DoAddNewProduct %s', [E.Message]));
    end;
  end;
end;

function TServerMethods1.DoCmd(ACmdType: string): TJSONValue;
var
  JSONRequest, JSONResponse: TJSONObject;
  metaData: TDSInvocationMetadata;
  I: Integer;
  AJsonLoaded: Boolean;
begin
  AJsonLoaded := False;
  metaData := GetInvocationMetadata;
  // enables calling TServerMethods1/CommandName?=JSON
  if (metaData.QueryParams.Count = 1) and (metaData.QueryParams.Names[0] = '') and
    (metaData.QueryParams.ValueFromIndex[0] <> '') then
    try
      JSONRequest := TJSONObject.ParseJSONValue(metaData.QueryParams.ValueFromIndex[0]) as TJSONObject;
      AJsonLoaded := Assigned(JSONRequest);
    except
    end;

  if not AJsonLoaded then
  begin
    JSONRequest := TJSONObject.Create;
    for I := 0 to Pred(metaData.QueryParams.Count) do
      JSONRequest.AddPair(metaData.QueryParams.Names[I], metaData.QueryParams.ValueFromIndex[I]);
  end;

  try
    JSONResponse := TJSONObject.Create;
    if ACmdType = '' then
      JSONRequest.TryGetValue<string>('CmdType', ACmdType);
    DoVerboseLogging(ACmdType + '; ' + SessionID + '; ' + JSONRequest.ToString);

    if CloseCommandActive then
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'Database Not Available (Close Command)');
    end
    else if ACmdType <> '' then
    begin
      if SameText(ACmdType, 'AddVndRtrn') then
        AddVndRtrn(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'UpdDlvStatus') then
        DeliveryStatus(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'AddBatchItem') then
        DoAddBatchItem(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'NewVendorInvoice') then
        DoNewVendorInvoice(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'PrintLabelRm') then
        DoRmLabelJson(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'PrdGlobalChanges') then
        DoPrdGlobalChangesJson(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'IsCustomerInDB') then
        DoIsCustomerInDB(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'AddCustomerToDB') then
        //DoAddCustomerToDB(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'AddUpdateCustomer') then
        //DoAddUpdateCustomer(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'PostPrintJob') then
        DoPostPrintJob(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'CustomerCardPayment') then
        DoCustomerCardPayment(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'LocationInventoryTransfer') then
        DoLocationInventoryTransfer(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'ProductInfo') then
        DoProductInfo(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'AddLineBusterBatch') then
        DoAddLineBusterBatch(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'CustAcntAvailLineBuster') then
        DoCustAcntAvailLineBuster(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'LineBusterClear') then
        DoLineBusterClear(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'AddNewProduct') then
        DoAddNewProduct(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'UpdateProduct') then
        DoUpdateProduct(JSONRequest, JSONResponse)
      else if SameText(ACmdType, 'SelfCheckoutStatus') then
        DoSelfCheckoutStatus(JSONRequest, JSONResponse)

      else
      begin
        JSONResponse.AddPair('Status', 'Error');
        JSONResponse.AddPair('Error', Format('Command %s not suppported', [ACmdType]));
      end;
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'No Command specified');
    end;

    try
      DoVerboseLogging(ACmdType + '; ' + SessionID + '; ' + JSONResponse.ToString);
    except
    end;
    Result := JSONResponse;
  finally
    if not AJsonLoaded then
      JSONRequest.Free;
  end;
end;

procedure TServerMethods1.DoCustAcntAvailLineBuster(JSONRequest, JSONResponse: TJSONObject);
var
  AAccountNo, AError, ADeviceID, ATel: string;
begin
  // Sleep(5000);
  AError := '';
  JSONRequest.TryGetValue<string>('AccountNo', AAccountNo);
  JSONRequest.TryGetValue<string>('DeviceID', ADeviceID);

  if AAccountNo.IsEmpty then
    AError := 'AccountNo is required';

  if AError.IsEmpty then
  begin
    if AAccountNo.Length >= 11 then
      AAccountNo := Copy(AAccountNo, 2, 10);

    { qryHelper.SQL.Text :=
      Format('Select LastName, Name, Address, City, State, Zip, Tel, Email, Cell, Account From Customer Where Account=%s',
      [QuotedStr(AAccountNo)]); }
    qryHelper.SQL.Clear;
    qryHelper.SQL.Add('Select LastName, Name, Address, City, State, Zip, Tel, Email, Cell, Account From Customer');
    qryHelper.SQL.Add(Format('Where Tel IN (Select Tel From Rebates Where SourceCode=%s And RefNum=%s)',
      [QuotedStr('L'), QuotedStr(AAccountNo)]));

    qryHelper.Open;
    if qryHelper.IsEmpty then
      AError := 'Not Valid AccountNo';
  end;

  if AError.IsEmpty then
  begin
    qryHelper2.SQL.Text := Format('Select Count(*) From DeliItms Where ID=%s', [QuotedStr(Copy(AAccountNo, 6, 5))]);
    qryHelper2.Open;
    if qryHelper2.Fields[0].AsInteger > 0 then
      AError := Format('ID in use (%d items)', [qryHelper2.Fields[0].AsInteger]);
    qryHelper2.Close;
  end;

  if AError.IsEmpty and qryHelper.Active then
  begin
    JSONResponse.AddPair('Status', 'Success');
    JSONResponse.AddPair('Lastname', qryHelper.FieldByName('LastName').AsString);
    JSONResponse.AddPair('Name', qryHelper.FieldByName('Name').AsString);
    JSONResponse.AddPair('Address', qryHelper.FieldByName('Address').AsString);
    JSONResponse.AddPair('City', qryHelper.FieldByName('City').AsString);
    JSONResponse.AddPair('State', qryHelper.FieldByName('State').AsString);
    JSONResponse.AddPair('Zip', qryHelper.FieldByName('Zip').AsString);
    JSONResponse.AddPair('Tel', qryHelper.FieldByName('Tel').AsString);
    JSONResponse.AddPair('Cell', qryHelper.FieldByName('Cell').AsString);
    JSONResponse.AddPair('Account', qryHelper.FieldByName('Account').AsString);
    JSONResponse.AddPair('Email', qryHelper.FieldByName('Email').AsString);
    JSONResponse.AddPair('CardNumber', AAccountNo);

    if not ADeviceID.IsEmpty then
      try
        ATel := qryHelper.FieldByName('Tel').AsString;
        qryHelper.Close;
        qryHelper.SQL.Clear;
        qryHelper.SQL.Add('Update SelfCheckoutStatus Set EndTime=Now(), EndAction=''Abandoned''');
        qryHelper.SQL.Add('Where DeviceID=:DeviceID And EndTime Is Null;');
        qryHelper.SQL.Add
          ('Insert Into SelfCheckoutStatus (Tel, CardID, DeviceId, StartTime, ScanCount, ItemsNotFound)');
        qryHelper.SQL.Add('Values(:Tel, :AcountNo, :DeviceID, Now(), 0, 0);');
        qryHelper.Prepare;
        qryHelper.ParamByName('Tel').AsString := ATel;
        qryHelper.ParamByName('AcountNo').AsString := AAccountNo;
        qryHelper.ParamByName('DeviceID').AsString := ADeviceID;
        qryHelper.ExecSQL;
      except
        on E: Exception do
          JSONResponse.AddPair('Report SelfCheckoutStatus Error', E.Message);
      end;
  end
  else
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', AError);
  end;
  qryHelper.Close;
end;

procedure TServerMethods1.DoCustomerCardPayment(JSONRequest, JSONResponse: TJSONObject);
var
  ATel, ACardNo, AExpire, ACVV2, AToken, ABody, ATID, APayDescrip, APayCode, AProcessor, ATisWebAddress,
    ANotifySql: string;
  AAmount: Currency;
  AXmlResp: TJvSimpleXML;
  I: Integer;
  AApproved: Boolean;
begin
  JSONRequest.TryGetValue<string>('Body', ABody);
  if not ABody.IsEmpty then
    JSONRequest := TJSONObject.ParseJSONValue(ABody) as TJSONObject;

  JSONRequest.TryGetValue<string>('Tel', ATel);
  JSONRequest.TryGetValue<string>('CardNo', ACardNo);
  JSONRequest.TryGetValue<string>('Expire', AExpire);
  JSONRequest.TryGetValue<string>('CVV2', ACVV2);
  JSONRequest.TryGetValue<string>('Token', AToken);
  JSONRequest.TryGetValue<string>('TID', ATID);
  JSONRequest.TryGetValue<string>('PayDescrip', APayDescrip);
  JSONRequest.TryGetValue<string>('PayCode', APayCode);
  JSONRequest.TryGetValue<string>('Processor', AProcessor);
  JSONRequest.TryGetValue<string>('TisWebAddress', ATisWebAddress);
  JSONRequest.TryGetValue<Currency>('Amount', AAmount);

  if ATel.IsEmpty then
    JSONResponse.AddPair('Error', 'No Tel passed');
  { else if not QueryCustomerInDB(ATel, '') then
    JSONResponse.AddPair('Error', 'Passed Tel not found in Customer List'); }

  if AToken.IsEmpty then
  begin
    if ACardNo.IsEmpty then
      JSONResponse.AddPair('Error', 'No CardNo passed')
    else if ACardNo.Length < 15 then
      JSONResponse.AddPair('Error', 'No valid CardNo passed');

    if AExpire.IsEmpty then
      JSONResponse.AddPair('Error', 'No Expire passed');
  end;

  if AAmount <= 0 then
    JSONResponse.AddPair('Error', 'No valid Amount passed');

  if JSONResponse.Count > 0 then
  begin
    JSONResponse.AddPair('Status', 'Error');
    Exit;
  end;

  // if not CdPayInit then
  if not frmServiceSettings.CdPayInit then
    JSONResponse.AddPair('Error', 'CdPayInit Failed');

  if JSONResponse.Count > 0 then
  begin
    JSONResponse.AddPair('Status', 'Error');
    Exit;
  end;

  AXmlResp := TJvSimpleXML.Create(nil);
  try
    AApproved := ProcessCardKnoxPay(ACardNo, AExpire, ACVV2, AToken, ATID, APayDescrip, APayCode, AProcessor, AAmount,
      AXmlResp, JSONResponse);
    if (JSONResponse.Count > 0) and JSONResponse.Pairs[0].JsonString.Value.Equals('Error') then
      Exit;

    if AApproved then
    begin
      JSONResponse.AddPair('Status', 'Success');
      JSONResponse.AddPair('Success', 'Payment Recorded')
    end
    else
    begin
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Declined', 'Payment NOT Recorded');
    end;

    JSONResponse.AddPair('Tel', ATel);
    for I := 0 to AXmlResp.Root.Items[0].ItemCount - 1 do
      JSONResponse.AddPair(AXmlResp.Root.Items[0].Items[I].Name, AXmlResp.Root.Items[0].Items[I].Value);

    if AApproved then
    begin
      ANotifySql := Format('EXECUTE PROCEDURE cdAddNotification(%s, %s, %s)',
        [QuotedStr('CustomerPayment'), QuotedStr(ATel), QuotedStr(JSONResponse.ToString)]);
      qryHelper.Close;
      if ATisWebAddress.IsEmpty then
        qryHelper.SQL.Text := ANotifySql
      else
      begin
        // Need CdEventWatcher to send to TServerMethods1 to enter record in Notify
        // we could use OpenSQLXml for this
        qryHelper.SQL.Text := Format('EXECUTE PROCEDURE cdAddNotificationB(%s, %s, %s, %s)',
          [QuotedStr('RestRequest'), QuotedStr(ATisWebAddress + '/api/v1/TServerMethods1/ExecuteSQL/'),
          QuotedStr(ANotifySql), QuotedStr('')]);
      end;
      qryHelper.ExecSQL;
    end;

    { try
      //ScheduleRunOnceNow('RunTis01', '\"C:\RunPOS\Tis01.exe\" NoPause', 'Sol');
      except
      on E: Exception do
      DoVerboseLogging(E.Message);
      end; }
  finally
    AXmlResp.Free;
    // FreeLibrary(FCdPayLibInst);
  end;
end;

function TServerMethods1.UpdatePostWebOrder(AInfo: TJSONObject): TJSONValue;
begin
  //Result := PostWebOrder(AInfo);
end;

function TServerMethods1.UpdateProduct: TJSONValue;
begin
  Result := DoCmd('UpdateProduct');
end;

function TServerMethods1.UpdDlvStatus: TJSONValue;
begin
  Result := DoCmd('UpdDlvStatus');
end;

function TServerMethods1.VerifyWebOrderPost(const AXmlString: string; var AOrderNum, AProcessor: string): Boolean;
var
  AXML: TJvSimpleXML;
  AXmlItem, AItem: TJvSimpleXMLElem;
begin
  AXML := TJvSimpleXML.Create(nil);
  try
    AXML.LoadFromString(AXmlString);
    AXmlItem := AXML.Root;
    AItem := AXmlItem.Items.ItemNamed['OrderID'];
    if Assigned(AItem) then
      AOrderNum := AItem.Value;
    AItem := AXmlItem.Items.ItemNamed['ProcessorCode'];
    if Assigned(AItem) then
      AProcessor := AItem.Value;
    Result := (AOrderNum <> '') and (AProcessor <> '');
  finally
    AXML.Free;
  end;
end;

function TServerMethods1.XMLNamedNodeValue(ANode: TJvSimpleXMLElem; const AName: string): string;
var
  I: Integer;
begin
  Result := '';
  for I := 0 to ANode.ItemCount - 1 do
    if SameText(ANode.Items[I].Name, AName) then
    begin
      Result := ANode.Items[I].Value;
      Break;
    end;
end;

end.
