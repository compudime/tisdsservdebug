unit ChangeListFile;

interface
uses
  SysUtils;

type
  EHandleInApp = class(Exception)
  end;

procedure FillProgChangeList_1;

implementation

uses AdsData, AdsTable, DataHelpersUnit{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

procedure FillProgChangeList_1;
var
  AQuery: TAdsQuery;
  function ReleaseToDate(AMinor, ARelease, ABuild: Integer): string;
  begin
    if AMinor = 5 then
    begin
      case ABuild of
        23:
          Result := '07/09/2019';
        22:
          Result := '06/12/2019';
        21:
          Result := '06/06/2019';
        20:
          Result := '05/29/2019';
        19:
          Result := '04/29/2019';
        18:
          Result := '04/08/2019';
        17:
          Result := '04/08/2019';
        16:
          Result := '04/08/2019';
        15:
          Result := '03/26/2019';
        14:
          Result := '03/13/2019';
        13:
          Result := '03/12/2019';
        12:
          Result := '03/05/2019';
        11:
          Result := '02/19/2019';
        10:
          Result := '02/12/2019';
        9:
          Result := '02/03/2019';
        8:
          Result := '01/22/2019';
        7:
          Result := '01/20/2019';
        6:
          Result := '01/09/2019';
        5:
          Result := '01/06/2019';
        4:
          Result := '01/02/2019';
        3:
          Result := '12/31/2018';
        2:
          Result := '12/16/2018';
        1:
          Result := '12/05/2018';
        0:
          Result := '10/31/2018';
      else
        Result := DateToStr(Date);
      end;
    end
    else if AMinor = 4 then
    begin
      case ABuild of
        25:
          Result := '10/30/2018';
        24:
          Result := '09/06/2018';
        23:
          Result := '09/04/2018';
        22:
          Result := '08/29/2018';
        21:
          Result := '08/29/2018';
        20:
          Result := '07/30/2018';
        19:
          Result := '07/12/2018';
        18:
          Result := '07/10/2018';
        17:
          Result := '07/06/2018';
        16:
          Result := '07/02/2018';
        15:
          Result := '06/25/2018';
        14:
          Result := '06/14/2018';
        13:
          Result := '06/05/2018';
        12:
          Result := '05/29/2018';
        11:
          Result := '05/22/2018';
        10:
          Result := '05/22/2018';
        9:
          Result := '05/13/2018';
        8:
          Result := '05/07/2018';
        7:
          Result := '04/24/2018';
        6:
          Result := '04/11/2018';
        5:
          Result := '04/10/2018';
        4:
          Result := '03/18/2018';
        3:
          Result := '03/16/2018';
        2:
          Result := '02/07/2018';
        1:
          Result := '01/12/2018';
        0:
          Result := '01/02/2018';
      else
        Result := DateToStr(Date);
      end;
    end
    else if AMinor = 3 then
    begin
      case ABuild of
        34:
          Result := '01/01/2018';
        33:
          Result := '12/14/2017';
        32:
          Result := '11/22/2017';
        31:
          Result := '11/03/2017';
        30:
          Result := '11/02/2017';
        29:
          Result := '09/27/2017';
        28:
          Result := '09/17/2017';
        27:
          Result := '09/07/2017';
        26:
          Result := '09/05/2017';
        25:
          Result := '08/31/2017';
        24:
          Result := '08/30/2017';
        23:
          Result := '08/16/2017';
        22:
          Result := '08/02/2017';
        21:
          Result := '07/10/2017';
        20:
          Result := '07/07/2017';
        19:
          Result := '07/06/2017';
        18:
          Result := '07/05/2017';
        {17:
          Result := '05/25/2017';
        16:
          Result := '05/11/2017';
        15:
          Result := '05/08/2017';
        14:
          Result := '03/26/2017';
        13:
          Result := '03/19/2017';
        12:
          Result := '03/16/2017';
        11:
          Result := '03/09/2017';
        10:
          Result := '03/07/2017';
        9:
          Result := '03/01/2017';
        8:
          Result := '03/01/2017';
        7:
          Result := '02/22/2017';
        6:
          Result := '02/14/2017';
        5:
          Result := '02/05/2017';
        4:
          Result := '02/02/2017';
        3:
          Result := '01/24/2017';
        2:
          Result := '01/24/2017';
        1:
          Result := '01/16/2017';
        0:
          Result := '01/05/2017';}
      else
        Result := DateToStr(Date);
      end;
    end
    {else if AMinor = 2 then
    begin
      case ABuild of
        51:
          Result := '12/30/2016';
        50:
          Result := '12/29/2016';
        49:
          Result := '12/27/2016';
        48:
          Result := '12/01/2016';
        47:
          Result := '11/29/2016';
        46:
          Result := '11/29/2016';
        45:
          Result := '11/23/2016';
        44:
          Result := '11/15/2016';
        43:
          Result := '11/15/2016';
        42:
          Result := '09/29/2016';
        41:
          Result := '09/04/2016';
        40:
          Result := '08/30/2016';
        39:
          Result := '08/11/2016';
        38:
          Result := '08/10/2016';
        37:
          Result := '07/12/2016';
        36:
          Result := '06/26/2016';
        35:
          Result := '06/20/2016';
        34:
          Result := '05/31/2016';
        33:
          Result := '05/16/2016';
        32:
          Result := '05/13/2016';
        31:
          Result := '05/12/2016';
        30:
          Result := '04/08/2016';
        29:
          Result := '03/16/2016';
        28:
          Result := '03/08/2016';
        27:
          Result := '02/17/2016';
        26:
          Result := '02/17/2016';
        25:
          Result := '02/16/2016';
        24:
          Result := '02/02/2016';
        23:
          Result := '01/04/2016';
        22:
          Result := '12/22/2015';
        21:
          Result := '11/19/2015';
        20:
          Result := '10/28/2015';
        19:
          Result := '10/21/2015';
        18:
          Result := '10/11/2015';
        17:
          Result := '08/17/2015';
        16:
          Result := '08/04/2015';
        15:
          Result := '07/20/2015';
        14:
          Result := '07/16/2015';
        13:
          Result := '07/07/2015';
        12:
          Result := '06/16/2015';
        11:
          Result := '06/15/2015';
        9:
          Result := '06/10/2015';
        8:
          Result := '05/19/2015';
        7:
          Result := '05/17/2015';
        6:
          Result := '04/22/2015';
        5:
          Result := '03/16/2015';
        4:
          Result := '02/24/2015';
        3:
          Result := '02/24/2015';
        1:
          Result := '02/10/2015';
        0:
          Result := '02/09/2015';
      else
        Result := DateToStr(Date);
      end;
    end
    else if AMinor = 1 then
    begin
      case ABuild of
        34:
          Result := '01/12/2015';
        33:
          Result := '01/06/2015';
        32:
          Result := '11/27/2014';
        31:
          Result := '11/24/2014';
        30:
          Result := '11/23/2014';
        29:
          Result := '11/10/2014';
        28:
          Result := '11/06/2014';
        27:
          Result := '10/29/2014';
        26:
          Result := '10/28/2014';
        25:
          Result := '10/24/2014';
        24:
          Result := '10/06/2014';
        23:
          Result := '09/29/2014';
        22:
          Result := '09/15/2014';
        21:
          Result := '09/10/2014';
        20:
          Result := '09/10/2014';
        19:
          Result := '08/31/2014';
        18:
          Result := '08/13/2014';
        17:
          Result := '08/06/2014';
        16:
          Result := '07/09/2014';
        15:
          Result := '06/24/2014';
        14:
          Result := '06/20/2014';
        12:
          Result := '05/21/2014';
        11:
          Result := '05/20/2014';
        10:
          Result := '05/11/2014';
        9:
          Result := '04/28/2014';
        8:
          Result := '04/27/2014';
        7:
          Result := '03/11/2014';
        6:
          Result := '02/24/2014';
        5:
          Result := '02/21/2014';
        4:
          Result := '02/12/2014';
        3:
          Result := '02/05/2014';
        2:
          Result := '02/02/2014';
        1:
          Result := '01/20/2014';
        0:
          Result := '01/06/2014';
      else
        Result := DateToStr(Date);
      end;
    end
    else
      Result := '08/31/2014';
    else if AMinor = 0 then
    begin
      case ABuild of
        295:
          Result := '12/09/2013';
        294:
          Result := '12/06/2013';
        293:
          Result := '12/05/2013';
        292:
          Result := '11/13/2013';
        291:
          Result := '10/15/2013';
        290:
          Result := '10/10/2013';
        289:
          Result := '10/09/2013';
        288:
          Result := '10/07/2013';
        287:
          Result := '09/08/2013';
        286:
          Result := '08/27/2013';
        284:
          Result := '08/26/2013';
        282:
          Result := '08/25/2013';
        281:
          Result := '08/07/2013';
        280:
          Result := '06/27/2013';
        279:
          Result := '06/25/2013';
        278:
          Result := '06/24/2013';
        277:
          Result := '06/20/2013';
        276:
          Result := '06/14/2013';
        275:
          Result := '06/13/2013';
        272:
          Result := '06/10/2013';
        271:
          Result := '06/10/2013'; // 271 wasn't released
        270:
          Result := '04/30/2013'; // 270, requires DataBase 1.61
        269:
          Result := '04/18/2013';
        268:
          Result := '03/20/2013';
        267:
          Result := '03/17/2013';
        266:
          Result := '03/12/2013';
        265:
          Result := '03/07/2013';
        264:
          Result := '02/19/2013';
        263:
          Result := '02/18/2013';
        262:
          Result := '02/17/2013';
        261:
          Result := '01/31/2013';
        260:
          Result := '01/20/2013';
        259:
          Result := '01/09/2013';
        258:
          Result := '12/17/2012';
          257:
          Result := '12/14/2012';
          256:
          Result := '12/03/2012';
          254:
          Result := '11/26/2012';
          253:
          Result := '11/15/2012';
          252:
          Result := '10/21/2012';
          251:
          Result := '10/16/2012';
          250:
          Result := '10/11/2012';
          249:
          Result := '09/12/2012';
          248:
          Result := '09/05/2012';
          247:
          Result := '09/04/2012';
          246:
          Result := '09/02/2012';
          245:
          Result := '08/27/2012';
          244:
          Result := '08/21/2012';
          243:
          Result := '08/20/2012';
          242:
          Result := '08/15/2012';
          241:
          Result := '08/06/2012';
          240:
          Result := '07/11/2012';
          239:
          Result := '07/04/2012';
          238:
          Result := '06/26/2012';
          237:
          Result := '06/18/2012';
          236:
          Result := '06/13/2012';
          235:
          Result := '06/04/2012';
          234:
          Result := '05/22/2012';
          233:
          Result := '05/17/2012';
          232:
          Result := '05/08/2012';
          231:
          Result := '05/08/2012';
          230:
          Result := '05/07/2012';
          229:
          Result := '05/07/2012';
          225:
          Result := '03/26/2012';
          224:
          Result := '03/04/2012';
          223:
          Result := '03/04/2012';
          222:
          Result := '02/27/2012';
          221:
          Result := '01/24/2012';
          220:
          Result := '01/23/2012';
          219:
          Result := '01/22/2012';
          218:
          Result := '01/19/2012';
          217:
          Result := '01/18/2012';
          216:
          Result := '01/18/2012';
          215:
          Result := '12/27/2011';
          214:
          Result := '12/14/2011';
          213:
          Result := '12/12/2011';
          212:
          Result := '12/12/2011';
          211:
          Result := '12/11/2011';
          210:
          Result := '12/08/2011';
          209:
          Result := '12/07/2011';
          208:
          Result := '12/01/2011';
      else
        Result := '08/27/2012';
      end;
    end;}
  end;
  procedure AddChangeList2(AType: string; AMinor, ARelease, ABuild: Integer; AInternal: Boolean;
    ADescription, ANote: string; AProgCat: string = 'General'; ASetupLevel: Integer = -1);
  var
    AReleaseDate: string;
  begin
    if Length(AType) > 15 then
      raise EHandleInApp.CreateFmt('Type "%s" is too long, max length is 15.', [AType]);

    if Length(ADescription) > 250 then
      raise EHandleInApp.CreateFmt('Setting description "%s" is too long, max length is 250.', [ADescription]);

    if Length(AProgCat) > 25 then
      raise EHandleInApp.CreateFmt('AProgCat "%s" is too long, max length is 25.', [AType]);

    AReleaseDate := ReleaseToDate(AMinor, ARelease, ABuild);
    AQuery.SQL.Add('Insert Into #ProgChanges');
    AQuery.SQL.Add('(ChangeType, Descrip, [Minor], [Release], [Build], [Internal], Note, ProgCat,');
    AQuery.SQL.Add('SetupLevel, [ReleaseDate])');
    AQuery.SQL.Add(Format('Values(%s, %s, %d, %d, %d, %s, %s, %s, %d, %s);', [QuotedStr(AType), QuotedStr(ADescription),
      AMinor, ARelease, ABuild, BoolToStr(AInternal, True), QuotedStr(ANote), QuotedStr(AProgCat), ASetupLevel,
      QuotedStr(AReleaseDate)]));
  end;

  {procedure AddChangeList(AType: string; ABuild: Integer; AInternal: Boolean; ADescription, ANote: string;
    AProgCat: string = 'General'; ASetupLevel: Integer = -1);
  begin
    AddChangeList2(AType, 0, 0, ABuild, AInternal, ADescription, ANote, AProgCat, ASetupLevel);
  end;}

const
  ctBugFix = 'Bug Fix';
  ctChange = 'Change';
  ctEnhancment = 'Enhancement';
  ctFeature = 'Feature';
  ctKnownIssue = 'Known Issue';

  slNull = -1;
  slNothing = 0;
  slSimple = 1;
  slComplex = 2;

  pcProducts = 'Products';
  pcCustomers = 'Customers';
  pcVendors = 'Vendors';
  pcCustomerOrders = 'Customer Orders';
  pcReports = 'Reports';
  pcLists = 'Lists';
  pcBrowse = 'Browse';
  pcTransaction = 'Transaction';
  pcGeneral = 'General';
  pcUserSetup = 'User Setup';
begin
  AQuery := DataHelpers.HelperQuery;

  AQuery.SQL.Clear;
  AQuery.SQL.Add('Try Drop Table #ProgChanges; Catch All End;');
  AQuery.SQL.Add('Create Table #ProgChanges (ChangeType Char(15), Descrip Char(250),');
  AQuery.SQL.Add('[Minor] Integer, [Build] Integer, [Release] Integer, [Internal] Logical, Note Memo,');
  AQuery.SQL.Add('ProgCat Char(25),SetupLevel Integer, [ReleaseDate] Date);');
  AQuery.ExecSQL;

  AQuery.SQL.Clear;
  {
    AddChangeList(ctBugFix, 48, True,
    'Adding new product thru vendor invoice after editing existing product might cause old product code to be overwritten in some instances', '');
    AddChangeList(ctBugFix, 48, True, 'Clicking X now closes program', 'Added IndexOf and Delete for FPushedModuleList');
    AddChangeList(ctBugFix, 48, True,
    'Editing Product code thru Vendor Invoice Item View, causes a lock error to be raised',
    'DoCascadeProductCodeChange attempted to change Prdmstr when record being posted');
    AddChangeList(ctFeature, 54, False, 'Enable emailing a Vendor PO Directly from PO screen',
    'Email address needs to be entered on vendor screen');
    AddChangeList(ctChange, 54, True, 'Email address at Customer and Vendor is now stored without "mailto:" prefix',
    'To fix old data, run: Update Venmstr Set email=substring(email,7,50) Where not email is null and substring(email,1,7)=''mailto:''; Update Customer Set email=substring(email,7,50) where not email is null and substring(email,1,7)=''mailto:''');
    AddChangeList(ctEnhancment, 54, False, 'Refresh Product screen info after doing a Cost Change', '');
    AddChangeList(ctEnhancment, 55, True, 'Layout Control save changes to database', '');
    AddChangeList(ctChange, 60, False,
    'Quick Delivery/Cost Change now requires to select Vendor before entering other data',
    'This change was required for handling properly the VenItemCode relationship');
    AddChangeList(ctEnhancment, 60, True, 'Scanning a Password card will now be ignored when not at Sign On screen', '');
    AddChangeList(ctEnhancment, 60, True,
    'Scanning a new product at Product List now removes the S08A that''s sent by Megallen scanners, and respects the CutPreCodeOnScan and CutCheckDigitOnScan settings.'
    , 'Previously these were only done when scanning at a Vendor Invoice');
    AddChangeList(ctEnhancment, 60, False,
    'Some enhancements in look and feel of User Control screen (sorting and filtering of columns)', '');
    AddChangeList(ctEnhancment, 60, False,
    'Added "Find User" button at User Setup screen, where password cards could be scanned and added if not found', '');
    AddChangeList(ctBugFix, 63, True, 'Fixed issue with applying Group Password Settings raising an error', '');
    AddChangeList(ctChange, 63, True, 'Bug Reports are now emailed to BugReports@CompuDime.com', '');
    AddChangeList(ctEnhancment, 64, True, 'Support for Passowrd Cards on Scanners that do not transmit Pre Digit', '');
    AddChangeList(ctFeature, 65, False, 'Setting to control column focus when editing Vendor Invoice',
    'Prog Prefs-> Vend Invoice-> VendInvColumnMove');
    AddChangeList(ctEnhancment, 66, True,
    'Support for MASTER Product, Customer and Vendor (for Customer use Tel 9999999999)', '');
    AddChangeList(ctFeature, 66, False, 'Copy Product option in View Product Screen', '');
    AddChangeList(ctBugFix, 68, True,
    'Fixed issue (since 66) of Vendor Invoice not being saved when pressing Update Price at Quick Delivery/Cost Change'
    , '');
    AddChangeList(ctEnhancment, 68, False, 'Update Small and Case Description when empty',
    'Prior it was updating only after editing Description.');
    AddChangeList(ctChange, 68, True, 'Program will not attempt copying update from AppPath when running Windows 98 OS',
    '');
    AddChangeList(ctBugFix, 68, True,
    'Fixed issue when editing CaseCode on existing item, error being raised with no message', '');
    AddChangeList(ctChange, 69, True,
    'Numeric out of range will now raise a user freindly box and will not send error reports', '');
    AddChangeList(ctChange, 69, True,
    '"Amount of Characters to Cut From Start of Scan When Adding Item" will now affect 11 and 7 digit codes as well',
    'Prior we handled only 13, 12 and 8 digit codes. Need to add support for 11 and 7 for scanners set not to submit Check-Digit but do submit Pre-Digit.');
    AddChangeList(ctEnhancment, 69, False,
    'Setting to set system to alert user when entering a duplicate vendor invoice # on same vendor',
    'Setting name ChkVndDup');
    AddChangeList(ctFeature, 70, False, 'ItemClass and Hechsher fields on Prduct file with drop down', '');
    AddChangeList(ctChange, 72, True,
    'qSales in ModuleCalcItemSales now uses TableType ttAdsDefault to work with ADT tables', '');
    AddChangeList(ctBugFix, 73, False,
    'Fixed issue when clicking View at Vendor Payment Register screen when no record is selected', 'IntToStr error');
    AddChangeList(ctEnhancment, 73, False, 'Support for reports to access ticket detail files together with POS app', '');
    AddChangeList(ctChange, 75, True, 'Auto change Mapped path in Ads.ini to UNC path', '');
    AddChangeList(ctEnhancment, 76, False, 'Allow scanning product code at code field on product edit screen', '');
    AddChangeList(ctFeature, 77, False, 'Added UnitCost to fields available as product info on bottom of vendor invoice',
    '');
    AddChangeList(ctEnhancment, 77, False,
    '"Quick Cost Change / Vendor Invoice" auto fills the last vendor and item code', '');
    AddChangeList(ctFeature, 77, False, 'Vendor Item List for all vendors combined',
    'Could be accessed thru the main vendor menu.');
    AddChangeList(ctEnhancment, 77, False,
    'Option on File menu to open and/or close scanner port. This will enable easier switching between the two programs'
    , '');
    AddChangeList(ctBugFix, 77, True, 'Vendor Invoice with discount, adjust cost to show original cost before discount',
    '');
    AddChangeList(ctBugFix, 78, True,
    'The "Canvas does not allow drawing in Windows 98" exception occurs when the application runs under Windows 98',
    'DevEx Build 45');
    AddChangeList(ctEnhancment, 79, False,
    'Report Prompts now force entry in upper case where applicable, so that proper info is returned', '');
    AddChangeList(ctChange, 79, False,
    'In "Quick Cost Change / Vendor Invoice" using Tab from Vendor selection will now work like enter', '');
    AddChangeList(ctChange, 79, False,
    'Auto selecting last vendor in "Quick Cost Change / Vendor Invoice" will now select vendors with no quantity if they have a cost.'
    , 'Previously only vendor invcoices with quantity were used.');
    AddChangeList(ctBugFix, 79, True,
    'List index out of bounds (-1), when doing a filter search on certain columns in Vendor Item List',
    'Issue caused by attempting a Locate on Lookup fields, we disable the search on those columns');
    AddChangeList(ctChange, 79, False, 'Option to Delete an Item from Vendor Item List for specific vendor', '');
    AddChangeList(ctEnhancment, 80, False, 'Added "View Program Changes" option', '');
    AddChangeList(ctEnhancment, 80, False,
    'Searching in a column of the Product, Vendor or Customer list automaticly sorts the active column', '');
    AddChangeList(ctBugFix, 80, True, 'Product List allow moving to list from search box with Shift-Tab shortcut', '');
    AddChangeList(ctEnhancment, 80, False,
    'Added keyboard shortcut (Ctrl-L) for Vendor Item List at View Vendor, also vendor name will now be displayed on top of screen', '');
    AddChangeList(ctFeature, 80, False,
    'Product List reports now allow to jump to Item Edit screen by clicking on a line in the report', '');
    AddChangeList(ctEnhancment, 81, True, 'Reports now use same conenction POS database as the complete program', '');
    AddChangeList(ctEnhancment, 81, False, 'Increased font size for all grids', 'From 8 to 10');
    AddChangeList(ctEnhancment, 82, False, 'Themed Display', '');
    AddChangeList(ctChange, 82, False, 'Highlight Amount column in Vendor Invoice', '');
    AddChangeList(ctChange, 82, False, 'Bold Summary row in Vendor Invoice', '');
    AddChangeList(ctBugFix, 82, True,
    'When using Column Move feature in Vendor Invoice, will now append new line only when coming from last line', '');
    AddChangeList(ctEnhancment, 82, False, 'Added more Product Actions on Product List View', '');
    AddChangeList(ctEnhancment, 82, False, 'Save & New option for Vendor Invoice', '');
    AddChangeList(ctChange, 82, True, 'Changed Shortcut for View from Ctrl-V to Ctrl-O', '');
    AddChangeList(ctChange, 83, False, 'Added WIC field on Product Edit View', '');
    AddChangeList(ctChange, 83, False, 'Added Customer List, Edit and Transaction View', '');
    AddChangeList(ctChange, 83, True, 'Calc item sales now resets date to today OnShow', '');
    AddChangeList(ctEnhancment, 83, False, 'Option to set Vendor Invoice to prompt Update Price on Cost Change', '');
    AddChangeList(ctBugFix, 83, True,
    'Changing Vendor Item code of exisiting line in Vendor Invoice now updates all product information', '');
    AddChangeList(ctChange, 83, True, 'Changed Shortcut for View from Ctrl-V to Alt-V', '');
    AddChangeList(ctBugFix, 83, True, 'Issue when loading Generate PO View', '');
    AddChangeList(ctEnhancment, 84, False, 'Added Product Sales Summary option at Vendor PO View', '');
    AddChangeList(ctEnhancment, 84, True, 'Support for multi EPSGrid options', '');
    AddChangeList(ctChange, 85, True, 'Calc item sales now set date to Date-30 in OnCreate and uses edited date OnShow',
    '');
    AddChangeList(ctChange, 85, False, 'Changed Shortcut for View from Ctrl-V to Ctrl-E', '');
    AddChangeList(ctChange, 85, True, 'Option to disable skins', '');
    AddChangeList(ctEnhancment, 86, False, 'Copy Product now also copies the additinal info (such as ingredients)', '');
    AddChangeList(ctBugFix, 86, True,
    'Items could now be deleted from Vendor Item List when viewing items for specidfic vendor', '');
    AddChangeList(ctBugFix, 86, True, 'Vendor Item Code Relationship issue introduced in version 83', '');
    AddChangeList(ctEnhancment, 87, False, 'Option to Insert line in middle of Vendor Invoice', '');
    AddChangeList(ctEnhancment, 87, False, 'When deleting a line in a Vendor Invoice, the line numbers are now reset',
    '');
    AddChangeList(ctBugFix, 87, True, 'Support for Product Scans with leading zeros and check digit (BcHandle 371-372)',
    '');
    AddChangeList(ctEnhancment, 88, False,
    'Shortcut key Ctrl-Enter now works on UpcCode and VendorItemCode columns in Vendor Invoice to get Item Lists', '');
    AddChangeList(ctBugFix, 88, True,
    'Double Click on Product or Customer in report should open item even list is filtered', '');
    AddChangeList(ctBugFix, 88, False,
    'Keyboard shortcuts for buttons now work on popup screens the same as on full screens', '');
    AddChangeList(ctChange, 88, True, 'Program version is now shown on Help->About instead of File->Version option', '');
    AddChangeList(ctChange, 88, False,
    'Vendor Item Code Relation prompt to replace relationship when code is assigned to different product', '');
    AddChangeList(ctBugFix, 89, True, 'Some abnormal behaviour in Product Cost Change', '');
    AddChangeList(ctBugFix, 90, True, 'GenericTextPrinter not found due to option to specify label printer feature', '');
    AddChangeList(ctBugFix, 91, True, 'Fix tab order on Product Price Update screen', '');
    AddChangeList(ctChange, 91, False,
    'Option to specify Label Printer (other than GenericTextPrinter) for Product Labels', '');
    AddChangeList(ctChange, 91, True,
    'Removed auto handling PageDown as mrOK on modal forms, and set only for Product Price Update',
    'Now needs to be specificly set by specifying mbYesToAll in AButtons param');
    AddChangeList(ctChange, 92, True, 'OnKeyDown event for Modules', '');
    AddChangeList(ctChange, 92, True,
    'Handle Enter on Filter Dates in Browse and CalcSales screens to move to next date or Refresh button', '');
    AddChangeList(ctChange, 92, True,
    'Main form is now set to actual height and width of Screen. WorkArea vs. Maximized', '');
    AddChangeList(ctChange, 92, True, 'MadExcept to include only minimal debug info', '');
    AddChangeList(ctChange, 93, True, 'Stopped some memory leaks including 2 that were added for each label or report',
    '');
    AddChangeList(ctChange, 94, False, 'Added sound support in vendor invoice and product scanning', '');
    AddChangeList(ctChange, 94, True,
    'Vendor Invoice will prompt Update Price on Cost Change (if set) when pressing OK.'
    , 'The old way it was auto saving the invoice on cost change even if user later pressed Cencel.');
    AddChangeList(ctChange, 94, False, 'Option to set Cost Change not to show in Vendor Invoices List', '');
    AddChangeList(ctEnhancment, 94, True, 'Updated menu and button Images', '');
    AddChangeList(ctBugFix, 94, True,
    'Fixed issue that allowed inserting duplicate products with filter set for product list', '');
    AddChangeList(ctEnhancment, 94, False,
    'Button at Case Price on Product View to auto calculate Case Price (multiplies regular price by units per case)',
    '');
    AddChangeList(ctBugFix, 95, True, 'Version 91 always enabled all Modules, even those that are not set in ModuleList',
    '');
    AddChangeList(ctEnhancment, 96, False, 'Added keyboard shortcuts to move between groups in Product Edit View', '');
    AddChangeList(ctEnhancment, 96, True,
    'Report Viewer now auto sets focus to report and allows using mouse wheel without first clicking on report area',
    '');
    AddChangeList(ctBugFix, 96, True,
    'Fixed issues that allowed unauthorized users to edit Groups and Protected Areas in User Setup', '');
    AddChangeList(ctBugFix, 97, True, 'Product Edit View screen layout got messed up', '');
    AddChangeList(ctBugFix, 98, True,
    'Starting new PO now auto sets Status to New so that PO could be saved without user setting status', '');
    AddChangeList(ctBugFix, 98, True,
    'Fixed issue when OnInvoiceDetailCostChange crashed with message "Could not convert variant of type (Null) into type (Currency)"', '');
    AddChangeList(ctChange, 98, False, 'Prevent adding Vendor with no name', '');
    AddChangeList(ctEnhancment, 99, False, 'Predefined date scope box for Reports using dates', '');
    AddChangeList(ctEnhancment, 99, False, 'Predefined date scope box for Browse Invoice and PO Lists', '');
    AddChangeList(ctEnhancment, 99, False,
    'Added ability to export report to PDF, by clicking PDF button in report preview screen', '');
    AddChangeList(ctBugFix, 99, True,
    'Fixed an issue with some reports templates that had the AllowPrintToFile property set to True', '');
    AddChangeList(ctFeature, 100, False,
    'Memorized Reports feature. (To setup a Memorized Report contact Computer Dimensions)', '');
    AddChangeList(ctBugFix, 101, True,
    'Fixed issue that raised "Error 5035: The requested lock could not be granted", when using "Insert Line" in a Vendor Invoice in some circumstances', '');
    AddChangeList(ctChange, 101, True, 'Added Confirm dialog when deleting a Vendor', '');
    AddChangeList(ctChange, 101, True,
    'Deleting a Vendor now cascades delete to DelItm, Delvry, VenItm, VHist, VCashPay, VendPoDt, VendPoHd and VPmtDist'
    , '');
    AddChangeList(ctBugFix, 101, True,
    'Fixed issue with low resultion screens showing less than expected on report queries containing multi advance button'
    , '');
    AddChangeList(ctEnhancment, 102, False,
    'Report exports to Excel now export numeric and currency columns as excel numerics', '');
    AddChangeList(ctChange, 104, True, 'Updated to DevEx Release 47 and ExpressLayout Version 2', '');
    AddChangeList(ctBugFix, 104, True, 'Fixed error when running report with very long rtm file name', '');
    AddChangeList(ctBugFix, 104, True, 'Fixed issue causing an Access Violation when quiting application',
    'ParamList.Free caused an AV, Free is already called in TppParameterList.Destroy (with nil)');
    AddChangeList(ctEnhancment, 104, False, 'Speed Up Application Startup Time',
    'Removed call to GenHelpers.PublicIP which could take very long');
    AddChangeList(ctBugFix, 104, False,
    'Fixed issue that sometimes prevented saving a Vendor Invoice if program open for several days', '');
    AddChangeList(ctBugFix, 104, False, 'Double Click on a line in Delivery List opens selected Vendor Invoice', '');
    AddChangeList(ctBugFix, 104, False, 'Double Click on an item in Vendor Item List opens selected Item', '');
    AddChangeList(ctChange, 105, True, 'All tables converted to ADT and part of DD', '');
    AddChangeList(ctChange, 105, False, 'Program now requires Advantage DLL version 9.1', '');
    AddChangeList(ctChange, 105, False, 'Product Edit now allows entering Bottle Deposit values more than 9', '');
    AddChangeList(ctChange, 106, True, 'Removed 2007 from Application Title', '');
    AddChangeList(ctChange, 106, True, 'Data Structure is now auto updated by Tis01', '');
    AddChangeList(ctChange, 106, True,
    'Program now checks for Database Version at startup and requires proper version of Register program to be installed'
    , '');
    AddChangeList(ctEnhancment, 106, False, 'Added Bill Due Date on Vendor Invoice', '');
    AddChangeList(ctEnhancment, 106, False, 'Added Memo on Customer Order', '');
    AddChangeList(ctEnhancment, 106, False, 'Added Memo on Vendor Invoice', '');
    AddChangeList(ctEnhancment, 106, False, 'Added bill totals and paid info on Vendor Invoice', '');
    AddChangeList(ctChange, 106, False, 'Added Pay Bill option on Vendor Invoice', '');
    AddChangeList(ctBugFix, 107, True, 'Divide by Zero in TVendHelpers.AmountBeforeDiscount', '');
    AddChangeList(ctBugFix, 107, True,
    'Customer view screen now shows the correct captions for aging fields when using custom ageing settings', '');
    AddChangeList(ctBugFix, 108, True, 'Fixed some issues with TcxLookupCombo caused by DevEx bug in 47', '');
    AddChangeList(ctBugFix, 108, True,
    'Fixed BillDueOn bug (field name is BillDue and new vendor invoice was referrring to BillDueOn', '');
    AddChangeList(ctEnhancment, 108, False, 'Added Register Transaction View', '');
    AddChangeList(ctEnhancment, 109, False, 'Save & New option for Cutomer Order', '');
    AddChangeList(ctChange, 109, False, 'Added sound support in Customer Order', '');
    AddChangeList(ctBugFix, 110, True, 'Fixed an issue that prevented saving new items without a CaseCode', '');
    AddChangeList(ctChange, 110, False,
    'Transaction View now includes a Print Report button to print transaction as report', '');
    AddChangeList(ctBugFix, 111, True,
    'Fixed issue that prevented saving a Copied Product if additional product info (PrdInfo) is not used', '');
    AddChangeList(ctChange, 112, False,
    'Transaction View now includes Sales done on cash customer account (111) 111-1111', '');
    AddChangeList(ctEnhancment, 112, False, 'Added option to edit Customer Credit Card Info on Customer View', '');
    AddChangeList(ctEnhancment, 112, False, 'Added Next and Previous options on Customer View', '');
    AddChangeList(ctFeature, 112, False, 'Added Season field to Product file and Reports', '');
    AddChangeList(ctBugFix, 112, True,
    'Speed up loading report query screen and statments by reducing amount of times we check if Tmpcalc etc are database files', '');
    AddChangeList(ctChange, 112, True,
    'Register Transaction List now shows only one date box enabling to select only one date and not a scope (for speed)'
    , '');
    AddChangeList(ctChange, 113, True, 'Fixed issue that prevented ReExecute for GridView after running a Print once',
    '');
    AddChangeList(ctEnhancment, 113, False, 'Option to Hide Memo And Summary on Vendor Invoice View',
    'Status is stored per computer');
    AddChangeList(ctFeature, 113, False, 'General Task List with Reminders.', 'Requires Tis01 Ver 1655');
    AddChangeList(ctEnhancment, 113, False, 'Task List with Reminders on Customer, Vendor and Product', '');
    AddChangeList(ctBugFix, 114, True, 'Fixed program behaviour when clicking Exit on File menu', '');
    AddChangeList(ctBugFix, 115, True, 'Register Transaction List was selecting Date>= rather than Date= for From Date',
    '');
    AddChangeList(ctFeature, 115, False, 'Added View Tickets with Sales for Product', '');
    AddChangeList(ctEnhancment, 115, False,
    'Quick Delivery/Cost Change could now be used when viewing Product thru Report', '');
    AddChangeList(ctBugFix, 116, True, 'Fixed AV when double clicking Customer in report',
    'DataSet is not yet assiggned in UpdateActionState');
    AddChangeList(ctBugFix, 117, True, 'Allow clearing Credit Card Info at customer', '');
    AddChangeList(ctEnhancment, 117, True, 'Mask for Ticket ID & Tel in Transactin Browse and View', '');
    AddChangeList(ctBugFix, 117, True,
    'Fixed recently introduced bug that caused Pushed screens not to reappear in some instances', '');
    AddChangeList(ctBugFix, 117, True, 'Fixed issue that prevented exporting a Customer Statement to PDF', '');
    AddChangeList(ctEnhancment, 117, False, 'Added option to print Customer Statement from Customer View screen', '');
    AddChangeList(ctFeature, 117, False,
    'Added option to email Customer Statement when customer email address is set for customer, by pressing the Email icon in Statement Preview. (May requires additional installation contact support)', '');
    AddChangeList(ctEnhancment, 118, True, 'Show Hour Glass while emailing Report', '');
    AddChangeList(ctBugFix, 118, True, 'Put back Entered By and Sales rep on Vendor Invoice', '');
    AddChangeList(ctBugFix, 119, True,
    'Fixed AV when user with no Task rights clicked on Task icon at Customer, product or Vendor.', '');
    AddChangeList(ctChange, 119, True, 'Changed Print, Preview icons to Image only', '');
    AddChangeList(ctChange, 119, True, 'Changed Print Labels button to say only Lables', '');
    AddChangeList(ctChange, 120, True, 'Removed "Task for" from default EventName in Tasks for ViewType', '');
    AddChangeList(ctBugFix, 120, True,
    'Fixed issue that prevented regular Vendor Invoices from showing in Vendor Invoice list when set to hide Quick Invoices', '');
    AddChangeList(ctEnhancment, 120, False, 'Enabled custom fields on Customer Edit View', '');
    AddChangeList(ctBugFix, 120, True,
    'Fixed issue that caused an AV when deleting a product at Product View when accessed thru Vendor Invoice View', '');
    AddChangeList(ctBugFix, 121, True,
    'Changed UpdateVendBalanceEtcPrepare to test with Select rather than Create to prevent 7041 when physical tmp file was deleted', '');
    AddChangeList(ctBugFix, 121, True, 'Fixed issue with scanning or adding product with code starting 99', '');
    AddChangeList(ctEnhancment, 121, True, 'Support for AlwaysPrompt option in Memorized Reports', '');
    AddChangeList(ctBugFix, 122, True, 'Recalculate totals when deleting line in Vendor Invoice', '');
    AddChangeList(ctEnhancment, 124, True, 'Allow setting RequiredRight paramter in Memorized Report', '');
    AddChangeList(ctChange, 125, False, 'Register Transaction View now includes moved details', '');
    AddChangeList(ctEnhancment, 125, False, 'Added Date Scope box to Calculate Product Sales Summary', '');
    AddChangeList(ctEnhancment, 125, False, 'Option to set default Date Scope value for Calculate Product Sales Summary',
    '');
    AddChangeList(ctBugFix, 128, True, 'Fixed Time input for Sale Begins and Expired', '');
    AddChangeList(ctBugFix, 129, True,
    'Fixed issue that could cause all customers to be deleted when deleting one customer', 'CustID is null');
    AddChangeList(ctBugFix, 129, True,
    'Fixed issue that occasionally caused the wrong item to be entered in Vendor Invoice when using the mouse to scroll and select from Product List', '');
    AddChangeList(ctBugFix, 130, False, 'Added Password Protect options for Customer List, Edit and Transaction List',
    '');
    AddChangeList(ctEnhancment, 131, False,
    'Program now respects same Database Server Type settings as Register Program (Tis01)', '');
    AddChangeList(ctEnhancment, 132, False, 'New Look & Feel for Grid View Preview and Printing', '');
    AddChangeList(ctEnhancment, 132, False, 'Option to save Grid View Preview to PDF', '');
    AddChangeList(ctBugFix, 132, True,
    'Fixed issue that caused Item Code to sometimes be empty when selecting Cancel on the "Select Desired Vendor Item Code Relation" view', '');
    AddChangeList(ctChange, 132, False,
    'Acces to Product Actions from Product List, now require access to Product Edit screen',
    'Labels are not blocked by this');
    AddChangeList(ctEnhancment, 133, False, 'Enabled custom fields on Product Edit View', '');
    AddChangeList(ctBugFix, 133, True,
    'Fixed issue when entering a new item in vendor invoice and Prefernces is set to Prompt to Update Price',
    'Could not convert variant of type (Null) into type (Currency)');
    AddChangeList(ctFeature, 134, False, 'Support for price Level 2 on Product Edit View', '');
    AddChangeList(ctEnhancment, 134, False, 'Enhanced layout Margin / Markup section on Product Edit View', '');
    AddChangeList(ctEnhancment, 134, False, 'Support for Category additinal info', '');
    AddChangeList(ctEnhancment, 135, False,
    'Added "Product View" and "Product Edit" password rights to allow viewing details with no editing rights', '');
    AddChangeList(ctEnhancment, 136, True, 'Enhanced look and feel for check box picker', 'TcdCheckBoxGrid');
    AddChangeList(ctFeature, 136, False, 'Ability to add items to open Vendor PO instead of creating new one', '');
    AddChangeList(ctFeature, 136, False,
    'Ability to filter Generate PO View and create Vendor PO only for items in filter', '');
    AddChangeList(ctChange, 136, False, 'System now checks for valid User ID on startup', '');
    AddChangeList(ctChange, 136, False,
    'Database Username is now dependent on User ID, (may require to run Tis01 [1787] once', '');
    AddChangeList(ctChange, 136, True,
    'Added EdxPrintDevice class errors to Simple Error list so user will get "No Default Printer" etc. messages', '');
    AddChangeList(ctEnhancment, 136, True, 'Option to use Scan Simulator on main menu', '');
    AddChangeList(ctEnhancment, 136, False,
    'Double Clicking on an Event in the Reminder View now opens the event for editing', '');
    AddChangeList(ctBugFix, 136, True, 'Added DebugInventoryChanges Option', '');
    AddChangeList(ctBugFix, 137, False, 'Respect custom fields read-only option, (Product and Customer Edit)', '');
    AddChangeList(ctFeature, 138, False,
    'To be Printed checkbox added for Customer Orders with column in Customer Orders List.',
    'Checkbox gets auto checked on new orders and cleared when printing order');
    AddChangeList(ctChange, 138, True, 'Grids now display 2 digits for year not 4', '');
    AddChangeList(ctBugFix, 138, True, 'Fixed issue with DebugInventoryChanges recording date as dd/mm instead if mm/dd',
    '');
    AddChangeList(ctFeature, 139, False,
    'Support to scan Application Identifier (AI) BarCodes in Vendor Invoice (Setting)', '');
    AddChangeList(ctFeature, 140, False,
    'Search box to search for item within Vendor Invoice, Vendor PO and Customer Order', '');
    AddChangeList(ctEnhancment, 140, False, 'Added Produce Origin field in Product Edit View', '');
    AddChangeList(ctChange, 141, True, 'Changed back grids to display 4 digits for year', '');
    AddChangeList(ctEnhancment, 141, False, 'Update Product Pricing now diplays the U/P/C next to Cost info', '');
    AddChangeList(ctFeature, 141, False, 'Import PO to Vendor Invoice Feature, (requires Tis01 [1968])', '');
    AddChangeList(ctFeature, 142, False, 'Support for Carton Codes in Vendor Invoice and Product Lookup', '');
    AddChangeList(ctEnhancment, 142, False, 'Added "Add New Category" password right', '');
    AddChangeList(ctBugFix, 143, True, 'Error on first use when accessing Transaction View thru Customer Report', '');
    AddChangeList(ctBugFix, 143, True, '"is not a valid date" error when emailing statments', '');
    AddChangeList(ctBugFix, 144, True, 'Issue with new reports not getting registered in Protected list', '');
    AddChangeList(ctEnhancment, 144, True, 'Reports now get registered in Protected list with Cat and SubCat', '');
    AddChangeList(ctEnhancment, 144, True, 'New Look & Feel for Password Setup', '(Now using LayoutControl)');
    AddChangeList(ctEnhancment, 144, False, 'Support for Price Level 2 on Update Product Pricing View', '');
    AddChangeList(ctEnhancment, 144, True, 'DD Admin user logon info could now be used to login in application', '');
    AddChangeList(ctChange, 144, False,
    'Pricing button on "Vendor Quick Cost Change View" now prompts to save cost change', '');
    AddChangeList(ctChange, 145, True, 'Statments should not use Custom Report Viewer', '');
    AddChangeList(ctBugFix, 146, True,
    'Fixed Allow setting RequiredRight paramter in Memorized Report. Also set to allow no Logon for -2 of RequiredRight'
    , '');
    AddChangeList(ctChange, 148, False,
    'Product info entered with negative amount in Vendor Invoice (for Vendor Returns) do not get recorded to Product file', '');
    AddChangeList(ctChange, 149, True, 'Program Preferences menu is now always visible when logged in as Dev', '');
    AddChangeList(ctChange, 149, False,
    'When "Prompt Update Price on Cost Change" is set, program will now prompt even for new product with no previous cost', '');
    AddChangeList(ctEnhancment, 150, False,
    'When negative quantity is entered in Vendor Invoice program now prompts for Unit or Case, (cost and U/P/C are adjusted accordingly)', '');
    AddChangeList(ctBugFix, 151, True,
    'Fixed issue when pressing BackSpace while searching Product by Code in Product List', '');
    AddChangeList(ctBugFix, 151, True,
    'Fixed issue that prevented saving Vendor Payment when no Vendor Invoice was done in same session', '');
    AddChangeList(ctEnhancment, 152, False,
    'Option to set "Default Price Calculation Mode" to Margin (vs. always starting with Markup)', '');
    AddChangeList(ctChange, 152, False,
    'New Customer now respects the DOS setting for Default Credit Limit and Restricted Payment Types', '');
    AddChangeList(ctEnhancment, 152, False, 'Option to view Product List of all Group Code Members from Parent', '');
    AddChangeList(ctBugFix, 153, True,
    'Fixed issue that caused with no PO created yet to be removed from list when creating PO with filter', '');
    AddChangeList(ctChange, 153, False, 'Added No Combine option in Product Edit Screen (were applicable)', '');
    AddChangeList(ctBugFix, 153, True, 'Fixed issue that prevented connection to RepDefs when port specified in Alias',
    '');
    AddChangeList(ctEnhancment, 154, True, 'Report Builder version 12.03', '');
    AddChangeList(ctFeature, 155, False, 'More Report Drill Down Options Added', '');
    AddChangeList(ctEnhancment, 155, True, 'IsAdminUser now has F Recht same length as Password Recht field', '');
    AddChangeList(ctChange, 155, True, 'New Licensing Method', '');
    AddChangeList(ctBugFix, 155, True, 'Fixed Tax Exempt entry on Customer View', '');
    AddChangeList(ctFeature, 155, False, 'Added Sale Begin and End options for each Sale Type separately',
    '(requires Tis01 [2440])');
    AddChangeList(ctBugFix, 155, True,
    'Fixed issue that in some instances caused unpredicted results when using the "Insert Line" option in Vendor Invoice'
    , '');
    AddChangeList(ctEnhancment, 156, False,
    'Adjust Inventory now allows input of decimal values if current Stock level contains a decimal value', '');
    AddChangeList(ctBugFix, 156, True, 'Fixed issue that prevented reports from showing in User Setup list', '');
    AddChangeList(ctBugFix, 156, True, 'Fixed issue with Filtering in lists (Unicode)', '');
    AddChangeList(ctEnhancment, 156, False, 'Support controling column focus when editing Vendor Invoice when scanning',
    '');
    AddChangeList(ctFeature, 156, False, 'Setting to control column focus when editing Customer Order',
    'Prog Prefs-> Vend Invoice-> CustInvColumnMove');
    AddChangeList(ctEnhancment, 156, False, 'Button at code column in Customer Order to select from Product List', '');
    AddChangeList(ctEnhancment, 156, False, 'Support for Scanner in Customer Order', '');
    AddChangeList(ctBugFix, 156, True, 'Fixed issue with vendor Invoice Remakrs (and other header info) not saving', '');
    AddChangeList(ctBugFix, 157, True, 'Fixed Issue with printing Bartender Sheets (Unicode)', '');
    AddChangeList(ctBugFix, 158, True, 'Fixed Issue with Com Port settings on New W/S (Delphi 2010 CPort)', '');
    AddChangeList(ctBugFix, 158, True, 'Fixed Issue with recording price change dates when done thru Vendor Invoice', '');
    AddChangeList(ctBugFix, 159, True, 'Fixed Issue with filters in Lists using a '' (Unicode)', '');
    AddChangeList(ctEnhancment, 159, False, 'Added begin and expire on Price Update view', '');
    AddChangeList(ctBugFix, 160, True, 'Fixed issue with loading certain memorized reports (Unicode)', '');
    AddChangeList(ctEnhancment, 160, True, 'Support for multi components on same line in More Info View', '');
    AddChangeList(ctEnhancment, 161, False, 'Enhanced Task List speed', '');
    AddChangeList(ctBugFix, 161, True,
    'Fixed issue that sometimes prevented opening a Task by double clicking in reminder list', '');
    AddChangeList(ctBugFix, 162, True, 'Issue with Memorized Reports not Loading properly (JvSimpleXML)', '');
    AddChangeList(ctBugFix, 163, True, 'Fixed issue with Scanner Registry info being reset', '');
    AddChangeList(ctEnhancment, 164, False, 'Add copy sale dates option on Product View and Update Price View', '');
    AddChangeList(ctBugFix, 165, True, 'Restore lost List Filters', '');
    AddChangeList(ctEnhancment, 166, False, 'Added Print option on View Group Code List', '');
    AddChangeList(ctEnhancment, 166, True, 'Fixed Width of BrowseGrid on first popup', '');
    AddChangeList(ctFeature, 168, False, 'Product Mix and Match entry', '');
    AddChangeList(ctChange, 168, False,
    'Shelf Sheets data files are no more stored in C:\, instead we use the program folder (Win 7)', '');
    AddChangeList(ctEnhancment, 168, False, 'Display warnings when updating Prices on Mix & Match Group members', '');
    AddChangeList(ctEnhancment, 169, False, 'Added print & preview option in Product Mix and Match entry', '');
    AddChangeList(ctBugFix, 169, True, 'Product Mix and Match now properly updates sale begin and end dates', '');
    AddChangeList(ctBugFix, 170, True, 'Fixed issue when entering search item in Customer Order', '');
    AddChangeList(ctBugFix, 171, True, 'Fixed issue that prevented manualy updating inventory on certain items',
    'Items not yet in OnHand table and a Null value exists in OnHand table');
    AddChangeList(ctFeature, 173, False, 'Import Items by Vendor, Category or Brand into Vendor Invoice', '');
    AddChangeList(ctBugFix, 173, True, 'Fixed calculation issue on some reports.', 'Report.InitializeParameters');
    AddChangeList(ctChange, 174, False,
    'Site Info (Name, Address, City, State, Zip and Tel are now set in Program Prefrences',
    'RepHead.ini values are ignored');
    AddChangeList(ctChange, 174, False, 'BarTender Engine Path is now set in Program Prefrences', '');
    AddChangeList(ctEnhancment, 174, False, 'Ability to have Password protection assigned to each Memorized Report', '');
    AddChangeList(ctBugFix, 175, True, 'Fixed issue with storing Prior Price after price changes', '');
    AddChangeList(ctBugFix, 175, True, 'Fixed View Invoices button at Customer List & View', '');
    AddChangeList(ctEnhancment, 175, False, 'Shortcut keys on Mix & Match View to check all Regular or Sale Prices', '');
    AddChangeList(ctEnhancment, 175, False, 'Import Items into Vendor Invoice, now imports in order selected', '');
    AddChangeList(ctFeature, 175, False, 'Enhanced search options in Product, Customer and Vendor lists', '');
    AddChangeList(ctEnhancment, 175, False, 'Added right click context menu on all Lists and Browse Views', '');
    AddChangeList(ctEnhancment, 175, False, 'Added right click context menu in Vendor Invoice', '');
    AddChangeList(ctFeature, 175, False, 'Arrows to switch lines in Vendor Invoice', '');
    AddChangeList(ctFeature, 175, False,
    'Enhanced search capabilities in Product, Customer and Vendor lists. Use Filter Panel (activate it by pressing Ctrl-F) or type in Filter Row starting with a +, such as +GLAD or use % for partial word searches', '');
    AddChangeList(ctBugFix, 175, True, 'Fixed issue with saving Custom Form Layout in Design Mode', '');
    AddChangeList(ctChange, 175, True,
    'Custom Form Layout Action is now active only if UserIsAdmin and Design Paramater', '');
    AddChangeList(ctFeature, 176, False, 'Open Product View from Register Transaction View', '');
    AddChangeList(ctFeature, 176, False, 'Global Product Changes from Product List', '');
    AddChangeList(ctFeature, 176, False,
    'Enter Vendor Item Code or Upccode in Vendor Invoice with % or + and select from matching item list', '');
    AddChangeList(ctFeature, 177, False, 'Open Product View from Mix & Match Group View', '');
    AddChangeList(ctFeature, 177, False, 'Option to print Product Labels from Mix & Match Group View', '');
    AddChangeList(ctFeature, 177, False, 'Merge Product Option', '');
    AddChangeList(ctFeature, 177, False, 'Merge Customer Option', '');
    AddChangeList(ctEnhancment, 178, False, 'Add Merged Customer & Product to Action Log', '');
    AddChangeList(ctChange, 178, True, 'After Customer & Product Merge move to new Item', '');
    AddChangeList(ctEnhancment, 179, False, 'Added Min Required for Discount to Update Price View', '');
    AddChangeList(ctEnhancment, 179, False, 'Drop down for Measure in Product Edit', '');
    AddChangeList(ctEnhancment, 179, False,
    'At Product View if category is not in list and user selects not to add it, Category is replaced with prior value.'
    , '');
    AddChangeList(ctBugFix, 179, True, 'Do not allow negative on Bottle Deposit in Product View', '');
    AddChangeList(ctBugFix, 179, True, 'Add some missing fields in Global Changes', '');
    AddChangeList(ctFeature, 180, False, 'Ability to show Product lookup values in Vendor Invoice on same line', '');
    AddChangeList(ctKnownIssue, 180, False,
    'When viewing Product values on Vendor Invoice line, some changed values may not reflect on line till after invoice is saved', '');
    AddChangeList(ctEnhancment, 180, False, 'Ability to Tab Thru on Filter Row in Lists', '');
    AddChangeList(ctEnhancment, 180, False, 'Record Price Changes to Log on all price change options', '');
    AddChangeList(ctBugFix, 180, True, 'Fixed Windows 7 issue with popup screens and task edit', '');
    AddChangeList(ctBugFix, 180, True, 'Fixed issue with printing muliple Shelf Sheets from Vendor Invoice', '');
    AddChangeList(ctBugFix, 181, True, 'Fixed issue that was hiding MixMatch warning on Product View Edit', '');
    AddChangeList(ctEnhancment, 181, False, 'Added Price Change option on Mix & Match View', '');
    AddChangeList(ctFeature, 182, False, 'Option to set Auto Logout on idle', '');
    AddChangeList(ctFeature, 182, False,
    'Muliple open report Tabs with option to Reload or Open New tab from existing one', '');
    AddChangeList(ctEnhancment, 182, False,
    'Grid Views from Reports now support Double Clicking to open Product or Customer View', '');
    AddChangeList(ctBugFix, 182, True, 'Fixed issue with search string that contains an apostrophe ('')', '');
    AddChangeList(ctEnhancment, 182, False,
    'Option to print sheets immediately for Product Sheets that print multiple per page', '');
    AddChangeList(ctKnownIssue, 182, True,
    'Printing a Vendor Invoice (or other form based reports) closes the active edit view', '');
    AddChangeList(ctEnhancment, 182, False,
    'Option to print Product Labels from Product List View for all items in list', '');
    AddChangeList(ctBugFix, 182, True, 'Fixed issue with printing Shelf Sheets with nore than one item per sheet', '');
    AddChangeList(ctBugFix, 184, True, 'Fixed issue with New/Copy Prducts and Customers', '');
    AddChangeList(ctBugFix, 185, True, 'Fixed issues with Cost Change View', '');
    AddChangeList(ctBugFix, 186, True, 'Fixed issue with setting a Logical value to False in Global Changes', '');
    AddChangeList(ctBugFix, 186, True, 'Fixed issue with emailing reports (SSL library 1.0.0.4 required)', '');
    AddChangeList(ctEnhancment, 187, False, 'Added option to "Clear ALL Sale Prices" in Global Changes', '');
    AddChangeList(ctBugFix, 187, True, 'Fixed issue with some instances of saving Mix & Match entry', '');
    AddChangeList(ctBugFix, 187, True,
    'Fixed issue that prevented the OnClose event from being called when opening other form thru main menu', '');
    AddChangeList(ctEnhancment, 188, True,
    'Product List combo box filter is now supported for Labels and Global Changes', '');
    AddChangeList(ctFeature, 189, False, 'Ability to attach Documents to Product, Customer and Vendor', '');
    AddChangeList(ctBugFix, 189, True, 'Fixed issue that prevented setting Vendor Invoice default header info', '');
    AddChangeList(ctBugFix, 190, True, 'Fixed issue with Grid Search Panel causing Invalid Variant Operation', '');
    AddChangeList(ctBugFix, 191, True, 'Fixed issue with Product View shwoing no data when returning from other Views',
    '');
    AddChangeList(ctFeature, 191, False, 'Option to View and Print Receipt from Transaction List', '');
    AddChangeList(ctBugFix, 192, True, 'Fixed issue with printing Product Sheets from Product List', '');
    AddChangeList(ctBugFix, 193, True, 'Fixed issue when adding new Category at Product Edit', '');
    AddChangeList(ctBugFix, 193, True, 'Fixed issue that prevented Custom Fields from showing in Product View', '');
    AddChangeList(ctEnhancment, 194, False, 'Vendor Invoice View now displays more vendor contact info', '');
    AddChangeList(ctEnhancment, 195, False, 'Option to set Customer Invoices to auto prompt to print when complete', '');
    AddChangeList(ctEnhancment, 197, False,
    'Added screen indicators on Product and Price Update Views to note if Sale Prices are active', '');
    AddChangeList(ctBugFix, 198, True, 'Fixed issue with # tables in some special reports', '');
    AddChangeList(ctEnhancment, 199, False,
    'Option to View and Print Receipt from "View Tickets with Sales for Product"', '');
    AddChangeList(ctEnhancment, 199, False, 'Added Unit / Measure to Product Global Changes', '');
    AddChangeList(ctEnhancment, 199, False,
    'More columns added to Product, Customer and Vendor lists, (thru Field Chooser by right click on any column header).'
    , '');
    AddChangeList(ctFeature, 199, False,
    'Option to save column layout at Product, Customer and Vendor lists (by workstation)', '');
    AddChangeList(ctChange, 199, False, '"Clear ALL Sale Prices" in Global Changes now clears the Begins and Expires',
    '');
    AddChangeList(ctBugFix, 200, True, 'Fixed issue with opening Custom Pricing in version 199', '');
    AddChangeList(ctEnhancment, 200, True, 'Reports now support multiple lines of Having (removing Having And)', '');
    AddChangeList(ctEnhancment, 201, False, 'Added Markup and Margin to available fields in Product List', '');
    AddChangeList(ctFeature, 201, False, 'Ability to allow Product View changes to be made from Vendor Invoice line', '');
    AddChangeList(ctBugFix, 202, True,
    'Fixed issue that prevented first saved column from loading in proper position in Lists. ', '');
    AddChangeList(ctBugFix, 203, True, 'Fixed issue with price update from product List in version 199', '');
    AddChangeList(ctBugFix, 204, True, 'Fixed some issues with FTS searches in Product List',
    'Errors 7094-7099 and 7103');
    AddChangeList(ctEnhancment, 205, False,
    'Ability to set Shelf Sheets to show Print Dialog and use it''s Preview button', '');
    AddChangeList(ctBugFix, 206, False, 'Fixed issue with saving Reports to PDF', '');
    AddChangeList(ctEnhancment, 207, False, 'Added Delivery Message on Customer View to auto include in delivery labels',
    'Requires Tis01 Ver 3029');
    AddChangeList(ctEnhancment, 207, True,
    'ModuleListProductsTable now uses own AdsTable so that filters do not get lost by FindBarcode', '');
    AddChangeList(ctBugFix, 208, False,
    'Fixed issue when closing "Vendor Generate Delivery Item List" in certain instances', '');
    AddChangeList(ctEnhancment, 208, False, 'Added menu and keyboard shortcut option to clear all filters from Lists',
    '');
    AddChangeList(ctBugFix, 208, False, 'Fixed issue with adding item to Mix & Match group (since version 199)', '');
    AddChangeList(ctBugFix, 208, False,
    'Fixed issue when selecting yes from "Add New Item" prompt after entering item code in search box on Product List.'
    , '');
    AddChangeList(ctBugFix, 208, False, 'Fixed issue to allow pressing enter in Product and Customer List', '');
    AddChangeList(ctChange, 208, False,
    'Returning to Product, Customer or Vendor list after editing now returns focus to List', '');
    AddChangeList(ctFeature, 208, False, 'View Product Adjustments', '');
    AddChangeList(ctBugFix, 209, False,
    'Fixed issue when copy sale dates in Product View did not work in certain instances', '');
    AddChangeList(ctEnhancment, 209, False,
    'Speed up initial Product List search when no calculated columns are displayed', '');
    AddChangeList(ctEnhancment, 209, False, 'Allow filtering on calculated columns in Product List', '');
    AddChangeList(ctBugFix, 210, False, 'Fixed issue in version 209 with filtering on some lists', '');
    AddChangeList(ctBugFix, 210, False,
    'Fixed issue with scanning products in popup Product List from Mix & Match or Vendor Invoice views', '');
    AddChangeList(ctBugFix, 211, True, 'Fixed issue when clearing custom drop down in Filter Lists', '');
    AddChangeList(ctChange, 211, False, 'Scanning product in Product List now moves focus to product in list', '');
    AddChangeList(ctEnhancment, 211, True, 'Reports now use MemData to increase speed', '');
    AddChangeList(ctBugFix, 213, True, 'Fix Label printing issue caused by MemData approach bug (version 211)', '');
    AddChangeList(ctFeature, 215, False, 'Customer Order Status Tracking', '');
    AddChangeList(ctBugFix, 215, False,
    'Redo report temp index filename algorithm to prevent issues in specific instances', '');
    AddChangeList(ctEnhancment, 215, True, 'Price Change action log now records the Per field for Package Price changes',
    '');
    AddChangeList(ctBugFix, 216, False, 'Fixed some report engine issues', '');
    AddChangeList(ctEnhancment, 216, False, 'New Skin types added', 'CdSkins.skinres is now required');
    AddChangeList(ctFeature, 216, False,
    'Option to copy row (with or without captions) from Lists (Product, Customer and Vendor) to clipboard', '');
    AddChangeList(ctFeature, 216, False,
    'Option to copy row (with or without captions) from Transaction Views to clipboard', '');
    AddChangeList(ctEnhancment, 216, False, 'Transaction Views now display Record # of Records in navigator area', '');
    AddChangeList(ctEnhancment, 216, False, 'Added alternate product description field', '');
    AddChangeList(ctBugFix, 216, False, 'Fixed Product list speed issue (bug introduced version 215)', '');
    AddChangeList(ctEnhancment, 216, False,
    'More columns added to Customer list with option to sort on any column, (thru Field Chooser by right click on any column header)', '');
    AddChangeList(ctChange, 216, True, 'Customer list now uses AdsQuery', '', pcCustomers, slNothing);
    AddChangeList(ctBugFix, 216, True,
    'Fixed issue that caused inadvertent Price Changes thru Vendor Invoice when closing the Price Update view before it completely loaded', '');
    AddChangeList(ctBugFix, 217, False,
    'Fixed issue when opening Product Additinal Info View (bug introduced version 216)', '', pcProducts, slNothing);
    AddChangeList(ctChange, 218, True, 'Scheduler now uses separate AdsConnection to prevent 6619', '');
    AddChangeList(ctEnhancment, 218, False, 'Option to filter Program Changes View by recent changes only', '');
    AddChangeList(ctBugFix, 219, False, 'Fixed issue with Auto Update in version 216 and up', '');
    AddChangeList(ctEnhancment, 219, False, 'Support to print labels for Customer Order Status Tracking', '',
    pcCustomers, slComplex);
    AddChangeList(ctChange, 219, True, 'Removed requirment to restart program after Report Import', '', pcReports);
    AddChangeList(ctBugFix, 220, True, 'Fixed issue with loading Report Settings including BarTend path in version 219',
    '', pcReports);
    AddChangeList(ctEnhancment, 221, False, 'Added option to Check Customer Credit Limit before taking Customer Order',
    '', pcCustomerOrders, slSimple);
    AddChangeList(ctEnhancment, 222, True, 'Removed Mask from FilterRow (DxGridHelpers)', '', pcLists, slNull);
    AddChangeList(ctEnhancment, 222, False, 'Report tab captions could now be set (right click in caption)', '',
    pcReports, slNothing);
    AddChangeList(ctBugFix, 222, True, 'Fixed stack overflow in certain instances at Product Edit', '', pcProducts,
    slNothing);
    AddChangeList(ctFeature, 222, False, 'Added continuous page scrolling option in Reports', '', pcReports, slNull);
    AddChangeList(ctFeature, 222, False, 'Added two-up page display with continuous option in Reports', '', pcReports,
    slNull);
    AddChangeList(ctEnhancment, 222, False, 'Vendor Item List now stays at same line when returning from Item View', '',
    pcVendors, slNull);
    AddChangeList(ctEnhancment, 222, False,
    'When viewing Register Transaction List for a Customer the scope list now contains all standard scope options', '',
    pcCustomers, slNull);
    AddChangeList(ctEnhancment, 223, False, 'Create Deliveries for Customer Order Status Tracking by Customer View', '',
    pcCustomers, slNull);
    AddChangeList(ctEnhancment, 223, False, 'Added ability to print Shelf Labels and Sheets for Custom Pricing.',
    'PrdPriceLabels Reports needs to be installed', pcProducts, slSimple);
    AddChangeList(ctBugFix, 223, True, 'Fixed version 222 issue that prevented opening Browse forms', '', pcGeneral,
    slNull);
    AddChangeList(ctBugFix, 223, False, 'Fixed issue that prevented updating tax from Global Changes', '', pcGeneral,
    slNull);
    AddChangeList(ctBugFix, 223, False, 'Fixed issue that prevented printing Labels for entire list in some instances',
    '', pcProducts, slNull);
    AddChangeList(ctEnhancment, 223, False, 'Improved speed with certain filters in Lists',
    'Use Upper for FilterRow where applicable', pcLists, slNothing);
    AddChangeList(ctBugFix, 223, True, 'Fixed issues with filters on some numeric fields (update GridData)',
    'GridData Update', pcProducts, slSimple);
    AddChangeList(ctEnhancment, 224, False, 'Added access to Custom Pricing from Vendor Invoice', '', pcVendors, slNull);
    AddChangeList(ctChange, 225, True, 'Product Sales does not change date back to Today on each Show', '', pcProducts,
    slNull);
    AddChangeList(ctChange, 225, False,
    'Returning to Vendor Invoice View after certain actions now returns focus to Grid', '', pcVendors, slNull);
    AddChangeList(ctChange, 225, True, 'Updated to RBuilder 14.04', '', pcGeneral, slNull);
    AddChangeList(ctChange, 225, True, 'Updated to DevEx v2011.2.5', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 225, False, 'Global Changes could now be accessed from Mix & Match List and View', '',
    pcGeneral, slNull);
    AddChangeList(ctEnhancment, 226, False, 'Shortcut key for Create Quick Order for Customer', '', pcCustomers,
    slComplex);
    AddChangeList(ctEnhancment, 226, False,
    'More columns added to Vendor Invoices List, (thru Field Chooser by right click on any column header)', '',
    pcVendors, slNull);
    AddChangeList(ctEnhancment, 226, False, 'More Product actions made available in Vendor Item List', '', pcProducts,
    slNull);
    AddChangeList(ctBugFix, 227, False,
    'Vendor Invoice totals were not getting updated after Import Items (when nothing was manually edited)', '',
    pcGeneral, slNull);
    AddChangeList(ctBugFix, 227, True, 'JVCL issue with XML in version 226', '', pcGeneral, slNull);
    AddChangeList(ctBugFix, 228, True,
    'Fixed issue opening Vendor Invoice List for a Vendor (bug introduced version 226)', '', pcVendors, slNull);
    AddChangeList(ctBugFix, 232, False, 'Fixed issue that prevented filters on some columns with reserved names', '',
    pcLists, slNull);
    AddChangeList(ctBugFix, 233, False,
    'Fixed issue that caused 2 records of Additinal Product Info to be recorded when copying Products', '', pcProducts,
    slNull);
    AddChangeList(ctBugFix, 233, False,
    'Fixed issue that prevented Global Changes filters on some fields with reserved names', '', pcProducts, slNull);
    AddChangeList(ctEnhancment, 233, False, 'Added Alt Description to Global Changes options', '', pcProducts, slNull);
    AddChangeList(ctFeature, 233, False, 'Added ability for new Grid Layout options for each Report output option', '',
    pcReports, slSimple);
    AddChangeList(ctFeature, 233, False, 'QuickBooks Integration to upload POS Vendor Invoices to QB Vendor Bills', '',
    pcVendors, slComplex);
    AddChangeList(ctEnhancment, 234, False, 'Added Utilities Menu (where applicable)', '', pcGeneral, slNull);
    AddChangeList(ctFeature, 234, False, 'Record and View detailed price change log', '', pcProducts, slSimple);
    AddChangeList(ctChange, 234, True, 'Updated to DevEx v2011.2.6', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 235, False, 'Enabled custom fields on Customer Order Edit View', '');
    AddChangeList(ctFeature, 235, False, 'View Customer Sales Summary from Customer List and View', '', pcCustomers,
    slNull);
    AddChangeList(ctFeature, 235, False, 'Added ability to Save and Restore Custom User View Layouts on Report Grids',
    '', pcReports, slNull);
    AddChangeList(ctEnhancment, 235, False, 'Added Time options in Product Sales Summary and Detail Views', '',
    pcProducts, slNull);
    AddChangeList(ctEnhancment, 235, False, 'Product Sales Summary and Detail Views on Mix & Match group', '',
    pcProducts, slNull);
    AddChangeList(ctBugFix, 235, False, 'Properly identify Voided sales in Transaction Detail View', '', pcTransaction,
    slNull);
    AddChangeList(ctChange, 235, True,
    'Report error are now handled internally and email send to bugreports@compudime.com', '', pcReports, slNull);
    AddChangeList(ctEnhancment, 236, True, 'Support for Custom Fields in Password Setup', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 236, False, 'Added option to apply all right to User / Group', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 237, True, 'Program Changes View now includes the Release Date for every change', '',
    pcGeneral, slNull);
    AddChangeList(ctFeature, 237, False, 'Support for Custom Fields in Customer Summary View', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 237, False, 'Transaction Actions could now be viewed from the Register Transaction View',
    '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 238, False, 'Option to set Active Tab when opening Customer Order', '', pcCustomerOrders, slSimple);
    AddChangeList(ctBugFix, 238, False, 'Fixed issue that prevented mouse scrolling Report Preview in some instances', '', pcReports, slNull);
    AddChangeList(ctChange, 238, True, 'Updated to RBuilder 14.05', '', pcGeneral, slNull);
    AddChangeList(ctFeature, 238, False, 'Support for Alert option at some Protected Actions in User Setup', '', pcGeneral, slNull);
    AddChangeList(ctBugFix, 239, False, 'Fixed issue that prevented skin data to load when program folder was not properly set', '', pcGeneral, slNull);
    AddChangeList(ctBugFix, 239, False, 'Fixed AV when closing some reports', '', pcReports, slNull);
    AddChangeList(ctEnhancment, 239, False, 'Speed up startup time when server port is specified in ads.ini paths', '', pcGeneral, slNull);
    AddChangeList(ctChange, 239, False, 'RTMPath setting auto added to Ads.ini when server port is used in RepDefs path', '', pcReports, slNull);
    AddChangeList(ctEnhancment, 240, False, 'Enhanced  handling of Expense Account access in QB', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 240, True, 'Additional Transaction Info View option (TikInfo)', '', pcGeneral, slSimple);
    AddChangeList(ctBugFix, 240, False, 'Fixed issue that prevented deleting a Customer when older Invoice format used', '', pcCustomers, slNull);
    AddChangeList(ctChange, 240, True, 'Scanner Settings are now stored in Program Prefrences (instead of Registry)', '', pcGeneral, slNull);
    AddChangeList(ctChange, 240, True, 'Scale Settings (where applicable) are now stored in Program Prefrences (instead of Registry)', '', pcGeneral, slNull);
    AddChangeList(ctChange, 241, True, 'CSV Export now uses #9 for feild seperator and does not add " around fields', '', pcReports, slNull);
    AddChangeList(ctChange, 241, True, 'Added XLXS and XML export options to Grid View', '', pcReports, slNull);
    AddChangeList(ctChange, 241, True, 'Some OptionsView properties could now be stored and loaded for custom grids', '', pcReports, slNull);
    AddChangeList(ctFeature, 241, False, 'Option to store Customer Cell Carrier (for SMS features)', '', pcGeneral, slNull);
    AddChangeList(ctFeature, 241, False, 'Added multiple Order header fields for shipping and scheduling', '', pcCustomerOrders, slComplex);
    AddChangeList(ctFeature, 241, False, 'Customer Order Live Pack option', '', pcCustomerOrders, slComplex);
    AddChangeList(ctChange, 241, True, 'Updated to DevEx v2011.2.7', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 241, False, 'Added dropdown for Bin field at Product View', '', pcProducts, slNull);
    AddChangeList(ctBugFix, 242, False, 'Fixed issue that prevented Deleting a Customer in certain instances', '', pcCustomers, slNull);
    AddChangeList(ctFeature, 242, False, 'Added CheckBox column on Product List to allow more custom filtering for Global Changes, Labels etc.', '', pcProducts, slNull);
    AddChangeList(ctChange, 242, True, 'Change in algorithm that deletes PoItem lines when generating PO to ensure agianst accidental deletion', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 243, False, 'Added Tree View in User Setup to move between Protected Areas', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 243, False, 'More columns in Product List including Markup and Profit Margin for all price options', '', pcProducts, slNull);
    AddChangeList(ctBugFix, 244, False, 'Fixed issue with Generate PO (bug introduced version 242)', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 245, False, 'Enhanced handling of Vendor Account in QB', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 245, False, 'Customer Order ehancments', '', pcCustomerOrders, slComplex);
    AddChangeList(ctFeature, 246, False, 'Auto Tare option for Scale Products.', '', pcProducts, slNull);
    AddChangeList(ctFeature, 246, False, 'Bi-County Scale Intergration options', '', pcProducts, slComplex);
    AddChangeList(ctEnhancment, 246, True, 'Enabled Custom filter options in column filter drop down', '', pcLists, slNull);
    AddChangeList(ctEnhancment, 246, False, 'Add Vendor Invoice Cost Flag to be visible when reviewing deliveries', '', pcVendors, slNull);
    AddChangeList(ctBugFix, 246, True, 'Aligned PoItem Cost field to match Vend PO and Invoice to circumvent numeric out of range when creating PO', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 247, False, 'Added Date / Due Date option for Customer Order List filter', '', pcCustomerOrders, slNull);
    AddChangeList(ctEnhancment, 248, False, 'Option to Insert line in middle of Customer Order', '', pcCustomerOrders, slNull);
    AddChangeList(ctFeature, 248, False, 'Arrows to switch lines in Customer Order', '', pcCustomerOrders, slNull);
    AddChangeList(ctFeature, 249, True, 'Support for PreScript and PostScript for Reports', '', pcReports, slNull);
    AddChangeList(ctBugFix, 249, False, 'Fix issue with Save and Load Column Layout on certain views', '', pcGeneral, slNull);
    AddChangeList(ctBugFix, 250, False, 'Fixed issue that prevented Mix and Match edit', '', pcProducts, slNull);
    AddChangeList(ctBugFix, 251, False, 'Fixed issue that caused program to freeze in certain instances after a scan', '', pcGeneral, slNull);
    AddChangeList(ctBugFix, 252, True, 'Fixed issue with "Missing EnvSetting field BPORT" alert on new version', '', pcGeneral, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Added Ship To option on Vendor Purchase Order', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Import Items previously received into Vendor Purchase Order', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Arrows to switch lines in Vendor Purchase Order', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Option to Insert line in middle of Vendor Purchase Order', '', pcVendors, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Last purchase amount and date at Customer View', '', pcCustomers, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Added Picked By entry option in Customer Delivery View', '', pcCustomers, slNull);
    AddChangeList(ctBugFix, 253, False, 'Fixed some issues when doing Global Changes for Products', '', pcProducts, slNull);
    AddChangeList(ctBugFix, 253, False, 'Fixed issue with Scanner not being activated when entering Code field', '', pcProducts, slNull);
    AddChangeList(ctEnhancment, 253, False, 'Option to set default Delivert Method in Customer Order', '', pcCustomerOrders, slSimple);
    AddChangeList(ctChange, 254, False, 'When called BarTend.exe is now opened in hidden mode', '', pcProducts, slNull);
    AddChangeList(ctChange, 254, False, 'Entering a value in Lb column leaving Qty column 0, now auto sets Qty value to 1', '', pcCustomerOrders, slNull);
    AddChangeList(ctBugFix, 254, False, 'Fixed issue with CheckBox column filter on Product List', '', pcProducts, slNull);
    AddChangeList(ctEnhancment, 254, False, 'Options to Clear the Cost Flag in Vendor Invoices on Cost change', '', pcVendors, slSimple);
    AddChangeList(ctEnhancment, 254, False, 'Added Account # field on Customer View', '', pcCustomers, slNull);
    AddChangeList(ctEnhancment, 256, True, 'Support in Grid Views for DisplayFormat', '', pcReports, slNull);
    AddChangeList(ctEnhancment, 256, True, 'Support to specify GridLayoutID in Memorized Reports', '', pcReports, slNull);
    AddChangeList(ctBugFix, 257, False, 'Properly sort Vendor PO Line #s  when reloading', '', pcVendors, slNull);
    AddChangeList(ctFeature, 258, False, 'Context aware drill down in Report Grids', '', pcReports, slNull);
    AddChangeList(ctBugFix, 258, False, 'Fixed issue that prevented scanning user cards at Password Dialog', '', pcGeneral, slNull);
  AddChangeList(ctEnhancment, 259, False, 'Added keyboard shortcut Ctrl-P ito Print button in Report Preview', '',
    pcReports, slNull);
  AddChangeList(ctEnhancment, 259, False, 'Added Order Source and Count values for Customer Delivery View', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 259, False,
    'Added Notify Via field at Customer View to be used with Notification Modules', '', pcCustomers, slComplex);
  AddChangeList(ctFeature, 259, False, 'Product Tags for on Product List and View', '', pcProducts, slNull);
  AddChangeList(ctBugFix, 259, False, 'Fixed recent issue that prevented saving Products when chaning code', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 260, True, 'LclData is now not in use while program is running', '', pcGeneral, slNull);
  AddChangeList(ctFeature, 260, False,
    'Respect "Transmit Scanner Data Only When Program In Foreground" setting from POS', '', pcGeneral, slNull);
  AddChangeList(ctFeature, 260, False, 'Customer Order Template feature', '', pcCustomerOrders, slComplex);
  AddChangeList(ctFeature, 260, False,
    'Support for SMS Notifications to Customers when New Quick Order is Received by "CompuDime Voice Management" server',
    '', pcCustomerOrders, slComplex);
  AddChangeList(ctFeature, 261, False, 'Support Tags in Reports', '', pcReports, slSimple);
  AddChangeList(ctFeature, 262, False, 'Support for "Sale Max By" to include Transaction, day and Campaign options', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 262, False, 'Added option in Global Changes to clear Min / Max values of all Sale prices',
    '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 262, False, 'Support Tags in Global Changes', '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 262, False, 'Support Tags as a Column in Product List', '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 263, False, 'Added First / Last Sold on Product Sales Summary View', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 263, False, 'Option to view Inventory Adjustments on Product', '', pcProducts, slNull);
  AddChangeList(ctChange, 263, False, 'BarTender will not hide when set to Show Print Dialog', '', pcProducts, slNull);
  AddChangeList(ctBugFix, 264, True,
    'Fixed issue with saved Column Layouts not reloading properly in certain instances', '', pcLists, slNull);
  AddChangeList(ctBugFix, 264, False, 'Fixed issue with filtering on Tags in Product List', '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 265, False, 'Calculate Quantity of Price Encoded items entered in Vendor Invoice', '',
    pcVendors, slNull);
  AddChangeList(ctEnhancment, 265, True, 'Support for CdComboBox in InfoDes Tables', '', pcGeneral, slComplex);
  AddChangeList(ctEnhancment, 265, False, 'Setting to allow MAPI email on Customer Orders', '', pcCustomerOrders,
    slSimple);
  AddChangeList(ctEnhancment, 265, False, 'Enable emailing Vendor Invoice to Vendor', '', pcVendors, slNull);
  AddChangeList(ctChange, 266, False, 'Added OnHand column to Product List for viewing and filtering', '',
    pcProducts, slNull);
  AddChangeList(ctChange, 266, True, 'Rearranged Product View to fit on lower resolution screens', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 266, False, 'Support for Customer Ship Route & Seq', '', pcCustomers, slNull);
  AddChangeList(ctBugFix, 266, True,
    'Fixed issue that cause an error when using Pack Order Item from Customer Invoice ', '', pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 267, True,
    'Change in algorithm when deleting lines in Customer Orders to prevent wrong deletions', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 267, True,
    'Change in algorithm when deleting lines in Vendor Invoices to prevent wrong deletions', '', pcVendors, slNull);
  AddChangeList(ctEnhancment, 268, True,
    'RepLog is now entered before running SQL and updated with TimeDone when complete', '', pcReports, slNull);
  AddChangeList(ctEnhancment, 268, False, 'Count amount of Labels printed from Customer Order Packaging', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 268, False, 'Enabled Preview and Print of Product, Customer and Vendor List', '',
    pcLists, slNull);
  AddChangeList(ctEnhancment, 269, False, 'Option to force entry of "Vendor Invoice #" in Vendor Invoice', '',
    pcVendors, slSimple);
  AddChangeList(ctEnhancment, 269, False, 'Proper handling of Product List fields where NULL Valid is set to No', '',
    pcProducts, slComplex);
  AddChangeList(ctBugFix, 269, True,
    'Fixed issue that caused attempts to print a PO after previously emailing one, to be sent to email instead of printing',
    '', pcVendors, slNull);
  AddChangeList(ctEnhancment, 269, False, 'Option to set Customer Task to "Alert on Register Transaction" in POS', '',
    pcCustomers, slNull);
  AddChangeList(ctBugFix, 269, True,
    'Fixed issue that caused an error when removing a sorted expression column from grid', '', pcLists, slNull);
  AddChangeList(ctBugFix, 269, False,
    'Fixed issue that would prevent saving changes to Vendor Invoice when Products referred to in the Invoice were deleted from Product List',
    '', pcVendors, slNull);
  AddChangeList(ctEnhancment, 270, False, 'Support for Vendor Credits in QB Integration', '', pcVendors, slNull);
  AddChangeList(ctEnhancment, 270, False, 'Customer Account History View', '', pcCustomers, slNull);
  AddChangeList(ctBugFix, 270, True,
    'Fixed issue that raised an error when attempting to View Sales Details on a Product for sales where the Ticket ID does not conatin a proper time',
    '', pcProducts, slNull);
  AddChangeList(ctChange, 270, False, 'Global Product Changes now get recorded to Action Log', '', pcProducts, slNull);
  AddChangeList(ctBugFix, 270, False,
    'Fixed issue that caused incorrect Price to be calculated when entering desired Profit Margin', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 271, True,
    'Data in Product View is now stored displayed and viewed via an in-memory table', '', pcProducts, slNull);
  AddChangeList(ctFeature, 271, False,
    'Ability to set Master for a Category or Brand (using the Item Class and Category or Brand fields)', '',
    pcProducts, slNull);
  AddChangeList(ctFeature, 271, False, 'Seacrh box on Customer List accross all columns', '', pcCustomers, slNull);
  AddChangeList(ctEnhancment, 271, False,
    'Search box on Customer and Product List use F8 to set focus to search box, press enter or arrow-down to move to list',
    '', pcLists, slNull);
  AddChangeList(ctEnhancment, 271, False, 'Added Last Vendor as column in Product List', '', pcProducts, slNull);
  AddChangeList(ctBugFix, 271, False, 'Fixed viewing Action Log should display proper value as Changed To', '',
    pcGeneral, slNull);
  AddChangeList(ctEnhancment, 271, False, 'Option to do Global Changes from Vendor Invoice or PO', '',
    pcVendors, slNull);
  AddChangeList(ctEnhancment, 271, False, 'Option to delete Customer Delivery View (with no Delivery Labels printed)',
    '', pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 271, False,
    'Fixed issue that caused Vendor Invoice in some instances to prompt Save Changes when no changes were made to Invoice',
    '', pcVendors, slNull);
  AddChangeList(ctEnhancment, 271, False, 'Added shortcut key Ctrl-O to "Report Options" button', '',
    pcReports, slNull);
  AddChangeList(ctEnhancment, 271, True, 'RepLog is now working properly with TimeDone for Reports doing ReExecute', '',
    pcReports, slNull);
  AddChangeList(ctBugFix, 271, False, 'Fixed some instances where entering a UPC-A code did not detect duplicate UPC-E',
    '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 271, False, 'View Discount, Credit Limits and Payment Restriction changes for Customer',
    '', pcCustomers, slNull);
  AddChangeList(ctFeature, 272, True, 'Support for Pivot Grid as a report output option', '', pcReports, slNull);
  AddChangeList(ctFeature, 275, False, 'Tabbed interface to switch between all open forms & reports', '',
    pcGeneral, slNull);
  AddChangeList(ctChange, 275, True, 'Skin changing in Main Menu is now always enabled if skin file is found', '',
    pcGeneral, slNull);
  AddChangeList(ctChange, 275, True, 'Default Skin is now MoneyTwins instead of Office2010Blue', '', pcGeneral, slNull);
  AddChangeList(ctEnhancment, 275, False, 'Some Customer Order Template enhancments', '', pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 276, True, 'Fixed issue that did not save Price changes in certain instances (since 271)', '',
    pcProducts, slNull);
  AddChangeList(ctBugFix, 277, True, 'Some Tab issues', '', pcGeneral, slNull);
  AddChangeList(ctEnhancment, 278, False,
    'Modified Global Changes View to show properly with different screen resolution settings', '', pcProducts, slNull);
  AddChangeList(ctChange, 278, False,
    'POS User Control settings "Edit Financial Data at Customer" and "Edit Customer Discount Field" are now respected at Customer Edit',
    '', pcCustomers, slNull);
  AddChangeList(ctEnhancment, 278, False, 'Added Frequency option for each line item in Customer Templates', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 279, False, 'Fixed some issues with Product View edit (since 271)', '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 280, False, 'Enhanced speed with certain popup Views', '', pcGeneral, slNull);
  AddChangeList(ctChange, 281, True, 'Updated to DevEx v2013.1.2', '', pcGeneral, slNull);
  AddChangeList(ctFeature, 281, False, 'Added support to store and activate Future Product Cost changes', '',
    pcProducts, slNull);
  AddChangeList(ctEnhancment, 281, False,
    'Ability to move, size and choose columns to display in Product Info section on Vendor Invoice footer (when enabled)',
    '', pcVendors, slSimple);
  AddChangeList(ctEnhancment, 281, False, 'Copy Ship Route and Seq from Customer when creating new Order or Template',
    '', pcCustomerOrders, slNull);
  AddChangeList(ctFeature, 281, False, 'Support for Order Short View', '', pcCustomerOrders, slComplex);
  AddChangeList(ctFeature, 281, False, 'Support to assign Payment Type and/or Store Credit Card info with Order', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 281, False, 'Added "Process Order" to allow internal sorting of orders to process', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctFeature, 282, False, 'Product Tags for on Product List and View', '', pcCustomers, slNull);
  AddChangeList(ctFeature, 282, False, 'Option to set minimum length for Customer Telephone #', '', pcCustomers,
    slSimple);
  AddChangeList(ctFeature, 282, False, 'Import Items by Tag, Category or Brand into Cutomer Order', '',
    pcCustomerOrders, slSimple);
  AddChangeList(ctFeature, 282, False,
    'Added CheckBox column on Customer List to allow custom filtering for Customer Reports (when opened thru the Report button on Customer List)',
    '', pcCustomers, slNull);
  AddChangeList(ctChange, 283, True, 'Updated to DevEx v2013.1.3', '', pcGeneral, slNull);
  AddChangeList(ctEnhancment, 286, False, 'Option to Factor quatity for Customer Orders when doing Template to Order',
    '', pcCustomerOrders, slNull);
  AddChangeList(ctBugFix, 287, False,
    'Fixed an issue that caused some reports to load wrong data when using the Refresh method', '', pcReports, slNull);
  AddChangeList(ctEnhancment, 288, False,
    'Customer Template List now has Schedule info columns available in Field Chooser', '', pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 288, False, 'Added Next and Previous buttons on Browse Views to mve to next date scope',
    '', pcGeneral, slNull);
  AddChangeList(ctBugFix, 288, False,
    'Double clicking on a Transaction when Register Transaction View was open does not refresh data in View', '',
    pcTransaction, slNull);
  AddChangeList(ctEnhancment, 288, True,
    'Resize issue when attempting to close a form that fails OnClose call, see note in "HideModuleModal"', '',
    pcGeneral, slNull);
  AddChangeList(ctChange, 288, True,
    'Scanner and Scale ports are no longer opened at program startup, instead they are only controlled by ScannerScaleStatus',
    '', pcGeneral, slNull);
  AddChangeList(ctBugFix, 289, True,
    'Fixed issue that caused Scale Quan to be entered for Non-Scale Item when Processing Orders', 'Incident #417',
    pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 290, False, 'Access Customer Custom Pricing by Customer and Product List and Views', '',
    pcProducts, slNull);
  AddChangeList(ctBugFix, 291, False,
    'Fixed issue under certain circumstances the Price Update view displays the previous Product', '',
    pcVendors, slNull);
  AddChangeList(ctFeature, 292, False, 'Global Changes on Customer List', '', pcCustomers, slNull);
  AddChangeList(ctFeature, 292, False, 'Options to restrict Customer Discount by Balance and Credit Limit values', '',
    pcCustomers, slNull);
  AddChangeList(ctEnhancment, 292, False,
    'Clicking on X of main form now prompts "Exit application" or "Close current tab"', '', pcGeneral, slNull);
  AddChangeList(ctBugFix, 292, False,
    'Fixed issue that caused product info on bottom Vendor Invoice not to be visible with certain skins (when set)', '',
    pcVendors, slNull);
  AddChangeList(ctEnhancment, 292, False, 'Enhanced the Future Product Cost input from Vendor Invoices', '',
    pcVendors, slNull);
  AddChangeList(ctChange, 292, False, 'Product Tags could now be up to 40 chars', '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 293, False, 'Added option to filter Customer Deliveries by POS Date', '',
    pcCustomerOrders, slNull);
  AddChangeList(ctEnhancment, 293, True, 'Product View now respects the EnvSetting NoFsWic to hide Food related edits',
    '', pcProducts, slNull);
  AddChangeList(ctEnhancment, 293, False, 'Option Vendor Invoices to prompt for Price Change when U/P/C is changed', '',
    pcVendors, slSimple);
  AddChangeList(ctEnhancment, 293, False, 'Option to set Default Output when printing Customer Orders', '',
    pcCustomerOrders, slSimple);
  AddChangeList(ctEnhancment, 293, False,
    'Sale status indicator column added to Product Info section on Vendor Invoice footer (when enabled)', '',
    pcVendors, slNull);
  AddChangeList(ctEnhancment, 293, True, 'Enabled setting email properties in program prefrences', '',
    pcGeneral, slNull);
  AddChangeList(ctBugFix, 293, False,
    'Fixed issue that caused Register Transaction View ticket & payment info not to be visible with certain skins (when set)',
    '', pcTransaction, slNull);
  AddChangeList(ctBugFix, 293, True,
    'Fixed Invalid Pointer Operation issue when closing application that used a report with ClickInfo', '',
    pcReports, slNull);
  AddChangeList(ctEnhancment, 293, True, 'Updated report in order to keep FsData and PayList closed', '',
    pcReports, slNull);
  AddChangeList(ctBugFix, 294, True, 'Fixed issue that prevented saving Vendor Invoices in latest release', '',
    pcVendors, slNull);
  AddChangeList(ctBugFix, 295, False,
    'Fixed issue that caused Register Transaction View not to show details in some instances', '',
    pcTransaction, slNull);

  AddChangeList2(ctBugFix, 1, 0, 0, False,
    'Fixed issue that preventing enetring data on new product when pressing New from the Product View', '',
    pcProducts, slNull);
  AddChangeList2(ctChange, 1, 0, 0, True, 'Updated to DevEx v2013.2.2', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 0, True, 'Updated to RBuilder 15.02', '', pcReports, slNull);
  AddChangeList2(ctChange, 1, 0, 0, True, 'Updated to Delphi XE5', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 1, False, 'Lists & Reports now support Tags up to 40 chars', '', pcProducts, slNull);
  //AddChangeList2(ctFeature, 1, 0, 2, False, '"Chat with Support" option in Help Menu', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 2, True, 'Changed MadExcept email port to 587 to allow sending from locations with blocked port 25', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 2, False, 'Support for Unicode fields in Product Search (when set)', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 2, False, 'Global Price Changes options realtive to different prices by $ or %', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 2, False, 'Support for drill down reports (where implemented)', '', pcReports, slNull);
  AddChangeList2(ctFeature, 1, 0, 3, True, 'Loads ZipCodes table on Update', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 3, False, 'Global Customer Changes View redesigned', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 1, 0, 3, False, 'Auto fill in City & State with entry of Zip Code', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 3, False, 'Fixed issue when clearing the Date fields in "Update Product Pricing View"', '', pcProducts, slNull);
  AddChangeList2(ctChange, 1, 0, 3, True, 'Updated to DevEx v2013.2.3', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 4, False, 'Global Product Changes View redesigned', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 4, False, 'Restored sound in application (since 1.1.0.0)', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 5, False, 'Fixed issue that prevented Globa l Changes on sale prices (since 1.1.0.3)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 6, True, 'Fixed issue that caused an AV in certain instances when clicking a Tab to move to another view', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 7, True, 'Support for RtmDevice in JobOptionList to support emailing Grids by SelectedView', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 7, False, 'Change email report procedures to work on more networks', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 7, False, 'Support for custom Port setting when emailing report', '', pcReports, slNull);
  AddChangeList2(ctChange, 1, 0, 8, True, 'Removed Thumbnails from Report Preview', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 1, 0, 8, True, 'Fixed issue that caused ADS 6412 error when AppEventsIdle was raised', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 8, False, 'Fixed issue that cuased wrong item to show in Custom Pricing View when accessed from Vendor Invoice in certain instances', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 1, 0, 8, False, 'Editing Product info from within a Vendor Invoice now always updates the current line in invoice', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 1, 0, 8, True, 'Fixed an issue that raised an error when updating from prior to version 1.1.0.3 (ZipCodes update)', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 8, False, 'Added option in Product List Report to filter by comparing markups on two price levels', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 8, False, 'Support custom fields in Global Product Changes', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 8, False, 'Support custom fields in Product List', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 8, True, 'Support for DataSnap connections', '', pcReports, slComplex);
  AddChangeList2(ctBugFix, 1, 0, 9, True, 'Abstract error on some lists (version 1.1.0.8)', '', pcLists, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 10, True, 'Support for for Prompts to return quoted strings', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 10, True, 'Support for VisibleForCustomization in Grid Layout Helpers', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 1, 0, 10, False, 'Options to set Report''s User View as Default, User Default or Workstation Default, to auto load when report is reloaded', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 10, False, 'Report drill downs now display as tabbed views (not popup)', '', pcReports, slNull);
  AddChangeList2(ctFeature, 1, 0, 11, False, 'Ability to View Customer orders by Order Dept', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctFeature, 1, 0, 11, False, 'Support for Inactive products', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 11, False, 'Support for Non-Inventory products', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 11, False, 'Support for Product Note', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 11, True, 'Version 1.1.0.10 issue with saving new products (Inactive can''t be null)', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 11, False, 'Edit "Unit Cost" cell on Vendor Invoice item to update Cost', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 11, False, 'Customize options  on Vendor Invoice detail columns', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 1, 0, 12, True, 'Version 1.1.0.11 issue with saving Vendor Invoice with Prd fields in use', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 1, 0, 14, True, 'Ability to customize LayoutControls using the DESIGN paramater and Admin UserName', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 15, False, 'QB Integration issue when attempting to update a Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 1, 0, 15, False, 'Fixed issue that confused reports, when refreshing a report while different reports open', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 16, False, 'Support for Hebrew in BarTender Sheet intermediate files', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 16, False, 'Added amounts Received & Adjusted on Product Sales Summary', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 16, False, 'Option to exclude categories from Factor adjustment when generating Orders from Templates', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 16, False, 'Customize options  on Vendor PO detail columns', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 1, 0, 16, False, 'Edit "Unit Cost" cell on Vendor PO item to update Cost', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 1, 0, 16, False, 'Vendor Item Code or Upccode in Vendor PO start with + or include % to select from matching item list', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 1, 0, 16, False, 'Fixed issue that caused wrong product to be inserted into Vendor Invoice in certain instances with double-click of Product List', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 1, 0, 17, False, 'Option to set program to Auto Generate Product Code for new products when attempting to save with empty code', '', pcProducts, slSimple);
  AddChangeList2(ctEnhancment, 1, 0, 17, False, 'Option to save column layout at Register Transaction View (by workstation)', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 17, False, 'Display Last vendor on Product View', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 17, False, 'Vendor Code field is now auto populated when saving vendor with empty code', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 1, 0, 17, False, 'Added Configure list for Returns, Discount etc.', '', pcTransaction, slSimple);
  AddChangeList2(ctBugFix, 1, 0, 17, True, 'Default Display Format for numeric non deicmal fields is no more $', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 17, True, 'Summary format is now defaulted to column Display Format', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 17, True, 'Support for ActionFilter in Report Click Actions', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 18, False, 'Support for keyboard wedge scanner to be handled like serial', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 18, False, 'Support for Custom List fields as product attributes', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 19, True, 'Cascade Tel Change and Merge to PmtDist and PmtOpen tables', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 1, 0, 19, False, 'Support for Pivot mode in Vendor Invoice with Custom List fields', '', pcVendors, slComplex);
  AddChangeList2(ctChange, 1, 0, 19, True, 'Updated to RBuilder 15.04', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 1, 0, 19, False, 'Fixed issue that prevented applying Product Global Changes to the Scale attribute', '', pcProducts, slNull);
  AddChangeList2(ctChange, 1, 0, 19, True, 'Product Global Changes now respects the EnvSetting NoFsWic to hide Food related options', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 20, False, 'Support for Product Stock Pivot View with Custom List fields', '', pcProducts, slComplex);
  AddChangeList2(ctChange, 1, 0, 20, True, 'Updated to DevEx v2014.1.4', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 20, True, 'Product Category View & Product Pricing Options now Added to Tab', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 22, False, 'Support for BOGO (Buy One Get One) Promotions using Mix & Match', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 23, False, 'Added Product Onhand field to Product List grid on Customer Order View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 23, True, 'Register Transaction View now respects the EnvSetting NoFsWic to hide Food related edits',
    '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 25, False, 'Fixed issue Product Sales Summary showing wrong amount Adjused', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 25, True, 'Fixed issue causing "Control has no Parent Window" message when closing popup (DevEx bug)', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 25, True, 'Updated to DevEx v2014.1.5', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 1, 0, 26, False, 'Support for Product Pivot view', '', pcProducts, slComplex);
  AddChangeList2(ctEnhancment, 1, 0, 27, True, 'Support for MemoEdit controls in InfoDes created forms', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 27, True, 'Support for Hidden fields in Infodes forms (by specifing negative Row for field)', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 27, False, 'Customer Delivery View "Cases" was showing Repacks count', '', pcCustomerOrders, slNull);
  AddChangeList2(ctFeature, 1, 0, 27, False, 'Added Product Packaging Store Info View', '', pcProducts, slSimple);
  AddChangeList2(ctEnhancment, 1, 0, 28, False, 'Ability to password protect viewing Product Cost', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 1, 0, 28, False, 'Fixed issue that caused error when double-clicking on line in View Inventory Adjustments', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 28, False, 'Added option on Register Transaction Browse to Hide Empty Transactions', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 28, False, 'Register Transaction Browse no includes Transaction Payment Type', '', pcTransaction, slNull);
  AddChangeList2(ctChange, 1, 0, 28, False, 'Auto Generated Product Code do not default to UPCA, to force UPCA codes set "Create UPCA codes for Auto Generated Product Codes"', '', pcProducts, slSimple);
  AddChangeList2(ctFeature, 1, 0, 29, False, 'Enhance List Tag Filter to allow Include / Exclude rows with empty Tags', '', pcLists, slNull);
  AddChangeList2(ctBugFix, 1, 0, 30, True, 'Fixed AV raised in certain instances when closing application with an open List View', '(DoGridUpdateOperations)', pcLists, slNull);
  AddChangeList2(ctFeature, 1, 0, 30, False, 'Ability to select default Double-Click / Enter action on Product List rows (right-click on List, select "Set Default Action")', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 31, True, 'BarTender now uses temp data file for each job', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 31, False, 'BarTender printing now runs in system tray', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 1, 0, 31, False, 'Fixed issue that prevented running certain reports (since 1.1.0.30)', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 1, 0, 32, False, 'Fixed issue that prevented double clicking on Vendor in List (since 1.1.0.30)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 1, 0, 32, False, 'Selected action on Product List now works for Scanned product', '', pcProducts, slNull);
  AddChangeList2(ctChange, 1, 0, 33, True, 'Changed HintStyle to ScreenTip instead of AdvancedHint', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 1, 0, 33, False, 'Saving a Report to PDF when target file is in use now displays descriptive error to user', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 1, 0, 33, False, 'Fixed issue that caused an error on some Browse Lists when selecting All as the Date Scope', '', pcBrowse, slNull);
  AddChangeList2(ctBugFix, 1, 0, 33, False, 'Fixed issue that caused empty page to show in Product View when scanning certain EAN-13 codes', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 1, 0, 33, False, 'Support for Vendor Item Code lookup by CompuConnect service', '', pcVendors, slComplex);
  AddChangeList2(ctFeature, 1, 0, 34, False, 'Support for Product lookup by CompuConnect service', '', pcProducts, slComplex);
  AddChangeList2(ctFeature, 1, 0, 34, False, 'Support for Group Email option of Reports', '', pcReports, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 0, False, 'Support to set Popups / Dropdowns in Report Queries to refresh lists on ReExecute', '', pcReports, slNull);
  AddChangeList2(ctChange, 2, 0, 0, False, 'Rule info could now be changed at Update Product Pricing View', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 0, True, 'Program Icon changed to CD icon', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 0, True, 'Updated to DevEx v2014.2.3', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 0, True, 'Now using XE7', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 1, True, 'Back to XE5 cause of VarIsNull on OldValue', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 3, True, 'Back to XE7 VarIsLikeNull', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 3, False, 'Support for Sub Categories', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 2, 0, 3, False, 'Support for Sale Days to specify Weekdays on Product Sale Prices', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 3, False, 'QB Integration now prompts to update QB Bill after changing Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 5, True, 'Enhancements to User Setup View', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 6, False, 'Fixed CompuConnect connection issues', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 6, False, 'Added customer balance and aging info on Customer Orders (with option to move aging to header area', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 6, False, 'Separate password bits for credit limit on Customer order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 2, 0, 6, False, 'Editing existing Credit Card Info on Customer View now needs "Edit Financial Data at Customer" rights', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 7, False, 'Added support to specify A/P Account for Vendor Invoice into QB Bills', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 7, False, 'Added support to set "Days after receipt Vendor Bills are due"', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 8, False, 'Fixed issue that prevented edit of Product code in recent release', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 8, False, 'Processing selected action on Product List now clears the search text', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 9, False, 'Fixed issue that caused previous report to display when calling new report after closing previous report with application close button', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 9, False, 'Global Changes now defaults Begins / Expires to 00:00 / 23:59 respectively', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 2, 0, 9, False, 'Support for importing Excel data to Vendor Invoice', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 9, True, 'Dialogs are now skinned', 'UseLatestCommonDialogs := False', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 9, True, 'Print / Export / Execute now update image according to caption', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 11, False, 'Fixed issue with unintended changes to Vendor Invoices Due Date (since in version 1.2.0.7)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 12, True, 'Issue in version 1.2.0.9 that prevents Vendor Invoices from updating Product Cost', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 13, False, 'Receipts printed from reports now include Regular vs. Sale Price and You Saved info', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 13, False, 'Product List report is now accessible from List using filter set in Grid', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 13, True, '"Report Options" button is not shown when ExecuteOnce is set on Report', '', pcReports, slNull);
  AddChangeList2(ctChange, 2, 0, 13, True, 'Updated to DevEx v2015.1.2', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 13, False, 'Added Find Panel for all Report Grids', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 13, False, 'Added Find Panel for all Browse Grids', '', pcBrowse, slNull);
  AddChangeList2(ctChange, 2, 0, 13, False, 'Keyboard shortcut to activate Find Panel in Lists was changed to Ctrl-F instead of F8', '', pcLists, slNull);
  AddChangeList2(ctChange, 2, 0, 13, True, 'Reports options form now respects screen WorkAreaHeight rather than Height', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 13, True, 'Report options form now accounts for form caption area according to skin', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 13, True, 'Popup menus are now skinned', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 13, True, 'Added GroupCaption in Prompt table to support dividing a Panel into more than one LayoutGroup', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 13, False, 'Support for SMS Blasting', '', pcReports, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 14, False, 'Added more actions in "Set Default Action" option of Product List ', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 14, False, 'Fixed Sales Summary calculation issue where sales include items with negative values', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 14, False, 'Fixed issue that prevented visiblity of "Report Options" in some circumstances ', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 14, False, 'Assigned Ctrl-I as shortcut for Sales Detail on Product List to remove conflict with Find Panel', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 14, False, 'Option to copy Vendor to all rows from selected row in Generate Purchase Orders', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 15, False, 'Eliminated occasional AV while closing application', 'Added DXSKINDYNAMICLOADING to defines to prevent skin finalization, Access violation at address 01A8D272', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 15, True, 'Fixed recent issue with CustList and CustomerStatement reports', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 16, True, 'Global changes for Ticket Detail (Alpha)', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 17, False, 'Option to assign Vendor Invoice Memo to QB Bills (instead of default "POS Invoice #" memo)', '', pcVendors, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 17, False, 'Option to set filter string to control which Vendor Invoices get posted to QB Bills', '', pcVendors, slSimple);
  AddChangeList2(ctFeature, 2, 0, 17, False, 'Support to push Register Log Summaries to QB Daily Sales Summary', '', pcTransaction, slSimple);
  AddChangeList2(ctFeature, 2, 0, 17, False, 'Support for Remote QB connection', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 18, False, 'Multiple Upcharge / Discount options on Vendor Invoice (Alpha)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 18, False, 'Prevent setting Case Price on Price Encoded Products (setting to override)', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 18, True, 'Support for limited Product Custom Price module (all price require a rule)', '', pcProducts, slComplex);
  AddChangeList2(ctChange, 2, 0, 18, True, 'RETRY_ADS_CONNECTS is now auto written to the Ads.ini set to Enabled', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 19, False, 'Bill / Credit support for Vendor Invoices', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 19, False, 'View Receipt is now in HTML format mimicking the Epson thermal receipt', '', pcTransaction, slNull);
  AddChangeList2(ctBugFix, 2, 0, 19, False, 'Clear UnitCost & OnHand info in Product View when viewing product with no UPC, Cost or Price set', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 19, False, 'Fixed issue (version 1.2.0.18) that prevented printing Product Labels under certain filter conditions', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 20, False, 'Splash screen now displays Required DB version', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 20, False, 'Help -> About now displays Required and Loaded DB versions', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 20, False, 'Fixed report scripting issue that caused some internal queries to fail (such as Import Items from Excel)', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 20, False, 'Option to set Auto Logout on idle', '', pcGeneral, slSimple);
  AddChangeList2(ctBugFix, 2, 0, 20, False, 'Adjusted Tab order in Sale Pricing section of Product View', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 2, 0, 20, True, 'Added support for DataSnap PutFile method', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 21, False, 'Fixed issue with program not restarting after downloading updated application file', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 21, False, 'Adjusted Customer Global Changes View to display ageing data properly', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 2, 0, 21, False, 'Added Today button to all calender drop downs', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 21, False, 'Fixed issue that caused an error when importing a PO into a Vendor Invoice under certain circumstances', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 21, False, 'Saved column layouts now include the Summary properties and foooter visiblity', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 21, False, 'Saved column layouts now include the Summary properties and foooter visiblity', '', pcBrowse, slNull);
  AddChangeList2(ctFeature, 2, 0, 21, False, 'Support to Delete ALL Vendor Invoices from QB from Vendor Invoices list', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 22, True, 'Application update procedure will now auto fill BcHandle, GridData and ZipCodes tables if empty even when update is not required by DB version', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 22, False, 'Auto Logout on Idle now includes an options to either Just Force Log Off or Close All Open Forms', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 22, True, 'Support for PostParamater for Reports to use with PostScript', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 22, False, 'Support for Select / Unselect / Reverse Selection with more descriptive Caption when selecting Master Product changes to apply', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 22, True, 'Updated to DevEx v2015.2.2', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 23, False, 'Added Print / Thermal Receipt option on Transaction Receipt View', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 23, False, 'Saving an open Vendor Invoice now keeps focus on active detail line', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 23, False, 'Support for Vendor Returns for items returned in Transactions Vendor return View and Invoice Import', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 23, False, 'Abilty to set User rights to Product List with visible products limited by filter', 'Use "Product List (Filtered %s)" Protected Action', pcProducts, slSimple);
  AddChangeList2(ctChange, 2, 0, 24, False, 'Transaction View now properly displays Payment Credits and Voids', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 24, False, 'View Tickets with Sales for Product now includes Unit Price column and Summaries', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 24, False, 'View Tickets with Sales for Product now support Column Layout saving', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 24, False, 'Shortcut for Inventory from Product List / View changed to Ctrl-O to avoid conflict', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 24, False, 'Register Transaction List now allows viewing multiple days of sales at once', '', pcTransaction, slNull);
  AddChangeList2(ctChange, 2, 0, 24, True, 'Added call to sp_SetStatementLimit, to remove limit on ADS v12', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 24, False, 'Option to set Component to Set Focus on New Order', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 24, False, 'Option to set Do Not Alert on Amount Exceed for Scale items', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 25, False, 'Added Next and Previous buttons on Report Options to move to next date scope', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 25, False, 'Date entry fields now support +/- entry to change to next/previous day', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 25, True, 'Adding Product in Total Inventory now defaults Pkg Case Sale Price Time Begins / Expires to 00:00 / 23:59 respectively', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 25, False, 'Generate Vendor Credits from Vendor Return List', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 25, True, 'Properly load Update folders on new installations', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 25, False, 'Fixed issue with some program features not enabled when using Multiple Upcharge / Discount Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 25, False, 'Edit options that their enabled / disabled depends on input of other edits now become enabled as soon as values have changed', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 25, False, 'Option to Copy Vendor Invoice to New Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 26, False, 'Support to allow overriding program email setting on report basis uisng XML commands', '', pcReports, slComplex);
  AddChangeList2(ctFeature, 2, 0, 27, False, 'Store Vendor Invoice / PO items to Clipboard and run Reports limited to stored items only', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 28, False, 'Store Customer Order / Register Transaction items to Clipboard and run Reports limited to stored items only', '', pcReports, slNull);
  AddChangeList2(ctChange, 2, 0, 28, False, 'Customer select View now clears all filters and Filter Box is focused when showing ', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 2, 0, 29, False, 'Fixed issue that caused some items to appear twice in when adding items from Vendor Item List', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 29, False, 'Option to set Customer orders to Auto Save without prompt to Save Changes', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 30, False, 'Support to specify Product fields to skip when using the Copy Product option', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 30, False, 'Fixed issue with Merge Customer', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 30, True, 'Support for ViewReceipt option in Report Grid Actions', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 31, False, 'Added Margin / Markup to Profit Sales Summary report Grid output', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 31, False, 'Fixed issue that prevented posting Relative Pricing Global Changes in certain instances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 31, False, 'Added column filter options for all columns in Vendor Invoices View', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 31, False, 'Support for Custom Lookup columns in Views', '', pcGeneral, slComplex);
  AddChangeList2(ctFeature, 2, 0, 31, False, 'Support for Conditional Cell Background Colors in Views', '', pcGeneral, slComplex);
  AddChangeList2(ctFeature, 2, 0, 31, False, 'Support for Product Custom Prices by Campaign on Price Update View', 'requires entries in Campaign list for Product Custom Prices setting', pcProducts, slComplex);
  AddChangeList2(ctFeature, 2, 0, 31, False, 'Support for Product Custom Prices by Campaign with Product Global Changes', 'requires entries in Campaign list for Product Custom Prices setting', pcProducts, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 31, False, 'Added Bogo column in Mix & Match Group View', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 31, False, 'Added Find Panel on Mix & Match Group View', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 31, False, 'Option to set Bogo On / Off for all products in Mix & Match Group at once', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 31, True, 'Added temporary option to "Load Special Column Values" for Unbound Columns', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 34, False, 'Support for FTP file handling in command line', '', pcGeneral, slComplex);
  AddChangeList2(ctBugFix, 2, 0, 34, False, 'Fixed issue that prevented filtering on ceratin Views in recent releases (since in version 1.2.0.31)', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 35, False, 'Tag Editor drop down should open upward when not enough space for complete list below', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 35, False, 'Allow saving column layout for Vendor Item Deliveries List View and increase screen width', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 35, False, 'Ability to use QB Items rather than Expenses when exporting Vendor Invoice to QB', '', pcVendors, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 35, False, 'Added Find Panel on User Setup View', '', pcUserSetup, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 35, False, 'Added Find Panel on Program Changes View', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 35, True, 'Tag Editor now displays the Popup List upwards when at bottom of screen', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 35, True, 'Popup List for Tag Editor width is now set to 300', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 35, False, 'Added Support for Customer Discount Expire Date', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 2, 0, 35, False, 'Added Support for Customer Discount Rules', '', pcCustomers, slComplex);
  AddChangeList2(ctFeature, 2, 0, 35, False, 'Added Support for Customer Access Pin', '', pcCustomers, slSimple);
  AddChangeList2(ctEnhancment, 2, 0, 35, False, 'Unapplied Payments now show on Customer Edit and Account History View', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 2, 0, 35, False, 'Ability to attach Documents to Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 36, False, 'Fixed an issue that could cause a Vendor Invoice created after a Vendor Credit to contain negative values', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 36, False, 'Fixed issues with loading some grids caused by Custom Lookup columns feature', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 37, False, 'Fixed issue that prevented doing Global Product Changes for Wic', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 2, 0, 37, False, 'Support for Department in Product List', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 37, False, 'Some fixes in user experience when moving between Groups and Users', '', pcUserSetup, slNull);
  AddChangeList2(ctChange, 2, 0, 37, True, 'Support for Tech username and password', '', pcUserSetup, slNull);
  AddChangeList2(ctChange, 2, 0, 38, True, 'Saving Layout Columns now prompts with option for Global when in Design Mode', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 38, False, 'Enlarged Markup / Margin value boxes on Price Update View to accomandate values larger than 100', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 38, False, 'Added Customer No Statement to Customer View and Global Changes', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 38, False, 'Option to Update Ageing for single customer on Customer View', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 2, 0, 38, False, 'View Product Sales Details added Summary View with summary for each price level sold', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 38, False, 'Fixed issue with Active Custom Price flag showing for products with zero priced entries', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 38, False, 'Fixed issue that prevented applying Customer Global Changes for Aging Alert', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 2, 0, 38, False, 'Fixed issue with markup / margin not updating for Campaign Pricing on Price Update View', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 38, False, 'Product Global Changes custom pricing "Add Campaign Price for All Records" now respects entries for "Begins / Expires" even when no price is set', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 38, False, 'View action was replaced with the Product / Customer actions in various screens to better reflect the expected action', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 38, False, 'Support for Select / Unselect / Reverse Selection rows in Vendor Invocie for Global Changes and Label printing (by selecting "Selected Items" from the "Select Items" combo)', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 39, False, 'Gift Card Transactions View for CD GiftSrv transactions', '', pcGeneral, slComplex);
  AddChangeList2(ctChange, 2, 0, 40, False, 'New updated application Icon', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 2, 0, 40, False, 'Support for Product Alias Codes', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 40, False, 'Support for pricing schemes according to Product Alias Code', '', pcProducts, slComplex);
  AddChangeList2(ctBugFix, 2, 0, 40, False, 'Fixed issue with some list popup being slow on initial loading when containg lots of different items', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 40, False, 'Fixed issue with confirm password not active on first attempt to add / edit user', '', pcUserSetup, slNull);
  AddChangeList2(ctFeature, 2, 0, 40, False, 'Option to copy order to new order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 40, False, 'Support for Custom fields on Browse Customer Orders view', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctChange, 2, 0, 41, False, 'Product seeks no search for PrdctSku when EnvSetting is set', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 41, False, 'TagEditor loading and searching enhancments', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 41, False, 'Fixed issue with reloading Product Custom Pkg Prices by Campaign on Price Update View', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 2, 0, 41, False, 'Support to push Register Log Summaries to QB Journal Entries', '', pcTransaction, slComplex);
  AddChangeList2(ctBugFix, 2, 0, 42, False, 'Fixed issue that prevented Campaign begins and expire dates to post on Price Update View when only dates were being edited', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 42, False, 'Ability to Password protect Add New Product', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 42, True, 'For new installations program now prompts for Site Name', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 42, True, 'Program Prefrences now includes a button to Select This Station (for Hardware settings etc.)', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 42, False, 'Gift Card Transactions View now includes a summary including Total Outstanding Gift Cards', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 2, 0, 43, False, 'Adding new record in Vendor Invoice now auto focuses on first editable column (including custom layouts)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 43, True, 'Support for Custom Fields in Customer Order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 43, True, 'Support for Custom Fields when importing Customer Order from XML', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 43, False, 'Support to save column layout at Customer Order View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 2, 0, 43, True, 'CrList table is now being closed after updating registration and user', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 43, False, 'Added Transaction Total and Customer Tel columns to Product Sales Detail View', '', pcProducts, slNull);
  AddChangeList2(ctChange, 2, 0, 43, False, 'Customer Discount Rule could now be cleared with the Del key', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 2, 0, 43, False, 'Option to Import Vendor Invoice items from Warehouse Orders', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 43, False, 'Added support to set scripted reports to export / email only if it contains data', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 43, False, 'Support to synchronize Vendor Balance info with QB Integration', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 43, False, 'Ability to set a specific Vendor Invoice not to export to QB with QB Integration', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 43, False, 'Adjustment value on Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctChange, 2, 0, 43, False, 'Product List now remembers previous set filters when adding products to Mix & Match group', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 44, False, 'Fix QB Vendor Invoice export calculation issue (since 1.2.0.43)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 44, False, 'Fixed an issue that caused an error loading Product Sales Detail View with no date set in certain instances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 45, False, 'Last printed Product Label/Sheet Format is now remembered for next use', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 45, False, '+ entered at end of Vendor Item or Product Code column in Vendor Invoice or PO passes entered code as filter to selection list when pressing Enter', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 45, False, 'Fixed issue when entering previously vendor discounted items to Vendor Invoice in Multiple Upcharge / Discount mode', '', pcVendors, slNull);
  AddChangeList2(ctChange, 2, 0, 45, False, 'Cost, Last Cost and Delivery info on Product View are now properly updated according to latest dated Vendor Invoice regardless of order it was enetred or edited', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 2, 0, 45, False, 'Fixed the search option in Invoice / Orders etc.', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 45, False, 'Opening Vendor Invoice thru Product Deliveries List now focuses on searched product', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 46, False, 'Fixed issue that prevented printing Shelf Labels in certain instances since 1.2.0.45', '', pcReports, slNull);
  AddChangeList2(ctFeature, 2, 0, 46, False, 'Support in Product List Reports to limit by products in Clipboard data from Vendor Invoice / Customer Orders etc.', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 46, False, 'Added customizable view of Product columns to Vendor Item Deliveries List', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 46, False, 'More columns now available in Column Chooser on Vendor Item Deliveries List', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 46, False, 'Fxied issue with Grid View of certain reports loading the wrong Dataset', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 2, 0, 47, True, 'Fixed issue that prevented opening Vendor Invoice in version 1.2.0.46', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 48, False, 'Fixed issue that caused certain Transaction to fail when exporting to QB Journal Entry', '', pcTransaction, slNull);
  AddChangeList2(ctChange, 2, 0, 48, False, 'New users added are now assign a Right level of 1 by default (previously defaut Right level was 0)', '', pcUserSetup, slNull);
  AddChangeList2(ctBugFix, 2, 0, 48, True, 'Fix in VendInvHelpers.qInvoiceChangesToPrdmstr to circumvent issues caused by changes to Cost, Last Cost etc. in version 1.2.0.45', '', pcVendors, slNull);
  AddChangeList2(ctChange, 2, 0, 49, False, 'Vendor Item Code column was changed to drop down view with auto popup and filter while typing', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 49, False, 'Fixed an issue that prevented certain reports from refreshing to latest data when opening same report in new tab', '', pcReports, slNull);
  AddChangeList2(ctChange, 2, 0, 49, True, 'Updated to DevEx v2016.2.2', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 49, False, 'Fixed issue that prevented loading Vendor Item Code List in latest release', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 49, False, 'Added support for many-to-one Vendor Item Code to Product Relationship', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 2, 0, 49, False, 'Added support to set U/P/C and Cost by Vendor Item Code', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 49, False, 'Auto refresh Vendor List after updating QB Balances from QB', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 2, 0, 49, False, 'Fixed issue that prevented storing Customer Order items to Clipboard ', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 2, 0, 49, False, 'Fixed issue that caused an error when pressing Date Scope arrow buttons with both date entries empty', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 49, False, 'Added Open Bill column on Customer History View (used with Bill Apply features)', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 49, False, 'Added operator filter helpers to grid columns for easy filtering', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 49, False, 'Customer History View now has the option to View Payment Distribution (when Bill Apply is set)', '', pcCustomers, slComplex);
  AddChangeList2(ctEnhancment, 2, 0, 49, False, 'Product List to add products on Customer Order View now provides same search capabilites as the general Product List View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 2, 0, 50, False, 'Restore Customer Order View splitter postion to allow for more displayed rows in selection Product List', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 2, 0, 50, False, 'Temporary removed operator filter helpers from Lists', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 2, 0, 51, False, 'Fixed some issues with many-to-one Vendor Item Code to Product relationship (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctChange, 2, 0, 51, False, 'Default column operator filter helper is now set to Begins With (on some columns)', '', pcGeneral, slNull);

  AddChangeList2(ctBugFix, 3, 0, 0, False, 'Opening Vendor Invoice thru Product Deliveries List now focuses on searched line (even with multiple lines of same product within invoice)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 0, False, 'Fixed issue with wrong info displaying in the Vendor Item Code Relationship after first popup (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 0, False, 'Fixed issue with Cost Change indicator not displaying properly in some instances (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 1, False, 'Fixed issue that caused Report Options view not to be resized properly to full width (since 1.2.0.49)', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 3, 0, 1, False, 'Fixed issue with certain popup screens not returning focus to Invoice Details grid', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 1, False, 'Fixed certain issues with Cost / U/P/C and Vendor Item Code changes in recent versions (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 1, False, 'Fixed issue that caused Inventory not to be udpated when deleting line in Vendor Invoice in certain circumstances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 1, False, 'Added Last Sold and Inactive columns to Mix & Match View with option to Show / Hide Inactive Products', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 2, True, 'Now assigning an internal name TableView1CheckBoxColumn to the Vendor Invoice CheckBoxColumn in an attempt to fix issues of CheckBox column not shwoing after saving custom column layout', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 2, False, 'Fixed issue that caused Cost Change Indicator not to reflect changes made to U/P/C without changing cost afterwards', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 2, False, 'Fixed issue that caused an error to be raised when applying a filter in the FilterRow of ceratin List Grids (since 1.2.0.49)', '', pcLists, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 2, False, 'Add support for Case Price at Customer Custom Pricing List and Edit View', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 3, 0, 2, False, 'Clear Filters shortcut was changed to Ctrl-S (instead of Ctrl-R) to prevent conflicting with View Product', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 2, False, 'Importing Customer Order from OrderExchange remove ().- and space from Phone field', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 3, 0, 2, False, 'Added Expand / Collapse option to Customer Order header info panel', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 3, 0, 2, False, 'Fixed issue that prevented adding products to Mix & Match Groups (since 1.3.0.1)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 4, False, 'Fixed issue that prevented prompting "Unit Return / Case Return" in Vendor Credit when Quan was being edited after line was posted ', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 4, False, 'Fix issue with column filters for certain special columns in Product List', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 4, False, 'Add supoprt in Global Product Changes for "Do Not Combine on 1 Line in Sale" when set (setup2)', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 4, False, 'Add support to export Gift Card Transactions View to XLS / XLSX', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 3, 0, 4, True, 'Updated to DevEx v2016.2.3', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 4, False, 'Fixed issue that caused Vendor Item Code Relation View not to show selection options properly in some cases (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 5, False, 'Fixed some behavior issues with new Vendor Item Code column changes in Vendor Invoice (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, True, 'Added Setting # for Program Prefrences with auto expand on searches', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 6, False, 'Fixed issue that prevented auto focusing on current product when opening Mix & Match Edit View from Product View (since 1.3.0.1)', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, False, 'Added support for custom report/label printing in Vendor Invoice when posting line and/or with column button', 'Requires setting VendInvLineEditReportJob in Program Prefrences', pcVendors, slComplex);
  AddChangeList2(ctBugFix, 3, 0, 6, False, 'Merge Delivery records (DlvCntrl) to Merge Customer procedure', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, False, 'Update Last Cost / Delivery and Last Sold info after a Merge Products to reflect merged info', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 6, False, 'Program Prefrences now add USER_ID to OSComputerName for terminal clients', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 6, False, 'Prompt for Cost or U/P/C change in Vendor Invoice now defaults to "Update Both" where applicable', '', pcVendors, slNull);
  AddChangeList2(ctChange, 3, 0, 6, False, 'Prevent Auto Logout on idle while report data is loading', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, False, 'Merge Product now provides option to add product being merged as Alias Code in selected product', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, False, 'Add support to export Customer Deliveries View to XLS / XLSX', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 6, False, 'Add support to export Register Transaction List to XLS / XLSX', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 6, False, 'Fixed issue with no Vendor Invoice Cost changes being posted to Product when not set to prompt for Cost Changes (This Delivery Only prompt)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 7, True, 'Fixed issue with Custom Column Styles when comparing with Other Column', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 7, True, 'Fixed issue that could cause all customers to be deleted when deleting one customer', 'Multiple customers set to same CustID, such as 0');
  AddChangeList2(ctEnhancment, 3, 0, 7, False, 'Added options to prefill Generate PO View with Qty of sales within time frame', '', pcVendors, slSimple);
  AddChangeList2(ctEnhancment, 3, 0, 7, False, 'Added options to prefill Generate PO View with last vendor', '', pcVendors, slSimple);
  AddChangeList2(ctBugFix, 3, 0, 7, False, 'Fixed issue that caused certain Product List actions to fail while a Find Panel filter is active', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 7, False, 'Added option to export User Setup - All Users view to Excel', '', pcUserSetup, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 7, False, 'User Setup - All Users view now includes the Right level for user in Column Captions', '', pcUserSetup, slNull);
  AddChangeList2(ctChange, 3, 0, 8, False, 'Assign proper alternate captions to all special columns so that Quick Column Customization shows captions for all options', '', pcVendors, slNull);
  AddChangeList2(ctChange, 3, 0, 8, False, 'Change certain message dialogs to Alert Window messages', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 8, False, 'Add support to export Customer Orders List View to XLS / XLSX', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 3, 0, 8, False, 'Fixed issue that prevented doing Global Changes on Product List in version 1.3.0.7', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 8, True, 'Changes in Station Registration to use IP and MAC were available', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 9, False, 'Save Vendor Invoice header info when using Save & New even with no changes to details', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 9, False, 'Fixed issue that prevented doing Global Changes on Product List with some products in list not Selected (since 1.3.0.8)', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 9, False, 'Adjusted row caption column in Global Product Changes View to make captions fully visible by default', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 10, True, 'Updated to DevEx v2016.2.4', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 3, 0, 10, False, 'New Custom Filter option for Product List using Report Query box', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 10, False, 'Use Legacy Grid Popup for Vendor Item Code Lookup', '', pcVendors, slNull);
  AddChangeList2(ctChange, 3, 0, 11, True, 'Downgraded to DevEx v2016.2.3', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 12, False, 'Fixed issue that prevented auto complete of existing Tags on Customer Edit View', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 12, False, 'Scroll bars are now available on Product Edit View even when not in Edit mode', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 12, False, 'Scroll bars are now available on Customer Edit View even when not in Edit mode', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 12, False, 'Fixed issue that prevented custom column move settings from working properly in new Vendor Code column in Vendor Invoice (since 1.2.0.49)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 13, False, 'Fixed issue that prevented doing Global Changes on Customer List (since version 1.3.0.7)', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 13, False, 'Fix issue with new Custom Filter option for Product List not effecting Global Changes etc.', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 14, False, 'Fixed issue with Gift Card Transactions View that caused gift card server to fail', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 15, False, 'Fixed issue that caused % sale mode to be reset to $ sale mode when using Price Update View', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 3, 0, 15, False, 'Added support for Unbound Columns in Report Grid Views', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 3, 0, 15, False, 'Fixed issue that prevented loading Product Sales Detail View in certain instances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 15, False, 'Added keyboard shortcut keys for sections in Customer Order View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctFeature, 3, 0, 16, True, 'Support for CD Expression Engine', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 3, 0, 16, False, 'Support for Do Before New Order custom setting for New Customer Orders', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 16, False, 'Option to have configured Delivery Messages show as Combo for Customer Orders Remark field', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctEnhancment, 3, 0, 16, False, 'Enhanced user experience when adding products to Vendor Invoice / Customer Order using Product Select View', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 3, 0, 16, False, 'Suuport in Custom Display engine to add Expression values', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 16, False, 'Customer order being Auto prinetd, now does not show any dialog for report options or printer settings', '', pcCustomerOrders, slNull);
  AddChangeList2(ctFeature, 3, 0, 17, False, 'USPS Address Verification Service now available within Customer List / Edit View', '', pcCustomers, slSimple);
  AddChangeList2(ctFeature, 3, 0, 17, False, 'GeoCoding Lookup Service now available within Customer List / Edit View', '', pcCustomers, slSimple);
  AddChangeList2(ctEnhancment, 3, 0, 17, False, 'Added Activity Log View on Customer Delivery Browse scoped on selected row', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 3, 0, 17, False, 'Changing Product Code now cascades all past sales to new code', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 17, False, 'Ship To support added for Customers', '', pcCustomers, slComplex);
  AddChangeList2(ctEnhancment, 3, 0, 17, False, 'Added support to select Ship-To address from current customer Ship-To List', '', pcCustomerOrders, slNothing);
  }
  AddChangeList2(ctChange, 3, 0, 18, True, 'Add alias to prd_ fields included in Vendor Invoice thru custom columns, to allow columns with duplicate in Delitm table (i.e NoDis)', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 18, False, 'Fixed issue that caused an error to when clicking on Transaction View grid while Transaction Report is ebing generated', '', pcTransaction, slNull);
  AddChangeList2(ctChange, 3, 0, 18, True, 'Changes to HourGlass cursor display, now constant for user input and query loading', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 3, 0, 18, False, 'Fixed issue that prevented Find Payment Record report fom loading signature images in certain instances', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 18, False, 'Enhancments in auto handling of certain Address Validation results', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 18, False, 'Enhancments in GeoCoding lookup service', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 18, False, 'Fixed issue with proper vendor item code not being set when using Tab to close Vendor Item dropdown', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 3, 0, 18, False, 'Added "Do Not Post Inventory" option to allow postpone adding Vendor Invoice to Inventory', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 18, True, 'Support for using +/- to date keywords in auto and memorized reports (Last_Month+15 etc.)', '', pcReports, slNull);
  AddChangeList2(ctChange, 3, 0, 18, True, 'Updated to DevEx v2017.1.3', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 18, True, 'Updated to Dephi 10.2', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 20, True, 'Updated RBuilder to 18', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 18, False, 'Option to include Site Name in Program Form Caption', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 19, False, 'Password protect Edit / Delete Customer Card on File', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 19, True, 'Back to Delphi XE7 - due to numeric Advantage issue on Nulls', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 20, True, 'Back to RBuilder 16.03 due to issues with multi page reports running endlessly', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 21, False, 'Fix some Task List issues', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 21, False, 'Support for YTD / MTD on Product View (Product List field need to be set accordingly)', '', pcProducts, slComplex);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue in Vendor Invoice with user leaving Vendor Item dropdown empty and focused dropdown item that caused focused item from drop down is entered in Invoice', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 3, 0, 22, False, 'Support to Delete Multiple Selected products from Product List', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue with "Do Not Post Inventory" option in Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctChange, 3, 0, 22, True, 'Change alogritum of Onhand Update to speed up saving Vendor Invoices', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue that caused search box entries to get deleted while typing when Custom Unbound columns are set', '', pcLists, slNull);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue that causes changes to Begin / Expire dates on Price Update View not to post when no other values are edited', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue that caused previously double clicked product to be entered to Vendor Invoice / PO / Customer Order when subsquently hitting Enter on different product from list', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 22, True, 'Updated RBuilder to 18', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 22, False, 'Fixed issue that caused "Unvalidated Records in Invoice" flag to be raised for all entries added with "Add items" option in Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 22, False, 'Added Note option on Customer Edit View', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 22, False, 'Support in Product List Report to filter by Products Active / Inactive', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 22, False, 'Support to Export Product Sales View to Excel', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 22, False, 'Print / Preview option on Sales Summary View', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 3, 0, 23, False, 'Support for SoundEx searches in Product List (with option to turn off using Seacrh Exact)', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 23, False, 'Respect Close.all command on program startup', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 23, False, 'Support Restart.all command - checking evry minute, user gets 3 minutes to finish work', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 23, False, 'Fixed issue that allowed running Product Global Changes with no filter set', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 23, True, 'Fixed issue that prevented setting Report promt Default Values in Rep Option table', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 3, 0, 23, False, 'Fixed issue that caused an error in some instances when using Refresh option in loaded Grid View Report', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 3, 0, 23, False, 'Fixed issue with Cost Up / Down mark not showing proper values in some instances on Vendor Invoice with Discount', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 23, False, 'Fixed issue with Vendor Invoice % Discount not recording properly when applied before first item entered on invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 23, False, 'Sale Price status indicators now repect the Time values as well (not only by Date)', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 24, True, 'All forms now inherit from TdxForm', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 24, False, 'Add Family Code on Product List / View / Global Changes', '', pcProducts, slNull);
  AddChangeList2(ctChange, 3, 0, 24, False, 'Customer Order add shortcut keys to Product Search List and Due Date sections', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 3, 0, 24, False, 'Disable Auto Save on empty Customer Order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 3, 0, 24, False, 'Fixed an issue that prevented udpated Cost from being saved to Vendor Item Code in certain instances when changing price on Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 24, False, 'Added more Product and Stock column to Generate Vendor PO View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 24, False, 'Restrict viewing Cost on Generate Vendor PO View when not authorized', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 24, False, 'Added Column Chooser and Save Column Layout option on Generate Vendor PO View', '', pcVendors, slNull);
  AddChangeList2(ctChange, 3, 0, 25, True, 'Changed behavior when Report Prompt is disabled thru a Custom Filter, now Query Component is created and not shown instaed of not created at all', '', pcReports, slNull);
  AddChangeList2(ctFeature, 3, 0, 26, False, 'Special Coupon List View to support Register Spend & Get Module', '', pcProducts, slComplex);
  AddChangeList2(ctBugFix, 3, 0, 27, False, 'Fixed issue that prevented applying Global product Changes in version 1.3.0.26', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 28, False, '"Exact Search" selection on Product List is now stored and reloaded on program startup', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 28, False, 'Product Delivery List now includes Date Filter option', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 28, False, 'Product Delivery List now includes option to Hide Zero Quantity Deliveries', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 3, 0, 28, False, 'Added support in Vendor Invoice for Print Cost / Amount option', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 3, 0, 28, False, 'Vendor Invoice now respects Not Allow Cost option (multi adjustment module only)', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 3, 0, 28, False, 'Prevent saving Customer Credit Card Info with empty Expire date', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 3, 0, 29, False, 'Added support to Restrict "Account Debits" for Customer (Does Not Restrict Charges on Credit Balance)', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 3, 0, 29, False, 'Support for Tokenized Customer Credit Card Info', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 3, 0, 30, False, 'Fixed issue that caused an error on Merge Customer', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 3, 0, 30, False, 'Product List "Current Specials" column includes specials expiring today', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 30, False, 'Added product column (Description, Category etc.) to Vendor Returns View', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 30, False, 'Fixed rounding when calculating Old Unit Cost in Vendor Invoice, this effects the way the Cost Change Level is calcualted', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 3, 0, 30, False, 'Support to Add Items to Customer Order from Scanned Batch', '(BathItems table thru HandHeld App)', pcCustomerOrders, slComplex);
  AddChangeList2(ctEnhancment, 3, 0, 31, True, 'Enhance "Add Items to Customer Order from Scanned Batch" feature', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 3, 0, 32, True, 'Hide Print Cost / Amount columns in old Vendor Invoice form', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 32, False, 'Fixed issue that caused wrongfully invalidated records when importing item to Vendor Invoice with Cost that differs than the previous Vendor Cost', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 32, False, 'Add Items Dialog (Vendor Invoice / PO and Customer Orders) now returns to previous focused record after Select All etc. procedures', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 32, True, 'Fixed issue with GridSizeByRecordAndColumnCount procedure', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 32, False, 'Fixed issue that prevented setting "Received from Vendor" dates on the Add Items to Vendor Invoice View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 32, False, 'Added ability to set Info Tab Sheet to Show when opening Customer Order', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctChange, 3, 0, 32, True, 'Changed Message Box prior to copying Updated application from server to timeout after 15 seconds', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 32, False, 'Speed up merge Product process', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 33, False, 'Enhanced Tag Editor', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 3, 0, 33, False, 'Fix issue when calculating Discount / Adjusments values on "Print Cost / Amount" columns in Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 33, False, 'Product List search tab on Customer Order Edit View now supports Show / Hide Inactive Products', '', pcCustomerOrders, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 33, False, 'Merge / Delete Product now handles Product Alias list', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 3, 0, 33, False, 'Fixed issue that prevented saving CC pyament Info on New Customer Orders', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 3, 0, 33, True, 'Updated to DevEx v2017.2.2', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 34, False, 'Option to "Disable New Touch Scroll Mode Bars"', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 34, False, 'Program Preferences is now always visible under File Menu (if not Admin logged in, program will prompt with Logon)', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 34, True, 'Vendor Returns View will now load when enabled in ModuleList even if not set in Envsta setting', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 3, 0, 34, False, 'Fixed issue that prevented loading certain Views in latest release due to TagEditor changes', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 3, 0, 34, True, 'Updated to DevEx v2017.2.3', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 34, False, 'Added support for Column Filters, Row Selection / Generate by Selection in Vendor Returns View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 34, False, 'Support printing Vendor Invoices from Vendor Invoice List View (with option to Multi Select Invoices)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 3, 0, 34, False, 'Support auto printing generated Vendor Returns (with checkbox on Generate Vendor Credits Dialog)', '', pcVendors, slNull);

  AddChangeList2(ctBugFix, 4, 0, 0, False, 'Fixed issue causing an Access Violation when closing window with application X', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 4, 0, 1, False, 'Fixed issue that prevented opening Customer Ship To List since 1.3.0.33 due to TagEditor changes', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 4, 0, 1, False, 'Fixed issue that prevented viewing Task List in recent version', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 4, 0, 1, False, 'Fixed issue with saving Tokenized Customer Credit Card Info', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 4, 0, 1, False, 'Custom Price Status on Product Edit View now repects the Unactive Flag on Custom Price', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 1, False, 'Fixed issue that caused wrong Product Cost to be set as default Cost on new Vendor Invoice when a different U/P/C was used in latest Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 4, 0, 2, False, 'Prevent error from being raised when operating system rights prevent creating / accessing RepHead.ini', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 2, False, 'Added support for Delete Record in Vendor Returns View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 2, False, 'Support on Vendor Invoice View to directly email invoice to Vendor', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 4, 0, 2, False, 'Fixed issue that caused Tag Filters on List not to take immediate effect in recent releases (since 1.3.0.33)', '', pcLists, slNull);
  AddChangeList2(ctFeature, 4, 0, 2, False, 'Support for Location Inventory, Product View / Adjust Inventory / Vendor Invoice / POS', 'Setup: #678 / #679 - POS OnHand Inventory Location - GLOBAL / LOCAL, Prefs: #424 - List of all Locations where Inventory is stored', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 2, False, 'Fixed issue that causes ADS error 7209 when opening Vendor Item Deliveries View in certain instances', 'Added DisableControls before reopening data', pcVendors, slNull);
  AddChangeList2(ctChange, 4, 0, 3, True, 'Changed PrdLocTransfer PostQueries to use new cdTransferOnHandLoc instead of script', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 4, 0, 3, False, 'Fixed issue that caused AV to be raised in cetain instances while using Find Panel in User Setup View', '', pcUserSetup, slNull);
  AddChangeList2(ctFeature, 4, 0, 3, False, 'Product Not for Sale option added', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 4, False, 'Fixed issue that prevented loading Grid View reports in latest release (1.4.0.3)', '', pcReports, slNull);
  AddChangeList2(ctFeature, 4, 0, 5, False, 'Support for Multi Location Product Info lookup', '', pcProducts, slComplex);
  AddChangeList2(ctFeature, 4, 0, 5, False, 'Support for Conditional Formatting Rules in Grids', '', pcGeneral, slComplex);
  AddChangeList2(ctEnhancment, 4, 0, 6, False, 'Add Multi Location Product Info lookup option to Vendor Invoice View', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 4, 0, 6, False, 'Added Product Sales Summary option at Vendor Invoice View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 7, True, 'Add Support for Unbound Column in Vendor PO View', '', pcVendors, slComplex);
  AddChangeList2(ctEnhancment, 4, 0, 7, True, 'Support for ViewCustAccountHist option in Report Grid Actions', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 7, False, 'Added column "Last Balance" to Customer Account History View to allow comparing to Running Balance and detect Discrepancy rows', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 7, True, 'Support to store Column Style formatting in GridData Style column', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 8, False, 'Support for Begin & Expire Date, Time and days on BOGO promotions', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 8, False, 'Modified load timing of Lookup Combo Lists in Report Query Option loading to improve loading speed', '', pcReports, slNull);
  AddChangeList2(ctChange, 4, 0, 8, True, 'Modified load timing of actions on program startup and improve status label messages', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 4, 0, 9, False, 'Support Excel import file for Location Inventory Transfer', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 9, False, 'Fixed issue that caused wrong Cost to be populated on Vendor Invoice on Products set with a U/P/C of 1 when using legacy Vendor Item Code selector', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 4, 0, 9, False, 'Support for Product Images', '', pcProducts, slComplex);
  AddChangeList2(ctEnhancment, 4, 0, 10, False, 'Customer Credit Card Info editing is now fully out of scope (requires Cardknox account)', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 10, False, 'Added Product Images support to Add Item View (add multi Products to Vendor Invoice / PO)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 10, False, 'Auto correct required missing ADS Server Port (6262) in Ads.ini settings', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 10, False, 'Support TisWin3Setup.exe', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 11, False, 'Bump version to move forward with TisWin3Setup.exe', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 12, False, 'Add CSV Export support with very large DataSets that could cause out of memory issue', '', pcReports, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 12, False, 'Added Receipt Via and Notify Via option to Customer Global Changes', '', pcCustomers, slNull);
  AddChangeList2(ctBugFix, 4, 0, 12, False, 'Fixed issue that prevented newly supported Custom Fields from showing in Lists with user saved Layout Columns', '', pcLists, slNull);
  AddChangeList2(ctChange, 4, 0, 12, False, 'Change Lookup Combo Lists in Report Query Option to load at form loading when set with a Default Value (change to behaivor intorduced in 1.4.0.8)', '', pcReports, slNull);
  AddChangeList2(ctFeature, 4, 0, 12, False, 'Ability to allow Product View changes to be made from Customer Order line', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctBugFix, 4, 0, 12, False, 'Fixed issue that caused Global Changes from Vendor Invoice to process items in invoice that were not Selected in certain instances', '"Vendor Invoice View (Alpha)" Only', pcVendors, slNull);
  AddChangeList2(ctBugFix, 4, 0, 12, False, 'Fixed issue with Min / Max values not being posted from Campaign Price Update when no other changes were made', '', pcProducts, slNull);
  AddChangeList2(ctChange, 4, 0, 12, True, 'Report Builder version 19.01', '', pcReports, slNull);
  AddChangeList2(ctChange, 4, 0, 13, True, 'Change ProgPrefReload Try Catch to Delete rather than Drop Table', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 13, False, 'Support MRU in Product / Customer Lists search box', '', pcLists, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 13, False, 'Support MRU in Product List search box in Customer Order View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 4, 0, 13, True, 'Support for InstallUpdateOnly command line to support TisWin3Setup', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 14, False, 'Enhance CSV Export to Double Quote each exported value to fix comma within field issues', '', pcReports, slNull);
  AddChangeList2(ctChange, 4, 0, 14, False, 'Store "Search Exact" status of Product List search on Customer Order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 4, 0, 14, False, 'Speed up load time of Product List view on Customer Order', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 4, 0, 14, False, 'Fix issue that caused Product List view on Customer Order MRU not to work after first order viewed', '', pcCustomerOrders, slNull);
  AddChangeList2(ctBugFix, 4, 0, 14, True, 'Fix issues with DoAppUpdate to copy and run TisWin3Setup.exe', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 4, 0, 14, True, 'Updated to DevEx v2018.1.2', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 4, 0, 15, True, 'Changed logic for DisableTouchScrollUIMode setting to use new ScrollbarMode setting instead of deprecated TouchScrollUIMode', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 4, 0, 15, False, 'Fixed issue with right click context menu not working on Lists in latest version', '', pcLists, slNull);
  AddChangeList2(ctBugFix, 4, 0, 15, False, 'Fixed issue with Search Box MRU not updating Cnt value in all instances', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 4, 0, 15, False, 'Changed Search Box MRU Popup to show upward  - to limit blocking grid rows', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 15, False, 'Added Action Log View support to Product List and View', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 15, False, 'Fixed issue with Application feedback Sound not working in recent versions', 'Resources were missing - maybe due to git?', pcGeneral, slNull);
  AddChangeList2(ctFeature, 4, 0, 16, False, 'Option to Filter Product List on Clipboard Data', '', pcProducts, slNull);
  AddChangeList2(ctChange, 4, 0, 16, True, 'Changed Set Product Clipboard action to show Alert Window instead of Message Dialog', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 16, False, 'Support for Color Palettes when using new Bezier scaled Skin', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 4, 0, 16, True, 'Report error log now includes the selected Output and Output ID', '', pcReports, slNull);
  AddChangeList2(ctFeature, 4, 0, 16, False, 'Added support to password protect "Product Global Changes on Prices", allowing to give rights to Global Changes but not on Global Price Changes', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 16, True, 'Removed extra line in row headers on Global Product Changes rows', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 16, False, 'List Current Tags in Product Global Changes Add / Remove Tag Editors', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 16, False, 'List Current Tags in Customer Global Changes Add / Remove Tag Editors', '', pcCustomers, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Changed logic to reload Filter on Tags List only when needed to speed up tag entry in filter', '', pcLists, slNull);
  //AddChangeList2(ctChange, 4, 0, 17, False, 'Added searched image path to No Product Images Found message when viewing Product Images and no images found', '', pcProducts, slNull);
  //AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Added Product Image file naming convention to No Product Images Found message', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Added setting to allow presetting Vendor Item Deliveries List filter by Start Days', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Added setting to allow presetting Vendor Item Deliveries List filter by Date Scope', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Drag and Drop or Browse to Add Images option added to Product Images View', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, True, 'Reverse chage made in 1.4.0.16 to remove extra line in row headers on Global Product Changes rows as it forced wrong widths in edit columns', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 17, False, 'Fixed issue with wrong results being displayed when entering numeric value in Find box on Product List and "Search Exact" is unchecked', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Support to disable MRU list in Product / Customer Lists search box', '', pcLists, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 17, False, 'Support to disable MRU list in Product List search box of Customer Order View', '', pcCustomerOrders, slNull);
  AddChangeList2(ctChange, 4, 0, 17, True, 'Updated to DevEx v2018.1.3', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 4, 0, 18, False, 'Added Status Edit / Filter options on Vendor Invoices', '', pcVendors, slNull);
  AddChangeList2(ctChange, 4, 0, 19, False, 'Restrict Product Edit Auto Tare value to positive values only', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 19, False, 'Fixed issue that prevented opening User Setup View in latest release', 'DevEx v2018.1.3 issue, removed ShowFindPanel call in FormCreate', pcUserSetup, slNull);
  AddChangeList2(ctBugFix, 4, 0, 20, False, 'Fixed issue that prevented adding more than one image to Product Images View in certain instances', '', pcProducts, slNull);
  AddChangeList2(ctChange, 4, 0, 20, False, 'Adding images to Product Images View  now auto sets the focus on the added image', '', pcProducts, slNull);
  AddChangeList2(ctChange, 4, 0, 20, True, 'RepLog table now includes Output ID and Report LastDate (Version)', '', pcReports, slNull);
  AddChangeList2(ctChange, 4, 0, 20, False, 'Add Inventory Location column in Vendor Invoice List View (when locations are set)', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 20, False, 'Option to copy single Cell value to clipboard in all Lists', '', pcLists, slNull);
  AddChangeList2(ctFeature, 4, 0, 20, False, 'Support to Recalculate Inventory for Product at Product View and by Utility menu', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 20, False, 'Fixed issue that caused Price changes to Mix and Match groups to log changes only for first product', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 21, False, 'Fixed issue that prevented Inventory changes from being recorded properly when changing UpcCode for a line in an existing Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctChange, 4, 0, 21, True, 'Product Location Transfer actions are now Visible only when Location List is set in Program Preferences', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 4, 0, 21, False, 'Fixed issue with Global Product Relative Price Changes not being cleared when showing Global Changes again', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 21, False, 'Global Customer Changes now supports Custom Customer Fields', '', pcCustomers, slNull);
  AddChangeList2(ctFeature, 4, 0, 22, True, 'Support for CdComboBox and CdComboBoxFixedList in Custom Fields at Product and Customer Views', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 4, 0, 23, False, 'Support to Add Items to Vendor PO from Scanned Batch', '(BathItems table thru HandHeld App)', pcCustomerOrders, slComplex);
  AddChangeList2(ctChange, 4, 0, 24, False, 'Product Price changes done through Global Changes or Mix & match products are now reflected in the Prior Price and Price Changed On values', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 4, 0, 24, False, 'Enhanced Conditional Formatting Support Rules in Grids', '', pcGeneral, slComplex);
  AddChangeList2(ctBugFix, 4, 0, 24, False, 'Prevent endless loop on program startup when newer version is required and it''s not in update path', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 4, 0, 25, True, 'Fixed issue that caused an endless loop when program attempts to restart itself by CompuDime network command', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 4, 0, 25, True, 'Program Login now calls cdUpdateCrUseData', '', pcGeneral, slNull);

  AddChangeList2(ctBugFix, 5, 0, 0, False, 'Fix issue that prvented auto running newer TisWin3Setup from version 1.4.0.24 with message that target "does not contain a newer setup version"', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 1, False, 'Added "Not for Sale" option to Product Global Changes', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 5, 0, 1, True, 'Updated to DevEx v2018.1.7', '', pcGeneral, slNull);
  AddChangeList2(ctChange, 5, 0, 1, True, 'Support for Product Searches by adding leading zero and check digit (BcHandle 391-400)', '', pcProducts, slNull);
  AddChangeList2(ctChange, 5, 0, 1, True, 'Changes to SQL used in TVendInvHelpers.LastCostForProduct to get cost from latest vendor Invoice to improve speed', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 2, False, 'Enhancments to Future Cost List feature', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 5, 0, 2, False, 'Option to set Vendor Invoices not to Update Product Cost use Future Cost List instead', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 2, False, 'Enetering Search box on Register Transaction List now auto focuses to start of box (with no selection)', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 2, False, 'Gift Card Transactions View now includes an option "Open Cards View"', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 3, False, 'Fixed issue with Recalculate Inventory when using Location Inventory in certain instances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 3, False, 'Recalculate Inventory for Product now respects the preset "Inventory Calc From Date" (EnvSet-InvClcFrm)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 3, False, 'Fixed issue that prevented Multi Location Product Info from displaying data in some instances', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 4, False, 'Product Location Transfer now supports Remote Locations', '', pcProducts, slNull);
  AddChangeList2(ctChange, 5, 0, 5, True, 'Product Calculate Sales now uses Stored procedure for data calculation to enforce uniformity with View Sales Location Data', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 5, False, 'Fixed issue that caused Vendor Invoice item Cost not to be populated when no previous delivery is found (Cost is filled by transfer etc.)', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 5, 0, 5, False, 'Support Multi Location Sales Data at Product List / View', '', pcProducts, slNull);
  AddChangeList2(ctChange, 5, 0, 5, False, 'Selecting All for Date Scope on Vendor Item Deliveries List now defaults to last 10 Years of data', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 6, False, 'Support setting a Vendor to "Do Not Update Product Last Vendor"', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 5, 0, 6, False, 'Fixed issue that caused Last Vendor to be wrongly changed when canceling a Quick Delivery/Cost Change entry', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 5, 0, 6, False, 'Support Barcode Scanner when is focused in Alias List on Product Edit View', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 7, False, 'Support assigning (default) QB Class Name to QB Integration Vendor Invoices', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 7, False, 'Enhance Style on Vendor Invoice Print Cost column to indicate greater than / less than values vs. Cost', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 7, False, 'Fixed issue with scanning new EAN8 item that are being scanned with a preface of S08FF', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 7, False, 'Support to view Mix & Match Group on Product List', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 7, False, 'Support to view Mix & Match Group on Vendor Invoice Edit View', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 7, False, 'Support editable Product fields in Vendor PO Edit View', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 7, False, 'Report Queries that Enable / Disable depending on other entries set, now change the Enabled status instantly with no need to move to other prompt first', '', pcReports, slNull);
  AddChangeList2(ctFeature, 5, 0, 7, False, 'Add Products Tag List View to View / Delete Product Tags', '', pcProducts, slNull);
  AddChangeList2(ctFeature, 5, 0, 7, False, 'Add Customer Tag List View to View / Delete Customer Tags', '', pcCustomers, slNull);
  AddChangeList2(ctChange, 5, 0, 8, False, 'Added Password Rights for Products & Customer Tag List View', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 8, False, 'Fixed display issue with Sale Price section on Update Product Pricing View not changing to Margin when selected in Price Calculation Method radio', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 8, False, 'Fixed issue with Vendor Invoices set not to Update Product Cost when deleting Vendor Invoice with other cost', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 5, 0, 8, False, 'Adjusted issue with Cost being posted to Vendor Invoice when set not to Update Product Cost', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 8, False, 'Prevent editing Future Cost list entry that was already Posted to Product', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 9, False, 'Support Last Quarter option in Date Scope Lists', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 9, False, 'Fixed an issue where using + / - in a Date Edit didn''t post changes when no other value was updated on View', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 5, 0, 9, False, 'Support Multi Location Delivery Data at Product List / View', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 9, False, 'Fixed issue that prevented doing Product Location Transfer from Remote Location to Local Location', '', pcVendors, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 9, False, 'Product Location Transfer between Remote Locations are now set to auto email resulting invoice to Vendor Email address set at Vendor View', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 9, False, 'Added confirmation dilaog when closing "Product Location Transfer View"', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 9, False, 'Program Preferecnes View now includes a Tree List of categories', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 10, True, 'Fixed issue that caused an endless loop when program attempts to restart itself by CompuDime network command', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 10, False, 'Fixed issue with delayed Summary calculation when using the Copy Vendor Invoice option', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 5, 0, 11, False, 'Fixed issue that removing cost related columns from Vendor Invoice View', '', pcVendors, slNull);
  AddChangeList2(ctBugFix, 5, 0, 11, False, 'Fixed rounding issue when Posting Future Cost with high unit cost precision', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 12, True, 'ScanResolve class now respects PrcEncd2 setting (setup2 #552)', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 12, False, 'Fixed issue with wrong Prior Cost being pushed to Future Cost when accessed from Vendor Invoice line', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 12, True, 'Running -JobFile switch now allows running as an additinal instance of application with no need to terminate open instances', '', pcReports, slNull);
  AddChangeList2(ctFeature, 5, 0, 12, False, 'Consignment Cost / Profit support at Product List and Transactions', '', pcProducts, slSimple);
  AddChangeList2(ctEnhancment, 5, 0, 12, False, 'Added support to assign Edit Rights to Product View by sections', '', pcProducts, slNull);
  AddChangeList2(ctChange, 5, 0, 13, False, 'Add Transaction section in main menu and move transaction related items from Customer', '', pcGeneral, slNull);
  AddChangeList2(ctFeature, 5, 0, 13, False, 'Add Line Buster Batches View', '', pcTransaction, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 13, False, 'Respect Product View Edit Rights in Product Global Changes View', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 14, False, 'Fix issue with new Transactions menu', '', pcGeneral, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 15, False, 'Added "Last ? Weeks" option in Prior Orders tab of Customer Orders, defaults to 6 Weeks could be changed', '', pcCustomerOrders, slSimple);
  AddChangeList2(ctBugFix, 5, 0, 15, False, 'Fix issue in latest version that caused some popup lists to move to other end of screen', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 16, False, 'Fixed issue that caused subsqent JibFile reports not to load all XML passed values', '', pcReports, slNull);
  AddChangeList2(ctChange, 5, 0, 17, False, 'Support custom Weight Encoded Barcode scanning', '', pcProducts, slComplex);
  AddChangeList2(ctBugFix, 5, 0, 18, True, 'Fix issue with missing edit components on Product Edit View in latest release', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 19, False, 'Support TLS to Email Reports for smtp servers other than GMail on port 465', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 5, 0, 19, False, 'Fixed issue with Tags Add / Remove not clearing on Global Changes view between uses', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 19, False, 'Support for Print Discount / Upcharge with Print Net column on Vendor Invoice', '', pcVendors, slNull);
  AddChangeList2(ctFeature, 5, 0, 20, False, 'Added support to specify QB Class to apply for QB Vendor Bills by each Vendor', '', pcVendors, slNull);
  AddChangeList2(ctChange, 5, 0, 20, False, 'When Consignment Cost / Profit option is active, Product Edit View now always defaults to the Price tab when opened', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 20, True, 'Report Builder version 19.03', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 20, False, 'Fixed issue that allowed Alias Codes orphans for New Products that were subsequently canceled', '', pcProducts, slNull);
  AddChangeList2(ctBugFix, 5, 0, 21, False, 'Fixed issue with Query Option screen for certain Reports showing with narrow Width', 'Test PO Email option', pcReports, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 21, False, 'Restoring saved Column Layouts now keep newly added columns invisible and need to be selected from Column Chooser', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 22, True, 'Add support for NewColumnsVisible tag in saved Column Layouts to force showing auto created Cross tab etc. columns', '', pcReports, slNull);
  AddChangeList2(ctBugFix, 5, 0, 22, False, 'Fixed issue that caused an AV when loading certain custom Query Option screens since version 1.5.0.20', 'due to Query Option width fix in that version', pcReports, slNull);
  AddChangeList2(ctChange, 5, 0, 22, False, 'Support above 100% for Consignee % - requires setting in Program Prefs', '', pcProducts, slSimple);
  AddChangeList2(ctChange, 5, 0, 23, True, 'Helper classes divided for GUI NonGUI', '', pcGeneral, slNull);
  AddChangeList2(ctBugFix, 5, 0, 23, False, 'Fixed issue that caused AV when doing an action on a New (not yet saved) Product when selecting "No" on Save prompt', '', pcProducts, slNull);
  AddChangeList2(ctEnhancment, 5, 0, 23, False, 'Support on Register Transaction List View to load transactions by Log #', '', pcTransaction, slNull);
  AddChangeList2(ctChange, 5, 0, 23, True, 'Updated to DevEx v2019.2.3', '', pcGeneral, slNull);

  //Update IsUpdatedDatabase when needed
  //AddChangeList2(ct, 5, 0, 24, False, '', '', pcGeneral, slNull);
  //Remember to add release date on top of file

  AQuery.ExecSQL;
end;

end.
