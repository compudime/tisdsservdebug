unit GeneralHelpersUnit;

interface

uses
  SysUtils, Classes, DB, AdsData, AdsTable;

type
  TRegisterQueryEvent = procedure(AName: string; var AllowRegister: Boolean) of object;
  TSoundOption = (soDone, soRecord, soError, soMemorize, soReminder);

  TGenHelpers = class
  private
    FRegisteredModulesList: TStringList;
    FNotRegisteredModulesList: TStringList;
    FPrefListQuery: TAdsQuery;
    FEnvTisQuery: TAdsQuery;
    FEnvTisQuery2: TAdsQuery;
    FEnvLocalQuery: TAdsQuery;
    // FEnvTisQuery: TAdsTable;
    // FEnvTisQuery2: TAdsTable;
    // FEnvLocalQuery: TAdsTable;
    FNoLocalSettings: Boolean;
    FGuidLastTimePart: Integer;
    FSiteNameLoaded: Boolean;
    FSiteName: string;
    FPublicIP: string;
    FOnIsModuleRegistered: TRegisterQueryEvent;
    function LocationCode: string;
    function GetSiteName: string;
    function GetStationUserName: string;
    function GetPublicIP: string;
    function GetSiteAddress: string;
    procedure LoadRegisteredModulesList;
    procedure SetSiteName(const Value: string);
  public
    property SiteName: string read GetSiteName write SetSiteName;
    property SiteAddress: string read GetSiteAddress;
    property StationUserName: string read GetStationUserName;
    property PublicIP: string read GetPublicIP;
    property OnIsModuleRegistered: TRegisterQueryEvent read FOnIsModuleRegistered write FOnIsModuleRegistered;

    destructor Destroy; override;
    function EnvSetting(const AValue: string): TField; overload;
    function EnvSetting(const AValue: string; const ANewValue: Variant): TField; overload;
    function ProgPref(const AValue: string): Variant; overload;
    procedure ProgPref(const AValue: string; var AStream: TMemoryStream); overload;
    function ProgPrefStationSet(const AValue: string): Boolean;
    procedure ProgPrefStore(const AValue, AStationID: string; AStream: TMemoryStream); overload;
    procedure ProgPrefStore(const AValue, AStationID: string; ANewValue: Variant); overload;
    procedure ProgPrefReload;
    function IsModuleRegistered(const AModuleName: string): Boolean;
    function IsModuleInList(const AModuleName: string): Boolean;
    procedure RecordAction(const AActn, ADescrip, ATable, AField: string; const AFromN, AToN: Double;
      const AFromC, AToC, ATcktID: string; const AMemoData: string = '');
    function ReadCmdLineArg(const ArgName: string): string;
    function NewIdString(const AType: string = 'F'): string;
    function NewCdGuidString(const ALength: Integer = 14): string;
    function GuidIdString: string;
    procedure DownloadTelInfo(const ATel, AFileName: string);
    function ReportIndyEmailSet: Boolean;
    procedure DoSound(Sound: TSoundOption);
    function GetPredefinedDate(const AValue: string; const AEndDate: Boolean = False): TDateTime;
    function StationCode: string;
    function LocateReportByName(const ARepName: string): Boolean;
    function UserAnswerByPromptID(const APromptID: string): string;
    function UserAnswerInternalByPromptID(const APromptID: string): string;
    function ReportsCount(const ACategory, AActLevel, AFilter: string): Integer;
  end;

{$IFNDEF SANDBOX}

function GenHelpers: TGenHelpers;
{$ENDIF}
function EnvSetting(const AValue: string): TField;
function ProgPref(const AValue: string): Variant;

const
  GlobalSetName = '<Global>';

implementation

uses DataHelpersUnit, Forms, Math, Types, Windows, dmDataModule, Variants, JvJclUtils, StrUtils,
  Utils, System.UITypes, MMSystem, GuiNonGuiHelpersUnit, ProjHelpersIUnit{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

{$IFNDEF SANDBOX}

var
  __GenHelpers: TGenHelpers;

function GenHelpers: TGenHelpers;
begin
  if not Assigned(__GenHelpers) then
    __GenHelpers := TGenHelpers.Create;
  Result := __GenHelpers;
end;
{$ENDIF}

function EnvSetting(const AValue: string): TField;
begin
  Result := GenHelpers.EnvSetting(AValue);
end;

function ProgPref(const AValue: string): Variant;
begin
  Result := GenHelpers.ProgPref(AValue);
end;

procedure TGenHelpers.LoadRegisteredModulesList;
begin
  FRegisteredModulesList.Clear;
  FNotRegisteredModulesList.Clear;
  with DataHelpers.HelperQuery do
  begin
    SQL.Text :=
      'Select List, IIF(Action=True And (Until Is Null or (Until > CurDate())), True, False) Action From ModuleList';
    Open;
    while not Eof do
    begin
      if Fields[1].AsBoolean then
        FRegisteredModulesList.Add(Fields[0].AsString)
      else
        FNotRegisteredModulesList.Add(Fields[0].AsString);
      Next;
    end;
    Close;
    FRegisteredModulesList.Sort;
    FRegisteredModulesList.CaseSensitive := False;
    FNotRegisteredModulesList.Sort;
    FNotRegisteredModulesList.CaseSensitive := False;
  end;
end;

function TGenHelpers.IsModuleInList(const AModuleName: string): Boolean;
var
  AIndex: Integer;
begin
  if not Assigned(FRegisteredModulesList) then
  begin
    FRegisteredModulesList := TStringList.Create;
    FNotRegisteredModulesList := TStringList.Create;
    LoadRegisteredModulesList;
  end;
  Result := FRegisteredModulesList.Find(AModuleName, AIndex);
end;

function TGenHelpers.IsModuleRegistered(const AModuleName: string): Boolean;
var
  AIndex: Integer;
begin
  Result := False;
  if Assigned(OnIsModuleRegistered) then
    OnIsModuleRegistered(AModuleName, Result);
  Result := Result or IsModuleInList(AModuleName);

  if (not Result) and (not FNotRegisteredModulesList.Find(AModuleName, AIndex)) then
    with DataHelpers.HelperQuery do
    begin
      SQL.Text := Format('Insert Into ModuleList (List, Action) Values(%s, False)', [QuotedStr(AModuleName)]);
      ExecSQL;
      LoadRegisteredModulesList;
    end;
end;

destructor TGenHelpers.Destroy;
begin
  if Assigned(FRegisteredModulesList) then
    FreeAndNil(FRegisteredModulesList);
  if Assigned(FNotRegisteredModulesList) then
    FreeAndNil(FNotRegisteredModulesList);
  if Assigned(FPrefListQuery) then
    FreeAndNil(FPrefListQuery);

  inherited Destroy;
end;

function TGenHelpers.EnvSetting(const AValue: string; const ANewValue: Variant): TField;
  procedure NewEnvTisValue(AFieldName, AFieldValue: Variant);
  begin
    with dmData.CreateTableObject(Application) do
      try
        TableName := dmData.EnvirSetttingsTable;
        Open;
        Edit;
        FieldByName(AFieldName).Value := AFieldValue;
        Post;
      finally
        Close;
        Free;
      end;
  end;

var
  ALocalSetttingsTable, AEnvirSetttingsTable, AEnvirSetttingsTable2: string;
begin
  if not Assigned(FEnvTisQuery) then
  begin
    AEnvirSetttingsTable := dmData.EnvirSetttingsTable;
    FEnvTisQuery := dmData.CreateQueryObject(Application);
    FEnvTisQuery.SQL.Text := Format('Select * Into #Temp_%s From %s;', [AEnvirSetttingsTable, AEnvirSetttingsTable]);
    FEnvTisQuery.ExecSQL;
    FEnvTisQuery.Close;
    FEnvTisQuery.AdsCloseSQLStatement;
    FEnvTisQuery.AdsConnection.CloseCachedTables;
    FEnvTisQuery.SQL.Text := Format('Select * From #Temp_%s;', [AEnvirSetttingsTable]);
    FEnvTisQuery.Open;
  end;

  AEnvirSetttingsTable2 := dmData.EnvirSetttingsTable2;
  if (not Assigned(FEnvTisQuery2)) and (AEnvirSetttingsTable2 <> '') then
  begin
    FEnvTisQuery2 := dmData.CreateQueryObject(Application);
    FEnvTisQuery2.SQL.Text := Format('Select * Into #Temp_%s From %s;', [AEnvirSetttingsTable2, AEnvirSetttingsTable2]);
    FEnvTisQuery2.ExecSQL;
    FEnvTisQuery2.Close;
    FEnvTisQuery2.AdsCloseSQLStatement;
    FEnvTisQuery2.AdsConnection.CloseCachedTables;
    FEnvTisQuery2.SQL.Text := Format('Select * From #Temp_%s;', [AEnvirSetttingsTable2, AEnvirSetttingsTable2,
      AEnvirSetttingsTable2]);
    FEnvTisQuery2.Open;
  end;

  ALocalSetttingsTable := dmData.LocalSetttingsTable;
  if not Assigned(FEnvLocalQuery) and (ALocalSetttingsTable <> '') then
  begin
    try
      FEnvLocalQuery := dmData.CreateQueryObject(Application);
      FEnvLocalQuery.AdsConnection := dmData.LocalConnection;
      FEnvLocalQuery.TableType := ttAdsADT;
      FEnvLocalQuery.SQL.Text := Format('Select * Into #Temp_%s From %s;',
        [ALocalSetttingsTable, ALocalSetttingsTable]);
      FEnvLocalQuery.ExecSQL;
      FEnvLocalQuery.Close;
      FEnvLocalQuery.AdsCloseSQLStatement;
      FEnvLocalQuery.AdsConnection.CloseCachedTables;
      FEnvLocalQuery.SQL.Text := Format('Select * From #Temp_%s;', [ALocalSetttingsTable]);
      FEnvLocalQuery.Open;
      if FEnvLocalQuery.FindField('ScnForOnl') = nil then
      begin
        FEnvLocalQuery.SQL.Text :=
          Format('Alter Table #Temp_%s Add Column ScnForOnl Logical; Update #Temp_%s Set ScnForOnl=False;',
          [ALocalSetttingsTable, ALocalSetttingsTable]);
        FEnvLocalQuery.ExecSQL;
        FEnvLocalQuery.SQL.Text := Format('Select * From #Temp_%s;', [ALocalSetttingsTable]);
        FEnvLocalQuery.Open;
      end;
    except
      FNoLocalSettings := True; // Check for 5185
      // raise;
    end;
  end
  else if not Assigned(FEnvLocalQuery) then
    FNoLocalSettings := True;

  if not VarIsNull(ANewValue) then
    NewEnvTisValue(AValue, ANewValue);

  Result := nil;

  if not FNoLocalSettings then
    Result := FEnvLocalQuery.FindField(AValue);

  if Result = nil then
    Result := FEnvTisQuery.FindField(AValue);

  if (Result = nil) and Assigned(FEnvTisQuery2) then
    Result := FEnvTisQuery2.FindField(AValue);

  if Result = nil then
    raise EHandleInApp.CreateFmt('Missing EnvSetting field %s.', [AValue]);
end;

function TGenHelpers.EnvSetting(const AValue: string): TField;
begin
  Result := EnvSetting(AValue, Null);
end;

procedure TGenHelpers.RecordAction(const AActn, ADescrip, ATable, AField: string; const AFromN, AToN: Double;
  const AFromC, AToC, ATcktID: string; const AMemoData: string);
var
  AGuid: string;
begin
  AGuid := GuidIdString;
  AGuid := Copy(AGuid, 3, 10);
  try // suppress data trunc messages
    DataHelpers.RunSimpleQuery
      (Format('EXECUTE PROCEDURE cdRecordAction (%s, %s, %s, %s, %s, %s, %f, %f, %s, %s, %s, %s);',
      [QuotedStr(AGuid), QuotedStr(GuiNonGuiHelpers.UserName), QuotedStr(AActn), QuotedStr(ADescrip), QuotedStr(ATable),
      QuotedStr(AField), AFromN, AToN, QuotedStr(AFromC), QuotedStr(AToC), QuotedStr(ATcktID), QuotedStr(AMemoData)]));
  except
  end;
end;

function TGenHelpers.ReportIndyEmailSet: Boolean;
begin

end;

function TGenHelpers.ReportsCount(const ACategory, AActLevel, AFilter: string): Integer;
begin

end;

function TGenHelpers.ReadCmdLineArg(const ArgName: string): string;
begin
  Result := GetCmdLineArg(ArgName, []);
end;

function TGenHelpers.NewCdGuidString(const ALength: Integer): string;
begin
  with DataHelpers.HelperQuery do
  begin
    SQL.Text := Format('Select cdGuid(%d) From System.Iota;', [ALength]);
    Open;
    Result := Fields[0].AsString;
  end;
end;

function TGenHelpers.NewIdString(const AType: string): string;
begin
  with DataHelpers.HelperQuery do
  begin
    SQL.Text := 'Select NewIdString(' + AType + ') From System.Iota;';
    Open;
    Result := Fields[0].AsString;
  end;
end;

function TGenHelpers.GuidIdString: string;
var
  Year, Month, Day: Word;
  CurTimePart: Integer;
begin
  DecodeDate(Now, Year, Month, Day);

  Result := '';
  Result := Result + LocationCode; // 1 LocationCode
  Result := Result + StationCode; // 1 StationCode
  Result := Result + IntToHex(Year - 2000, 1); // 1 Year - in year 2016 it will addtnl digit
  Result := Result + Chr(Month + 64); // 1 Month
  if Day < 10 then
    Result := Result + IntToStr(Day) // 1 Day in Month
  else
    Result := Result + Chr(Day + 55);

  CurTimePart := Round(Frac(Now) * 24 * 60 * 60);
  FGuidLastTimePart := Max(CurTimePart, FGuidLastTimePart + 1);
  Result := Result + IntToHex(FGuidLastTimePart, 5); // 5 Time

  Result := Result + IntToHex(RandomRange(1, 32), 2); // 2 digit Random
end;

function TGenHelpers.LocateReportByName(const ARepName: string): Boolean;
begin

end;

function TGenHelpers.LocationCode: string;
var
  AField: TField;
begin
  Result := 'A';
  AField := EnvSetting('LocCode');
  if (AField <> nil) and (AField.AsString <> '') then
    Result := AField.AsString;
end;

function TGenHelpers.StationCode: string;
begin
  Result := GetEnvVar('USER_ID');
  if Result = '' then
    Result := '0';
end;

function TGenHelpers.UserAnswerByPromptID(const APromptID: string): string;
begin
  Result := '';
end;

function TGenHelpers.UserAnswerInternalByPromptID(const APromptID: string): string;
begin

end;

function TGenHelpers.ProgPref(const AValue: string): Variant;
var
  AValueType: string;
  bValue: Boolean;
  dValue: TDateTime;
  nValue: Double;
  AStream: TStringStream;
begin
  if not Assigned(FPrefListQuery) then
    ProgPrefReload;

  if FPrefListQuery.Locate('SettingName', AValue, []) then
  begin
    AValueType := FPrefListQuery.FieldByName('ValueType').AsString;
    Result := FPrefListQuery.FieldByName('Value').AsString;
    if (Result = '') and (FPrefListQuery.FieldByName('MValue').AsString <> '') then
    begin
      // Result := FPrefListQuery.FieldByName('MValue').AsString;
      AStream := TStringStream.Create;
      TBlobField(FPrefListQuery.FieldByName('MValue')).SaveToStream(AStream);
      Result := AStream.DataString;
      AStream.Free;
    end;

    if SameText(AValueType, 'Boolean') then
    begin
      if TryStrToBool(Result, bValue) then
        Result := bValue
      else
        Result := False;
    end
    else if SameText(AValueType, 'Date') then
    begin
      if TryStrToDate(Result, dValue) then
        Result := dValue
      else
        Result := NullDate;
    end
    else if SameText(AValueType, 'Numeric') then
    begin
      if TryStrToFloat(Result, nValue) then
        Result := nValue
      else
        Result := 0;
    end;
  end;
end;

procedure TGenHelpers.ProgPrefReload;
begin
  if not Assigned(FPrefListQuery) then
  begin
    FPrefListQuery := dmData.CreateQueryObject(nil);
    { ProjectHelpers. } FillProgPrefList;
  end;

  FPrefListQuery.SQL.Clear;
  // FPrefListQuery.SQL.Add('Try Drop Table #Loaded_ProgPrefs; Catch All End;');
  FPrefListQuery.SQL.Add('Try');
  FPrefListQuery.SQL.Add('Select p1.SettingName, p1.ValueType, p2.Value, p2.MValue,');
  FPrefListQuery.SQL.Add('False StationSet, False StationOnly');
  FPrefListQuery.SQL.Add('Into #Loaded_ProgPrefs From #ProgControl p1');
  FPrefListQuery.SQL.Add('Left Outer Join ProgControl p2 ON p2.SettingName=p1.SettingName');
  FPrefListQuery.SQL.Add('Where p1.SettingName=''__NO_SUCH_SETTING__FOUND__'';');
  FPrefListQuery.SQL.Add('Catch All Delete From #Loaded_ProgPrefs; End;');

  FPrefListQuery.SQL.Add
    ('Insert Into #Loaded_ProgPrefs (SettingName, ValueType, Value, MValue, StationSet, StationOnly)');
  FPrefListQuery.SQL.Add('Select p1.SettingName, p1.ValueType, p2.Value, p2.MValue,');
  FPrefListQuery.SQL.Add('(Not p2.Value Is Null or Not p2.MValue Is Null) StationSet, p1.StationOnly');
  FPrefListQuery.SQL.Add('From #ProgControl p1');
  FPrefListQuery.SQL.Add('Left Outer Join ProgControl p2 ON p2.SettingName=p1.SettingName');
  FPrefListQuery.SQL.Add('And p2.StationID=:Station And Not p1.GlobalOnly;');

  FPrefListQuery.SQL.Add('Update p Set Value=p2.Value, MValue=p2.MValue');
  FPrefListQuery.SQL.Add('From #Loaded_ProgPrefs p, ProgControl p2');
  FPrefListQuery.SQL.Add('Where p2.SettingName=p.SettingName And p2.StationID=:Global');
  FPrefListQuery.SQL.Add('And Not p.StationOnly');
  FPrefListQuery.SQL.Add('And Not p2.SettingName IN (Select SettingName From ProgControl');
  FPrefListQuery.SQL.Add('Where StationID=:Station);');

  FPrefListQuery.SQL.Add('Insert Into #Loaded_ProgPrefs (SettingName, ValueType, Value, MValue, StationSet)');
  FPrefListQuery.SQL.Add('Select SettingName, ''UnKnown'', Value, MValue, True From ProgControl');
  FPrefListQuery.SQL.Add('Where StationID=:Station And Not SettingName IN');
  FPrefListQuery.SQL.Add('(Select SettingName From #Loaded_ProgPrefs);');

  FPrefListQuery.SQL.Add('Insert Into #Loaded_ProgPrefs (SettingName, ValueType, Value, MValue, StationSet)');
  FPrefListQuery.SQL.Add('Select SettingName, ''UnKnown'', Value, MValue, False From ProgControl');
  FPrefListQuery.SQL.Add('Where StationID=:Global And Not SettingName IN');
  FPrefListQuery.SQL.Add('(Select SettingName From #Loaded_ProgPrefs);');

  FPrefListQuery.SQL.Add('Select * From #Loaded_ProgPrefs;');
  FPrefListQuery.Prepare;

  FPrefListQuery.ParamByName('Station').AsString := GetOSComputerName;
  FPrefListQuery.ParamByName('Global').AsString := GlobalSetName;
  FPrefListQuery.Open;
end;

function TGenHelpers.GetSiteAddress: string;
begin

end;

function TGenHelpers.GetSiteName: string;
var
  AQuery: TAdsQuery;
begin
  if not FSiteNameLoaded then
  begin
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select Value From ProgControl Where StationID=''<Internal>'' And SettingName=''SiteValue1'';';
    AQuery.Open;
    FSiteName := AQuery.Fields[0].AsString;
    FSiteNameLoaded := True;
  end;

  Result := FSiteName
end;

function TGenHelpers.GetStationUserName: string;
begin
  Result := GenHelpers.SiteName;
  if Result <> '' then
    Result := Result + '-' + GetOSComputerName;
end;

function TGenHelpers.GetPublicIP: string;
var
  AFileName: string;
begin
  if FPublicIP <> '' then
    Result := FPublicIP
  else
  begin
    AFileName := TempFileName('txt');
    Result := '<UnKnown>';
    try
      if GetInetFile('http://www.weethet.nl/getip.php', AFileName) then
        Result := ReadStringFromFile(AFileName);
    except
    end;
    FPublicIP := Result;
  end;
end;

procedure TGenHelpers.DownloadTelInfo(const ATel, AFileName: string);
begin
  GetInetFile('http://www.411.com/search/ReversePhone?full_phone=' + ATel, AFileName)
end;

procedure TGenHelpers.ProgPref(const AValue: string; var AStream: TMemoryStream);
begin
  if not Assigned(FPrefListQuery) then
    ProgPrefReload;

  if FPrefListQuery.Locate('SettingName', AValue, []) then
  begin
    if not Assigned(AStream) then
      AStream := TMemoryStream.Create;
    TBlobField(FPrefListQuery.FieldByName('MValue')).SaveToStream(AStream);
    AStream.Position := 0;
  end;

  { AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select * From ProgControl Where StationID=:StationID And SettingName=:SettingName;';
    AQuery.Prepare;
    AQuery.ParamByName('StationID').AsString := GlobalSetName;
    AQuery.ParamByName('SettingName').AsString := AValue;
    AQuery.Open;

    if not AQuery.IsEmpty then
    begin
    if not Assigned(AStream) then
    AStream := TMemoryStream.Create;
    TMemoField(AQuery.FieldByName('MValue')).SaveToStream(AStream);
    AStream.Position := 0;
    end; }
end;

function TGenHelpers.ProgPrefStationSet(const AValue: string): Boolean;
begin
  if not Assigned(FPrefListQuery) then
    ProgPrefReload;

  if FPrefListQuery.Locate('SettingName', AValue, []) then
    Result := FPrefListQuery.FieldByName('StationSet').AsBoolean
  else
    Result := False;
end;

procedure TGenHelpers.ProgPrefStore(const AValue, AStationID: string; AStream: TMemoryStream);
var
  AQuery: TAdsQuery;
  // BlobStream: TStream;
begin
  if not Assigned(AStream) then
    Exit;

  ProgPrefStore(AValue, AStationID, ProgPref(AValue));

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * From ProgControl Where StationID=:StationID And SettingName=:SettingName;';
  AQuery.Prepare;
  AQuery.ParamByName('StationID').AsString := AStationID;
  AQuery.ParamByName('SettingName').AsString := AValue;
  AQuery.RequestLive := True;
  AQuery.Open;
  if not AQuery.IsEmpty then
  begin
    { AQuery.Edit;
      AQuery.FieldByName('Value').AsString := '';
      BlobStream := AQuery.CreateBlobStream(AQuery.FieldByName('MValue'), bmWrite);
      BlobStream.CopyFrom(AStream, 0);
      BlobStream.Free;
      AQuery.Post; }
    AQuery.Edit;
    AQuery.FieldByName('Value').AsString := '';
    // AQuery.FieldByName('MValue').AsString := StreamToString(AStream);
    TBlobField(AQuery.FieldByName('MValue')).LoadFromStream(AStream);
    AQuery.Post;
  end;

  ProgPrefReload;
end;

procedure TGenHelpers.ProgPrefStore(const AValue, AStationID: string; ANewValue: Variant);
var
  AQuery: TAdsQuery;
  bInsert: Boolean;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select SettingName From ProgControl Where StationID=:StationID And SettingName=:SettingName;';
  AQuery.Prepare;
  AQuery.ParamByName('StationID').AsString := AStationID;
  AQuery.ParamByName('SettingName').AsString := AValue;
  AQuery.Open;
  bInsert := AQuery.IsEmpty;
  AQuery.Close;

  if bInsert then
    AQuery.SQL.Text :=
      'Insert Into ProgControl (StationID, SettingName, Value) Values(:StationID, :SettingName, :NewValue);'
  else
    AQuery.SQL.Text :=
      'Update ProgControl Set Value=:NewValue Where StationID=:StationID And SettingName=:SettingName;';

  AQuery.Prepare;
  AQuery.ParamByName('StationID').AsString := AStationID;
  AQuery.ParamByName('SettingName').AsString := AValue;
  AQuery.ParamByName('NewValue').AsString := ANewValue;
  try
    AQuery.ExecSQL;
  except
    on E: Exception do
    begin
      if Pos('Data truncated', E.Message) = 0 then
        raise;
    end;
  end;

  ProgPrefReload;
end;

procedure TGenHelpers.DoSound(Sound: TSoundOption);
const
{$IFNDEF TISLIBDLL}
  cDONE = 'DONE';
  cERROR = 'ERROR';
  cRECORD = 'RECORD';
  cMEMORIZE = 'MEMORIZE';
  cREMINDER = 'REMINDER';
{$ELSE}
  cDONE = 'TON2';
  cERROR = 'UNLISTED';
  cRECORD = 'TON1';
  cMEMORIZE = 'TON3';
  cREMINDER = 'BUTTON';
{$ENDIF}
var
  cSound: PChar;
begin
  cSound := '';
  case Sound of
    soDone:
      cSound := cDONE;
    soError:
      cSound := cERROR;
    soRecord:
      cSound := cRECORD;
    soMemorize:
      cSound := cMEMORIZE;
    soReminder:
      cSound := cREMINDER;
  end;
  if cSound <> '' then
    PlaySound(cSound, 0, SND_RESOURCE)
end;

function TGenHelpers.GetPredefinedDate(const AValue: string; const AEndDate: Boolean): TDateTime;
var
  APreValue: string;
begin
  if AEndDate then
    APreValue := 'Between'
  else
    APreValue := '';

  Result := Date; //ActiveReport.GetDefValueD(AValue, APreValue);
end;

procedure TGenHelpers.SetSiteName(const Value: string);
var
  AQuery: TAdsQuery;
begin
  if SiteName <> '' then
    raise EHandleInApp.CreateFmt('Site Name already set: %s, overwrite not allowed', [SiteName]);

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Merge ProgControl ON (StationID=''<Internal>'' And SettingName=''SiteValue1'')');
  AQuery.SQL.Add(Format('When Matched Then Update Set Value=%s', [QuotedStr(Value)]));
  AQuery.SQL.Add('When Not Matched Then Insert (StationID, SettingName, Value)');
  AQuery.SQL.Add(Format('Values(''<Internal>'', ''SiteValue1'', %s)', [QuotedStr(Value)]));
  AQuery.ExecSQL;
  FSiteNameLoaded := False;
end;

{$IFNDEF SANDBOX}

initialization

finalization

if Assigned(__GenHelpers) then
  FreeAndNil(__GenHelpers);
{$ENDIF}

end.
