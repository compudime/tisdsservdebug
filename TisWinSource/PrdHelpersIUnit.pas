unit PrdHelpersIUnit;

interface

uses
  SysUtils, Classes, DB, AdsData, AdsTable, Xml.XMLIntf, System.JSON;

type
  EHandleInApp = class(Exception)
  end;

  TPriceCalcMode = (pcmMarkup, pcmProfitMargin);

  TPrdHelpersI = class
  private
    FLookQry: TAdsQuery;
    FPrdInfoQry: TAdsQuery;
    FCatInfoQry: TAdsQuery;
    FtbCartonCode: TAdsQuery;
    FPrdCstmPrcsQry: TAdsQuery;
    FtbCartonCodeAll: TAdsTable;
    FtbCartonCodeAllPrdLookup: TAdsTable;
    FtbMixHD: TAdsTable;
    FtbPrdAlias: TAdsTable;
    FtbPrdAliasAll: TAdsQuery;
    FStrCategory: TStringList;
    FStrBrand: TStringList;
    FStrDepartment: TStringList;
    FStrItemClass: TStringList;
    FStrSeason: TStringList;
    FStrProduceOrigin: TStringList;
    FStrHechsher: TStringList;
    FStrMeasure: TStringList;
    FStrBin: TStringList;
    FStrSubCategory: TStringList;
    FStrCCustom_List1: TStringList;
    FStrCCustom_List2: TStringList;
    FStrCCustom_List3: TStringList;
    FStrCCustom_List4: TStringList;
    FStrCCustom_List5: TStringList;
    FCsPrcAllowedOnPrcEncoded: Boolean;
    FQryPriceChangeCstmPrice: TAdsQuery;
    FInventoryLocationList: string;
    FInventoryLocationListNoRemote: string;

    procedure PrdQuery(ACode: string);
    function GetInventoryUpdateTableName: string;
    function GetInventoryLocUpdateTableName: string;
    procedure PrdInfoQuery(ACode: string);
    procedure CatInfoQuery(ACode: string);
    function GettbCartonCode: TAdsQuery;
    function GettbCartonCodeAll: TAdsTable;
    function GettbMixHD: TAdsTable;
    function FindBarcodeEx(ADataSet: TDataSet; const AFieldName: string; var ACode, APreDigit: string): Boolean;
    function GettbPrdAlias: TAdsTable;
    function GettbPrdAliasAll: TAdsQuery;
    procedure DoCallMergePrdDataProcViaNotify(var AOldCode: string; const ANewCode: string;
      const AAddAlias, ANoDelete, AUpdatePrdmstrCode: Boolean);
  protected
    function CalcMarkup(const ACost, APrice: Currency; AUpc: Integer = 1; APerPackage: Integer = 1): Currency;
    function CalcProfitMargin(const ACost, APrice: Currency; AUpc: Integer = 1; APerPackage: Integer = 1): Currency;
    function CalcPriceFromProfitMargin(const ACost, AMargin: Currency; AUpc, APerPackage: Integer): Currency;
    function CalcPriceFromMarkup(const ACost, AMarkup: Currency; AUpc: Integer = 1; APerPackage: Integer = 1): Currency;
  public
    property InventoryUpdateTableName: string read GetInventoryUpdateTableName;
    property InventoryLocUpdateTableName: string read GetInventoryLocUpdateTableName;
    property tbCartonCode: TAdsQuery read GettbCartonCode;
    property tbCartonCodeAll: TAdsTable read GettbCartonCodeAll;
    property tbMixHD: TAdsTable read GettbMixHD;
    property tbPrdAlias: TAdsTable read GettbPrdAlias;
    property tbPrdAliasAll: TAdsQuery read GettbPrdAliasAll;

    procedure ScanResolveListForCode(const ACode: string; AList: TStringList);
    procedure ProductPrdServerValuesToStringList(AStringList: TStringList; xmlData: IXMLNode; const AIndex: Integer);

    function MixMatchGroupFromPrdCode(const AItemCode: string): string;
    function PrdCodeIsUnique(ADataSet: TDataSet; var ADupDescription: string): Boolean;
    function AliasCodeIsUnique(ADataSet: TDataSet; var ADupDescription: string): Boolean;
    function PrdLookupRecord(const ACode: string): TDataSet;
    function PrdLookupValue(const ACode, AFieldName: string): string;
    function FindBarcode(ADataSet: TDataSet; var ACode: string): Boolean; overload;
    function FindBarcode(ADataSet: TDataSet; var ACode, APrice, AWeight: string): Boolean; overload;
    function PrdFieldComment(const AField: string): string;
    function DoRemoveKitItems(const AItemCode, AAddOnCode: string): Integer;
    procedure DoUpdateInventory;
    procedure CopyPrdRecord(ADataSet: TDataSet; ACopyCode: string);
    function CalcDiscountFromSaleMode(const AOrigPrice, ADiscount: Currency; const ASaleMode: string;
      ADollarDiscount: Currency = 0): Currency;
    function GetInventoryValue(const AUpcCode: string): Double;
    function CalcMarkupByCalcMode(ACalcMode: TPriceCalcMode; const ACost, APrice: Currency; AUpc: Integer;
      APerPackage: Integer = 1): Currency;
    function CalcPriceByCalcMode(ACalcMode: TPriceCalcMode; const ACost, AValue: Currency; AUpc: Integer;
      APerPackage: Integer = 1): Currency;
    procedure DoDeleteProduct(const AItemCode: string);
    procedure DoCascadeProductCodeChange(AOldCode, ANewCode: string; AUpdatePrdmstr: Boolean);
    procedure RecordPriceChange(ADataSet: TDataSet; AView: string); overload;
    procedure RecordPriceChange(AField: TField; AView: string; AUppField: TField = nil); overload;
    procedure AdjustNewCodeOnScan(var ACode: string);
    function PrdInfoRecord(const ACode: string): TDataSet;
    procedure PostInventoryAdjustment(const ACode, AMemo: string; const AQuan: Double; AInvLocation, AUserName: string;
      AUnitCost: Currency);
    procedure CopyPrdInfoRecord(const AOrigCode, ANewCode: string);
    function CatInfoRecord(const ACode: string): TDataSet;
    procedure DoDeleteCartonCode(const ACartonCode, AItemCode: string);
    function FindUpcFromCartonCode(var ACode: string): Boolean;
    procedure DoDeleteCstmPrcItem(const AUpcCode, ACustCode: string);
    function GetMixMatchDescription(const AMixMatchID: string): string;
    function GetMixMatchCount(const AMixMatchID: string): Integer;
    procedure DoCallMergePrdDataProc(var AOldCode: string; const ANewCode: string; const AAddAlias, ANoDelete: Boolean);
    function RestrictCasePrice(const ACode: string): Boolean;
    function LoadPriceChangeCstmPriceData(const ACode, AGroupCaption: string): TAdsQuery;
    function InventoryLocationListS(const IncludeRemote: Boolean): string;
    procedure StrListFromField(AStrList: TStringList; const AFieldName: string; const AFilter: string = '';
      AReload: Boolean = False);
    function PricingOptionsExist(const ACategory, ACode: string): Boolean;
    function HasMasterClass(const AItemClass, AClassCode, AMasterField: string): Boolean;
    function CustomerPriceItemRecord(const ACustCode, AUpcCode: string): TDataSet;
    function NonPostedFutureCostExists(const ACode: string): Boolean;
    procedure GenerateNewPrdCode(ADataSet: TDataSet);
    procedure AddNewPrd(ADataSet: TDataSet; ACode: string; AValueList: TStringList = nil); overload;
    procedure AddNewPrd(JSONRequest, JSONResponse: TJSONObject); overload;
    procedure UpdatePrd(JSONRequest, JSONResponse: TJSONObject); overload;
    function AddNewProduct(ACode: string; Params: TParams): string;
    procedure PrdServerValuesToDataSet(ADataSet: TDataSet; AValueList: TStringList);

    destructor Destroy; override;
    constructor Create;
  end;

{$IFNDEF SANDBOX}

function PrdHelpersI: TPrdHelpersI;
{$ENDIF}
{$IFDEF TISLIBDLL}

var
  __PrdHelpersI: TPrdHelpersI;
{$ENDIF}

implementation

uses Variants, dmDataModule, dmDataModulePrd, DataHelpersUnit, Math, Tis_CreatDBObjects, StrUtils, Utils,
  System.UITypes, System.Generics.Collections, System.SyncObjs, GeneralHelpersUnit, ProjHelpersIUnit,
  ScanResolveUnit{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

{ TPrdHelpers }

const
  UpdInvenTableName = '#UpdInvenTable';
  UpdInvenLocTableName = '#UpdInvenLocTable';
  PrdFieldListNoCopy = ';CODE;VENITMCODE;PRDCTSKU;LIST;LSTPRICE;PRCHENGE;TODAY;CSTLAST;CSTLASTC;CSTMOST;CSTLEAST;' +
    'CSTCHENGE;LSTDEL;DTMINHAND;DTONHAND;CASECODE;DTSTART;LSTVEND;MTDQTY;MTDDOLLAR;PTDQTY;PTDDOLLAR;YTDQTY;' +
    'YTDDOLLAR;ITEMCLASS;LASTSOLD;MIXMATCHID;DELDATE;LINEID;INACTIVE;SOUNDEXLIST;ENTERPRISEID;STOREID;UPDATEDON;';

{$IFNDEF SANDBOX}
{$IFNDEF TISLIBDLL}

var
  __PrdHelpersI: TPrdHelpersI;
{$ENDIF}

function PrdHelpersI: TPrdHelpersI;
begin
  if not Assigned(__PrdHelpersI) then
    __PrdHelpersI := TPrdHelpersI.Create;
  Result := __PrdHelpersI;
end;
{$ENDIF}
{$I PasRight.inc}

procedure TPrdHelpersI.AddNewPrd(ADataSet: TDataSet; ACode: string; AValueList: TStringList);
var
  AQuery: TAdsQuery;
  x: Integer;
begin
  ADataSet.Insert;
  ADataSet.FieldByName('TBegins').AsString := '0000';
  ADataSet.FieldByName('TExpires').AsString := '2359';
  ADataSet.FieldByName('PTBegins').AsString := '0000';
  ADataSet.FieldByName('PTExpires').AsString := '2359';
  ADataSet.FieldByName('CTBegins').AsString := '0000';
  ADataSet.FieldByName('PTExpires').AsString := '2359';
  ADataSet.FieldByName('SaleMode').AsString := '$';
  ADataSet.FieldByName('CsSaleMode').AsString := '$';
  ADataSet.FieldByName('PkSaleMode').AsString := '$';
  ADataSet.FieldByName('UPC').AsInteger := 1;
  ADataSet.FieldByName('Pack').AsInteger := 1;
  ADataSet.FieldByName('DtStart').AsDateTime := Date;
  ADataSet.FieldByName('NonInven').AsBoolean := False;
  ADataSet.FieldByName('Inactive').AsBoolean := False;
  ADataSet.FieldByName('LineID').AsString := GenHelpers.NewIdString('d');

  DataHelpers.ApplyFieldDefaults(ADataSet, 'Prdmstr');

  ADataSet.FieldByName('Req4Disc').AsCurrency := EnvSetting('Tot4Dis').AsCurrency;
  ADataSet.FieldByName('Tax').AsBoolean := EnvSetting('Plus4Tax').AsBoolean;

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * From Prdmstr Where Code=''MASTER'';';
  AQuery.Open;
  if not AQuery.IsEmpty then
    for x := 0 to AQuery.FieldCount - 1 do
      if (Pos(';' + UpperCase(AQuery.Fields[x].FieldName) + ';',
        PrdFieldListNoCopy + UpperCase(EnvSetting('PrdCpyExl').AsString + ';')) = 0) and
        (ADataSet.FindField(AQuery.Fields[x].FieldName) <> nil) and not AQuery.Fields[x].IsNull then
        ADataSet.FieldByName(AQuery.Fields[x].FieldName).Value := AQuery.Fields[x].Value;

  PrdServerValuesToDataSet(ADataSet, AValueList);

  // Assign code if specified
  if ACode <> '' then
    ADataSet.FieldByName('Code').AsString := ACode;
end;

procedure TPrdHelpersI.AddNewPrd(JSONRequest, JSONResponse: TJSONObject);
var
  ACode, ADupMessage: string;
  AErrorIfFieldNotFound: Boolean;
  ADataSet: TDataSet;
  AEnumP: TJSONPairEnumerator;
  AField: TField;
  APair: TJSONPair;
begin
  ADataSet := dmDataPrd.tbPrdmstr;
  JSONRequest.TryGetValue<string>('Code', ACode);
  if not JSONRequest.TryGetValue<Boolean>('ErrorIfFieldNotFound', AErrorIfFieldNotFound) then
    AErrorIfFieldNotFound := False
  else
    JSONRequest.RemovePair('ErrorIfFieldNotFound');
  if ACode.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'AddNewPrd required parameter missing (Code)');
    Exit;
  end;

  if not ADataSet.Active then
    ADataSet.Open;

  AddNewPrd(ADataSet, ACode);
  AEnumP := JSONRequest.GetEnumerator;
  while AEnumP.MoveNext do
  begin
    APair := AEnumP.Current;
    AField := ADataSet.FindField(APair.JsonString.Value);

    if AErrorIfFieldNotFound and (not Assigned(AField)) then
    begin
      ADataSet.Cancel;
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'Field ' + APair.JsonString.Value + ' Not Found');
      Exit;
    end;

    if Assigned(AField) and (APair.JsonValue.Value <> '') then
      AField.AsString := APair.JsonValue.Value;
  end;
  if not PrdCodeIsUnique(ADataSet, ADupMessage) then
  begin
    ADataSet.Cancel;
    JSONResponse.AddPair('Status', 'Error');
    ADupMessage := ReplaceStr(ADupMessage, #10#13, ' ');
    JSONResponse.AddPair('Error', ADupMessage);
    Exit;
  end;
  try
    ADataSet.Post;
  except
    ADataSet.Cancel;
    raise;
  end;
  JSONResponse.AddPair('Status', 'Success');
end;

function TPrdHelpersI.AddNewProduct(ACode: string; Params: TParams): string;
var
  I: Integer;
  AField: TField;
  ADataSet: TDataSet;
begin
  ADataSet := dmDataPrd.tbPrdmstr;
  AddNewPrd(ADataSet, ACode);
  if ACode = '' then
    GenerateNewPrdCode(ADataSet);
  Result := ADataSet.FieldByName('Code').AsString;
  for I := 0 to Params.Count - 1 do
  begin
    AField := ADataSet.FindField(Params[I].Name);
    if Assigned(AField) then
      AField.AsString := Params[I].AsString;
  end;
  ADataSet.Post;
end;

procedure TPrdHelpersI.AdjustNewCodeOnScan(var ACode: string);
var
  ACutPre: Integer;
  ACutCheckDigit: Boolean;
  ASR: TScanResolve;
begin
  ASR := ProjectHelpersI.ScanResolve;
  ACutCheckDigit := ProgPref('CutCheckDigitOnScan');
  ACutPre := ProgPref('CutPreCodeOnScan');

  if Copy(ACode, 1, 5) = 'S08FF' then
    ACode := Copy(ACode, 1, 4) + Copy(ACode, 6, MaxInt);

  if Copy(ACode, 1, 3) = 'S08' then
  begin
    ACode := Copy(ACode, 5, Length(ACode) - 4);
    if ACutPre >= 4 then
      ACutPre := ACutPre - 4;
  end;

  case Length(ACode) of
    13: { EAN13 }
      begin
        if ACutCheckDigit and (Copy(ACode, 13, 1) = ASR.GetChkDgt(Copy(ACode, 1, 12))) then
          ACode := Copy(ACode, 1, 12);
        if ACutPre > 0 then
          ACode := Copy(ACode, ACutPre + 1, Length(ACode) - ACutPre);
      end;
    12: { UPCA }
      begin
        if ACutCheckDigit and (Copy(ACode, 12, 1) = ASR.GetChkDgt(Copy(ACode, 1, 11))) then
          ACode := Copy(ACode, 1, 11);
        if ACutPre > 0 then
          ACode := Copy(ACode, ACutPre + 1, Length(ACode) - ACutPre);
      end;
    11: { UPCA, if set to cut assume that Pre-Digit is scanned and Check-Digit is disabled }
      begin
        if ACutPre > 0 then
          ACode := Copy(ACode, ACutPre + 1, Length(ACode) - ACutPre);
      end;
    8: { EAN8 and UPCE }
      begin
        if ACutCheckDigit and (Copy(ACode, 8, 1) = ASR.GetChkDgt(Copy(ACode, 1, 7))) then
          ACode := Copy(ACode, 1, 7);
        if (ACutPre > 1) or ((ACutPre = 1) and (Copy(ACode, 1, 1) = '0')) then
          // UPCE is always 0, EAN8 is not 0
          ACode := Copy(ACode, ACutPre + 1, Length(ACode) - ACutPre);
      end;
    7: { EAN8 and UPCE, if set to cut assume that Pre-Digit is scanned and Check-Digit is disabled }
      begin
        if ACutPre > 0 then // UPCE is always 0, EAN8 is not 0
          ACode := Copy(ACode, ACutPre + 1, Length(ACode) - ACutPre);
      end;
  end;
end;

function TPrdHelpersI.AliasCodeIsUnique(ADataSet: TDataSet; var ADupDescription: string): Boolean;
var
  AEnteredCode: string;
  AScanOpts: TDataSet;
  AOldValue: variant;
  AThisPass: Boolean;
  procedure ChkPrdField(APrdField: string);
  begin
    Result := not DataHelpers.RecordExits('Prdmstr', APrdField,
      Format('Upper(%s)=%s', [APrdField, QuotedStr(AEnteredCode)]));

    if not Result then
      ADupDescription := Format('AliasCode is duplicated in %s value of a product.', [APrdField]);
  end;

begin
  Result := True;
  AEnteredCode := ADataSet.FieldByName('AliasCode').AsString;

  if AEnteredCode = '' then
  begin
    Result := False;
    ADupDescription := 'AliasCode can''t be empty';
  end;

  AOldValue := ADataSet.FieldByName('AliasCode').OldValue;
  if VarIsLikeNull(AOldValue) then
    AOldValue := '';

  if Result and (AEnteredCode <> AOldValue) then
  begin
    Result := not DataHelpers.RecordExits('PrdAlias', 'AliasCode',
      Format('Upper(AliasCode)=%s', [QuotedStr(AEnteredCode)]));
    if not Result then
      ADupDescription := 'AliasCode is duplicated in an existing entry.';
  end;

  if Result then
    ChkPrdField('Code');

  if Result then
    ChkPrdField('CaseCode');

  if Result then
    ChkPrdField('PrdctSku');

  if not Result then
    Exit;

  AScanOpts := ProjectHelpersI.ScanResolve.Execute(AEnteredCode);

  // Prdmstr
  AScanOpts.First;
  while not AScanOpts.Eof do
  begin
    Result := not DataHelpers.RecordExits('Prdmstr', 'Code',
      Format('Upper(Code)=%s', [QuotedStr(AScanOpts.FieldByName('Scan').AsString)]));
    if not Result then
    begin
      ADupDescription := Format('Code is duplicated with Code: %s,'#10#13'(ID #%d).',
        [AScanOpts.FieldByName('Scan').AsString, AScanOpts.FieldByName('Indx').AsInteger]);
      Break;
    end;
    AScanOpts.Next;
  end;

  if not Result then
    Exit;

  // PrdAlias
  AScanOpts.First;
  while not AScanOpts.Eof do
  begin
    AThisPass := not DataHelpers.RecordExits('PrdAlias', 'AliasCode',
      Format('Upper(AliasCode)=%s', [QuotedStr(AScanOpts.FieldByName('Scan').AsString)]));
    if (not AThisPass) and (AOldValue <> AScanOpts.FieldByName('Scan').AsString) then
    begin
      Result := False;
      // Can't set Result above cause when caseCode is being edited Result=False for nothing
      ADupDescription := Format('Code is duplicated with AliasCode: %s,'#10#13'(ID #%d).',
        [AScanOpts.FieldByName('Scan').AsString, AScanOpts.FieldByName('Indx').AsInteger]);
      Break;
    end;
    AScanOpts.Next;
  end;
end;

function TPrdHelpersI.CalcDiscountFromSaleMode(const AOrigPrice, ADiscount: Currency; const ASaleMode: string;
  ADollarDiscount: Currency): Currency;
begin
  if ADollarDiscount = 0 then
    ADollarDiscount := ADiscount;
  { if (AOrigPrice > 0) and (ADiscount > 0) then
    begin
    if ASaleMode = '%' then
    Result := Max(0, AOrigPrice * (1 - (ADiscount / 100)))
    else
    Result := ADollarDiscount;
    end; }
  // Changed 8/3/16 - In Price Update using Campaign pricing for line with no regular price was not getting any value
  if (AOrigPrice > 0) and (ADiscount > 0) and (ASaleMode = '%') then
    Result := Max(0, AOrigPrice * (1 - (ADiscount / 100)))
  else
    Result := ADollarDiscount;
end;

function TPrdHelpersI.CalcMarkup(const ACost, APrice: Currency; AUpc, APerPackage: Integer): Currency;
begin
  Result := 0;
  if AUpc = 0 then
    AUpc := 1;
  if APerPackage = 0 then
    APerPackage := 1;
  if (ACost <> 0) and (APrice <> 0) then
    Result := (((APrice / APerPackage) - (ACost / AUpc)) / (ACost / AUpc)) * 100;
end;

function TPrdHelpersI.CalcMarkupByCalcMode(ACalcMode: TPriceCalcMode; const ACost, APrice: Currency;
  AUpc, APerPackage: Integer): Currency;
begin
  if ACalcMode = pcmProfitMargin then
    Result := CalcProfitMargin(ACost, APrice, AUpc, APerPackage)
  else
    Result := CalcMarkup(ACost, APrice, AUpc, APerPackage);
end;

function TPrdHelpersI.CalcPriceByCalcMode(ACalcMode: TPriceCalcMode; const ACost, AValue: Currency;
  AUpc, APerPackage: Integer): Currency;
begin
  if ACalcMode = pcmProfitMargin then
    Result := CalcPriceFromProfitMargin(ACost, AValue, AUpc, APerPackage)
  else
    Result := CalcPriceFromMarkup(ACost, AValue, AUpc, APerPackage);
end;

function TPrdHelpersI.CalcPriceFromMarkup(const ACost, AMarkup: Currency; AUpc, APerPackage: Integer): Currency;
begin
  Result := 0;
  if AUpc = 0 then
    AUpc := 1;
  if APerPackage = 0 then
    APerPackage := 1;
  if (ACost <> 0) and (AMarkup <> 0) then
    Result := ((ACost / AUpc) * APerPackage) + (((ACost / AUpc) * APerPackage) * (AMarkup / 100));
end;

function TPrdHelpersI.CalcPriceFromProfitMargin(const ACost, AMargin: Currency; AUpc, APerPackage: Integer): Currency;
begin
  Result := 0;
  if AUpc = 0 then
    AUpc := 1;
  if APerPackage = 0 then
    APerPackage := 1;
  if (ACost <> 0) and (AMargin <> 0) then
    Result := ((ACost / AUpc) * APerPackage) / (1 - (AMargin / 100));
end;

function TPrdHelpersI.CalcProfitMargin(const ACost, APrice: Currency; AUpc, APerPackage: Integer): Currency;
begin
  Result := 0;
  if AUpc = 0 then
    AUpc := 1;
  if APerPackage = 0 then
    APerPackage := 1;
  if (ACost <> 0) and (APrice <> 0) then
    Result := (1 - ((ACost / AUpc) / (APrice / APerPackage))) * 100;
end;

procedure TPrdHelpersI.CatInfoQuery(ACode: string);
begin

  if not Assigned(FCatInfoQry) then
  begin
    FCatInfoQry := dmData.CreateQueryObject(nil);
    FCatInfoQry.RequestLive := True;
    FCatInfoQry.SQL.Text := 'Select * From CatInfo Where Upper(Code)=:CatCode;';
    FCatInfoQry.Prepare;
  end;

  with FCatInfoQry do
  begin
    Close;
    ParamByName('CatCode').AsString := ACode;
    Open;
  end;
end;

function TPrdHelpersI.CatInfoRecord(const ACode: string): TDataSet;
begin
  CatInfoQuery(ACode);
  Result := FCatInfoQry;
end;

procedure TPrdHelpersI.CopyPrdInfoRecord(const AOrigCode, ANewCode: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Try Drop Table #TempPrdInf1; Catch All End;');
  AQuery.SQL.Add('Select * Into #TempPrdInf1 From PrdInfo');
  AQuery.SQL.Add('Where UpcCode=:OldCode;');
  AQuery.SQL.Add('Update #TempPrdInf1 Set UpcCode=:NewCode;');
  AQuery.SQL.Add('Insert Into PrdInfo Select * From #TempPrdInf1;');
  AQuery.Prepare;
  AQuery.ParamByName('OldCode').AsString := AOrigCode;
  AQuery.ParamByName('NewCode').AsString := ANewCode;
  try
    AQuery.ExecSQL; // PrdInfo might not exist
    AQuery.Close;
    AQuery.AdsCloseSQLStatement;
  except
  end;
end;

procedure TPrdHelpersI.CopyPrdRecord(ADataSet: TDataSet; ACopyCode: string);
var
  AQuery: TAdsQuery;
  x: Integer;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select * From Prdmstr Where Code=%s;', [QuotedStr(ACopyCode)]);
  AQuery.Open;
  if not AQuery.IsEmpty then
    for x := 0 to AQuery.FieldCount - 1 do
      if (Pos(';' + UpperCase(AQuery.Fields[x].FieldName) + ';',
        PrdFieldListNoCopy + UpperCase(EnvSetting('PrdCpyExl').AsString + ';')) = 0) and
        (ADataSet.FindField(AQuery.Fields[x].FieldName) <> nil) and not AQuery.Fields[x].IsNull then
        ADataSet.FieldByName(AQuery.Fields[x].FieldName).Value := AQuery.Fields[x].Value;
end;

constructor TPrdHelpersI.Create;
var
  AQuery: TAdsQuery;
begin
  FStrCategory := TStringList.Create;
  FStrSubCategory := TStringList.Create;
  FStrDepartment := TStringList.Create;
  FStrBrand := TStringList.Create;
  FStrItemClass := TStringList.Create;
  FStrSeason := TStringList.Create;
  FStrProduceOrigin := TStringList.Create;
  FStrHechsher := TStringList.Create;
  FStrMeasure := TStringList.Create;
  FStrBin := TStringList.Create;
  FStrCCustom_List1 := TStringList.Create;
  FStrCCustom_List2 := TStringList.Create;
  FStrCCustom_List3 := TStringList.Create;
  FStrCCustom_List4 := TStringList.Create;
  FStrCCustom_List5 := TStringList.Create;

  FInventoryLocationList := '__Not_Set__';

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select UpcCode, Cast(0 AS SQL_NUMERIC(18,2)) Amount Into %s From OnHand Where UpcCode=%s;',
    [UpdInvenTableName, QuotedStr('')]);
  AQuery.ExecSQL;

  AQuery.SQL.Text :=
    Format('Select UpcCode, Location, Cast(0 AS SQL_NUMERIC(18,2)) Amount Into %s From OnHandLoc Where UpcCode=%s;',
    [UpdInvenLocTableName, QuotedStr('')]);
  AQuery.ExecSQL;

  FCsPrcAllowedOnPrcEncoded := EnvSetting('CsPrcEncd').AsBoolean;
end;

function TPrdHelpersI.CustomerPriceItemRecord(const ACustCode, AUpcCode: string): TDataSet;
begin
  if not Assigned(FPrdCstmPrcsQry) then
  begin
    FPrdCstmPrcsQry := dmData.CreateQueryObject(nil);
    FPrdCstmPrcsQry.RequestLive := True;
    FPrdCstmPrcsQry.SQL.Text := 'Select * From CstmPrcs Where Upper(PrdCode)=:PrdCode And Upper(CstCode)=:CstCode;';
    FPrdCstmPrcsQry.Prepare;
  end;

  with FPrdCstmPrcsQry do
  begin
    Close;
    ParamByName('CstCode').AsString := UpperCase(ACustCode);
    ParamByName('PrdCode').AsString := UpperCase(AUpcCode);
    Open;
  end;

  Result := FPrdCstmPrcsQry;
end;

destructor TPrdHelpersI.Destroy;
begin
  if Assigned(FLookQry) then
    FLookQry.Free;
  if Assigned(FPrdInfoQry) then
    FPrdInfoQry.Free;
  if Assigned(FPrdCstmPrcsQry) then
    FPrdCstmPrcsQry.Free;
  if Assigned(FCatInfoQry) then
    FCatInfoQry.Free;
  if Assigned(FtbCartonCode) then
    FtbCartonCode.Free;
  if Assigned(FtbCartonCodeAll) then
    FtbCartonCodeAll.Free;
  if Assigned(FtbCartonCodeAllPrdLookup) then
    FtbCartonCodeAllPrdLookup.Free;
  if Assigned(FtbMixHD) then
    FtbMixHD.Free;
  if Assigned(FtbPrdAlias) then
    FtbPrdAlias.Free;
  if Assigned(FtbPrdAliasAll) then
    FtbPrdAliasAll.Free;

  if Assigned(FStrCategory) then
    FStrCategory.Free;
  if Assigned(FStrSubCategory) then
    FStrSubCategory.Free;
  if Assigned(FStrDepartment) then
    FStrDepartment.Free;
  if Assigned(FStrBrand) then
    FStrBrand.Free;
  if Assigned(FStrItemClass) then
    FStrItemClass.Free;
  if Assigned(FStrSeason) then
    FStrSeason.Free;
  if Assigned(FStrProduceOrigin) then
    FStrProduceOrigin.Free;
  if Assigned(FStrHechsher) then
    FStrHechsher.Free;
  if Assigned(FStrMeasure) then
    FStrMeasure.Free;
  if Assigned(FStrBin) then
    FStrBin.Free;
  if Assigned(FStrCCustom_List1) then
    FStrCCustom_List1.Free;
  if Assigned(FStrCCustom_List2) then
    FStrCCustom_List2.Free;
  if Assigned(FStrCCustom_List3) then
    FStrCCustom_List3.Free;
  if Assigned(FStrCCustom_List4) then
    FStrCCustom_List4.Free;
  if Assigned(FStrCCustom_List5) then
    FStrCCustom_List5.Free;
  inherited;
end;

procedure TPrdHelpersI.DoCallMergePrdDataProc(var AOldCode: string; const ANewCode: string;
  const AAddAlias, ANoDelete: Boolean);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Execute Procedure cdMergePrdDataB(:NewCode, :OldCode, :AddAlias, :NoDelete);');
  AQuery.SQL.Add('Execute Procedure cdUpdateLastCostPrdInfo(:NewCode, Null);');
  AQuery.SQL.Add('Update Prdmstr Set LastSold=(Select Max(h.Date) From TmpCalc t, TicketHD h');
  AQuery.SQL.Add('Where Upper(t.Code)=:NewCode And h.ID=t.TicketID And h.TType=''S'') Where Code=:NewCode;');
  AQuery.Prepare;
  AQuery.ParamByName('NewCode').AsString := ANewCode;
  AQuery.ParamByName('OldCode').AsString := AOldCode;
  AQuery.ParamByName('AddAlias').AsBoolean := AAddAlias;
  AQuery.ParamByName('NoDelete').AsBoolean := ANoDelete;
  try
    AQuery.ExecSQL;
    AOldCode := ANewCode;
  except
    on E: Exception do
    begin
      if Pos('The stored procedure name is invalid', E.Message) > 0 then
        raise EHandleInApp.Create('Required stored procedure missing, database upgrade required.')
      else if Pos('Data truncated', E.Message) > 0 then // ignore
        AOldCode := ANewCode
      else
        raise;
    end;
  end;
end;

procedure TPrdHelpersI.DoCallMergePrdDataProcViaNotify(var AOldCode: string; const ANewCode: string;
  const AAddAlias, ANoDelete, AUpdatePrdmstrCode: Boolean);
var
  AStrList: TStringList;
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AStrList := TStringList.Create;
  try
    AStrList.Clear;
    if AUpdatePrdmstrCode then
      AStrList.Add('Update Prdmstr Set Code=:NewCode Where Code=:OldCode;');
    AStrList.Add('Execute Procedure cdMergePrdDataB(:NewCode, :OldCode, :AddAlias, :NoDelete);');
    AStrList.Add('Execute Procedure cdUpdateLastCostPrdInfo(:NewCode, Null);');
    AStrList.Add('Update Prdmstr Set LastSold=(Select Max(h.Date) From TmpCalc t, TicketHD h');
    AStrList.Add('Where Upper(t.Code)=:NewCode And h.ID=t.TicketID And h.TType=''S'') Where Code=:NewCode;');

    AStrList.Text := AStrList.Text.Replace(':NewCode', QuotedStr(ANewCode));
    AStrList.Text := AStrList.Text.Replace(':OldCode', QuotedStr(AOldCode));
    AStrList.Text := AStrList.Text.Replace(':AddAlias', BoolToStr(AAddAlias, True));
    AStrList.Text := AStrList.Text.Replace(':NoDelete', BoolToStr(ANoDelete, True));
    AQuery.SQL.Text := 'EXECUTE PROCEDURE cdAddNotification(''SQLCommand'', '''', :Data3);';
    AQuery.Prepare;
    AQuery.ParamByName('Data3').AsString := AStrList.Text;
    AQuery.ExecSQL;
  finally
    AStrList.Free;
  end;
end;

procedure TPrdHelpersI.DoCascadeProductCodeChange(AOldCode, ANewCode: string; AUpdatePrdmstr: Boolean);
begin
  GenHelpers.RecordAction('EDIT PRODUCT', 'Edit Product Code', 'Prdmstr', 'Code', 0, 0, AOldCode, ANewCode, ANewCode);
  DoCallMergePrdDataProc(AOldCode, ANewCode, False, True);
end;

procedure TPrdHelpersI.DoDeleteCartonCode(const ACartonCode, AItemCode: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Delete From CartonLs Where Upper(CartonCode)=:CartonCode And Upper(UpcCode)=:ItemCode;');
  AQuery.Prepare;
  AQuery.ParamByName('CartonCode').AsString := ACartonCode;
  AQuery.ParamByName('ItemCode').AsString := AItemCode;
  AQuery.ExecSQL;
end;

procedure TPrdHelpersI.DoDeleteCstmPrcItem(const AUpcCode, ACustCode: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Delete From CstmPrcs Where Upper(PrdCode)=:PrdCode And Upper(CstCode)=:CstCode;');
  AQuery.Prepare;
  AQuery.ParamByName('PrdCode').AsString := AUpcCode;
  AQuery.ParamByName('CstCode').AsString := ACustCode;
  AQuery.ExecSQL;
end;

procedure TPrdHelpersI.DoDeleteProduct(const AItemCode: string);
var
  AQuery: TAdsQuery;
begin
  // Modification here should be done at DoDeleteSelected as well
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Delete From VenItm Where UpcCode=:ItemCode;');
  AQuery.SQL.Add('Try Delete From Synom Where Upper(UpcCode)=:ItemCode; Catch All End;');
  AQuery.SQL.Add('Try Delete From AddOnPrd Where Upper(UpcCode)=:ItemCode; Catch All End;');
  AQuery.SQL.Add('Try Delete From PrdInfo Where Upper(UpcCode)=:ItemCode; Catch All End;');
  AQuery.SQL.Add('Delete From MixMatch Where Upper(UpcCode)=:ItemCode;');
  AQuery.SQL.Add
    ('Delete From PrdAlias Where PrdLineID=(Select Top 1 LineID From Prdmstr Where Upper(Code)=:ItemCode);');
  AQuery.SQL.Add('Delete From Prdmstr Where Upper(Code)=:ItemCode;');
  AQuery.Prepare;
  AQuery.ParamByName('ItemCode').AsString := AItemCode;
  AQuery.ExecSQL;
end;

function TPrdHelpersI.DoRemoveKitItems(const AItemCode, AAddOnCode: string): Integer;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Delete From AddOnPrd Where UpcCode=%s And AddOn=%s',
    [QuotedStr(AItemCode), QuotedStr(AAddOnCode)]);
  AQuery.ExecSQL;
  Result := AQuery.RowsAffected;
end;

procedure TPrdHelpersI.DoUpdateInventory;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Declare @Tries Integer, @Crs Cursor;');

  AQuery.SQL.Add
    (Format('Delete From %s Where Not UpcCode IN (Select Code From Prdmstr Where Not Code Is Null And Not NonInven);',
    [UpdInvenTableName]));
  AQuery.SQL.Add
    (Format('Delete From %s Where Not UpcCode IN (Select Code From Prdmstr Where Not Code Is Null And Not NonInven);',
    [UpdInvenLocTableName]));

  AQuery.SQL.Add('Set @Tries=0;');
  AQuery.SQL.Add(Format('While (@Tries<5) And (Select Count(*) From %s Where Amount<>0)>0 Do', [UpdInvenTableName]));
  AQuery.SQL.Add('Set @Tries=@Tries+1;');
  AQuery.SQL.Add(Format('Open @Crs AS Select * From %s Where Amount<>0;', [UpdInvenTableName]));
  AQuery.SQL.Add('While Fetch @Crs do');
  AQuery.SQL.Add('Try');
  AQuery.SQL.Add('Merge Into OnHand o ON o.UpcCode=@Crs.UpcCode');
  AQuery.SQL.Add('When Matched Then Update Set Amount=');
  AQuery.SQL.Add('IIF((IfNull(o.Amount,0)+@Crs.Amount)<99999 And (IfNull(o.Amount,0)+@Crs.Amount)>-9999,');
  AQuery.SQL.Add('IfNull(o.Amount,0)+@Crs.Amount, 0)');
  AQuery.SQL.Add('When Not Matched Then Insert (UpcCode, Amount) Values(@Crs.UpcCode, @Crs.Amount);');
  AQuery.SQL.Add(Format('Update %s Set Amount=0 Where UpcCode=@Crs.UpcCode;', [UpdInvenTableName]));
  AQuery.SQL.Add('Catch All End Try;');
  AQuery.SQL.Add('End While;');
  AQuery.SQL.Add('Close @Crs;');
  AQuery.SQL.Add('End While;');

  AQuery.SQL.Add('Set @Tries=0;');
  AQuery.SQL.Add(Format('While (@Tries<5) And (Select Count(*) From %s Where Amount<>0)>0 Do', [UpdInvenLocTableName]));
  AQuery.SQL.Add('Set @Tries=@Tries+1;');
  AQuery.SQL.Add(Format('Open @Crs AS Select * From %s Where Amount<>0;', [UpdInvenLocTableName]));
  AQuery.SQL.Add('While Fetch @Crs do');
  AQuery.SQL.Add('Try');
  AQuery.SQL.Add('EXECUTE PROCEDURE cdUpdateOnHandLoc(@Crs.Location, @Crs.UpcCode, @Crs.Amount);');
  AQuery.SQL.Add(Format('Update %s Set Amount=0', [UpdInvenLocTableName]));
  AQuery.SQL.Add('Where Location=@Crs.Location And UpcCode=@Crs.UpcCode;');
  AQuery.SQL.Add('Catch All End Try;');
  AQuery.SQL.Add('End While;');
  AQuery.SQL.Add('Close @Crs;');
  AQuery.SQL.Add('End While;');

  AQuery.ExecSQL;
end;

function TPrdHelpersI.FindBarcode(ADataSet: TDataSet; var ACode, APrice, AWeight: string): Boolean;
var
  ADataSetWasFiltered: Integer;
  AOrigCode, APreDigit: string;
  ANPrice, ATest: Integer;
  ANWeight: Double;
begin
  ADataSetWasFiltered := -1;
  AOrigCode := ACode;

  if not Assigned(ADataSet) then
    ADataSet := dmDataPrd.tbPrdmstr;

  if not ADataSet.Active then
    ADataSet.Open;

  if ADataSet.Filtered then
  begin
    ADataSetWasFiltered := 1;
    ADataSet.Filtered := False;
  end;

  Result := SameText(ADataSet.FieldByName('Code').AsString, ACode);

  if not Result then
  begin
    APreDigit := '';
    Result := FindBarcodeEx(ADataSet, 'Code', ACode, APreDigit);
    if Result and (APreDigit = '2') and ((Copy(ACode, 7, 5) = '00000') or (Copy(ACode, 7, 5) = '99999')) then
    begin
      APrice := IfThen(Copy(ACode, 7, 5) = '00000', Copy(AOrigCode, 8, 4), Copy(AOrigCode, 7, 5));
      Val(APrice, ANPrice, ATest);
      if (ATest = 0) and (ANPrice > 0) and (ADataSet.FindField('Price') <> nil) and
        (ADataSet.FieldByName('Price').AsFloat > 0) then
      begin
        ANWeight := ANPrice / (ADataSet.FieldByName('Price').AsFloat * 100);
        AWeight := FloatToStr(ANWeight);
      end;
    end;
  end;

  if (not Result) and (ACode <> '') then
  begin
    Result := ADataSet.Locate('CaseCode', ACode, []);
    if Result then
      ACode := ADataSet.FieldByName('Code').AsString;
  end;

  if (not Result) and (ACode <> '') and EnvSetting('PrdctSku').AsBoolean then
  begin
    Result := ADataSet.Locate('PrdctSku', ACode, []);
    if Result then
      ACode := ADataSet.FieldByName('Code').AsString;
  end;

  if not Result then // PrdAlias
  begin
    Result := FindBarcodeEx(tbPrdAlias, 'AliasCode', ACode, APreDigit);
    if Result then
    begin
      Result := tbPrdAlias.Locate('AliasCode', ACode, []);
      if Result then
      begin
        Result := ADataSet.Locate('LineID', tbPrdAlias.FieldByName('PrdLineID').AsString, []);
        if Result then
          ACode := ADataSet.FieldByName('Code').AsString;
      end;
      if not Result then
        ACode := AOrigCode;
    end;
  end;

  if (not Result) and (ADataSetWasFiltered = 1) then // if found, do not clear filter
    ADataSet.Filtered := True;
end;

function TPrdHelpersI.FindBarcode(ADataSet: TDataSet; var ACode: string): Boolean;
var
  APrice, AWeight: string;
begin
  Result := FindBarcode(ADataSet, ACode, APrice, AWeight);
end;

function TPrdHelpersI.FindBarcodeEx(ADataSet: TDataSet; const AFieldName: string; var ACode, APreDigit: string)
  : Boolean;
var
  AScanOpts: TDataSet;
  AQuery: TAdsQuery;
begin
  Result := ADataSet.Locate(AFieldName, ACode, []);
  if Result and (not SameText(ADataSet.FieldByName(AFieldName).AsString, ACode)) then
    ACode := ADataSet.FieldByName(AFieldName).AsString;

  if not Result then
  begin
    AScanOpts := ProjectHelpersI.ScanResolve.Execute(ACode);
    AScanOpts.First;
    while not AScanOpts.Eof do
    begin
      if (Length(Trim(AScanOpts.FieldByName('Scan').AsString)) >= 10) and
        (((AScanOpts.FieldByName('PreDigit').AsString = '1') and (Copy(AScanOpts.FieldByName('Scan').AsString, 1,
        2) = '99')) or ((AScanOpts.FieldByName('PreDigit').AsString = '') and
        (Copy(AScanOpts.FieldByName('Scan').AsString, 1, 3) = '199')))
      // or ((AScanOpts.FieldByName('PreDigit').AsString = '') and (Copy(AScanOpts.FieldByName('Scan').AsString, 1, 2) = '99')))
      then
      begin
        ACode := '';
        Break;
      end;

      Result := ADataSet.Locate(AFieldName, AScanOpts.FieldByName('Scan').AsString, []);
      if Result then
      begin
        ACode := AScanOpts.FieldByName('Scan').AsString;
        APreDigit := AScanOpts.FieldByName('PreDigit').AsString;
        Break;
      end;

      AScanOpts.Next;
    end;
  end;
  if (not Result) and (Length(Trim(ACode)) in [6, 7, 10, 11]) then
  begin
    AQuery := DataHelpers.HelperQuery;
    if SameText(AFieldName, 'AliasCode') then
      AQuery.SQL.Text := 'Select AliasCode From PrdAlias Where SubString(AliasCode, 2, :Len)=:Code'
    else
      AQuery.SQL.Text := 'Select Code From Prdmstr Where SubString(Code, 2, :Len)=:Code';
    AQuery.Prepare;
    if Length(Trim(ACode)) in [6, 7] then
      AQuery.ParamByName('Len').AsInteger := 6
    else if Length(Trim(ACode)) in [10, 11] then
      AQuery.ParamByName('Len').AsInteger := 10;
    AQuery.ParamByName('Code').AsString := ACode;
    AQuery.Open;
    if not AQuery.IsEmpty then
    begin
      Result := ADataSet.Locate(AFieldName, AQuery.Fields[0].AsString, []);
      if Result then
        ACode := AQuery.Fields[0].AsString;
    end;
  end;
end;

function TPrdHelpersI.FindUpcFromCartonCode(var ACode: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * From CartonLs Where Upper(CartonCode)=:CartonCode;';
  AQuery.Prepare;
  AQuery.ParamByName('CartonCode').AsString := ACode;
  AQuery.Open;

  Result := (not AQuery.IsEmpty) and (AQuery.FieldByName('UpcCode').AsString <> '');
  if Result then
    ACode := AQuery.FieldByName('UpcCode').AsString;
end;

procedure TPrdHelpersI.GenerateNewPrdCode(ADataSet: TDataSet);
var
  AIncNum: Integer;
  AIsUnique, AUPCA: Boolean;
  AMsg, AResult: string;
begin
  AUPCA := ProgPref('PrdCodeAutoGenUPCA');
  AIsUnique := False;
  while not AIsUnique do
  begin
    AIncNum := dmData.GetIncNum('Prdmstr', 'AutoCode', 0, True);
    if AUPCA then
    begin
      AResult := '0' + PadR(IntToStr(AIncNum), 10, '0');
      if AResult = '00999999999' then
      begin
        AResult := '';
        Break;
      end;
      AResult := AResult + ProjectHelpersI.ScanResolve.GetChkDgt(AResult);
    end;
    ADataSet.FieldByName('Code').AsString := AResult;
    AIsUnique := PrdCodeIsUnique(ADataSet, AMsg);
  end;
end;

function TPrdHelpersI.GetInventoryLocUpdateTableName: string;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Delete From %s;', [UpdInvenLocTableName]);
  AQuery.ExecSQL;
  Result := UpdInvenLocTableName;
end;

function TPrdHelpersI.GetInventoryUpdateTableName: string;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Delete From %s;', [UpdInvenTableName]);
  AQuery.ExecSQL;
  Result := UpdInvenTableName;
end;

function TPrdHelpersI.GetInventoryValue(const AUpcCode: string): Double;
var
  AQuery: TAdsQuery;
begin
  Result := 0;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select Amount From OnHand Where Upper(UpcCode)=:UpcCode;';
  AQuery.Prepare;
  AQuery.ParamByName('UpcCode').AsString := AUpcCode;
  AQuery.Open;
  if not AQuery.IsEmpty then
    Result := AQuery.Fields[0].AsFloat;
end;

function TPrdHelpersI.GetMixMatchCount(const AMixMatchID: string): Integer;
var
  AQuery: TAdsQuery;
begin
  Result := 0;
  if AMixMatchID <> '' then
  begin
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select Count(*) From MixMatch Where GroupID=:GroupID;';
    AQuery.Prepare;
    AQuery.ParamByName('GroupID').AsString := AMixMatchID;
    AQuery.Open;
    if not AQuery.IsEmpty then
      Result := AQuery.Fields[0].AsInteger;
  end;
end;

function TPrdHelpersI.GetMixMatchDescription(const AMixMatchID: string): string;
var
  AQuery: TAdsQuery;
begin
  Result := '';
  if AMixMatchID <> '' then
  begin
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select Description From MixHD Where GroupID=:GroupID;';
    AQuery.Prepare;
    AQuery.ParamByName('GroupID').AsString := AMixMatchID;
    AQuery.Open;
    if not AQuery.IsEmpty then
      Result := AQuery.Fields[0].AsString;
  end;
end;

function TPrdHelpersI.GettbCartonCode: TAdsQuery;
begin
  if not Assigned(FtbCartonCode) then
  begin
    FtbCartonCode := dmData.CreateQueryObject(nil);
    FtbCartonCode.SQL.Add('Select c.UpcCode, c.CartonCode, p.[Desc], p.Size, p.Cat, p.Brand, ');
    FtbCartonCode.SQL.Add('p.Price, p.Cost, p.CstLast');
    FtbCartonCode.SQL.Add('From CartonLs c Left Outer Join Prdmstr p On Upper(c.UpcCode)=Upper(p.Code)');
    FtbCartonCode.SQL.Add('Where Upper(c.UpcCode)=:UpcCode');
  end;
  Result := FtbCartonCode;
end;

function TPrdHelpersI.GettbCartonCodeAll: TAdsTable;
begin
  if not Assigned(FtbCartonCodeAll) then
  begin
    FtbCartonCodeAll := dmData.CreateTableObject(nil);
    FtbCartonCodeAll.TableName := 'CartonLs';
    FtbCartonCodeAll.IndexName := 'CartonCode';
    FtbCartonCodeAllPrdLookup := dmData.CreateTableObject(nil);
    FtbCartonCodeAllPrdLookup.TableName := 'Prdmstr';
    LoadLookups(FtbCartonCodeAll, FtbCartonCodeAllPrdLookup);
    FtbCartonCodeAll.Open;
  end;
  Result := FtbCartonCodeAll;
end;

function TPrdHelpersI.GettbMixHD: TAdsTable;
begin
  if not Assigned(FtbMixHD) then
  begin
    FtbMixHD := dmData.CreateTableObject(nil);
    FtbMixHD.TableName := 'MixHD';
    FtbMixHD.Open;
  end;
  Result := FtbMixHD;
end;

function TPrdHelpersI.GettbPrdAlias: TAdsTable;
begin
  if not Assigned(FtbPrdAlias) then
  begin
    FtbPrdAlias := dmData.CreateTableObject(nil);
    FtbPrdAlias.TableName := 'PrdAlias';
    FtbPrdAlias.Open;
  end;
  Result := FtbPrdAlias;
end;

function TPrdHelpersI.GettbPrdAliasAll: TAdsQuery;
begin
  if not Assigned(FtbPrdAliasAll) then
  begin
    FtbPrdAliasAll := dmData.CreateQueryObject(nil);
    FtbPrdAliasAll.SQL.Add('Select a.*, p.Code, p.[Desc], p.Brand, p.Size, p.Cat, p.SubCat, p.Cost, p.Price,');
    FtbPrdAliasAll.SQL.Add('p.UPC, p.CstLastC, p.NonInven From PrdAlias a Left Outer Join Prdmstr p ON p.LineID=a.PrdLineID;');
    FtbPrdAliasAll.Open;
  end;
  Result := FtbPrdAliasAll;
end;

function TPrdHelpersI.HasMasterClass(const AItemClass, AClassCode, AMasterField: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  Result := False;
  if AClassCode <> '' then
  begin
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := Format('Select * From Prdmstr Where Upper(ItemClass)=:ItemClass And Upper(%s)=:ClassCode;',
      [AMasterField]);
    AQuery.Prepare;
    AQuery.ParamByName('ItemClass').AsString := AItemClass;
    AQuery.ParamByName('ClassCode').AsString := AClassCode;
    AQuery.Open;
    Result := not AQuery.IsEmpty;
    // leave AQuery as-is so ApplyMasterDiffs could rely upon it
  end;
end;

function TPrdHelpersI.InventoryLocationListS(const IncludeRemote: Boolean): string;
var
  AQuery: TAdsQuery;
begin
  if SameText(FInventoryLocationList, '__Not_Set__') then
  begin
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select Location, Remote From InvLocList;';
    AQuery.Open;
    FInventoryLocationList := '';
    FInventoryLocationListNoRemote := '';
    while not AQuery.Eof do
    begin
      FInventoryLocationList :=
        Format('%s"%s",', [FInventoryLocationList, AQuery.FieldByName('Location').AsString.Trim]);
      if not AQuery.FieldByName('Remote').AsBoolean then
        FInventoryLocationListNoRemote :=
          Format('%s"%s",', [FInventoryLocationListNoRemote, AQuery.FieldByName('Location').AsString.Trim]);
      AQuery.Next;
    end;
    AQuery.Close;

    if (not FInventoryLocationList.IsEmpty) and (FInventoryLocationList[Length(FInventoryLocationList)] = ',') then
      Delete(FInventoryLocationList, Length(FInventoryLocationList), 1);
    if (not FInventoryLocationListNoRemote.IsEmpty) and
      (FInventoryLocationListNoRemote[Length(FInventoryLocationListNoRemote)] = ',') then
      Delete(FInventoryLocationListNoRemote, Length(FInventoryLocationListNoRemote), 1);
  end;

  if IncludeRemote then
    Result := FInventoryLocationList
  else
    Result := FInventoryLocationListNoRemote;
end;

function TPrdHelpersI.LoadPriceChangeCstmPriceData(const ACode, AGroupCaption: string): TAdsQuery;
begin
  if not Assigned(FQryPriceChangeCstmPrice) then
  begin
    FQryPriceChangeCstmPrice := dmData.CreateQueryObject(nil);
    FQryPriceChangeCstmPrice.SQL.Add('Select * From PrdPrice');
    FQryPriceChangeCstmPrice.SQL.Add('Where Cat=''PRDMSTR'' And Upper(UpcCode)=:Code And Caption=:GroupCaption;');
    FQryPriceChangeCstmPrice.Prepare;
  end;

  FQryPriceChangeCstmPrice.Close;
  FQryPriceChangeCstmPrice.ParamByName('Code').AsString := ACode;
  FQryPriceChangeCstmPrice.ParamByName('GroupCaption').AsString := AGroupCaption;
  FQryPriceChangeCstmPrice.Open;
  Result := FQryPriceChangeCstmPrice;
end;

function TPrdHelpersI.MixMatchGroupFromPrdCode(const AItemCode: string): string;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Select GroupID  From MixMatch Where Upper(UpcCode)=:ItemCode;');
  AQuery.Prepare;
  AQuery.ParamByName('ItemCode').AsString := AItemCode;
  AQuery.Open;
  Result := AQuery.FieldByName('GroupID').AsString;
end;

function TPrdHelpersI.NonPostedFutureCostExists(const ACode: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text :=
    'Select Count(*) Cnt From CostList Where TableName=''Prdmstr'' And KeyValue=:Key And PostedOn Is Null';
  AQuery.Prepare;
  AQuery.ParamByName('Key').AsString := ACode;
  AQuery.Open;
  Result := AQuery.Fields[0].AsInteger > 0;
end;

procedure TPrdHelpersI.PostInventoryAdjustment(const ACode, AMemo: string; const AQuan: Double;
  AInvLocation, AUserName: string; AUnitCost: Currency);
var
  AQuery: TAdsQuery;
  InvUpdTable, InvLocUpdTable: string;
  AAct: string;
const
  InvenPost = 'Insert Into %s Values(:UpcCode, :Amount);';
  InvenLocPost = 'Insert Into %s Values(:UpcCode, :InvLocation, :Amount);';
  AdjustPost = 'Insert Into AdjInv ([UpcCode], [Amount], [Act], [Name], [Memo], [Date], [Time], Location, UnitCost) ' +
    'Select :UpcCode, :Amount, :Act, :Name, :AMemo, CurDate(), Convert(CurTime(), SQL_CHAR), :InvLocation, ' +
    ':UnitCost From system.iota;';
begin
  InvUpdTable := InventoryUpdateTableName;
  InvLocUpdTable := InventoryLocUpdateTableName;
  if AQuan < 0 then
    AAct := '-'
  else
    AAct := '+';

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(Format(InvenPost, [InvUpdTable]));
  if AInvLocation <> '' then
    AQuery.SQL.Add(Format(InvenLocPost, [InvLocUpdTable]));
  AQuery.SQL.Add(AdjustPost);
  AQuery.Prepare;
  AQuery.ParamByName('UpcCode').AsString := ACode;
  AQuery.ParamByName('Amount').AsFloat := AQuan;
  AQuery.ParamByName('AMemo').AsString := AMemo;
  AQuery.ParamByName('Act').AsString := AAct;
  AQuery.ParamByName('Name').AsString := AUserName;
  AQuery.ParamByName('InvLocation').AsString := AInvLocation;
  AQuery.ParamByName('UnitCost').AsCurrency := AUnitCost;
  try
    AQuery.ExecSQL;
  except
    on E: Exception do
    begin
      if (Pos('Data truncated', E.Message) > 0) then
        Exit
      else
        raise;
    end;
  end;
  DoUpdateInventory;
end;

function TPrdHelpersI.PrdCodeIsUnique(ADataSet: TDataSet; var ADupDescription: string): Boolean;
var
  AField: TField;
  AScanOpts: TDataSet;
  AThisPass: Boolean;
  procedure CheckNamedField(AName, ASearch: string);
  var
    AOldValue: variant;
  begin
    AField := ADataSet.FindField(AName);
    AOldValue := AField.OldValue;
    if VarIsLikeNull(AOldValue) then
      AOldValue := '';
    if Assigned(AField) and (AField.AsString <> '') and (AField.Value <> AOldValue) then
    begin
      Result := not DataHelpers.RecordExits('Prdmstr', AName,
        Format('Upper(%s)=%s', [ASearch, QuotedStr(AField.AsString)]));

      if not Result then
      begin
        if SameText(AName, ASearch) then
          ADupDescription := Format('%s is duplicated in another product.', [AName])
        else
          ADupDescription := Format('%s is duplicated in %s value of another product.', [AName, ASearch]);
      end;

      if Result and SameText(AName, ASearch) then // Check PrdAlias once for Code, CaseCode and PrdctSku
      begin
        Result := not DataHelpers.RecordExits('PrdAlias', 'AliasCode',
          Format('Upper(AliasCode)=%s', [QuotedStr(AField.AsString)]));
        if not Result then
          ADupDescription := Format('%s is duplicated in an AliasCode value for a product.', [AName]);
      end;
    end;
  end;

begin
  Result := True;

  if ADataSet.FieldByName('Code').AsString = '' then
  begin
    Result := False;
    ADupDescription := 'CODE can''t be empty';
  end;

  if Result then
    CheckNamedField('Code', 'Code');

  if Result then
    CheckNamedField('Code', 'CaseCode');

  if Result then
    CheckNamedField('Code', 'PrdctSku');

  if Result then
    CheckNamedField('CaseCode', 'CaseCode');

  if Result then
    CheckNamedField('CaseCode', 'Code');

  if Result then
    CheckNamedField('CaseCode', 'PrdctSku');

  if Result then
    CheckNamedField('PrdctSku', 'PrdctSku');

  if Result then
    CheckNamedField('PrdctSku', 'Code');

  if Result then
    CheckNamedField('PrdctSku', 'CaseCode');

  if not Result then
    Exit;

  AScanOpts := ProjectHelpersI.ScanResolve.Execute(ADataSet.FieldByName('Code').AsString);
  AScanOpts.First;
  while not AScanOpts.Eof do
  begin
    AThisPass := not DataHelpers.RecordExits('Prdmstr', 'Code',
      Format('Upper(Code)=%s', [QuotedStr(AScanOpts.FieldByName('Scan').AsString)]));
    // Result := not ADataSet.Locate('Code', AScanOpts.FieldByName('Scan').AsString, []);
    if (not AThisPass) and (VarIsLikeNull(ADataSet.FieldByName('Code').OldValue) or
      (ADataSet.FieldByName('Code').OldValue <> AScanOpts.FieldByName('Scan').AsString)) then
    begin
      Result := False; // Can't set Result above cause when caseCode is being edited Result=False for nothing
      ADupDescription := Format('Code is duplicated with Code: %s,'#10#13'(ID #%d).',
        [AScanOpts.FieldByName('Scan').AsString, AScanOpts.FieldByName('Indx').AsInteger]);
      Break;
    end;
    AScanOpts.Next;
  end;

  if not Result then
    Exit;

  // PrdAlias
  AScanOpts.First;
  while not AScanOpts.Eof do
  begin
    Result := not DataHelpers.RecordExits('PrdAlias', 'AliasCode',
      Format('Upper(AliasCode)=%s', [QuotedStr(AScanOpts.FieldByName('Scan').AsString)]));
    if not Result then
    begin
      ADupDescription := Format('Code is duplicated with AliasCode: %s,'#10#13'(ID #%d).',
        [AScanOpts.FieldByName('Scan').AsString, AScanOpts.FieldByName('Indx').AsInteger]);
      Break;
    end;
    AScanOpts.Next;
  end;
end;

function TPrdHelpersI.PrdFieldComment(const AField: string): string;
begin
  // Result := DataHelpers.GetADSFieldComment(dmData.Dict, 'Prdmstr', AField);
  Result := DataHelpers.GetADSFieldComment('Prdmstr', AField);
end;

procedure TPrdHelpersI.PrdInfoQuery(ACode: string);
begin
  if not Assigned(FPrdInfoQry) then
  begin
    FPrdInfoQry := dmData.CreateQueryObject(nil);
    FPrdInfoQry.RequestLive := True;
    FPrdInfoQry.SQL.Text := 'Select * From PrdInfo Where Upper(UpcCode)=:UpcCode;';
    FPrdInfoQry.Prepare;
  end;

  with FPrdInfoQry do
  begin
    Close;
    ParamByName('UpcCode').AsString := ACode;
    Open;
  end;
end;

function TPrdHelpersI.PrdInfoRecord(const ACode: string): TDataSet;
begin
  PrdInfoQuery(ACode);
  Result := FPrdInfoQry;
end;

function TPrdHelpersI.PrdLookupRecord(const ACode: string): TDataSet;
begin
  PrdQuery(ACode);
  Result := FLookQry;
end;

function TPrdHelpersI.PrdLookupValue(const ACode, AFieldName: string): string;
begin
  PrdQuery(ACode);
  Result := FLookQry.FieldByName(AFieldName).AsString;
  FLookQry.Close;
end;

procedure TPrdHelpersI.PrdQuery(ACode: string);
begin
  if not Assigned(FLookQry) then
  begin
    FLookQry := dmData.CreateQueryObject(nil);
    FLookQry.SQL.Text := 'Select * From Prdmstr Where Upper(Code)=:Code';
    FLookQry.Prepare;
  end;

  with FLookQry do
  begin
    Close;
    ParamByName('Code').AsString := ACode;
    Open;
  end;
end;

procedure TPrdHelpersI.PrdServerValuesToDataSet(ADataSet: TDataSet; AValueList: TStringList);
var
  I: Integer;
begin
  if Assigned(AValueList) then
    for I := 0 to AValueList.Count - 1 do
      if (Pos(';' + UpperCase(AValueList.Names[I]) + ';', PrdFieldListNoCopy + UpperCase(EnvSetting('PrdCpyExl')
        .AsString + ';')) = 0) and (ADataSet.FindField(AValueList.Names[I]) <> nil) then
        try
          ADataSet.FindField(AValueList.Names[I]).Value := AValueList.ValueFromIndex[I];
        except
        end;
end;

function TPrdHelpersI.PricingOptionsExist(const ACategory, ACode: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  Result := False;
  if not GenHelpers.IsModuleRegistered(MODULE_EDIT_PRD_PRICE) then
    Exit;

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select Top 1 * From PrdPrice Where Upper(Cat)=:Category And Upper(UpcCode)=Upper(:Code);';
  AQuery.Prepare;
  AQuery.ParamByName('Category').AsString := UpperCase(ACategory);
  AQuery.ParamByName('Code').AsString := UpperCase(ACode);
  AQuery.Open;
  Result := not AQuery.IsEmpty;
end;

procedure TPrdHelpersI.ProductPrdServerValuesToStringList(AStringList: TStringList; xmlData: IXMLNode;
  const AIndex: Integer);
var
  ANode: IXMLNode;
  I: Integer;
  ANodeName: string;
begin
  ANode := xmlData.ChildNodes.FindNode('items');
  if Assigned(ANode.ChildNodes.FindNode('item')) then
    ANode := ANode.ChildNodes[AIndex];

  AStringList.Clear;
  for I := 0 to ANode.ChildNodes.Count - 1 do
    if ANode.ChildNodes[I].Text <> '' then
    begin
      ANodeName := ANode.ChildNodes[I].NodeName;
      if SameText(ANodeName, 'Description') then
        ANodeName := 'Desc';
      AStringList.Add(Format('%s=%s', [ANodeName, UpperCase(ANode.ChildNodes[I].Text)]));
    end;
end;

procedure TPrdHelpersI.RecordPriceChange(AField: TField; AView: string; AUppField: TField);
var
  AOldValue: Currency;
  AOldUpp: Integer;
  AUppData: string;
begin
  AOldValue := 0;
  if not VarIsLikeNull(AField.OldValue) then
    AOldValue := AField.OldValue;

  if AField.AsCurrency = AOldValue then
    Exit;

  AUppData := '';
  AOldUpp := 0;
  if Assigned(AUppField) then
  begin
    if not VarIsLikeNull(AUppField.OldValue) then
      AOldUpp := AUppField.OldValue;
    AUppData := Format('Per From: %d, To: %d', [AOldUpp, AUppField.AsInteger]);
  end;

  GenHelpers.RecordAction('PRICE_CHANGE', 'Change ' + AField.FieldName, 'Prdmstr', AField.FieldName, AOldValue,
    AField.AsCurrency, AView, AUppData, AField.DataSet.FieldByName('Code').AsString);
end;

procedure TPrdHelpersI.RecordPriceChange(ADataSet: TDataSet; AView: string);
begin
  RecordPriceChange(ADataSet.FieldByName('Price'), AView);
  RecordPriceChange(ADataSet.FieldByName('CasePrice'), AView);
  RecordPriceChange(ADataSet.FieldByName('PkgPrc'), AView, ADataSet.FieldByName('UPP'));
  RecordPriceChange(ADataSet.FieldByName('Sale'), AView);
  RecordPriceChange(ADataSet.FieldByName('CaseSale'), AView);
  RecordPriceChange(ADataSet.FieldByName('SPkgPrc'), AView, ADataSet.FieldByName('SUPP'));
end;

function TPrdHelpersI.RestrictCasePrice(const ACode: string): Boolean;
begin
  Result := (not FCsPrcAllowedOnPrcEncoded) and (Length(ACode) = 11) and (Copy(ACode, 1, 1) = '2') and
    ((Copy(ACode, 7, 5) = '00000') or (Copy(ACode, 7, 5) = '99999'));
end;

procedure TPrdHelpersI.ScanResolveListForCode(const ACode: string; AList: TStringList);
var
  AScanOpts: TDataSet;
begin
  if not Assigned(AList) then
    AList := TStringList.Create;
  AList.Sorted := True;
  AList.Duplicates := dupIgnore;
  AScanOpts := ProjectHelpersI.ScanResolve.Execute(ACode);
  AScanOpts.First;
  while not AScanOpts.Eof do
  begin
    AList.Add(AScanOpts.FieldByName('Scan').AsString);
    AScanOpts.Next;
  end;
end;

procedure TPrdHelpersI.StrListFromField(AStrList: TStringList; const AFieldName, AFilter: string; AReload: Boolean);
var
  AFieldNameSql: string;
  procedure StrListFromFieldEx(AStrListEx: TStringList);
  var
    AQuery: TAdsQuery;
  begin
    if (AStrListEx.Count = 0) or AReload then
    begin
      AStrListEx.Clear;
      AQuery := DataHelpers.HelperQuery;
      if AFilter = '' then
        AQuery.SQL.Text := Format('Select [%s] From Prdmstr Where [%s]<>'''' Group By 1;', [AFieldName, AFieldName])
      else
        AQuery.SQL.Text := Format('Select [%s] From Prdmstr Where [%s]<>'''' And %s Group By 1;',
          [AFieldName, AFieldName, AFilter]);
      AQuery.Open;
      while not AQuery.Eof do
      begin
        AStrListEx.Add(AQuery.Fields[0].AsString);
        AQuery.Next;
      end;
    end;
    AStrList.Assign(AStrListEx);
  end;

begin
  AFieldNameSql := AFieldName;
  if SameText(AFieldName, 'Cat') then
    StrListFromFieldEx(FStrCategory)
  else if SameText(AFieldName, 'Brand') then
    StrListFromFieldEx(FStrBrand)
  else if SameText(AFieldName, 'Department') then
  begin
    AFieldNameSql := 'Upper(' + AFieldName + ')';
    StrListFromFieldEx(FStrDepartment);
  end
  else if SameText(AFieldName, 'ItemClass') then
    StrListFromFieldEx(FStrItemClass)
  else if SameText(AFieldName, 'Season') then
    StrListFromFieldEx(FStrSeason)
  else if SameText(AFieldName, 'PrdOrigin') then
    StrListFromFieldEx(FStrProduceOrigin)
  else if SameText(AFieldName, 'Hechsher') then
    StrListFromFieldEx(FStrHechsher)
  else if SameText(AFieldName, 'Unit') then
    StrListFromFieldEx(FStrMeasure)
  else if SameText(AFieldName, 'Bin') then
    StrListFromFieldEx(FStrBin)
  else if SameText(AFieldName, 'SubCat') then
  begin
    FStrSubCategory.Clear;
    StrListFromFieldEx(FStrSubCategory);
  end
  else if SameText(AFieldName, 'CCustom_List1') then
    StrListFromFieldEx(FStrCCustom_List1)
  else if SameText(AFieldName, 'CCustom_List2') then
    StrListFromFieldEx(FStrCCustom_List2)
  else if SameText(AFieldName, 'CCustom_List3') then
    StrListFromFieldEx(FStrCCustom_List3)
  else if SameText(AFieldName, 'CCustom_List4') then
    StrListFromFieldEx(FStrCCustom_List4)
  else if SameText(AFieldName, 'CCustom_List5') then
    StrListFromFieldEx(FStrCCustom_List5);
end;

procedure TPrdHelpersI.UpdatePrd(JSONRequest, JSONResponse: TJSONObject);
var
  ACode, AOrigCode, ADupMessage: string;
  AErrorIfFieldNotFound, AUpdateCodeToFull: Boolean;
  ADataSet: TDataSet;
  AEnumP: TJSONPairEnumerator;
  AField: TField;
  APair: TJSONPair;
begin
  ADataSet := dmDataPrd.tbPrdmstr;
  if JSONRequest.TryGetValue<string>('Code', ACode) then
    JSONRequest.RemovePair('Code');

  if ACode.IsEmpty then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'UpdatePrd required parameter missing (Code)');
    Exit;
  end;

  AOrigCode := ACode;

  if not JSONRequest.TryGetValue<Boolean>('ErrorIfFieldNotFound', AErrorIfFieldNotFound) then
    AErrorIfFieldNotFound := False
  else
    JSONRequest.RemovePair('ErrorIfFieldNotFound');

  if not JSONRequest.TryGetValue<Boolean>('UpdateCodeToFull', AUpdateCodeToFull) then
    AUpdateCodeToFull := False
  else
    JSONRequest.RemovePair('UpdateCodeToFull');

  if not FindBarcode(ADataSet, ACode) then
  begin
    JSONResponse.AddPair('Status', 'Error');
    JSONResponse.AddPair('Error', 'Item not found');
    Exit;
  end;

  ADataSet.Edit;
  AEnumP := JSONRequest.GetEnumerator;
  while AEnumP.MoveNext do
  begin
    APair := AEnumP.Current;
    AField := ADataSet.FindField(APair.JsonString.Value);

    if AErrorIfFieldNotFound and (not Assigned(AField)) then
    begin
      ADataSet.Cancel;
      JSONResponse.AddPair('Status', 'Error');
      JSONResponse.AddPair('Error', 'Field ' + APair.JsonString.Value + ' Not Found');
      Exit;
    end;

    if Assigned(AField) and (APair.JsonValue.Value <> '') then
      AField.AsString := APair.JsonValue.Value;
  end;
  try
    ADataSet.Post;
  except
    ADataSet.Cancel;
    raise;
  end;

  if AUpdateCodeToFull and (((ACode.Length in [10, 11]) and (AOrigCode.Length = 12)) or
    ((ACode.Length in [6, 7]) and (AOrigCode.Length = 8))) then
    DoCallMergePrdDataProcViaNotify(ACode, AOrigCode, False, True, True);
  JSONResponse.AddPair('Status', 'Success');
end;

end.
