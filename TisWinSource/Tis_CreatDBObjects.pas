unit Tis_CreatDBObjects;

interface

uses
  DB, Classes, SysUtils, Utils, Dialogs, AdsTable;

procedure CreateAllFieldObjects;
//procedure MyLoadFieldEvents;
procedure SetMinValue0(tTable: TDataSet; sField: string);
procedure SetMinValueTo0(tTable: TDataSet; sFld: string);
procedure LoadLookups(TrgtData, lkpData1: TDataSet;
  lkpData2: TDataSet = nil; lkpData3: TDataSet = nil; sTrgt: string = '';
  bNoOpen: Boolean = False);
procedure CreateCalcFields(DataSet: TDataSet; StrFName, sType, Format:
  string; bNoOpen: Boolean = False);
procedure DoLookUp(sDataSet, lkpData: TDataSet; sType, sFieldname,
  sDisplayLabel, sKeyFields, sLookUpKeyFields, sLookUpresultField: string;
  ASize: Integer = 20);
procedure OpenTables(dm: TDataModule);
procedure FormatTelFields(DS: TDataSet);

implementation

uses dmDataModule, dmDataModulePrd, DynaFileds{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};


procedure CreateAllFieldObjects;
  procedure PrdmstrCalcFields(DS: TDataSet);
  var
    MuFormat: string;
  begin
    GenerateFieldComponents(DS, False);

    MuFormat := '0.00%';
    CreateCalcFields(DS, 'CalPriceMarkup', 'F', MuFormat, True);
    CreateCalcFields(DS, 'CalPriceMarkupMR', 'F', MuFormat, True);

    if (DS.FindField('Cost2') <> nil) and (DS.FindField('Price2') <> nil) then
    begin
      CreateCalcFields(DS, 'CalPriceMarkup2', 'F', MuFormat, True);
      CreateCalcFields(DS, 'CalPriceMarkupMR2', 'F', MuFormat, True);
    end;

    if (DS.FindField('Cost3') <> nil) and (DS.FindField('Price3') <> nil) then
    begin
      CreateCalcFields(DS, 'CalPriceMarkup3', 'F', MuFormat, True);
      CreateCalcFields(DS, 'CalPriceMarkupMR3', 'F', MuFormat, True);
    end;

    CreateCalcFields(DS, 'CalCostValue', 'C', '', True);
    CreateCalcFields(DS, 'CalCostPrUnt', 'F', '#0.00###', True);
    CreateCalcFields(DS, 'CalRetailValue', 'C', '', True);
    CreateCalcFields(DS, 'CalRetailValue', 'F', '#.00', True);
    CreateCalcFields(DS, 'CalOnHndCs', 'I', '', True);
    CreateCalcFields(DS, 'CalOnHndUn', 'I', '', True);
    CreateCalcFields(DS, 'CalOnHndAv', 'I', '', True);
  end;
  procedure OrderDtCalcFields(DS: TDataSet);
  begin
    GenerateFieldComponents(DS, False);
    CreateCalcFields(DS, 'Amount', 'C', '', True);
    CreateCalcFields(DS, 'OldAmount', 'C', ''); {we open here}
  end;
  procedure OrdrDtCalcFields(DS: TDataSet);
  begin
    GenerateFieldComponents(DS, False);
    CreateCalcFields(DS, 'TotalPr', 'C', '', True);
    CreateCalcFields(DS, 'TotalWt', 'F', '#.00');
  end;
  procedure QuoteDTCalcFields(DS: TDataSet);
  begin
    GenerateFieldComponents(DS, False);
    CreateCalcFields(DS, 'TotalPr', 'C', '', True);
    CreateCalcFields(DS, 'TotalWt', 'F', '#.00');
  end;
  procedure DoSpecialHandling;
  begin
//    LoadLookUps(dmDataPrd.tbPrdmstr, dmDataPrd.tbFabric);
//    LoadLookups(dmDataPrd.tbAddOnPrd, dmDataPrd.tbPrdmstr);
  end;
begin
  PrdmstrCalcFields(dmDataPrd.tbPrdmstr);
//  FormatTelFields(dmDataCust.tbCustomer);

  //MyLoadFieldEvents;

  DoSpecialHandling;

  {$IFNDEF NONGUI}
  OpenTables(dmData);
  {$ENDIF}
  OpenTables(dmDataPrd);
//  OpenTables(dmDataCust);
end;

procedure OpenTables(dm: TDataModule);
var
  x: Integer;
begin
  for x := 0 to dm.ComponentCount - 1 do
  begin
    if (Dm.Components[x] is TAdsTable) then
      if TAdsTable(dm.Components[x]).Active <> True then
        TAdsTable(dm.Components[x]).open;
  end;
end;


{procedure MyLoadFieldEvents;
var
  TF: TField;
  function FindFieldByFieldName(DataSet: TDataSet; FieldName: string): TField;
  var
    x: integer;
  begin
    Result := nil;
    for x := 0 to DataSet.FieldCount do
      if SameText(DataSet.Fields[x].FieldName, FieldName) then
      begin
        Result := DataSet.Fields[x];
        Break;
      end;
  end;
begin
  TF := (dmDataPrd.tbPrdmstr.FindComponent('tbPrdmstrCOST') as TField);
  if TF <> nil then
    TF.OnChange := dmDataPrd.tbPrdmstrCOSTChange;
  TF := (dmDataPrd.tbPrdmstr.FindComponent('tbPrdmstrPRICE') as TField);
  if TF <> nil then
    TF.OnChange := dmDataPrd.tbPrdmstrPRICEChange;
  TF := FindFieldByFieldName(dmDataPrd.tbPrdmstr, 'Style');
  if TF <> nil then
    TF.OnChange := dmDataPrd.tbPrdmstrSTYLEChange;
end;}

procedure CreateCalcFields(DataSet: TDataSet; StrFName, sType, Format:
  string; bNoOpen: Boolean = False);
var
  T: TField;
  strDataSet: string;
begin
  T := (DataSet.FindComponent(DataSet.Name + StrFName) as TField);
  if (T = nil) then
  begin
    strDataSet := UpperCase(DataSet.Name);
    StrFName := UpperCase(StrFName);
    if DataSet.Active then
      DataSet.Close;
    if sType = 'F' then
    begin
      T := TFloatField.Create(DataSet);
      TFloatField(T).DisplayFormat := Format;
    end
    else if sType = 'S' then
      T := TStringField.Create(DataSet)
    else if sType = 'L' then
      T := TBooleanField.Create(DataSet)
    else if sType = 'I' then
      T := TIntegerField.Create(DataSet)
    else if sType = 'C' then
    begin
      T := TCurrencyField.Create(DataSet);
      if Format <> '' then
        TCurrencyField(T).DisplayFormat := Format;
    end;
    T.FieldName := StrFName;
    T.Name := DataSet.Name + T.FieldName;
    T.DisplayLabel := StrFName;
    T.DataSet := DataSet;
    T.FieldKind := fkCalculated;
    T.Calculated := True;
    //    DataSet.OnCalcFields := AllCalcFields; // assign event handler
    if not bNoOpen then
      DataSet.Open;
  end;
end;

procedure LoadLookups(TrgtData, lkpData1: TDataSet;
  lkpData2: TDataSet = nil; lkpData3: TDataSet = nil; sTrgt: string = '';
  bNoOpen: Boolean = False);
// ==sTrgt== is used if target Ads Table Name is difrent then DBF name
//           i.e. you have 2 Diffrent AdsTable for One DBF and each one has difrent lookUps
//           I Hope You Understood if not look in the function at sTrgt
// sTrgt is Case Senstive
var
  x: integer;
begin
  if sTrgt = '' then
    sTrgt := UpperCase(TAdsTable(TrgtData).TableName);
  x := Pos('.', sTrgt);
  if x > 0 then
    sTrgt := Copy(sTrgt, 1, x - 1);

  GenerateFieldComponents(TrgtData, False); //if  it wasn't done for this DS yet

  if SameText(sTrgt, 'AddOnPrd') then
  begin
    TrgtData.Close;
    DoLookUp(TrgtData, lkpData1, 'S', 'lkpDesc', 'Description', 'ADDON', 'CODE', 'DESC');
    DoLookUp(TrgtData, lkpData1, 'S', 'lkpType', 'Type', 'ADDON', 'CODE', 'Type');
    if not bNoOpen then
      TrgtData.Open;
  end
  else if SameText(sTrgt, 'VenItm') or SameText(sTrgt, 'CartonLs') then
  begin
    TrgtData.Close;
    DoLookUp(TrgtData, lkpData1, 'S', 'Desc', 'Description', 'UpcCode', 'Code', 'Desc');
    DoLookUp(TrgtData, lkpData1, 'S', 'Brand', 'Brand', 'UpcCode', 'Code', 'Brand');
    DoLookUp(TrgtData, lkpData1, 'S', 'Size', 'Size', 'UpcCode', 'Code', 'Size');
    DoLookUp(TrgtData, lkpData1, 'S', 'Cat', 'Category', 'UpcCode', 'Code', 'Cat');
    DoLookUp(TrgtData, lkpData1, 'S', 'SubCat', 'Sub Category', 'UpcCode', 'Code', 'SubCat');
    DoLookUp(TrgtData, lkpData1, 'C', 'PrdCost', 'Prd Cost', 'UpcCode', 'Code', 'Cost');
    DoLookUp(TrgtData, lkpData1, 'C', 'Price', 'Price', 'UpcCode', 'Code', 'Price');
    DoLookUp(TrgtData, lkpData1, 'C', 'CstLastC', 'CstLastC', 'UpcCode', 'Code', 'CstLastC');
    DoLookUp(TrgtData, lkpData1, 'I', 'PrdUPC', 'Prd UPC', 'UpcCode', 'Code', 'UPC');
    DoLookUp(TrgtData, lkpData1, 'L', 'NonInven', 'Non-Inventory', 'UpcCode', 'Code', 'NonInven');
    DoLookUp(TrgtData, lkpData2, 'S', 'VendName', 'Vendor Name', 'VendID', 'VendID', 'Name');
    if not bNoOpen then
      TrgtData.Open;
  end
  else if SameText(sTrgt, 'PrdAlias') then
  begin
    TrgtData.Close;
    DoLookUp(TrgtData, lkpData1, 'S', 'Code', 'Code', 'PrdLineID', 'LineID', 'Code');
    DoLookUp(TrgtData, lkpData1, 'S', 'Desc', 'Description', 'PrdLineID', 'LineID', 'Desc');
    DoLookUp(TrgtData, lkpData1, 'S', 'Brand', 'Brand', 'PrdLineID', 'LineID', 'Brand');
    DoLookUp(TrgtData, lkpData1, 'S', 'Size', 'Size', 'PrdLineID', 'LineID', 'Size');
    DoLookUp(TrgtData, lkpData1, 'S', 'Cat', 'Category', 'PrdLineID', 'LineID', 'Cat');
    DoLookUp(TrgtData, lkpData1, 'S', 'SubCat', 'Sub Category', 'PrdLineID', 'LineID', 'SubCat');
    DoLookUp(TrgtData, lkpData1, 'C', 'Cost', 'Cost', 'PrdLineID', 'LineID', 'Cost');
    DoLookUp(TrgtData, lkpData1, 'C', 'Price', 'Price', 'PrdLineID', 'LineID', 'Price');
    DoLookUp(TrgtData, lkpData1, 'C', 'CstLastC', 'CstLastC', 'PrdLineID', 'LineID', 'CstLastC');
    DoLookUp(TrgtData, lkpData1, 'I', 'UPC', 'UPC', 'PrdLineID', 'LineID', 'UPC');
    DoLookUp(TrgtData, lkpData1, 'L', 'NonInven', 'Non-Inventory', 'UpcCode', 'Code', 'NonInven');
    if not bNoOpen then
      TrgtData.Open;
  end
  else if SameText(sTrgt, 'Prdmstr') then
  begin
    TrgtData.Close;
    DoLookUp(TrgtData, lkpData1, 'S', 'lkpFabricName', 'Fabric Name', 'Style', 'FabricCode', 'FabricName');
    if not bNoOpen then
      TrgtData.Open;
  end;
end;

procedure DoLookUp(sDataSet, lkpData: TDataSet; sType, sFieldname,
  sDisplayLabel, sKeyFields, sLookUpKeyFields, sLookUpresultField: string;
  ASize: Integer = 20);
var
  strDataSet: string;
  T: TField;
begin
  if sDataSet.FindField(sKeyFields) <> nil then
  begin
    strDataSet := UpperCase(SDataSet.Name);
    if SDataSet.Active then SDataSet.Close;
    if sType = 'F' then
    begin
      T := TFloatField.Create(SDataSet);
      TFloatField(T).DisplayFormat := sDisplayLabel;
    end
    else if sType = 'I' then
      T := TIntegerField.Create(SDataSet)
    else if sType = 'C' then
      T := TCurrencyField.Create(SDataSet)
    else if sType = 'D' then
      T := TDateField.Create(SDataSet)
    else //if sType = 'S' then
    begin
      T := TStringField.Create(SDataSet);
      T.Size := ASize; { TODO : // Maybe Check for size of lookup field }
    end;
    with T do
    begin
      FieldName := sFieldName;
      Name := strDataSet + FieldName;
      DisplayLabel := sDisplayLabel;
      DataSet := SDataSet;
      FieldKind := fkLookup;
      KeyFields := sKeyFields;
      LookupDataSet := lkpData;
      LookupKeyFields := sLookupKeyFields;
      LookupResultField := sLookupResultField;
    end;
  end;
end;

procedure SetMinValue0(tTable: TDataSet; sField: string);
var
  tTester: TFloatField;
begin
  tTester := TFloatField(tTable.FindField(sField));
  if (tTester <> nil) and (tTester.MinValue = 0) then
    tTester.MinValue := (0 - tTester.MaxValue);
end;

procedure SetMinValueTo0(tTable: TDataSet; sFld: string);
var
  fld: TFloatField;
begin
  fld := TFloatField(tTable.FindField(sFld));
  if (fld <> nil) then
    fld.MinValue := 0;
end;

procedure AllCalcFields(DataSet: TDataSet);
var
  T: TField;
begin
  if Dataset.State = dsCalcFields then
  begin
    // UniqId for any applicable DataSet
    T := (DataSet.FindComponent(DataSet.Name + 'CalcRecNo') as TIntegerField);
    if (T <> nil) then
      T.Value := DataSet.RecNo;
  end;
end;

procedure FormatTelFields(DS: TDataSet);
var
  sTelFormat: string;
begin
  GenerateFieldComponents(DS, False);
  sTelFormat := '!\(000\)000-0000;0; ';
  if (DS.FindField('Tel') <> nil) then
    DS.FindField('Tel').EditMask := sTelFormat;
  if (DS.FindField('Tel2') <> nil) then
    DS.FindField('Tel2').EditMask := sTelFormat;
  if (DS.FindField('Cell') <> nil) then
    DS.FindField('Cell').EditMask := sTelFormat;
  if (DS.FindField('Fax') <> nil) then
    DS.FindField('Fax').EditMask := sTelFormat;
end;

end.
