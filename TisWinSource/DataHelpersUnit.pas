unit DataHelpersUnit;

interface

uses
  SysUtils, Classes, DB, AdsData, AdsFunc, AdsTable;

type
  TDataHelpers = class
  private
    FHelperQuery: TAdsQuery;
    FSimpleQuery: TAdsQuery;
    FDataDefQuery: TAdsQuery;
    FAdsServerVersion: Integer;
    function GetHelperQuery: TAdsQuery;
    function GetHelperQueryActive: TAdsQuery;
    function GetAdsServerVersion: Integer;
  protected
    function VarIsLikeNull(const V: Variant): Boolean;
  public
    property HelperQuery: TAdsQuery read GetHelperQuery;
    property HelperQueryActive: TAdsQuery read GetHelperQueryActive;
    property AdsServerVersion: Integer read GetAdsServerVersion;

    procedure CascadeDelete(const ATable, AWhere: string);
    procedure CascadeUpdate(const ATable, AFieldAndNewValue, AWhere: string);
    function RecordExits(const ATable, AFieldName, AWhere: string): Boolean;
    // function GetADSFieldComment(oDict: TAdsDictionary; strTableName, strFieldName: string): string;
    function GetADSFieldComment(strTableName, strFieldName: string): string;
    function SQLUpdateAllFieldsExcept(const ASource, ATarget, ASkipFields, AWhere: string): string;
    function SQLUpdateFromDatasetExcept(ASource: TDataSet; const ATargetTable, ASkipFields, AWhere: string): string;
    procedure RunSimpleQuery(const ASql: string);
    function SQLInsertAllFieldsExcept(const ASource, ATarget, ASkipFields, AWhere: string;
      bSelectInto: Boolean = False): string;
    procedure CopyFileRecord(ADataSet: TDataSet; ASkipFields: string);
    function GetIncNum(ATable, AField: string; numDel: Integer): Integer;
    procedure MakeEdtMode(DataSet: TDataSet);
    procedure SetDataScope(ADataSet: TDataSet; const AIndex: string; ARange: Variant);
    procedure ClearDataScope(ADataSet: TDataSet);
    procedure DatabaseVersion(var AMajorVersion, AMinorVersion: Integer);
    function SQLUpdateFieldsInclude(const ASource, ATarget, AIncludeFields, AWhere: string): string;
    function SQLInsertFieldsInclude(const ASource, ATarget, AIncludeFields, AWhere: string;
      bSelectInto: Boolean = False): string;
    procedure RedoLineNumbers(DataSet: TDataSet; const ALineField: string);
    procedure ApplyFieldDefaults(DataSet: TDataSet; const ATableName: string);

    destructor Destroy; override;
  end;

{$IFNDEF SANDBOX}
function DataHelpers: TDataHelpers;
{$ENDIF}
function VarIsLikeNull(const V: Variant): Boolean;

implementation

uses dmDataModule, Variants, AdsCnnct, StrUtils, Windows{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

{$IFNDEF SANDBOX}
var
  __DataHelpers: TDataHelpers;

function DataHelpers: TDataHelpers;
begin
  if not Assigned(__DataHelpers) then
  begin
    __DataHelpers := TDataHelpers.Create;
    __DataHelpers.FAdsServerVersion := -1;
  end;
  Result := __DataHelpers;
end;
{$ENDIF}

function VarIsLikeNull(const V: Variant): Boolean;
begin
  Result := DataHelpers.VarIsLikeNull(V);
end;

{ TDataHelpers }

procedure TDataHelpers.ApplyFieldDefaults(DataSet: TDataSet; const ATableName: string);
var
  AQuery: TAdsQuery;
  AValue: Variant;
  AFieldName, ADefValue: string;
begin
  if not Assigned(FDataDefQuery) then
  begin
    FDataDefQuery := dmData.CreateQueryObject(nil);
    FDataDefQuery.SQL.Text := 'Select Name, Field_Type, Field_Default_Value From System.Columns ' +
      'Where Parent=:TableName and Not Field_Default_Value Is Null;';
    FDataDefQuery.Prepare;
    if not dmData.tbIncNum.Active then
      dmData.tbIncNum.Open;
  end;

  AQuery := FDataDefQuery;
  if (not AQuery.Active) or (not SameText(AQuery.ParamByName('TableName').AsString, ATableName)) then
  begin
    AQuery.Close;
    AQuery.ParamByName('TableName').AsString := ATableName;
    AQuery.Open;
  end;
  while not AQuery.Eof do
  begin
    AFieldName := AQuery.FieldByName('Name').AsString;
    if (DataSet.FindField(AFieldName) <> nil) and DataSet.FieldByName(AFieldName).IsNull then
    begin
      ADefValue := AQuery.FieldByName('Field_Default_Value').AsString;
      MakeEdtMode(DataSet);
      if ADefValue = QuotedStr('') then
        DataSet.FieldByName(AFieldName).AsString := ''
      else
      begin
        if DataSet.FieldByName(AFieldName).DataType in [ftSmallint, ftInteger, ftWord, ftFloat, ftCurrency, ftBCD,
          ftFMTBcd, ftExtended, ftSingle, ftDate] then
          AValue := dmData.tbIncNum.AdsEvalNumericExpr(ADefValue)
        else if DataSet.FieldByName(AFieldName).DataType in [ftBoolean] then
          AValue := dmData.tbIncNum.AdsEvalLogicalExpr(ADefValue)
        else if (Copy(ADefValue, Length(ADefValue) - 1, 2) = '()') or (Copy(ADefValue, 1, 1) = '''') then
          // Expression or QuotedStr
          AValue := dmData.tbIncNum.AdsEvalStringExpr(ADefValue)
        else
          AValue := dmData.tbIncNum.AdsEvalStringExpr(QuotedStr(ADefValue));
        OutputDebugString(PChar('ApplyFieldDefaults: ' + AFieldName));
        DataSet.FieldByName(AFieldName).Value := AValue;
      end;
    end;
    AQuery.Next;
  end;
end;

procedure TDataHelpers.CascadeDelete(const ATable, AWhere: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := HelperQuery;

  AQuery.SQL.Text := Format('Delete From %s Where %s', [ATable, AWhere]);
  AQuery.ExecSQL;
end;

procedure TDataHelpers.CascadeUpdate(const ATable, AFieldAndNewValue, AWhere: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := HelperQuery;

  AQuery.SQL.Text := Format('Update %s Set %s Where %s', [ATable, AFieldAndNewValue, AWhere]);
  AQuery.ExecSQL;

end;

destructor TDataHelpers.Destroy;
begin
  if Assigned(FHelperQuery) then
    FHelperQuery.Free;

  //Adstable.pas crashes when calling AdsCloseSQLStatement on this Query from TisLib3.dll
  try
    if Assigned(FSimpleQuery) then
      FSimpleQuery.Free;
  except
  end;

  if Assigned(FDataDefQuery) then
    FDataDefQuery.Free;

  inherited Destroy;
end;

function TDataHelpers.GetADSFieldComment(strTableName, strFieldName: string): string;
var
  AQuery: TAdsQuery;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select [Comment] From System.Columns Where [Parent]=%s And [Name]=%s',
    [QuotedStr(strTableName), QuotedStr(strFieldName)]);
  AQuery.Open;
  Result := AQuery.Fields[0].AsString;
end;

function TDataHelpers.GetAdsServerVersion: Integer;
begin
  if FAdsServerVersion = -1 then
  begin
    HelperQuery.SQL.Text :=
      'Select Cast(SubString(Version,1,2) AS SQL_INTEGER) From (EXECUTE PROCEDURE sp_mgGetInstallInfo()) a';
    HelperQueryActive.Open;
    FAdsServerVersion := HelperQueryActive.Fields[0].AsInteger;
  end;
  Result := FAdsServerVersion;
end;

function TDataHelpers.GetHelperQuery: TAdsQuery;
begin
  if not Assigned(FHelperQuery) then
    FHelperQuery := dmData.CreateQueryObject(nil);

  FHelperQuery.Close;

  Result := FHelperQuery;
  //Result := dmData.CreateQueryObject(nil);
end;

function TDataHelpers.GetHelperQueryActive: TAdsQuery;
begin
  if not Assigned(FHelperQuery) then
    FHelperQuery := GetHelperQuery;

  Result := FHelperQuery;
end;

function TDataHelpers.RecordExits(const ATable, AFieldName, AWhere: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := HelperQuery;

  AQuery.Close;
  AQuery.SQL.Text := Format('Select %s From %s Where %s', [AFieldName, ATable, AWhere]);
  AQuery.Open;

  Result := (not AQuery.IsEmpty);
end;

procedure TDataHelpers.RedoLineNumbers(DataSet: TDataSet; const ALineField: string);
var
  ALine, ARecNo: Integer;
begin
  ALine := 1;
  DataSet.DisableControls;
  ARecNo := DataSet.RecNo;
  DataSet.First;
  try
    while not DataSet.Eof do
    begin
      DataSet.Edit;
      DataSet.FieldByName(ALineField).AsInteger := ALine;
      DataSet.Next;
      Inc(ALine);
    end;
  finally
    try
      DataSet.RecNo := ARecNo;
    except
    end;
    DataSet.EnableControls;
  end;
end;

{ function TDataHelpers.GetADSFieldComment(oDict: TAdsDictionary; strTableName: string; strFieldName: string): string;
  var
  usLength: UNSIGNED16;
  ulErrorCode: UNSIGNED32;
  aucProperty: array [0 .. ADS_DD_MAX_PROPERTY_LEN] of AceChar;
  begin
  aucProperty[0] := #0;
  usLength := ADS_DD_MAX_PROPERTY_LEN;
  try
  if not oDict.IsConnected then
  oDict.Connect;
  oDict.GetFieldProperty(strTableName, strFieldName, ADS_DD_COMMENT, @aucProperty, usLength);
  Result := string(aucProperty);
  except
  on E: EAdsDatabaseError do
  begin
  Ace.AdsGetLastError(@ulErrorCode, @aucProperty, @usLength);
  if ((ulErrorCode <> AE_PROPERTY_NOT_SET) and (ulErrorCode <> AE_SUCCESS)) then
  if (ulErrorCode = 5005) then
  Result := string(aucProperty)
  else
  raise ;
  end;
  end;
  oDict.Disconnect;
  end; }

function TDataHelpers.SQLUpdateAllFieldsExcept(const ASource, ATarget, ASkipFields, AWhere: string): string;
var
  AQuery: TAdsQuery;
  x: Integer;
  AFieldList, AField: string;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select * From %s', [ASource]);
  AQuery.Open;

  AFieldList := '';
  for x := 0 to Pred(AQuery.FieldCount) do
  begin
    AField := AQuery.Fields[x].FieldName;
    if Pos(';' + UpperCase(AField) + ';', ';' + UpperCase(ASkipFields) + ';') = 0 then
    begin
      AField := '[' + AField + ']';
      AFieldList := AFieldList + AField + '=s.' + AField + ', ';
    end;
  end;
  Delete(AFieldList, Length(AFieldList) - 1, 2);

  Result := Format('Update %s Set %s From %s t, %s s %s;', [ATarget, AFieldList, ATarget, ASource, AWhere]);
end;

function TDataHelpers.SQLUpdateFieldsInclude(const ASource, ATarget, AIncludeFields, AWhere: string): string;
var
  AQuery: TAdsQuery;
  x: Integer;
  AFieldList, AField: string;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select * From %s', [ASource]);
  AQuery.Open;

  AFieldList := '';
  for x := 0 to Pred(AQuery.FieldCount) do
  begin
    AField := AQuery.Fields[x].FieldName;
    if Pos(';' + UpperCase(AField) + ';', ';' + UpperCase(AIncludeFields) + ';') > 0 then
    begin
      AField := '[' + AField + ']';
      AFieldList := AFieldList + AField + '=s.' + AField + ', ';
    end;
  end;
  Delete(AFieldList, Length(AFieldList) - 1, 2);

  Result := Format('Update %s Set %s From %s t, %s s %s;', [ATarget, AFieldList, ATarget, ASource, AWhere]);
end;

function TDataHelpers.SQLUpdateFromDatasetExcept(ASource: TDataSet;
  const ATargetTable, ASkipFields, AWhere: string): string;
var
  AQuery: TAdsQuery;
  x: Integer;
  AFieldList, AField: string;
  AQuote: Boolean;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select * From %s %s', [ATargetTable, AWhere]);
  AQuery.Open;

  AFieldList := '';
  for x := 0 to Pred(AQuery.FieldCount) do
  begin
    AField := AQuery.Fields[x].FieldName;
    if Pos(';' + UpperCase(AField) + ';', ';' + UpperCase(ASkipFields) + ';') = 0 then
    begin
      if (ASource.FindField(AField) <> nil) and
        (VarCompareValue(ASource.FieldByName(AField).Value, AQuery.FieldByName(AField).Value) <> vrEqual) then
      begin
        AQuote := ASource.FieldByName(AField).DataType in [ftDate, ftString, ftMemo, ftFixedChar, ftWideString,
          ftFixedWideChar];
        if (ASource.FieldByName(AField).AsString = '') and (ASource.FieldByName(AField).DataType = ftDate) then
          AFieldList := AFieldList + Format('[%s]=Null, ', [AField])
        else
          AFieldList := AFieldList + Format('[%s]=%s, ',
            [AField, IfThen(AQuote, QuotedStr(ASource.FieldByName(AField).AsString),
            ASource.FieldByName(AField).AsString)]);
      end;
    end;
  end;
  if AFieldList <> '' then
  begin
    Delete(AFieldList, Length(AFieldList) - 1, 2);
    Result := Format('Update %s Set %s %s;', [ATargetTable, AFieldList, AWhere]);
  end
  else
    Result := '';
end;

function TDataHelpers.VarIsLikeNull(const V: Variant): Boolean;
begin
  Result := VarIsNull(V) or VarIsEmpty(V) or VarIsClear(V); // Unassigned);
end;

procedure TDataHelpers.RunSimpleQuery(const ASql: string);
begin
  if not Assigned(FSimpleQuery) then
    FSimpleQuery := dmData.CreateQueryObject(nil);
  FSimpleQuery.SQL.Text := ASql;
  FSimpleQuery.ExecSQL;

  // dmData.POSConnection.Execute(ASql); //using Connection.Execute dosen't use proper lock and CharType
end;

function TDataHelpers.SQLInsertAllFieldsExcept(const ASource, ATarget, ASkipFields, AWhere: string;
  bSelectInto: Boolean): string;
var
  AQuery: TAdsQuery;
  x: Integer;
  AFieldList, AField: string;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select * From %s', [ASource]);
  AQuery.Open;

  AFieldList := '';
  for x := 0 to Pred(AQuery.FieldCount) do
  begin
    AField := AQuery.Fields[x].FieldName;
    if Pos(';' + UpperCase(AField) + ';', ';' + UpperCase(ASkipFields) + ';') = 0 then
    begin
      AField := '[' + AField + ']';
      AFieldList := AFieldList + AField + ', ';
    end;
  end;
  Delete(AFieldList, Length(AFieldList) - 1, 2);

  if bSelectInto then
    Result := Format('Select %s Into %s From %s %s;', [AFieldList, ATarget, ASource, AWhere])
  else
    Result := Format('Insert Into %s (%s) Select %s From %s %s;', [ATarget, AFieldList, AFieldList, ASource, AWhere]);
end;

function TDataHelpers.SQLInsertFieldsInclude(const ASource, ATarget, AIncludeFields, AWhere: string;
  bSelectInto: Boolean): string;
var
  AQuery: TAdsQuery;
  x: Integer;
  AFieldList, AField: string;
begin
  AQuery := HelperQuery;
  AQuery.SQL.Text := Format('Select * From %s', [ASource]);
  AQuery.Open;

  AFieldList := '';
  for x := 0 to Pred(AQuery.FieldCount) do
  begin
    AField := AQuery.Fields[x].FieldName;
    if Pos(';' + UpperCase(AField) + ';', ';' + UpperCase(AIncludeFields) + ';') > 0 then
    begin
      AField := '[' + AField + ']';
      AFieldList := AFieldList + AField + ', ';
    end;
  end;
  Delete(AFieldList, Length(AFieldList) - 1, 2);

  if bSelectInto then
    Result := Format('Select %s Into %s From %s %s;', [AFieldList, ATarget, ASource, AWhere])
  else
    Result := Format('Insert Into %s (%s) Select %s From %s %s;', [ATarget, AFieldList, AFieldList, ASource, AWhere]);
end;

procedure TDataHelpers.CopyFileRecord(ADataSet: TDataSet; ASkipFields: string);
var
  i: Integer;
  FieldValueArray: array [0 .. 200] of Variant;
begin
  ADataSet.DisableControls;
  try
    { Store current record values }
    for i := 0 to (ADataSet.FieldCount - 1) do
      if Pos(';' + UpperCase(ADataSet.Fields[i].FieldName) + ';', ';' + UpperCase(ASkipFields) + ';') = 0 then
        FieldValueArray[i] := ADataSet.Fields[i].Value;

    ADataSet.Insert;

    { Save the values to the new record }
    for i := 0 to (ADataSet.FieldCount - 1) do
      if not VarIsNull(FieldValueArray[i]) then
        ADataSet.Fields[i].Value := FieldValueArray[i];
  finally
    ADataSet.EnableControls;
  end;
end;

function TDataHelpers.GetIncNum(ATable, AField: string; numDel: Integer): Integer;
begin
  Result := dmData.GetIncNum(ATable, AField, numDel);
end;

procedure TDataHelpers.MakeEdtMode(DataSet: TDataSet);
begin
  if not(DataSet.State in [dsInsert, dsEdit]) then
    DataSet.Edit;
end;

procedure TDataHelpers.SetDataScope(ADataSet: TDataSet; const AIndex: string; ARange: Variant);
begin
  with ADataSet as TAdsTable do
  begin
    DisableControls;
    IndexName := AIndex;
    SetRange([ARange], [ARange]);
    EnableControls;
  end;
end;

procedure TDataHelpers.ClearDataScope(ADataSet: TDataSet);
begin
  with ADataSet as TAdsTable do
    CancelRange;
end;

procedure TDataHelpers.DatabaseVersion(var AMajorVersion, AMinorVersion: Integer);
begin
  with HelperQuery do
  begin
    SQL.Text := 'Select Version_Major, Version_Minor From System.Dictionary;';
    Open;
    AMajorVersion := Fields[0].AsInteger;
    AMinorVersion := Fields[1].AsInteger;
    Close;
  end;
end;

{$IFNDEF SANDBOX}
initialization

finalization

if Assigned(__DataHelpers) then
  FreeAndNil(__DataHelpers);
{$ENDIF}

end.
