unit VendHelpersIUnit;

interface

uses Classes, DB, AdsData, AdsTable;

type
  TVendHelpersI = class
  private
    FtbVenMstr: TAdsTable;
    FtbVHist: TAdsTable;
    FtbVPoItem: TAdsTable;
    FtbVenItem: TAdsQuery;
    FtbVenItemAll: TAdsTable;
    FtbVenItemAllPrdLookup: TAdsTable;
    FtbVenItemAllVenLookup: TAdsTable;
    FtbVenItemDelList: TAdsQuery;
    FtbVenItmLkp: TAdsQuery;
    FVendorBilling: Integer;
    FVenItmRltnTable: string;

    function GettbVenMstr: TAdsTable;
    function GettbVHist: TAdsTable;
    procedure RcrdHist(AVendID, ADelID: string; APayID: Integer; ABill, APaid, AUnApplied: Currency; ADate: TDate);
    function GettbVenItem: TAdsQuery;
    function GettbVenItemAll: TAdsTable;
    function GetVendorBilling: Boolean;
    function GettbVenItemDelList: TAdsQuery;
    function GettbVenPoItem: TAdsTable;
    function GettbVenItmLkp: TAdsQuery;
    function GetVenItmRltnTable: string;
  protected
    property VenItmRltnTable: string read GetVenItmRltnTable;
    procedure AddNewVend(ADataSet: TDataSet);
    function FindInvoiceByDelNO(const ADelNO: Integer; var ADelID: string): Boolean;
    function FindInvoiceByID(const ADelID: string; var AVendID: string): Boolean;
    function FindPoByPoNum(const APoNum: Integer; var APOID: string): Boolean;
    function FindPOByID(const APOID: string; var AVendID: string): Boolean;
    function FindPaymentByID(const APayID: Integer): Boolean;
  public
    property tbVenMstr: TAdsTable read GettbVenMstr;
    property tbVHist: TAdsTable read GettbVHist;
    property tbVenItem: TAdsQuery read GettbVenItem;
    property tbVenItemAll: TAdsTable read GettbVenItemAll;
    property tbVenPoItem: TAdsTable read GettbVenPoItem;
    property tbVenItmLkp: TAdsQuery read GettbVenItmLkp;
    property tbVenItemDelList: TAdsQuery read GettbVenItemDelList;
    property VendorBilling: Boolean read GetVendorBilling;

    function FindVendorByID(ADataSet: TDataSet; const AVendID: string): Boolean;
    procedure UpdateVendBalanceEtcPrepare(AQuery: TAdsQuery);
    procedure UpdateVendBalanceEtc(const AVendID: string);
    procedure BillInvoice(AVendID, ADelID: string; ABill: Currency; ADate: TDate);
    function VendLookupRecord(const AVendID: string): TAdsQuery;
    function AmountBeforeDiscount(Amount: Currency; Disc: Currency): Currency;
    function AmountAfterDiscount(Amount: Currency; Disc: Currency): Currency;
    function LastVendorForItem(const AUpcCode: string): string;
    function VendEmailAddress(const AVendID: string): string;
    function VendFieldInfo(const AVendID, AFieldName: string): string;
    procedure DoDeleteVenItmCode(const AVendID, AItemCode, AVenItmC: string);
    procedure DoDeleteVendor(const AVendID: string);
    procedure DoUpcVenCodeLookup(ADataSet: TDataSet; AEditField: TField; var AInternalChange: Boolean);

    procedure GenerateVendorCode(AVendorName: string; var AVendCode: string);
    function AddNewVendor(AName: string; ANoLstVend: Boolean = False): Boolean;
    constructor Create;
    destructor Destroy;
  end;

{$IFNDEF SANDBOX}

function VendHelpersI: TVendHelpersI;
{$ENDIF}

implementation

uses SysUtils, dmDataModule, Tis_CreatDBObjects, DataHelpersUnit, GeneralHelpersUnit, Variants,
  System.UITypes, ProjHelpersIUnit{$IFNDEF NONGUI}, VendHelpersUnit {$ENDIF}{$IFDEF SANDBOX},
  ServerMethodsUnit1{$ENDIF};

{$IFNDEF NONGUI}

type
  TVendHelpersX = class(TVendHelpers)
  end;
{$ENDIF}

const
  QbStatusUpd = 'Update Delvry Set QbUpdated=CURRENT_TIMESTAMP(), QbPaid=%s, QbBilled=%f Where DelID=%s; ';
  QbBalanceUpd = 'Update Venmstr Set QbBalUpdated=CURRENT_TIMESTAMP(), QbBalance=%f Where VendID=%s; ';

{$IFNDEF SANDBOX}

var
  __VendHelpersI: TVendHelpersI;

function VendHelpersI: TVendHelpersI;
begin
  if not Assigned(__VendHelpersI) then
    __VendHelpersI := TVendHelpersI.Create;
  Result := __VendHelpersI;
end;
{$ENDIF}
{$I PasRight.inc}
{ TVendHelpersI }

procedure TVendHelpersI.AddNewVend(ADataSet: TDataSet);
var
  AQuery: TAdsQuery;
  I: Integer;
begin
  ADataSet.Insert;
  ADataSet.FieldByName('VendID').AsString := GenHelpers.GuidIdString;

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * From VenMstr Where Code=''MASTER'';';
  AQuery.Open;
  if not AQuery.IsEmpty then
    for I := 0 to AQuery.FieldCount - 1 do
      if (Pos(';' + UpperCase(AQuery.Fields[I].FieldName) + ';',
        ';VENDID;NAME;ADDRESS;ADDRESS2;CITY;STATE;ZIP;TEL;TEL2;FAX;CELL;CONTACT;EMAIL;WEB;BALANCE;LSTPAYA;LSTPAYD;LSTBILA;LSTBILD;AMTUNAP;CODE;DTSTART;')
        = 0) and (ADataSet.FindField(AQuery.Fields[I].FieldName) <> nil) then
        ADataSet.FieldByName(AQuery.Fields[I].FieldName).Value := AQuery.Fields[I].Value;
end;

function TVendHelpersI.AddNewVendor(AName: string; ANoLstVend: Boolean): Boolean;
var
  ADataSet: TDataSet;
begin
  ADataSet := tbVenMstr;
  if not ADataSet.Active then
    ADataSet.Open;

  AddNewVend(ADataSet);
  ADataSet.FieldByName('Name').AsString := AName;
  ADataSet.FieldByName('NoLstVend').AsBoolean := ANoLstVend;
  ADataSet.Post;

  Result := True;
end;

function TVendHelpersI.AmountAfterDiscount(Amount, Disc: Currency): Currency;
begin
  if (Amount <> 0) and (Disc <> 0) then
    Result := Amount * (1 - (Disc / 100))
  else
    Result := Amount;
end;

function TVendHelpersI.AmountBeforeDiscount(Amount, Disc: Currency): Currency;
begin
  if (Amount <> 0) and (Disc <> 0) then
    Result := Amount / (1 - (Disc / 100))
  else
    Result := Amount;
end;

procedure TVendHelpersI.BillInvoice(AVendID, ADelID: string; ABill: Currency; ADate: TDate);
begin
  if not VendorBilling then
    Exit;
  RcrdHist(AVendID, ADelID, 0, ABill, 0, 0, ADate);
end;

constructor TVendHelpersI.Create;
begin
  FVendorBilling := -1;
end;

destructor TVendHelpersI.Destroy;
begin
  if Assigned(FtbVenMstr) then
    FreeAndNil(FtbVenMstr);

  if Assigned(FtbVHist) then
    FreeAndNil(FtbVHist);

  if Assigned(FtbVPoItem) then
    FreeAndNil(FtbVPoItem);

  if Assigned(FtbVenItem) then
    FreeAndNil(FtbVenItem);

  if Assigned(FtbVenItemAll) then
    FreeAndNil(FtbVenItemAll);

  if Assigned(FtbVenItemAllPrdLookup) then
    FreeAndNil(FtbVenItemAllPrdLookup);

  if Assigned(FtbVenItemAllVenLookup) then
    FreeAndNil(FtbVenItemAllVenLookup);

  if Assigned(FtbVenItemDelList) then
    FreeAndNil(FtbVenItemDelList);

  if Assigned(FtbVenItmLkp) then
    FreeAndNil(FtbVenItmLkp);
end;

procedure TVendHelpersI.DoDeleteVendor(const AVendID: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;

  AQuery.SQL.Add('Delete From DelItm Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From Delvry Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VenItm Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VHist Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VCashPay Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VendPoDt Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VendPoHd Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VPmtDist Where VendID=:VendID;');
  AQuery.SQL.Add('Delete From VenMstr Where VendID=:VendID;');

  AQuery.Prepare;
  AQuery.ParamByName('VendID').AsString := AVendID;

  AQuery.AdsConnection.BeginTransaction;
  try
    AQuery.ExecSQL;
  except
    on E: Exception do
    begin
      AQuery.AdsConnection.Rollback;
      raise;
    end;
  end;
  AQuery.AdsConnection.Commit;
end;

procedure TVendHelpersI.DoDeleteVenItmCode(const AVendID, AItemCode, AVenItmC: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Delete From VenItm Where VendID=:VendID And UpcCode=:ItemCode And VenItmC=:VenItmC;');
  AQuery.Prepare;
  AQuery.ParamByName('VendID').AsString := AVendID;
  AQuery.ParamByName('ItemCode').AsString := AItemCode;
  AQuery.ParamByName('VenItmC').AsString := AVenItmC;
  AQuery.ExecSQL;
end;

procedure TVendHelpersI.DoUpcVenCodeLookup(ADataSet: TDataSet; AEditField: TField; var AInternalChange: Boolean);
var
  AQuery: TAdsQuery;
  sUpc, sVenItm: string;
  ACodeField, AVCodeField: string;
  sSQL_UPC, sSQL_VEN: string;
  bPromptUser, bDone, bUpcInList, bVenInList: Boolean;
  AQueryCount: Integer;
  procedure PrepareSQL;
  begin
    AQuery.SQL.Clear;
    AQuery.SQL.Add('Try Drop Table #' + VenItmRltnTable + '; Catch All End;');
    AQuery.SQL.Add('Select v.VendID, v.UpcCode, v.VenItmC, p.[Desc] Descrip, Cast(v.UPC AS SQL_INTEGER) UPC,');
    AQuery.SQL.Add('v.Cost, 0 SelectAction');
    AQuery.SQL.Add('Into #' + VenItmRltnTable + ' From Venitm v Left Outer Join Prdmstr p ON v.UpcCode=Upper(p.Code)');
    AQuery.SQL.Add(Format('Where VendID=%s', [QuotedStr(ADataSet.FieldByName('VendID').AsString)]));

    sSQL_UPC := Format('UpcCode=%s', [QuotedStr(sUpc)]);
    sSQL_VEN := Format('VenItmC=%s', [QuotedStr(sVenItm)]);
    if (sUpc <> '') and (sVenItm <> '') then
      AQuery.SQL.Add(Format(' And (%s or %s)', [sSQL_UPC, sSQL_VEN]))
    else if (sUpc <> '') and (sVenItm = '') then
      AQuery.SQL.Add(Format(' And %s', [sSQL_UPC]))
    else // if (sUpc = '') and (sVenItm <> '') then
      AQuery.SQL.Add(Format(' And %s', [sSQL_VEN]));

    AQuery.SQL.Add('; Select * From #' + VenItmRltnTable + ';');
    AQuery.Open;
  end;
  procedure DoAddCurrentEntries(const ASelectAction: Integer);
  begin
    AQuery.Insert;
    AQuery.FieldByName('VendID').AsString := ADataSet.FieldByName('VendID').AsString;
    AQuery.FieldByName('UpcCode').AsString := sUpc;
    AQuery.FieldByName('VenItmC').AsString := sVenItm;
    AQuery.FieldByName('Descrip').AsString := ADataSet.FieldByName('Descrip').AsString;
    AQuery.FieldByName('UPC').AsInteger := ADataSet.FieldByName('UPCC').AsInteger;
    AQuery.FieldByName('Cost').AsCurrency := ADataSet.FieldByName('OrigCst').AsCurrency;
    AQuery.FieldByName('SelectAction').AsInteger := ASelectAction;
    AQuery.Post;
  end;
  procedure AddToVenItem(ANoClear: Boolean = False);
  begin
    if sVenItm = '' then
    begin
      sVenItm := sUpc; // store temporary code
      ADataSet.FieldByName('VenItmCde').AsString := sVenItm;
    end;

    if not ANoClear then
      AQuery.SQL.Clear;
    AQuery.SQL.Add('Insert Into Venitm (VendID, UpcCode, VenItmC, UPC, Cost)');
    AQuery.SQL.Add('Values(:VendID, :UpcCode, :VenItm, :UPC, :Cost)');
    AQuery.Prepare;
    AQuery.ParamByName('VendID').AsString := ADataSet.FieldByName('VendID').AsString;
    AQuery.ParamByName('UpcCode').AsString := sUpc;
    AQuery.ParamByName('VenItm').AsString := sVenItm;
    AQuery.ParamByName('UPC').AsInteger := ADataSet.FieldByName('UPCC').AsInteger;
    AQuery.ParamByName('Cost').AsCurrency := ADataSet.FieldByName('OrigCst').AsCurrency;
    AQuery.ExecSQL;
  end;
  procedure SetSelectAction(const AValue: Integer; AFilter: string = '');
  begin
    AQuery.Close;
    AQuery.SQL.Add(Format('Update #' + VenItmRltnTable + ' Set SelectAction=%d', [AValue]));
    if AFilter <> '' then
      AQuery.SQL.Add(Format('Where %s', [AFilter]));
    AQuery.SQL.Add('; Select * From #' + VenItmRltnTable + ';');
    AQuery.Open;
  end;

begin
  {$IFDEF SANDBOX}
  //Exit;
  {$ENDIF}
  if ADataSet.FieldByName('VendID').AsString = '' then
    Exit;

  bPromptUser := False;
  // bDone := False;
  if ADataSet.FindField('UpcCode') <> nil then
    ACodeField := 'UpcCode'
  else if ADataSet.FindField('Code') <> nil then
    ACodeField := 'Code'
  else
    raise EHandleInApp.Create('"DoUpcVenCodeLookup", Code field name not assigned.');

  if ADataSet.FindField('VenItmCde') <> nil then
    AVCodeField := 'VenItmCde'
  else if ADataSet.FindField('VenItem') <> nil then
    AVCodeField := 'VenItem'
  else
    raise EHandleInApp.Create('"DoUpcVenCodeLookup", Vendor Code field name not assigned.');

  sUpc := ADataSet.FieldByName(ACodeField).AsString;
  sVenItm := ADataSet.FieldByName(AVCodeField).AsString;
  AQuery := VendHelpersI.tbVenItmLkp;
  PrepareSQL;
  AQueryCount := AQuery.RecordCount;

  if (AQueryCount = 1) and (sUpc <> '') and (not SameText(sUpc, sVenItm)) and
    SameText(sUpc, AQuery.FieldByName('VenItmC').AsString) then
  begin
    AQuery.SQL.Clear;
    AQuery.SQL.Add('Delete From VenItm Where VendID=:VendID And VenItmC=:UpcCode;');
    AddToVenItem(True);
  end
  else if (sUpc <> '') and (sVenItm = '') then
  begin
    // bDone := AQueryCount <= 1;
    if AQueryCount = 0 then
      AddToVenItem
    else if AQueryCount = 1 then
      ADataSet.FieldByName(AVCodeField).AsString := AQuery.FieldByName('VenItmC').AsString
    else
      bPromptUser := True;
  end
  else if (sVenItm <> '') and (sUpc = '') then
  begin
    // bDone := AQueryCount <= 1;
    if AQueryCount = 1 then
    begin
      AInternalChange := False; // reset so we check if UpcCode exists and fill defaults
      ADataSet.FieldByName(ACodeField).AsString := AQuery.FieldByName('UpcCode').AsString;
    end
    else if AQueryCount > 1 then
    begin
      bPromptUser := True;
      SetSelectAction(2);
    end;
  end
  else // (sVenItm <> '') and (sUpc <> '')
  begin
    bVenInList := AQuery.Locate('VenItmC', sVenItm, [loCaseInsensitive]);
    bUpcInList := AQuery.Locate('UpcCode', sUpc, [loCaseInsensitive]);

    bDone := (AQueryCount = 0) or AQuery.Locate('UpcCode;VenItmC', VarArrayOf([sUpc, sVenItm]), [loCaseInsensitive]);
    bPromptUser := not bDone;
    if AQueryCount = 0 then
      AddToVenItem;
    if bPromptUser then
    begin
      if bVenInList then
        DoAddCurrentEntries(5)
      else if (not bVenInList) and bUpcInList then
      begin
        DoAddCurrentEntries(3);
        DoAddCurrentEntries(1);
      end;
      // Ignoring for now same UpcCode found multiple times, but in that case maybe no Replace option
    end;
  end;

{$IFNDEF NONGUI}
  TVendHelpersX(VendHelpers).UpcVenCodeLookupPrompt(sUpc, sVenItm, ACodeField, AVCodeField, bPromptUser,
    AInternalChange, AQuery, ADataSet, AEditField);
{$ENDIF}
end;

function TVendHelpersI.FindInvoiceByDelNO(const ADelNO: Integer; var ADelID: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select DelNO, DelID From Delvry Where DelNO=%d', [ADelNO]);
  AQuery.Open;

  Result := AQuery.Fields[0].AsInteger <> 0;
  if Result then
    ADelID := AQuery.Fields[1].AsString;
end;

function TVendHelpersI.FindInvoiceByID(const ADelID: string; var AVendID: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select DelID, VendID From Delvry Where DelID=%s', [QuotedStr(ADelID)]);
  AQuery.Open;

  Result := AQuery.Fields[0].AsString <> '';
  if Result then
    AVendID := AQuery.Fields[1].AsString;
end;

function TVendHelpersI.FindPaymentByID(const APayID: Integer): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select PayID From VHist Where PayID=%d', [APayID]);
  AQuery.Open;

  Result := AQuery.Fields[0].AsInteger > 0;
end;

function TVendHelpersI.FindPOByID(const APOID: string; var AVendID: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select POID, VendID From VendPoHd Where POID=%s', [QuotedStr(APOID)]);
  AQuery.Open;

  Result := AQuery.Fields[0].AsString <> '';
  if Result then
    AVendID := AQuery.Fields[1].AsString;
end;

function TVendHelpersI.FindPoByPoNum(const APoNum: Integer; var APOID: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select PoNum, POID From VendPoHd Where PoNum=%d', [APoNum]);
  AQuery.Open;

  Result := AQuery.Fields[0].AsInteger <> 0;
  if Result then
    APOID := AQuery.Fields[1].AsString;
end;

function TVendHelpersI.FindVendorByID(ADataSet: TDataSet; const AVendID: string): Boolean;
begin
  if not Assigned(ADataSet) then
    ADataSet := tbVenMstr;

  Result := ADataSet.Locate('VendID', AVendID, []);
end;

procedure TVendHelpersI.GenerateVendorCode(AVendorName: string; var AVendCode: string);
var
  AQuery: TAdsQuery;
  I: Integer;
  AFound: Boolean;
begin
  AVendorName := UpperCase(AVendorName);
  AVendCode := '';
  for I := 1 to Length(AVendorName) do
    if CharInSet(AVendorName[I], ['0' .. '9', 'A' .. 'Z']) then
    begin
      AVendCode := AVendCode + AVendorName[I];
      if Length(AVendCode) = 5 then
        Break;
    end;

  AFound := False;
  I := -1;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select Count(*) From VenMstr Where Upper(Code)=:VendCode;';
  AQuery.Prepare;
  while (not AFound) and (I < 99999) do
  begin
    Inc(I);
    if (I > 0) and (I <= 9) then
      AVendCode := Copy(AVendCode, 1, 4) + Trim(IntToStr(I))
    else if (I >= 10) and (I <= 99) then
      AVendCode := Copy(AVendCode, 1, 3) + Trim(IntToStr(I))
    else if (I >= 100) and (I <= 999) then
      AVendCode := Copy(AVendCode, 1, 2) + Trim(IntToStr(I))
    else if (I >= 1000) and (I <= 9999) then
      AVendCode := Copy(AVendCode, 1, 1) + Trim(IntToStr(I))
    else if (I >= 10000) and (I <= 99999) then
      AVendCode := Trim(IntToStr(I));

    AQuery.Close;
    AQuery.ParamByName('VendCode').AsString := AVendCode;
    AQuery.Open;
    AFound := AQuery.Fields[0].AsInteger = 0;
  end;
end;

function TVendHelpersI.GettbVenItem: TAdsQuery;
begin
  if not Assigned(FtbVenItem) then
  begin
    FtbVenItem := dmData.CreateQueryObject(nil);
    FtbVenItem.SQL.Add('Select v.VendID, v.UpcCode, v.VenItmC, p.[Desc], v.UPC, p.Size, p.Cat, p.Brand, ');
    FtbVenItem.SQL.Add('p.Price, v.Cost, p.CstLast, p.UPC PrdUPC, p.Cost PrdCost, p.CstLastC, p.NonInven');
    FtbVenItem.SQL.Add('From VenItm v Left Outer Join Prdmstr p On Upper(v.UpcCode)=Upper(p.Code)');
    FtbVenItem.SQL.Add('Where VendID=:VendID');
    FtbVenItem.SQL.Add('Order By v.VendID, v.VenItmC');
  end;
  Result := FtbVenItem;
end;

function TVendHelpersI.GettbVenItemAll: TAdsTable;
begin
  if not Assigned(FtbVenItemAll) then
  begin
    FtbVenItemAll := dmData.CreateTableObject(nil);
    FtbVenItemAll.TableName := 'VenItm';
    FtbVenItemAll.IndexName := 'VenItmC';
    FtbVenItemAllPrdLookup := dmData.CreateTableObject(nil);
    FtbVenItemAllPrdLookup.TableName := 'Prdmstr';
    FtbVenItemAllVenLookup := dmData.CreateTableObject(nil);
    FtbVenItemAllVenLookup.TableName := 'VenMstr';
    LoadLookups(FtbVenItemAll, FtbVenItemAllPrdLookup, FtbVenItemAllVenLookup);
    FtbVenItemAll.Open;
  end;
  Result := FtbVenItemAll;
end;

function TVendHelpersI.GettbVenItemDelList: TAdsQuery;
begin
  if not Assigned(FtbVenItemDelList) then
  begin
    FtbVenItemDelList := dmData.CreateQueryObject(nil);
    FtbVenItemDelList.SQL.Add('Select d.*, h.*, v.*, p.*,');
    FtbVenItemDelList.SQL.Add('IIF(IfNull(d.Cost,0)=0,0,((p.Price-(d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC)))/');
    FtbVenItemDelList.SQL.Add('(d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC)))*100) Prd_PriceUp,');
    FtbVenItemDelList.SQL.Add('IIF(IfNull(d.Cost,0)=0,0,IIF(IfNull(p.Price,0)=0,0,');
    FtbVenItemDelList.SQL.Add('(1-((d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC))/p.Price))*100)) Prd_PriceGin,');
    FtbVenItemDelList.SQL.Add('IIF(IfNull(d.Cost,0)=0,0,((p.Sale-(d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC)))/');
    FtbVenItemDelList.SQL.Add('(d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC)))*100) Prd_SaleUp,');
    FtbVenItemDelList.SQL.Add('IIF(IfNull(d.Cost,0)=0,0,IIF(IfNull(p.Sale,0)=0,0,');
    FtbVenItemDelList.SQL.Add('(1-((d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC))/p.Sale))*100)) Prd_SaleGin,');
    FtbVenItemDelList.SQL.Add('IIF(IfNull(d.Cost,0)=0,0,d.Cost/IIF(IfNull(d.UPCC,0)=0,1,d.UPCC)) Prd_UnitCost');
    FtbVenItemDelList.SQL.Add('From Delitm d Inner Join Delvry h On h.DelID=d.DelID');
    FtbVenItemDelList.SQL.Add('Inner Join VenMstr v ON v.VendID=h.VendID');
    FtbVenItemDelList.SQL.Add('Inner Join Prdmstr p On p.Code=d.upcCode');
    FtbVenItemDelList.SQL.Add('Where d.UpcCode=:UpcCode');
    if ProgPref('HideCostQuickList') then
      FtbVenItemDelList.SQL.Add('And Not IfNull(h.Quick, False)');
    FtbVenItemDelList.SQL.Add('And (:NoHideZero or d.AmntDel<>0)');
    FtbVenItemDelList.SQL.Add('And h.Date >= :DateFrom');
    FtbVenItemDelList.SQL.Add('And h.Date <= :DateTo');
    // FtbVenItemDelList.SQL.Add('And VendID=:VendID');
    FtbVenItemDelList.SQL.Add('Order By h.Date Desc;');
  end;
  Result := FtbVenItemDelList;
end;

function TVendHelpersI.GettbVenItmLkp: TAdsQuery;
begin
  if not Assigned(FtbVenItmLkp) then
  begin
    FtbVenItmLkp := dmData.CreateQueryObject(nil);
    FtbVenItmLkp.RequestLive := True;
  end;
  Result := FtbVenItmLkp;
end;

function TVendHelpersI.GettbVenMstr: TAdsTable;
begin
  if not Assigned(FtbVenMstr) then
  begin
    FtbVenMstr := dmData.CreateTableObject(nil);
    FtbVenMstr.TableName := 'VenMstr';
    FormatTelFields(tbVenMstr);
    FtbVenMstr.Open;
    TNumericField(FtbVenMstr.FieldByName('Balance')).DisplayFormat := '$,0.00';
    TNumericField(FtbVenMstr.FieldByName('LstPayA')).DisplayFormat := '$,0.00';
    TNumericField(FtbVenMstr.FieldByName('LstBilA')).DisplayFormat := '$,0.00';
    TNumericField(FtbVenMstr.FieldByName('AmtUnAp')).DisplayFormat := '$,0.00';
  end;
  Result := FtbVenMstr;
end;

function TVendHelpersI.GettbVenPoItem: TAdsTable;
begin
  if not Assigned(FtbVPoItem) then
  begin
    FtbVPoItem := dmData.CreateTableObject(nil);
    FtbVPoItem.TableName := 'PoItem';
    FtbVPoItem.Open;
  end;
  Result := FtbVPoItem;
end;

function TVendHelpersI.GettbVHist: TAdsTable;
begin
  if not Assigned(FtbVHist) then
  begin
    FtbVHist := dmData.CreateTableObject(nil);
    FtbVHist.TableName := 'VHist';
    FtbVHist.Open;
  end;
  Result := FtbVHist;
end;

function TVendHelpersI.GetVendorBilling: Boolean;
begin
  if FVendorBilling = -1 then
    FVendorBilling := Integer(GenHelpers.IsModuleRegistered('Vendor Register List'));

  Result := FVendorBilling = 1;
end;

function TVendHelpersI.GetVenItmRltnTable: string;
begin
  if FVenItmRltnTable = '' then
    FVenItmRltnTable := 'VenItmRltn'; // + GenHelpers.GuidIdString;
  Result := FVenItmRltnTable;
end;

function TVendHelpersI.LastVendorForItem(const AUpcCode: string): string;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Select VendID From Delvry Where Delno=(');
  AQuery.SQL.Add('Select Max(h2.DelNo) From DelItm d2, Delvry h2');
  AQuery.SQL.Add(Format('Where d2.UpcCode=''%s'' And (d2.AmntDel>0 Or d2.Cost>0) And h2.DelID=d2.DelID)', [AUpcCode]));
  AQuery.Open;
  Result := AQuery.Fields[0].AsString;
end;

procedure TVendHelpersI.RcrdHist(AVendID, ADelID: string; APayID: Integer; ABill, APaid, AUnApplied: Currency;
  ADate: TDate);
var
  ADataSet: TDataSet;
  AFoundRecord: Boolean;
begin
  ADataSet := tbVHist;
  AFoundRecord := False;

  if (ADelID = '') and (APayID = 0) then
    raise Exception.Create('Error 1 in RcrdHist: Can''t record history with no record ID.')
  else if ADelID <> '' then
    AFoundRecord := ADataSet.Locate('DelID', ADelID, [])
  else if APayID > 0 then
    AFoundRecord := ADataSet.Locate('PayID', APayID, []);

  if AFoundRecord and (AVendID <> ADataSet.FieldByName('VendID').AsString) then
    raise Exception.CreateFmt('Error 2 in RcrdHist: VendID mismatch, Request: %s, Record: %s.',
      [AVendID, ADataSet.FieldByName('VendID').AsString]);

  if not AFoundRecord then
  begin
    ADataSet.Insert;
    ADataSet.FieldByName('VendID').AsString := AVendID;
    if ADelID <> '' then
      ADataSet.FieldByName('DelID').AsString := ADelID;
    if APayID > 0 then
      ADataSet.FieldByName('PayID').AsInteger := APayID;
    ADataSet.FieldByName('Date').AsDateTime := ADate;
    ADataSet.FieldByName('Time').AsString := FormatDateTime('hh:mm:ss', Now);
    if ADelID <> '' then
      ADataSet.FieldByName('TType').AsString := 'O'
    else if APayID > 0 then
      ADataSet.FieldByName('TType').AsString := 'P';
    ADataSet.FieldByName('ID').AsString := GenHelpers.GuidIdString;
  end;

  DataHelpers.MakeEdtMode(ADataSet);

  ADataSet.FieldByName('Amount').AsCurrency := ABill;
  ADataSet.FieldByName('Paid').AsCurrency := APaid;
  ADataSet.FieldByName('Unapplied').AsCurrency := AUnApplied;
  ADataSet.Post;

  UpdateVendBalanceEtc(ADataSet.FieldByName('VendID').AsString);
end;

procedure TVendHelpersI.UpdateVendBalanceEtc(const AVendID: string);
var
  AQuery: TAdsQuery;
begin
  if not VendorBilling then
    Exit;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;

  UpdateVendBalanceEtcPrepare(AQuery);

  AQuery.SQL.Add('Delete From #TmpUpdVend;');
  AQuery.SQL.Add
    ('Insert Into #TmpUpdVend (Hist, UnAppPay) Select Sum(IfNull(Amount,0) - IfNull(Paid,0)), Sum(IfNull(Unapplied,0)) From Vhist Where VendID=:VendID;');
  AQuery.SQL.Add
    ('Update #TmpUpdVend Set LstBil=Amount, BilD=Date From VHist b Where b.VendID=:VendID and b.DelID In (Select Max(DelID) From VHist Where VendID=:VendID);');
  AQuery.SQL.Add
    ('Update #TmpUpdVend Set LstPay=Paid, PayD=Date From VHist b Where b.VendID=:VendID and b.PayID In (Select Max(PayID) From VHist Where VendID=:VendID);');
  AQuery.SQL.Add
    ('Update VenMstr Set Balance=Hist,LstBilA=LstBil, LstBilD=BilD, LstPayA=LstPay, LstPayD=PayD, AmtUnAp=UnAppPay From #TmpUpdVend Where VendID=:VendID;');
  AQuery.Prepare;
  AQuery.ParamByName('VendID').AsString := AVendID;
  AQuery.ExecSQL;
end;

procedure TVendHelpersI.UpdateVendBalanceEtcPrepare(AQuery: TAdsQuery);
begin
  if not AQuery.AdsConnection.TransactionActive then
  begin
    AQuery.SQL.Add('Try Select * From #TmpUpdVend; Catch ADS_SCRIPT_EXCEPTION If __errcode=7112 Then');
    AQuery.SQL.Add
      ('Create Table #TmpUpdVend (Hist Money, LstBil Money, LstPay Money, BilD Date, PayD Date, UnAppPay Money);');
    AQuery.SQL.Add('Else Raise; End IF; End Try;');
  end;
end;

function TVendHelpersI.VendEmailAddress(const AVendID: string): string;
begin
  Result := VendFieldInfo(AVendID, 'EMail');
end;

function TVendHelpersI.VendFieldInfo(const AVendID, AFieldName: string): string;
var
  AQuery: TAdsQuery;
begin
  Result := '';
  AQuery := VendLookupRecord(AVendID);
  if not AQuery.IsEmpty then
  begin
    if AQuery.FindField(AFieldName) <> nil then
      Result := AQuery.FieldByName(AFieldName).AsString
    else
      raise EHandleInApp.CreateFmt('VendFieldInfo Field %s not found.', [AFieldName]);
  end;
end;

function TVendHelpersI.VendLookupRecord(const AVendID: string): TAdsQuery;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select * From VenMstr Where VendID=%s', [QuotedStr(AVendID)]);
  AQuery.Open;
  Result := AQuery;
end;

{$IFNDEF SANDBOX}

initialization

finalization

if Assigned(__VendHelpersI) then
  FreeAndNil(__VendHelpersI);
{$ENDIF}

end.
