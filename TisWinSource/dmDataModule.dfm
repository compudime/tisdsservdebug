object dmData: TdmData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 209
  Width = 363
  object POSConnection: TAdsConnection
    AliasName = 'TisWin3DD'
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    AfterConnect = POSConnectionAfterConnect
    BeforeConnect = POSConnectionBeforeConnect
    Left = 24
    Top = 8
  end
  object LocalConnection: TAdsConnection
    ConnectPath = 'C:\'
    AdsServerTypes = [stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    BeforeConnect = LocalConnectionBeforeConnect
    Left = 24
    Top = 64
  end
  object tbIncNum: TAdsTable
    StoreActive = False
    AdsConnection = POSConnection
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    TableName = 'Incnum2'
    Left = 312
    Top = 8
  end
  object tbWorkGrps: TAdsTable
    AdsConnection = POSConnection
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    StoreDefs = True
    TableName = 'Workgrps'
    Left = 256
    Top = 64
  end
  object tbPassword: TAdsTable
    AdsConnection = POSConnection
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    StoreDefs = True
    TableName = 'Password'
    Left = 256
    Top = 8
  end
  object tbWorkRigt: TAdsTable
    AdsConnection = POSConnection
    AdsTableOptions.AdsLockType = Compatible
    AdsTableOptions.AdsCharType = OEM
    StoreDefs = True
    TableName = 'WorkRigt'
    Left = 256
    Top = 120
  end
  object ScheduleConnection: TAdsConnection
    AliasName = 'TisWin3DD'
    AdsServerTypes = [stADS_REMOTE, stADS_LOCAL]
    LoginPrompt = False
    StoreConnected = False
    AfterConnect = POSConnectionAfterConnect
    BeforeConnect = POSConnectionBeforeConnect
    Left = 160
    Top = 10
  end
end
