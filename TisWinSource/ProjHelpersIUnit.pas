unit ProjHelpersIUnit;

interface

uses Windows, Messages, SysUtils, Classes, DB, AdsData, AdsTable, AdsCnnct, AdsDictionary, System.Generics.Collections,
  System.JSON, ScanResolveUnit;

type
  EHandleInApp = class(Exception)
  end;

  TProjectHelpersI = class
  private
    FScanResolve: TScanResolve;
    FTmpClcAlPath: string;

    procedure InitTempPayList;
    function GetTmpClcAlPath: string;
    function GetScanResolve: TScanResolve;
  public
    property TmpClcAlPath: string read GetTmpClcAlPath;
    property ScanResolve: TScanResolve read GetScanResolve;

    procedure FillProgChangeList;
    function ZipCodeInfo(const AZipCode: string; var ACity, AState: string): Boolean;
    procedure Prepare_TempDelItems1_Table;
    function PayListDataSet: TDataSet;
    function GridFieldIsVisible(const AGridForm, AGridName, AFieldName: string): Boolean;
    procedure AddAttachDocs(const AOrigin, AOriginID, AFileName, AFileDescription: string);

    constructor Create;
    destructor Destroy; override;
  end;


{$IFNDEF SANDBOX}
function ProjectHelpersI: TProjectHelpersI;
{$ENDIF}
procedure FillProgPrefList;

implementation

uses DataHelpersUnit, Math, Types, dmDataModule, Variants, Utils, TypInfo, StrUtils, GeneralHelpersUnit{$IFDEF SANDBOX},
  ServerMethodsUnit1{$ENDIF};

{$IFNDEF SANDBOX}
var
  __ProjectHelpersI: TProjectHelpersI;

function ProjectHelpersI: TProjectHelpersI;
begin
  if not Assigned(__ProjectHelpersI) then
    __ProjectHelpersI := TProjectHelpersI.Create;
  Result := __ProjectHelpersI;
end;
{$ENDIF}

{ TProjectHelpersI }
procedure TProjectHelpersI.AddAttachDocs(const AOrigin, AOriginID, AFileName, AFileDescription: string);
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add('Declare @i Integer;');
  AQuery.SQL.Add('Set @i=(Select Max(KeyField) From DocList');
  AQuery.SQL.Add('Where Origin=:Origin And OriginID=:OriginID);');
  AQuery.SQL.Add('Insert Into DocList (Origin, OriginID, FileName, Description, KeyField)');
  AQuery.SQL.Add('Values(:Origin, :OriginID, :FileName, :FileDescription, @i+1);');
  AQuery.Prepare;
  AQuery.ParamByName('Origin').AsString := AOrigin;
  AQuery.ParamByName('OriginID').AsString := AOriginID;
  AQuery.ParamByName('FileName').AsString := AFileName;
  AQuery.ParamByName('FileDescription').AsString := AFileDescription;
  AQuery.ExecSQL;
end;

constructor TProjectHelpersI.Create;
begin
  InitTempPayList;
end;

destructor TProjectHelpersI.Destroy;
begin

  inherited;
end;

procedure TProjectHelpersI.FillProgChangeList;
begin
  //FillProgChangeList_1;
end;

function TProjectHelpersI.GetScanResolve: TScanResolve;
var
  AQuery: TAdsQuery;
  APrcEncd2: Boolean;
  AWghCodPre: string;
begin
  if not Assigned(FScanResolve) then
  begin
    APrcEncd2 := EnvSetting('PrcEncd2').AsBoolean;
    AWghCodPre := EnvSetting('WghCodPre').AsString.Trim;

    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := 'Select * From BcHandle Order By Indx;';
    AQuery.Open;
    FScanResolve := TScanResolve.Create(AQuery, APrcEncd2, AWghCodPre);
  end;

  Result := FScanResolve;
end;

function TProjectHelpersI.GetTmpClcAlPath: string;
var
  FOldPath: string;
begin
  if FTmpClcAlPath = '' then
  begin
    FOldPath := dmData.POSConnection.GetConnectionPath;
    if (FOldPath <> '') and (FOldPath[Length(FOldPath)] <> '\') then
      FOldPath := FOldPath + '\';
    FTmpClcAlPath := ExpandFileName(FOldPath + '..\OldTkts\TmpClcAL');
  end;
  Result := FTmpClcAlPath;
end;

function TProjectHelpersI.GridFieldIsVisible(const AGridForm, AGridName, AFieldName: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select Count(*) From GridData Where [Form]=%s And [Grid]=%s And [Field]=%s And Active',
    [QuotedStr(AGridForm), QuotedStr(AGridName), QuotedStr(AFieldName)]);
  AQuery.Open;
  Result := AQuery.Fields[0].AsInteger > 0;
end;

procedure TProjectHelpersI.InitTempPayList;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * Into #Temp_PayList From PayList;';
  AQuery.Open;
  AQuery.Close;
  AQuery.AdsCloseSQLStatement;
  AQuery.AdsConnection.CloseCachedTables;
end;

function TProjectHelpersI.PayListDataSet: TDataSet;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * From #Temp_PayList;';
  AQuery.Open;
  Result := AQuery;
end;

procedure TProjectHelpersI.Prepare_TempDelItems1_Table;
const
  ATableScript = 'Try Delete From #TempDelItems1; Catch All End; ' +
    'Try Select Code UpcCode, Cast('''' AS SQL_CHAR(15)) VenItmCde, ' +
    'Cast(0 AS SQL_NUMERIC(8, 2)) AmntDel, UPC UpcC, Cost, [Desc] Descrip, Cat, Brand, Size, ' +
    'Cast(Null AS SQL_CHAR(38)) LineID, Cast(Null AS SQL_BIT) [Selected], Cast(Null AS SQL_INTEGER) [Sort], ' +
    'Cast('''' AS SQL_CHAR(50)) SourceLineID, Cast('''' AS SQL_CHAR(50)) SourceTable, ' +
    'Cast(Null AS SQL_CHAR(25)) CCustom1, Cast(Null AS SQL_CHAR(25)) CCustom2, Cast(Null AS SQL_CHAR(25)) CCustom3, ' +
    'Cast(Null AS SQL_DOUBLE) NCustom1, Cast(Null AS SQL_DOUBLE) NCustom2, Cast(Null AS SQL_DOUBLE) NCustom3, ' +
    'Price, Scale, Bin, Cast(0 AS SQL_NUMERIC(8, 2)) LB, Cast(Null AS SQL_CHAR(100)) Remark, ' +
    'Cast(Null AS SQL_BIT) Cases, Cast(Null AS SQL_BIT) UsePrice ' +
    'Into #TempDelItems1 From Prdmstr Where Upper(Code)=''__NO__SUCH__CODE''; ' + 'Catch All End; '; // +
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := ATableScript;
  AQuery.ExecSQL;
end;

function TProjectHelpersI.ZipCodeInfo(const AZipCode: string; var ACity, AState: string): Boolean;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select City, State From ZipCodes Where ZipCode=:ZipCode;';
  AQuery.Prepare;
  AQuery.ParamByName('ZipCode').AsString := AZipCode;
  AQuery.Open;

  Result := not AQuery.IsEmpty;
  if Result then
  begin
    Result := True;
    ACity := AQuery.FieldByName('City').AsString;
    AState := AQuery.FieldByName('State').AsString;
  end;
end;

procedure FillProgPrefList;
var
  AQuery: TAdsQuery;
  procedure AddPrefList(AID: Integer; ASettingCat, ASettingName, ASettingDesc, AType, AEditType, AEditValues: string;
    AStationOnly: Boolean = False; AGlobalOnly: Boolean = False; ASettingClass: string = ''; ARemark: string = '');
  begin
    if Length(ASettingName) > 75 then
      raise EHandleInApp.CreateFmt('Setting name "%s" is too long, max length is 75.', [ASettingName]);

    if Length(ASettingDesc) > 100 then
      raise EHandleInApp.CreateFmt('Setting Description "%s" is too long, max length is 100.', [ASettingDesc]);

    if Length(AEditValues) > 250 then
      raise EHandleInApp.CreateFmt('Setting Value "%s" is too long, max length is 250.', [AEditValues]);

    if Length(ASettingClass) > 25 then
      raise EHandleInApp.CreateFmt('Setting Class "%s" is too long, max length is 25.', [ASettingClass]);

    if Length(ARemark) > 100 then
      raise EHandleInApp.CreateFmt('Setting Remark "%s" is too long, max length is 100.', [ARemark]);

    if Pos(Format(';%s;', [AType]), ';String;Boolean;Numeric;Date;Memo;') = 0 then
      raise EHandleInApp.CreateFmt
        ('%s is not a valid ValueType, valid types are (String, Boolean, Numeric, Date, Memo)', [AType]);

    AQuery.SQL.Add('Insert Into #ProgControl (SettingCat, SettingName, SettingDesc, ValueType, EditType,');
    AQuery.SQL.Add('EditValues, StationOnly, GlobalOnly, SettingClass, SettingID, SettingRemark)');
    AQuery.SQL.Add(Format('Values(''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'', %s, %s, ''%s'', %d, ''%s'');',
      [ASettingCat, ASettingName, ASettingDesc, AType, AEditType, AEditValues, BoolToStr(AStationOnly, True),
      BoolToStr(AGlobalOnly, True), ASettingClass, AID, ARemark]));
  end;

begin
  AQuery := DataHelpers.HelperQuery;

  AQuery.SQL.Clear;
  AQuery.SQL.Add('Try Drop Table #ProgControl; Catch All End;');
  AQuery.SQL.Add('Select Cast('''' AS SQL_CHAR(25)) SettingCat, SettingName,');
  AQuery.SQL.Add('Cast('''' AS SQL_CHAR(100)) SettingDesc, Cast('''' AS SQL_CHAR(15)) ValueType,');
  AQuery.SQL.Add('Cast('''' AS SQL_CHAR(25)) EditType, Repeat('' '', 250) EditValues, False StationOnly,');
  AQuery.SQL.Add('False GlobalOnly, Cast('''' AS SQL_CHAR(25)) SettingClass, Cast(Null AS SQL_INTEGER) SettingID,');
  AQuery.SQL.Add('Cast('''' AS SQL_CHAR(100)) SettingRemark');
  AQuery.SQL.Add('Into #ProgControl From ProgControl');
  AQuery.SQL.Add('Where StationID Is Null;');
  AQuery.Open;

  AQuery.SQL.Clear;
  AddPrefList(101, 'General', 'GenTxtPrinter', 'Default GenericTextPrinter', 'String', 'Printers', '', False, False,
    'Hardware');
  AddPrefList(102, 'General', 'GenUseScale', 'Use Scale', 'Boolean', '', '', True, False, 'Hardware');
  AddPrefList(103, 'General', 'DisableSkins', 'Disable Skins', 'Boolean', '', '');
  AddPrefList(104, 'General', 'SkinName', 'Skin Name', 'String', '', '');
  AddPrefList(105, 'General', 'AllowSkinChange', 'Allow Changing Skins by User', 'Boolean', '', '');
  AddPrefList(106, 'General', 'PrdLabelPrinter', 'Product Label Printer', 'String', 'Printers', '', False, False,
    'Hardware');
  AddPrefList(107, 'General', 'UseScanSimulator', 'Enable Scan Simulator on Main Menu', 'Boolean', '', '');
  AddPrefList(108, 'General', 'BarTenderEnginePath', 'BarTender Engine Path', 'String', '', '', False, False,
    'Hardware');
  AddPrefList(109, 'General', 'BarTenderShowDialog', 'BarTender Show Print Dialog', 'Boolean', '', '');
  AddPrefList(110, 'General', 'BarTenderNoTray', 'BarTender Do Not Run to System Tray', 'Boolean', '', '', False, False,
    'Hardware');
  AddPrefList(111, 'General', 'LogOutOnIdleTime', 'Auto Logout on Idle (Specify Minutes)', 'Numeric', '', '');
  AddPrefList(112, 'General', 'LogOutOnIdleAction', 'Auto Logout on Idle Action Options', 'Numeric', 'ImageComboBox',
    'Force Log Off@0;Close Open Forms@1');
  AddPrefList(113, 'General', 'DefaultDocsPath', 'Attached Documents Path', 'String', '', '');
  AddPrefList(114, 'General', 'ChangeLogFltrStart', 'Browse Change Log Filter Start (Days)', 'Numeric', '', '');
  AddPrefList(115, 'General', 'ChangeLogFltrDateScope',
    'Browse Change Log Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(116, 'General', 'CellCarrierGateways', 'Cell Carrier Gateway List', 'String', 'ImageComboEdit', '');
  AddPrefList(117, 'General', 'ConfigReasonListTypes', 'Reason List Types (for Configure List)', 'String',
    'ImageComboEdit', '');
  AddPrefList(118, 'General', 'TouchMode', 'Touch Mode', 'Boolean', '', '');
  AddPrefList(119, 'General', 'RepEnterpriseID', 'Filter / Group Reports by Enterprise ID', 'Boolean', '', '');
  AddPrefList(120, 'General', 'RepStoreID', 'Filter / Group Reports by Store ID', 'Boolean', '', '');
  AddPrefList(121, 'General', 'SiteNameInCaption', 'Include Site Name in Program Caption', 'Boolean', '', '');
  AddPrefList(122, 'General', 'TaskNoAutoAssignUser', 'Do Not Auto Assign New Task to Current User', 'Boolean', '', '');
  AddPrefList(123, 'General', 'DisableTouchScrollUIMode', 'Disable New Touch Scroll Mode Bars', 'Boolean', '', '');
  AddPrefList(124, 'General', 'AdsWebServer', 'AdsWeb Server Address with Port (i.e. 127.0.0.1:6272)',
    'String', '', '');
  AddPrefList(125, 'General', 'AdsWebLocList',
    'AdsWeb Server Location List - comma delimited (AdsWeb Locations should be Location + TIS)', 'String', '', '');

  AddPrefList(151, 'Site Info', 'HeaderName2', 'Site Name Line 2', 'String', '', '');
  AddPrefList(152, 'Site Info', 'HeaderAddress1', 'Site Address Line 1', 'String', '', '');
  AddPrefList(153, 'Site Info', 'HeaderAddress2', 'Site Address Line 2', 'String', '', '');
  AddPrefList(154, 'Site Info', 'HeaderCityStateZip1', 'Site City State Zip Line 1', 'String', '', '');
  AddPrefList(155, 'Site Info', 'HeaderCityStateZip2', 'Site City State Zip Line 2', 'String', '', '');
  AddPrefList(156, 'Site Info', 'HeaderTel1', 'Site Tel Line 1', 'String', '', '');
  AddPrefList(157, 'Site Info', 'HeaderTel2', 'Site Tel Line 2', 'String', '', '');

  AddPrefList(158, 'Site Info', 'EmailHostAddress', 'Email Host Address', 'String', '', '');
  AddPrefList(159, 'Site Info', 'EmailUserName', 'Email User Name', 'String', '', '');
  AddPrefList(160, 'Site Info', 'EmailPassword', 'Email Password', 'String', '', '');
  AddPrefList(161, 'Site Info', 'EmailBCC', 'Email BCC', 'String', '', '');
  AddPrefList(162, 'Site Info', 'EmailPort', 'Email Port', 'Numeric', '', '');
  AddPrefList(163, 'Site Info', 'EmailFromAddress', 'Email From Address', 'String', '', '');
  AddPrefList(164, 'Site Info', 'EmailFromName', 'Email From Name (defaults to Site Name)', 'String', '', '');

  AddPrefList(201, 'Scanner', 'ScannerPort', 'Scanner Port', 'Numeric', 'ComboBox', '0,1,2,3,4,5,6,7,8,9', True, False,
    'Hardware');
  AddPrefList(202, 'Scanner', 'ScannerBaudRate', 'Scanner Baud Rate', 'String', 'ComboBox', '9600, 19200, 38400, 56000',
    True, False, 'Hardware');
  AddPrefList(203, 'Scanner', 'ScannerStopBits', 'Scanner Stop Bits', 'String', 'ComboBox', '1, 2', True, False,
    'Hardware');
  AddPrefList(204, 'Scanner', 'ScannerDataBits', 'Scanner Data Bits', 'String', 'ComboBox', '7, 8', True, False,
    'Hardware');
  AddPrefList(205, 'Scanner', 'ScannerParity', 'Scanner Parity', 'String', 'ComboBox', 'None, Odd, Even, Mark, Space',
    True, False, 'Hardware');
  AddPrefList(206, 'Scanner', 'ScannerFlowControl', 'Scanner Flow Control', 'String', 'ComboBox',
    'Hardware, Software, None', True, False, 'Hardware');
  AddPrefList(207, 'Scanner', 'ScanSimulatorKey',
    'Prefix for Scan Simulator - up to 2 chars defaults to ^~ (UseScanSimulator needs to be set)', 'String', '', '');

  AddPrefList(251, 'Scale', 'ScalePort', 'Scale Port', 'Numeric', 'ComboBox', '0,1,2,3,4,5,6,7,8,9', True, False,
    'Hardware');
  AddPrefList(252, 'Scale', 'ScaleBaudRate', 'Scale Baud Rate', 'String', 'ComboBox', '9600, 19200, 38400, 56000', True,
    False, 'Hardware');
  AddPrefList(253, 'Scale', 'ScaleStopBits', 'Scale Stop Bits', 'String', 'ComboBox', '1, 2', True, False, 'Hardware');
  AddPrefList(254, 'Scale', 'ScaleDataBits', 'Scale Data Bits', 'String', 'ComboBox', '7, 8', True, False, 'Hardware');
  AddPrefList(255, 'Scale', 'ScaleParity', 'Scale Parity', 'String', 'ComboBox', 'None, Odd, Even, Mark, Space', True,
    False, 'Hardware');
  AddPrefList(256, 'Scale', 'ScaleFlowControl', 'Scale Flow Control', 'String', 'ComboBox', 'Hardware, Software, None',
    True, False, 'Hardware');
  AddPrefList(257, 'Scale', 'ScaleType', 'Scale Type', 'String', 'ComboBox',
    'AND, Megellan, WeightTronics, MettlerToledoSICS, MettlerToledo', True, False, 'Hardware');

  AddPrefList(301, 'Customers', 'CalcCustSummDateScope', 'Calculate Sales Summary Date Scope', 'String',
    'DateScopeList', '');
  AddPrefList(302, 'Customers', 'CstGlobalChangesExpandList', 'Customer Global Changes Expand List', 'String', '',
    '', True);
  AddPrefList(303, 'Customers', 'GiftCardServerHost', 'Gift Card Server Host followed by Port (i.e. 192.168.1.10:2366)',
    'String', '', '', False, True);
  AddPrefList(304, 'Customers', 'CustAddrsValidation', 'Enable Customer USPS Address Validation / Postal Carrier Route',
    'Boolean', '', '', False, True);
  AddPrefList(305, 'Customers', 'CustAddrsGeoCoding', 'Enable Customer Address GeoCoding', 'Boolean', '', '',
    False, True);
  AddPrefList(306, 'Customers', 'CustAddrsGeoNoValidAddrs',
    'Customer Address GeoCoding for addresses that failed Address Validation', 'Boolean', '', '', False, False, '',
    'Dependant on #305 - Customer Address GeoCoding');
  AddPrefList(307, 'Customers', 'CustAddrsGeoWithValidation',
    'Do GeoCoding with Addresss Validation action in Customer List and View', 'Boolean', '', '', False, False, '',
    'Dependant on #305 - Customer Address GeoCoding - USE WITH CAUTION');

  AddPrefList(321, 'Customer Orders', 'CustInvAutoSave', 'Alert to Save Cust Invoice After New Records',
    'Numeric', '', '');
  AddPrefList(322, 'Customer Orders', 'CustInvFltrStart', 'Browse Customer Invoice Filter Start (Days)',
    'Numeric', '', '');
  AddPrefList(353, 'Customer Orders', 'CustInvFltrDateScope',
    'Browse Customer Invoice Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(324, 'Customer Orders', 'CustInvDefDueDate', 'Default Due Date (Days)', 'Numeric', '', '');
  AddPrefList(325, 'Customer Orders', 'CustInvColumnMove', 'XML Formatted Column Movement Instruction', 'Memo',
    'XML', '');
  AddPrefList(326, 'Customer Orders', 'CustInvChkCreditLimit', 'Cust Invoice Check credit Limit on New Invoice',
    'Boolean', '', '');
  AddPrefList(327, 'Customer Orders', 'CustInvAutoPrint',
    'Alert to Print Cust Invoice at "Close" or "Save & New" (1=Always, 2=Due Today)', 'Numeric', '', '');
  AddPrefList(328, 'Customer Orders', 'CustInvShowTabSheet', 'Tab Sheet to Show when opening Customer Invoice',
    'String', 'ComboBox', 'tsOrderInfo,tsProductList,tsLastOrder');
  AddPrefList(329, 'Customer Orders', 'CustInvPrdListCats', 'Cust Invoice Categories to Include in Product List',
    'String', '', '');
  AddPrefList(330, 'Customer Orders', 'CustTmpltFltrStart', 'Browse Customer Templates Filter Start (Days)',
    'Numeric', '', '');
  AddPrefList(331, 'Customer Orders', 'CustTmpltFltrDateScope',
    'Browse Customer Templates Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(332, 'Customer Orders', 'CustInvDefDlvMethod', 'Default Delivery Method', 'String', 'ComboBox',
    'Delivery, Pickup');
  AddPrefList(333, 'Customer Orders', 'CustInvEmailMapi', 'Enable emailing Cust Inv using MAPI client',
    'Boolean', '', '');
  AddPrefList(334, 'Customer Orders', 'NoPackOrdLabels', 'Do not auto print Pack Order labels', 'Boolean', '', '');
  AddPrefList(335, 'Customer Orders', 'SpaceBarFillLine', 'Always mark line as Filled after Space Bar',
    'Boolean', '', '');
  AddPrefList(336, 'Customer Orders', 'CustInvDefPrintOut', 'Default Print Method', 'String', 'ImageComboBox',
    '<Not Set>@;Customer Print@OrdPrn4;Combined@OrdWeek2;Combined with Barcode@OrdWeek3;Pick Slip@OrdPick1;');
  AddPrefList(337, 'Customer Orders', 'CustInvUseOrdDept', 'Enable Order Department entry', 'Boolean', '', '',
    False, True);
  AddPrefList(338, 'Customer Orders', 'CustInvDefOrdDept', 'Default Order Department (for New Orders and Order Browse)',
    'String', '', '');
  AddPrefList(339, 'Customer Orders', 'CustInvAgeOnTop', 'Show Customer Aging in Order Heading Info', 'Boolean', '', '',
    False, True);
  AddPrefList(340, 'Customer Orders', 'CustInvNewFocusComponent', 'Component to Set Focus on New Order', 'String',
    'ComboBox', 'edtDate, DetailGrid');
  AddPrefList(341, 'Customer Orders', 'PackNoAlertScaleQuan', 'Do Not Alert on Amount Exceed for Scale items',
    'Boolean', '', '', False, True);
  AddPrefList(342, 'Customer Orders', 'CustInvAutoSaveNoPrompt', 'Auto Save Orders (do not prompt Save Changes)',
    'Boolean', '', '', False, True);
  AddPrefList(343, 'Customer Orders', 'DoBeforeNewOrd', 'Script to run Before New Customer Order', 'String', 'Pascal',
    '', False, True);
  AddPrefList(344, 'Customer Orders', 'CustInvRemarkUseDlvMsgs',
    'Use Combo Box of Delivery Messages for Remark field in Customer Order', 'Boolean', '', '', False, True);
  AddPrefList(345, 'Customer Orders', 'CustInvShowInfoSheet', 'Info Tab Sheet to Show when opening Customer Invoice',
    'String', 'ComboBox', '"Status History","Memo","Customer Message","Balance && Aging","Payment Info"');
  AddPrefList(346, 'Customer Orders', 'CustInvPriorOrderRange',
    'Range of weeks for "Last ? Weeks" option in Prior Order tab on Customer Order (defaults 6 Weeks)',
    'Numeric', '', '');

  AddPrefList(351, 'Register Transactions', 'TransactionBrowseHideEmpty',
    'Hide empty Transactions on Transaction Browse View', 'Boolean', '', '', True);
  AddPrefList(352, 'Register Transactions', 'RegLogDashAdjustTime', 'Enable Adjust Time etc. in Register Log Dashboard',
    'Boolean', '', '');
  AddPrefList(353, 'Register Transactions', 'RegLogDashAdjustDiscrepancy',
    'Enable Adjust Discrepancy in Register Log Dashboard', 'Boolean', '', '');
  AddPrefList(354, 'Register Transactions', 'QbSalesReceiptItems', 'XML formatted data of to map POS data to QB Items',
    'Memo', 'XML', '', False, True);
  AddPrefList(355, 'Register Transactions', 'QbSalesReceiptSummary', 'Summary level to export Register Logs to QB',
    'String', 'ComboBox', 'LogNum,StartDate,Week,Month', False, True);
  AddPrefList(356, 'Register Transactions', 'QbSalesExportMode', 'Export mode for Register Logs to QB', 'String',
    'ComboBox', '"Sales Receipt","Journal Entry"', False, True);

  AddPrefList(376, 'Cust Deliveries', 'CustDlvFltrStart', 'Browse Customer Deliveries Filter Start (Days)',
    'Numeric', '', '');
  AddPrefList(377, 'Cust Deliveries', 'CustDlvFltrDateScope',
    'Browse Customer Deliveries Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(378, 'Cust Deliveries', 'CustDlvAutoPrintLabels',
    'Auto to Print Cust Deliveries Labels when Creating New.', 'Boolean', '', '');
  AddPrefList(379, 'Cust Deliveries', 'CustDlvDfltOrdSource', 'Default Order Source when Creating New.', 'String',
    'ComboBox', 'Fax,Phone,Email,Other');
  AddPrefList(380, 'Cust Deliveries', 'CustDlvRcvdNotifySms',
    '"CompuDime Voice Management" ID for SMS Notifications to Customers when New Quick Order is Received', 'Numeric',
    '', '', False, True);
  AddPrefList(381, 'Cust Deliveries', 'CustDlvUseFilledBy', 'Display "Filled By" Field on Quick Order',
    'Boolean', '', '');

  AddPrefList(401, 'Products', 'AutoPriceChngLabel', 'Auto Print Price Change Labels', 'Boolean', '', '');
  AddPrefList(402, 'Products', 'ListProductsFilters', 'Filters to Display in Product List Filter Bar', 'String',
    'ImageComboEdit', '');
  AddPrefList(403, 'Products', 'CutPreCodeOnScan', 'Amount of Characters to Cut From Start of Scan When Adding Item',
    'Numeric', '', '');
  AddPrefList(404, 'Products', 'CutCheckDigitOnScan', 'Cut Check Digit From End of Scan When Adding Item',
    'Boolean', '', '');
  AddPrefList(405, 'Products', 'CalcSalesDateScope', 'Calculate Sales Summary Date Scope', 'String',
    'DateScopeList', '');
  AddPrefList(406, 'Products', 'DebugInventoryChanges', 'Debug Inventory Changes to InvTrk Table', 'Boolean', '', '');
  AddPrefList(407, 'Products', 'DefaultPriceCalcMode', 'Default Price Calculation Mode (Markup / Margin)', 'String',
    'ComboBox', 'Markup, Margin');
  AddPrefList(408, 'Products', 'PrdGlobalChangesExpandList', 'Product Global Changes Expand List', 'String', '',
    '', True);
  AddPrefList(409, 'Products', 'PrdCostInfoEtcCollapsed', 'Product View Cost Etc Collapsed', 'Boolean', '', '', True);
  AddPrefList(410, 'Products', 'PrdTagsLayoutGroupWidth', 'Product Tags LayoutGroup Width', 'Numeric', '', '', True);
  AddPrefList(411, 'Products', 'PrdCodeAutoGen', 'Auto Generate Product Code for New Products', 'Boolean', '', '',
    False, True);
  AddPrefList(412, 'Products', 'PrdCodeAutoGenUPCA', 'Create UPCA codes for Auto Generated Product Codes',
    'Boolean', '', '');
  AddPrefList(413, 'Products', 'PrdPivotSnapList', 'Product Pivot DataSnap List', 'String', 'ImageComboEdit', '',
    False, True);
  AddPrefList(414, 'Products', 'CompuConnectPrdServer', 'Use Compu Connect PrdServer', 'Boolean', '', '');
  AddPrefList(415, 'Products', 'CompuConnectPrdServerUpload', 'Enable Compu Connect PrdServer Upload Product List',
    'Boolean', '', '');
  AddPrefList(416, 'Products', 'CompuConnectPrdSearch', 'CompuConnect Product Search features', 'Boolean', '', '',
    False, True);
  AddPrefList(417, 'Products', 'PrdSaveLastDefaultAction', 'Save Last Default Action on Product List',
    'Boolean', '', '');
  AddPrefList(418, 'Products', 'PrdListDefaultAction', 'Default Action on Product List', 'String', '', '');
  AddPrefList(419, 'Products', 'TktDetailGlobalChanges', 'Global Changes for Ticket Details', 'Boolean', '', '',
    True, False);
  AddPrefList(420, 'Products', 'TktDetailGlobalChangesInternals',
    'Enable Internals in Global Changes for Ticket Details', 'Boolean', '', '', True, False);
  AddPrefList(421, 'Products', 'PrdPricingRequireRule',
    'Require a Rule for each entry in "Product Pricing Options" (Custom Pricing)', 'Boolean', '', '');
  AddPrefList(422, 'Products', 'CstmPrcsCaptionList', 'Campaign list for Product Custom Prices', 'String', '', '',
    False, True);
  AddPrefList(423, 'Products', 'PrdNotesLayoutGroupWidth', 'Product Notes LayoutGroup Width', 'Numeric', '', '', True);
  // 424
  AddPrefList(425, 'Products', 'PrdImagesPath', 'Path for Product Images (use \\Server\ShareName\Path\ format)',
    'String', '', '');
  AddPrefList(426, 'Products', 'VendDlvListFltrStart', 'Vendor Item Deliveries List Filter Start (Days)',
    'Numeric', '', '');
  AddPrefList(427, 'Products', 'VendDlvListFltrDateScope',
    'Vendor Item Deliveries List Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(428, 'Products', 'AllwConsignAbove100', 'Allow Consignment Above 100 Percent for Consignee', 'Boolean',
    '', '', False, True);
  AddPrefList(325, 'Products', 'PrdLocTransferBatchAction', 'XML Formatted Report Action for Product Location Transfers when Not Found Products recorded Batch', 'Memo', 'XML', '', False, False, '', 'Use: <ParamAK20CEB1 Description="Batch ID">CurrentBatchID</ParamAK20CEB1>');

  AddPrefList(451, 'Vendor Invoice', 'CostThisDlvOnly',
    'Vendor Invoice Prompt on Cost Change (Product, Vendor Item Code or This Delivery Only options)',
    'Boolean', '', '');
  AddPrefList(452, 'Vendor Invoice', 'VendInvAutoSave', 'Alert to Save Vend Invoice After New Records',
    'Numeric', '', '');
  AddPrefList(453, 'Vendor Invoice', 'CnfrmAllLblVendInv', 'Confirm Print All Labels on Vendor Invoice',
    'Boolean', '', '');
  AddPrefList(454, 'Vendor Invoice', 'VendInvFltrStart', 'Browse Vendor Invoice Filter Start (Days)',
    'Numeric', '', '');
  AddPrefList(455, 'Vendor Invoice', 'VendInvLblCntByQty', 'Default Product Label Count to Line Quantity',
    'Boolean', '', '');
  AddPrefList(456, 'Vendor Invoice', 'VendInvColumnMove', 'XML Formatted Column Movement Instruction', 'Memo',
    'XML', '');
  AddPrefList(457, 'Vendor Invoice', 'CostQuickUpdatePrice', 'Quick Cost Change Prompt Update Price on Cost Change',
    'Boolean', '', '');
  AddPrefList(458, 'Vendor Invoice', 'PromptUpdatePrice',
    'Prompt Update Price on Cost Change (0=Never 1=Cost-Raise 2=Cost-Change 3=Not-This-Delivery-Only)',
    'Numeric', '', '');
  AddPrefList(459, 'Vendor Invoice', 'HideCostQuickList', 'Hide Quick Cost Change from Vendor Invoice List',
    'Boolean', '', '');
  AddPrefList(460, 'Vendor Invoice', 'VendInvFltrDateScope',
    'Browse Vendor Invoice Date Scope (overrides "Filter Start (Days)")', 'String', 'DateScopeList', '');
  AddPrefList(461, 'Vendor Invoice', 'HideMemoAndSummary', 'Hide Memo And Summary on Vendor Invoice View', 'Boolean',
    '', '', True);
  AddPrefList(462, 'Vendor Invoice', 'VendUseAICodes', 'Support Application Identifier (AI) BarCodes in Vendor Invoice',
    'Boolean', '', '');
  AddPrefList(463, 'Vendor Invoice', 'ClearCostFlagOptions',
    'Options to Clear the Cost Flag in Vendor Invoices on Cost change', 'Numeric', 'ImageComboBox',
    'Prompt User@0;Never Auto Clear@1;Always Auto Clear@2');
  AddPrefList(464, 'Vendor Invoice', 'ForceVendInvNum', 'Force entry of Vendor Invoice # in Vendor Invoice',
    'Boolean', '', '');
  AddPrefList(465, 'Vendor Invoice', 'PromptUpdatePriceUpcChange',
    'Prompt Update Price on UPC Change (0=Never, 1=UPC Change)', 'Numeric', '', '');
  AddPrefList(466, 'Vendor Invoice', 'DefVendInvQuanZero',
    'Default Quantity of Vendor Invoice item to zero (default is 1)', 'Boolean', '', '', False, False, '',
    'For Quick Vendor Invoices use #481');
  AddPrefList(467, 'Vendor Invoice', 'VendInvPivotTemplate1', 'Vendor Invoice Pivot Template1 (comma delimited)',
    'String', 'ImageComboEdit', '');
  AddPrefList(468, 'Vendor Invoice', 'CompuConnectVendItm', 'CompuConnect Vendor Item features', 'Boolean', '', '',
    False, True);
  AddPrefList(469, 'Vendor Invoice', 'VendInvBillDueDays', 'Days after receipt Vendor Bills are due',
    'Numeric', '', '');
  AddPrefList(470, 'Vendor Invoice', 'QbConnectString', 'QB Connection String (for remote connection)',
    'String', '', '');
  AddPrefList(471, 'Vendor Invoice', 'QbBillCompanyName',
    'QB Company File for Vendor Invoices (to allow access when QB is closed)', 'String', '', '');
  AddPrefList(472, 'Vendor Invoice', 'QbBillExpenseAccount', 'QB Default Expense Account for Vendor Invoices',
    'String', '', '');
  AddPrefList(473, 'Vendor Invoice', 'QbBillAPAccount', 'QB A/P Account', 'String', '', '');
  AddPrefList(474, 'Vendor Invoice', 'QbBillFilterSQL',
    'SQL Filter to filter Bills being uploaded to QB (Delvry d, VenMstr v)', 'String', '', '', False, True);
  AddPrefList(475, 'Vendor Invoice', 'QbBillUseMemoField', 'Post Vendor Invoice Memo to QB Bills', 'Boolean', '', '',
    False, True);
  AddPrefList(476, 'Vendor Invoice', 'QbVendExpenseItemMode', 'QB Vendor Invoice Detail Mode Expenses vs. Items',
    'String', 'ComboBox', 'Expenses, Items');
  AddPrefList(477, 'Vendor Invoice', 'WHDataSnap', 'Warehouse DataSnap Address to Import Orders into Vendor Invoice',
    'String', '', '');
  AddPrefList(478, 'Vendor Invoice', 'VendInvLineEditReportJob',
    'XML Formatted Report Job to call with Tag button of Vendor Invoice Row', 'Memo', 'XML', '');
  AddPrefList(479, 'Vendor Invoice', 'VendInvLineEditReportJobAuto',
    'Auto process XML Formatted Report Job when posting Vendor Invoice Row', 'Boolean', '', '');
  AddPrefList(480, 'Vendor Invoice', 'VenItemCodeLegacyPopup', 'Use Legacy Grid Popup for Vendor Item Code Lookup',
    'Boolean', '', '');
  AddPrefList(481, 'Vendor Invoice', 'DefQVendInvQuanZero',
    'Default Quantity of Quick Vendor Invoice / Cost Change to zero (default is 1)', 'Boolean', '', '', False, False,
    '', 'When not set #466 will effect Quick Invoices as well');
  AddPrefList(482, 'Vendor Invoice', 'VendInvUsePrintCost', 'Use Print Cost & Amount Columns in Vendor Invoice',
    'Boolean', '', '');
  AddPrefList(483, 'Vendor Invoice', 'VendInvNoPrdCost',
    'Vendor Invoice Cost should Not effect Product List Cost values', 'Boolean', '', '', False, True);
  AddPrefList(484, 'Vendor Invoice', 'QbBillClassName', 'QB Class Name for Vendor Invoices', 'String', '', '');

  AddPrefList(501, 'Vendor Invoice Upcharges', 'VendInvUpcharge1Caption', 'Vendor Invoice Upcharge 1 Caption', 'String',
    '', '', False, True);
  AddPrefList(502, 'Vendor Invoice Upcharges', 'VendInvUpcharge2Caption', 'Vendor Invoice Upcharge 2 Caption', 'String',
    '', '', False, True);
  AddPrefList(503, 'Vendor Invoice Upcharges', 'VendInvUpcharge3Caption', 'Vendor Invoice Upcharge 3 Caption', 'String',
    '', '', False, True);
  AddPrefList(504, 'Vendor Invoice Upcharges', 'VendInvUpcharge4Caption', 'Vendor Invoice Upcharge 4 Caption', 'String',
    '', '', False, True);
  AddPrefList(505, 'Vendor Invoice Upcharges', 'VendInvUpcharge5Caption', 'Vendor Invoice Upcharge 5 Caption', 'String',
    '', '', False, True);
  AddPrefList(506, 'Vendor Invoice Upcharges', 'VendInvUpcharge1UpDown', 'Vendor Invoice Upcharge 1 UpDown', 'Numeric',
    'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);
  AddPrefList(507, 'Vendor Invoice Upcharges', 'VendInvUpcharge2UpDown', 'Vendor Invoice Upcharge 2 UpDown', 'Numeric',
    'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);
  AddPrefList(508, 'Vendor Invoice Upcharges', 'VendInvUpcharge3UpDown', 'Vendor Invoice Upcharge 3 UpDown', 'Numeric',
    'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);
  AddPrefList(509, 'Vendor Invoice Upcharges', 'VendInvUpcharge4UpDown', 'Vendor Invoice Upcharge 4 UpDown', 'Numeric',
    'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);
  AddPrefList(510, 'Vendor Invoice Upcharges', 'VendInvUpcharge5UpDown', 'Vendor Invoice Upcharge 5 UpDown', 'Numeric',
    'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);
  AddPrefList(511, 'Vendor Invoice Upcharges', 'VendInvUpcharge1CalcMethod', 'Vendor Invoice Upcharge 1 CalcMethod',
    'Numeric', 'ImageComboBox', 'User Defined@0;Value@1;Percentage@2', False, True);
  AddPrefList(512, 'Vendor Invoice Upcharges', 'VendInvUpcharge2CalcMethod', 'Vendor Invoice Upcharge 2 CalcMethod',
    'Numeric', 'ImageComboBox', 'User Defined@0;Value@1;Percentage@2', False, True);
  AddPrefList(513, 'Vendor Invoice Upcharges', 'VendInvUpcharge3CalcMethod', 'Vendor Invoice Upcharge 3 CalcMethod',
    'Numeric', 'ImageComboBox', 'User Defined@0;Value@1;Percentage@2', False, True);
  AddPrefList(514, 'Vendor Invoice Upcharges', 'VendInvUpcharge4CalcMethod', 'Vendor Invoice Upcharge 4 CalcMethod',
    'Numeric', 'ImageComboBox', 'User Defined@0;Value@1;Percentage@2', False, True);
  AddPrefList(515, 'Vendor Invoice Upcharges', 'VendInvUpcharge5CalcMethod', 'Vendor Invoice Upcharge 5 CalcMethod',
    'Numeric', 'ImageComboBox', 'User Defined@0;Value@1;Percentage@2', False, True);

  AddPrefList(516, 'Vendor Invoice Upcharges', 'VendInvPrintUpcharge1Caption',
    'Vendor Invoice Print Upcharge 1 Caption', 'String', '', '', False, True);
  AddPrefList(517, 'Vendor Invoice Upcharges', 'VendInvPrintUpcharge1UpDown', 'Vendor Invoice Print Upcharge 1 UpDown',
    'Numeric', 'ImageComboBox', 'User Defined@0;Upcharge@1;Discount@2', False, True);

  AddPrefList(526, 'Vendor PO', 'VendPOFltrStart', 'Browse Vendor PO Filter Start (Days)', 'Numeric', '', '');
  AddPrefList(527, 'Vendor PO', 'VendPOFltrDateScope', 'Browse Vendor PO Date Scope (overrides "Filter Start (Days)")',
    'String', 'DateScopeList', '');
  AddPrefList(528, 'Vendor PO', 'VendPOAddToOpen', 'Prompt to Add to Open PO instead of New', 'Boolean', '', '');

  // AddPrefList(0, '', '', '', '', '', '');
  // AddPrefList(ASettingID, ASettingCat, ASetting, ASettingDesc, AType, AEditType, AEditValues);
  AQuery.ExecSQL;
end;

end.
