unit ScanResolveUnit;

interface

uses Windows, Messages, SysUtils, Classes, DB, dxmdaset;

type
  TScanResolve = class
  private
    FScanOptions: TdxMemData;
    FResults: TdxMemData;
    FFullScan: string;
    FPrcEncd2: Boolean;
    FWghCodPre: string;
    function UpcA2E(sStr: string): string;
    function UpcE2A(sStr: string): string;
  protected
    property FullScan: string read FFullScan;
    property PrcEncd2: Boolean read FPrcEncd2;
    property WghCodPre: string read FWghCodPre;
    function ParseSetCode: string;
    function ParseSystemCode: string;
    function ParseCondition: Boolean;
    function MeetsThisCondition(const AFunction, ACondition: string): Boolean;
    function ParseExpression(AExpression, AString: string): string;
    function ParseThisExpression(const AFunction, AExpression, AFixedScan: string): string;
  public
    property Results: TdxMemData read FResults;

    function GetChkDgt(sStr: string): string;

    function Execute(const AScan: string): TDataSet;
    constructor Create(ADataSet: TDataSet; APrcEncd2: Boolean; AWghCodPre: string);
    destructor Destroy; override;
  end;

implementation

uses
  Utils;

{ TScanResolve }

constructor TScanResolve.Create(ADataSet: TDataSet; APrcEncd2: Boolean; AWghCodPre: string);
begin
  FScanOptions := TdxMemData.Create(nil);
  FResults := TdxMemData.Create(nil);
  FPrcEncd2 := APrcEncd2;
  FWghCodPre := AWghCodPre;

  //AQuery := DataHelpers.HelperQuery;
  //AQuery.SQL.Text := 'Select * From BcHandle Order By Indx;';
  //AQuery.Open;
  FScanOptions.CreateFieldsFromDataSet(ADataSet);
  FScanOptions.LoadFromDataSet(ADataSet);
  FScanOptions.Open;

  with FResults.FieldDefs.AddFieldDef do
  begin
    Name := 'Scan';
    DataType := ftString;
    Size := 20;
    CreateField(FResults);
  end;
  with FResults.FieldDefs.AddFieldDef do
  begin
    Name := 'PreDigit';
    DataType := ftString;
    Size := 1;
    CreateField(FResults);
  end;
  with FResults.FieldDefs.AddFieldDef do
  begin
    Name := 'Indx';
    DataType := ftInteger;
    CreateField(FResults);
  end;
  FResults.SortedField := 'Indx';
  FResults.Open;
end;

destructor TScanResolve.Destroy;
begin
  if Assigned(FScanOptions) then
    FScanOptions.Free;
  if Assigned(FResults) then
    FResults.Free;
end;

function TScanResolve.Execute(const AScan: string): TDataSet;
begin
  FFullScan := AScan;
  FResults.First;
  while not FResults.IsEmpty do
    FResults.Delete;

  FScanOptions.First;
  while not FScanOptions.Eof do
  begin
    if ParseCondition then
    begin
      FResults.Insert;
      FResults.FieldByName('Scan').AsString := ParseSetCode;
      FResults.FieldByName('PreDigit').AsString := ParseSystemCode;
      FResults.FieldByName('Indx').AsInteger := FScanOptions.FieldByName('Indx').AsInteger;
      FResults.Post;
    end;
    FScanOptions.Next;
  end;
  Result := FResults;
end;

function TScanResolve.MeetsThisCondition(const AFunction, ACondition: string): Boolean;
var
  sAParam, sParam1, sParam2, sParam3: string;
  sTempCond: string;
  nPos, i: Integer;
begin
  sParam1 := '';
  sParam2 := '';
  sParam3 := '';
  Result := True;
  sTempCond := ACondition;
  i := 1;
  repeat
    nPos := Pos(',', sTempCond);
    if nPos > 0 then
    begin
      sAParam := Copy(sTempCond, 1, nPos - 1);
      sTempCond := Copy(sTempCond, nPos, Length(sTempCond));
    end
    else
    begin
      sAParam := sTempCond;
      sTempCond := '';
    end;
    if Copy(sTempCond, 1, 1) = ',' then
      sTempCond := Copy(sTempCond, 2, Length(sTempCond));
    case i of
      1:
        sParam1 := sAParam;
      2:
        sParam2 := sAParam;
      3:
        sParam3 := sAParam;
    end;
    Inc(i);
  until nPos = 0;
  if SameText(AFunction, '-PrcEncd2') then
    Result := not PrcEncd2
  else if SameText(AFunction, 'IsWghCode') then
  begin
    Result := (WghCodPre<>'') and (Copy(FullScan, 1, 1)=WghCodPre);
    if Result and (sParam1<>'') then
      Result := Copy(FullScan, 1, Length(sParam1))=sParam1;
  end
  else if SameText(AFunction, 'LenMin') then
    Result := Length(FullScan) >= StrToInt(sParam1)
  else if SameText(AFunction, 'LenMax') then
    Result := Length(FullScan) <= StrToInt(sParam1)
  else if SameText(AFunction, 'Len') then
    Result := Length(FullScan) = StrToInt(sParam1)
  else if SameText(AFunction, 'Left') then
    Result := Copy(FullScan, 1, StrToInt(sParam1)) = sParam2
  else if SameText(AFunction, '-Left') then
    Result := Copy(FullScan, 1, StrToInt(sParam1)) <> sParam2
  else if SameText(AFunction, 'Mid') then
    Result := Copy(FullScan, StrToInt(sParam1), StrToInt(sParam2)) = sParam3
  else if SameText(AFunction, 'Contains') then
    Result := Pos(sParam1, FullScan) > 0
end;

function TScanResolve.ParseCondition: Boolean;
var
  ACondition, ASearchString, ACurrentCond: string;
  x, nPos1, nPos2: Integer;
begin
  Result := True;
  ACondition := FScanOptions.FieldByName('Cond').AsString;
  if ACondition.IsEmpty then
    Exit;
  for x := 1 to 9 do
  begin
    case x of
      1:
        ASearchString := '<LENMIN,';
      2:
        ASearchString := '<LENMAX,';
      3:
        ASearchString := '<LEN,';
      4:
        ASearchString := '<LEFT,';
      5:
        ASearchString := '<-LEFT,';
      6:
        ASearchString := '<MID,';
      7:
        ASearchString := '<CONTAINS,';
      8:
        ASearchString := '<-PRCENCD2';
      9:
        ASearchString := '<ISWGHCODE,';
    end;
    repeat
      nPos1 := Pos(ASearchString, ACondition);
      nPos2 := 0;
      if nPos1 > 0 then
        nPos2 := Pos('>', Copy(ACondition, nPos1, Length(ACondition))) + nPos1;
      if nPos2 > nPos1 then
      begin
        ACurrentCond := Copy(ACondition, nPos1, nPos2 - nPos1);
        ACondition := Trim(StrTran(ACondition, ACurrentCond, ''));
        // remove CurrentCond from Condition List

        ACurrentCond := Trim(StrTran(ACurrentCond, ASearchString, '')); // remove func name
        ACurrentCond := Copy(ACurrentCond, 1, Length(ACurrentCond) - 1); // remove last >
        ASearchString := Copy(ASearchString, 2, Length(ASearchString) - 2);
        // remove < and , from func name
        Result := MeetsThisCondition(ASearchString, ACurrentCond);

        if not Result then
          Break;
      end;
    until nPos2 = 0;
    if not Result then
      Break;
  end;
end;

function TScanResolve.ParseSetCode: string;
begin
  Result := ParseExpression(FScanOptions.FieldByName('SetCode').AsString, FullScan);
end;

function TScanResolve.ParseSystemCode: string;
begin
  Result := ParseExpression(FScanOptions.FieldByName('SetSysCode').AsString, FullScan);
  if Length(Result) > 1 then
    Result := '';
end;

function TScanResolve.ParseExpression(AExpression, AString: string): string;
var
  ASearchString, ACurrentExp: string;
  nPos1, nPos2: Integer;
begin
  Result := FullScan;
  repeat
    AExpression := Trim(AExpression);
    if SameText(Copy(AExpression, 1, 4), '<CL,') then
      ASearchString := '<CL,'
    else if SameText(Copy(AExpression, 1, 4), '<CR,') then
      ASearchString := '<CR,'
    else if SameText(Copy(AExpression, 1, 5), '<CDE,') then
      ASearchString := '<CDE,'
    else if SameText(Copy(AExpression, 1, 3), '<CD') then
      ASearchString := '<CD'
    else if SameText(Copy(AExpression, 1, 6), '<CUTL0') then
      ASearchString := '<CUTL0'
    else if SameText(Copy(AExpression, 1, 8), '<SUBSTR,') then
      ASearchString := '<SUBSTR,'
    else if SameText(Copy(AExpression, 1, 8), '<UPCE2A,') then
      ASearchString := '<UPCE2A,'
    else if SameText(Copy(AExpression, 1, 8), '<UPCA2E,') then
      ASearchString := '<UPCA2E,'
    else if SameText(Copy(AExpression, 1, 6), '<PADR,') then
      ASearchString := '<PADR,'
    else if SameText(Copy(AExpression, 1, 4), '<AL,') then
      ASearchString := '<AL,'
    else if SameText(Copy(AExpression, 1, 4), '<AR,') then
      ASearchString := '<AR,'
    else
      ASearchString := 'NOTHING_TO_DO';

    nPos1 := Pos(ASearchString, AExpression);
    nPos2 := 0;
    if nPos1 > 0 then
      nPos2 := Pos('>', Copy(AExpression, nPos1, Length(AExpression))) + nPos1;
    if nPos2 > nPos1 then
    begin
      ACurrentExp := Copy(AExpression, nPos1, nPos2 - nPos1);
      AExpression := Trim(StrTran(AExpression, ACurrentExp, ''));
      // remove CurrentCond from Condition List

      ACurrentExp := Trim(StrTran(ACurrentExp, ASearchString, '')); // remove func name
      ACurrentExp := Copy(ACurrentExp, 1, Length(ACurrentExp) - 1); // remove last >
      ASearchString := Copy(ASearchString, 2, Length(ASearchString) - 1); // remove < from func name
      if Copy(ASearchString, Length(ASearchString), 1) = ',' then
        ASearchString := Copy(ASearchString, 1, Length(ASearchString) - 1);
      // remove , from func name
      Result := ParseThisExpression(ASearchString, ACurrentExp, Result);
    end;
  until nPos2 = 0;
end;

function TScanResolve.ParseThisExpression(const AFunction, AExpression, AFixedScan: string): string;
var
  sAParam, sParam1, sParam2, sParam3, sDumm1, sDumm2: string;
  sTempExp: string;
  nPos, i: Integer;
begin
  sParam1 := '';
  sParam2 := '';
  sParam3 := '';
  sTempExp := AExpression;
  i := 1;
  repeat
    nPos := Pos(',', sTempExp);
    if nPos > 0 then
    begin
      sAParam := Copy(sTempExp, 1, nPos - 1);
      sTempExp := Copy(sTempExp, nPos, Length(sTempExp));
    end
    else
    begin
      sAParam := sTempExp;
      sTempExp := '';
    end;
    if Copy(sTempExp, 1, 1) = ',' then
      sTempExp := Copy(sTempExp, 2, Length(sTempExp));
    case i of
      1:
        sParam1 := sAParam;
      2:
        sParam2 := sAParam;
      3:
        sParam3 := sAParam;
    end;
    Inc(i);
  until nPos = 0;

  if SameText(AFunction, 'CL') then
    Result := Copy(AFixedScan, StrToInt(sParam1) + 1, Length(AFixedScan))
  else if SameText(AFunction, 'CR') then
    Result := Copy(AFixedScan, 1, Length(AFixedScan) - StrToInt(sParam1))
  else if SameText(AFunction, 'CDE') then
    Result := AFixedScan + GetChkDgt(UpcE2A(Copy(AFixedScan, StrToInt(sParam1), 6)))
  else if SameText(AFunction, 'CD') then
    Result := AFixedScan + GetChkDgt(AFixedScan)
  else if SameText(AFunction, 'AL') then
    Result := sParam1 + AFixedScan
  else if SameText(AFunction, 'AR') then
    Result := AFixedScan + sParam1
  else if SameText(AFunction, 'SUBSTR') then
    Result := Copy(AFixedScan, StrToInt(sParam1), StrToInt(sParam2))
  else if SameText(AFunction, 'CUTL0') then
  begin
    Result := AFixedScan;
    while Copy(Result, 1, 1) = '0' do
      Result := Copy(Result, 2, Length(Result) - 1);
  end
  else if SameText(AFunction, 'UPCE2A') then
  begin
    sDumm1 := '';
    sDumm2 := '';
    if StrToInt(sParam1) > 1 then
      sDumm1 := Copy(AFixedScan, 1, StrToInt(sParam1) - 1);
    if Length(AFixedScan) > (6 + (StrToInt(sParam1) - 1)) then
      sDumm2 := Copy(AFixedScan, 6 + StrToInt(sParam1), Length(AFixedScan));
    Result := sDumm1 + UpcE2A(Copy(AFixedScan, StrToInt(sParam1), 6)) + sDumm2;
  end
  else if SameText(AFunction, 'UPCA2E') then
  begin
    sDumm1 := '';
    sDumm2 := '';
    if StrToInt(sParam1) > 1 then
      sDumm1 := Copy(AFixedScan, 1, StrToInt(sParam1) - 1);
    if Length(AFixedScan) > (10 + (StrToInt(sParam1) - 1)) then
      sDumm2 := Copy(AFixedScan, 11 + (StrToInt(sParam1) - 1), Length(AFixedScan));
    Result := sDumm1 + UpcA2E(Copy(AFixedScan, StrToInt(sParam1), 10)) + sDumm2;
  end
  else if SameText(AFunction, 'PADR') then
    Result := PadR(AFixedScan, StrToInt(sParam1), sParam2)
end;

function TScanResolve.UpcE2A(sStr: string): string;
begin
  Result := Copy(sStr, 1, 6);
  if Length(Result) < 6 then
    Exit;

  case Result[6] of
    '0':
      Insert('00000', Result, 3);
    '1':
      Insert('10000', Result, 3);
    '2':
      Insert('20000', Result, 3);
    '3':
      Insert('00000', Result, 4);
    '4':
      Insert('00000', Result, 5);
    '5' .. '9':
      Insert('0000', Result, 6);
  end;
  Result := Copy(Result, 1, 10);
end;

function TScanResolve.UpcA2E(sStr: string): string;
begin
  Result := Copy(sStr, 1, 10);
  if (Pos(Copy(sStr, 3, 1), '012') > 0) and (Copy(sStr, 4, 4) = '0000') then
    Result := Copy(sStr, 1, 2) + Copy(sStr, 8, 3) + Copy(sStr, 3, 1)
  else if Copy(sStr, 4, 5) = '00000' then
    Result := Copy(sStr, 1, 3) + Copy(sStr, 9, 2) + '3'
  else if Copy(sStr, 5, 5) = '00000' then
    Result := Copy(sStr, 1, 4) + Copy(sStr, 10, 1) + '4'
  else if (Pos(Copy(sStr, 10, 1), '5678') > 0) and (Copy(sStr, 6, 4) = '0000') then
    Result := Copy(sStr, 1, 5) + Copy(sStr, 10, 1);
end;

function TScanResolve.GetChkDgt(sStr: string): string;
var
  x, clc1, clc2, chkDgt: Integer;
begin
  clc1 := 0;
  clc2 := 0;

  while Length(sStr) < 12 do
    sStr := '0' + sStr;

  for x := 1 to Length(sStr) do
    if (AnsiChar(sStr[x]) in ['0' .. '9']) and (x mod 2 > 0) then
      clc1 := clc1 + StrToInt(Copy(sStr, x, 1));

  for x := 1 to Length(sStr) do
    if (AnsiChar(sStr[x]) in ['0' .. '9']) and (x mod 2 = 0) then
      clc2 := clc2 + StrToInt(Copy(sStr, x, 1));

  clc2 := clc2 * 3;

  clc1 := clc1 + clc2;
  clc2 := Trunc(Int(clc1 / 10) + 1) * 10; // goto nearest 10

  chkDgt := clc2 - clc1;
  if chkDgt = 10 then
    chkDgt := 0;
  Result := Trim(IntToStr(chkDgt));
end;

end.
