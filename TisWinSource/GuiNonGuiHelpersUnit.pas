unit GuiNonGuiHelpersUnit;

interface

type
  TGuiNonGuiHelpers = class
  private
    function GetStatus(const Index: Integer): string;
    procedure SetStatusIndex(const Index: Integer; const Value: string);
    function GetUserName: string;
  public
    property Status: string index 0 read GetStatus write SetStatusIndex;
    property Status2: string index 1 read GetStatus write SetStatusIndex;
    property Status3: string index 2 read GetStatus write SetStatusIndex;
    property UserName: string read GetUserName;
  end;

{$IFNDEF SANDBOX}
function GuiNonGuiHelpers: TGuiNonGuiHelpers;
{$ENDIF}

implementation

uses
  Classes, Forms{$IFNDEF NONGUI}, Modules, PrdHelpersUnit, ProjRepHelpers{$ENDIF}, dmDataModule,
  SysUtils, JvVersionInfo, GeneralHelpersUnit, AdsTable, PrdHelpersIUnit{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

{$IFNDEF SANDBOX}
var
  __GuiNonGuiHelpers: TGuiNonGuiHelpers;

function GuiNonGuiHelpers: TGuiNonGuiHelpers;
begin
  if not Assigned(__GuiNonGuiHelpers) then
    __GuiNonGuiHelpers := TGuiNonGuiHelpers.Create;
  Result := __GuiNonGuiHelpers;
end;
{$ENDIF}

{ TGuiNonGuiHelpers }
function TGuiNonGuiHelpers.GetStatus(const Index: Integer): string;
begin
{$IFDEF NONGUI}
  Result := '';
{$ELSE}
  case index of
    2:
      Result := ModuleInfoManager.Status2;
    3:
      Result := ModuleInfoManager.Status3;
  else
    Result := ModuleInfoManager.Status;
  end;
{$ENDIF}
end;

function TGuiNonGuiHelpers.GetUserName: string;
begin
{$IFDEF NONGUI}
  Result := '';
{$ELSE}
  Result := ModuleInfoManager.UserName;
{$ENDIF}
end;

procedure TGuiNonGuiHelpers.SetStatusIndex(const Index: Integer; const Value: string);
begin
{$IFNDEF NONGUI}
  case index of
    2:
      ModuleInfoManager.Status2 := Value;
    3:
      ModuleInfoManager.Status3 := Value;
  else
    ModuleInfoManager.Status := Value;
  end;
{$ENDIF}
end;

{$IFDEF NONGUI}
{$ELSE}
{$ENDIF}

end.
