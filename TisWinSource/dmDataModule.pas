unit dmDataModule;

interface

uses
  SysUtils, Classes, DB, adsdata, adsfunc, adstable, adsset, adscnnct;

type
  TdmData = class(TDataModule)
    POSConnection: TAdsConnection;
    LocalConnection: TAdsConnection;
    tbIncNum: TAdsTable;
    tbWorkGrps: TAdsTable;
    tbPassword: TAdsTable;
    tbWorkRigt: TAdsTable;
    ScheduleConnection: TAdsConnection;
    procedure DataModuleCreate(Sender: TObject);
    procedure LocalConnectionBeforeConnect(Sender: TObject);
    procedure POSConnectionBeforeConnect(Sender: TObject);
    procedure POSConnectionAfterConnect(Sender: TObject);
  private
    FQueryIncNum: TAdsQuery;
    function LastUsedIncNum(ATable, AField: string): Integer;
    function UserPass(const AUserID: string): string;
    procedure AddPortToPath(var APath: string);
  public
    function CreateQueryObject(AOwner: TComponent): TAdsQuery;
    function CreateTableObject(AOwner: TComponent): TAdsTable;
    function GetIncNum(ATable, AField: string; numDel: Integer = 0; SkipCheckkLastUsed: Boolean = False): Integer;
    function EnvirSetttingsTable: string;
    function LocalSetttingsTable: string;
    function EnvirSetttingsTable2: string;
    procedure AssignConnectionHandle(AConn: ADSHANDLE);
    function DatabasePath: string;
  end;

{$IFNDEF SANDBOX}
function dmData: TdmData;
{$ENDIF}

{$IFDEF TISLIBDLL}
var
  FdmData: TdmData;
{$ENDIF}

implementation

uses Math, Forms, Utils, IniFiles, GeneralHelpersUnit, JvVersionInfo{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};
{$R *.dfm}

{$IFNDEF SANDBOX}
{$IFNDEF TISLIBDLL}
var
  FdmData: TdmData;
{$ENDIF}
{$ENDIF}

procedure AdsIniPathsToUNC;
var
  strPath, strMap, strUNC, sJunk: string;
  IniFile: TIniFile;
  procedure ChangeOneIniEntry(AConnectName: string);
  begin
    sJunk := IniFile.ReadString('Databases', AConnectName, '');
    if (sJunk <> '') and SameText(strMap, Copy(sJunk, 1, Length(strMap))) then
    begin
      sJunk := StringReplace(sJunk, Copy(sJunk, 1, Length(strMap)), strUNC, []);
      IniFile.WriteString('Databases', AConnectName, sJunk);
    end;
  end;

begin
  strPath := GetAliasPathAndFileName;

  if SameText(strPath, 'ads.ini') then
    strPath := ExtractFilePath(ParamStr(0)) + 'ADS.INI';

  IniFile := TIniFile.Create(strPath);
  try
    strMap := IniFile.ReadString('Paths', 'MapName', '');
    strUNC := IniFile.ReadString('Paths', 'UNC', '');

    if strMap = '' then // get Map from AppUpdate
    begin
      sJunk := ExtractFileName(ParamStr(0));
      strMap := IniFile.ReadString('AppUpdate', sJunk, '');
      if (Length(strMap) >= 3) and (strMap[2] = ':') and (Upcase(strMap[1]) >= 'A') and (Upcase(strMap[1]) <= 'Z') then
      begin
        strMap := Copy(strMap, 1, 3);
        IniFile.WriteString('Paths', 'MapName', strMap);
      end;
    end;

    if (strMap <> '') and (strUNC = '') then
    begin
      strUNC := ExpandUNCFileName(strMap);
      if not SameText(strMap, Copy(strUNC, 1, Length(strMap))) then
        IniFile.WriteString('Paths', 'UNC', strUNC)
      else
        strUNC := '';
    end;

    if (strMap <> '') and (strUNC <> '') then
    begin
      if not SameText(strUNC, ExpandUNCFileName(strMap)) then // Server was replaced
      begin
        sJunk := strUNC;
        strUNC := ExpandUNCFileName(strMap);
        strMap := sJunk;
        IniFile.WriteString('Paths', 'UNC', strUNC)
      end;

      ChangeOneIniEntry('TisWin3DD');
      ChangeOneIniEntry('RepData');
      ChangeOneIniEntry('RepDefs');
    end;
  finally
    IniFile.Free;
  end;
end;

{$IFNDEF SANDBOX}
function dmData: TdmData;
begin
  if FdmData = nil then
  begin
    AdsIniPathsToUNC;
    FdmData := TdmData.Create(Application);
  end;
  Result := FdmData;
end;
{$ENDIF}

procedure TdmData.AddPortToPath(var APath: string);
var
  APos: Integer;
begin
  if string(Copy(APath, 1, 2)).StartsWith('\\') then
  begin
    APos := Pos(':6262', APath);
    if APos = 0 then
    begin
      APos := Pos('\', Copy(APath, 3, MaxInt)) + 2;
      if APos > 2 then
        Insert(':6262', APath, APos);
    end;
  end;
end;

procedure TdmData.AssignConnectionHandle(AConn: ADSHANDLE);
begin
  if Assigned(POSConnection) and (not POSConnection.IsConnected) then
  begin
    POSConnection := TAdsConnection.CreateWithHandle(Self, AConn);
    tbWorkGrps.AdsConnection := POSConnection;
    tbPassword.AdsConnection := POSConnection;
    tbWorkRigt.AdsConnection := POSConnection;
    tbIncNum.AdsConnection := POSConnection;
  end;
end;

function TdmData.CreateQueryObject(AOwner: TComponent): TAdsQuery;
begin
  Result := TAdsQuery.Create(AOwner);
  Result.AdsConnection := POSConnection;
  Result.AdsTableOptions.AdsCharType := OEM;
  Result.AdsTableOptions.AdsLockType := Compatible;
end;

function TdmData.CreateTableObject(AOwner: TComponent): TAdsTable;
begin
  Result := TAdsTable.Create(AOwner);
  Result.AdsConnection := POSConnection;
  Result.AdsTableOptions.AdsCharType := OEM;
  Result.AdsTableOptions.AdsLockType := Compatible;
end;

function TdmData.DatabasePath: string;
var
  APos: Integer;
begin
  Result := POSConnection.GetConnectionPath;
  APos := Pos(':6262', Result);
  if APos > 0 then
    Result := Copy(Result, 1, APos - 1) + Copy(Result, APos + 5, Length(Result));
end;

procedure TdmData.DataModuleCreate(Sender: TObject);
var
  APath: string;
  AIni: TIniFile;
begin
  //ACE.AdsSetEpoch(1960);

{$IFDEF SANDBOX}
   Exit;
{$ENDIF}

  if IsDLL then
    Exit;
  try
    POSConnection.Connect;
  except
    APath := POSConnection.GetConnectionWithDDPath;
    AddPortToPath(APath);
    if not SameText(POSConnection.GetConnectionWithDDPath, APath) then
    begin
      AIni := TIniFile.Create(ExtractFilePath(ParamStr(0)) + 'Ads.ini');
      try
        AIni.WriteString('Databases', 'TisWin3DD', APath);
        APath := AIni.ReadString('Databases', 'RepDefs', '');
        AddPortToPath(APath);
        if not SameText(AIni.ReadString('Databases', 'RepDefs', ''), APath) then
          AIni.WriteString('Databases', 'RepDefs', APath);
      finally
        AIni.Free;
      end;
      APath := POSConnection.AliasName;
      POSConnection.AliasName := '';
      POSConnection.AliasName := APath;
      ScheduleConnection.AliasName := '';
      ScheduleConnection.AliasName := APath;
    end;
  end;

  // Users
  // ADSSYS  CD@57!@#$%54321
  // User    mazel123
end;

function TdmData.GetIncNum(ATable, AField: string; numDel: Integer; SkipCheckkLastUsed: Boolean): Integer;
var
  posDBF: Integer;
  bLocked: Boolean;
  KeyName: string;
begin
  ATable := UpperCase(ATable);
  AField := UpperCase(AField);

  posDBF := Pos('.DBF', ATable);
  if posDBF = 0 then
    posDBF := Pos('.ADT', ATable);

  if posDBF > 0 then
    ATable := Copy(ATable, 1, posDBF - 1);

  KeyName := Format('%s_%s', [ATable, AField]);

  bLocked := False;
  with tbIncNum do
  begin
    if not Active then
      Open;

    if not Locate('Table', KeyName, []) then
    begin
      Insert;
      FieldByName('Table').AsString := KeyName;
      Post;
    end;

    while not bLocked do
      bLocked := AdsLockRecord(0);

    try
      if (numDel = 0) then
        Result := FieldByName('IncNum').AsInteger + 1
      else // if (numDel <> 0)
      begin
        if (numDel = FieldByName('IncNum').AsInteger) then
          Result := FieldByName('IncNum').AsInteger - 1
        else
          Result := FieldByName('IncNum').AsInteger;
      end;

      if not SkipCheckkLastUsed then
        Result := Max(Result, LastUsedIncNum(ATable, AField) + 1);

      Edit;
      FieldByName('IncNum').AsInteger := Result;
      Post;
    finally
      AdsUnlockRecord(0);
    end;
  end;
end;

function TdmData.LastUsedIncNum(ATable, AField: string): Integer;
begin
  if not Assigned(FQueryIncNum) then
    FQueryIncNum := CreateQueryObject(Self);

  Result := 0;

  with FQueryIncNum do
  begin
    SQL.Text := Format('Select Top 1 %s From %s Order By %s Desc', [AField, ATable, AField]);
    Close;
    Open;

    if not IsEmpty then
      Result := Fields[0].AsInteger;
  end;
end;

function TdmData.EnvirSetttingsTable: string;
begin
  Result := 'FsData';
end;

function TdmData.LocalSetttingsTable: string;
begin
  Result := 'LclData';
end;

function TdmData.EnvirSetttingsTable2: string;
begin
  Result := 'TisData';
end;

procedure TdmData.LocalConnectionBeforeConnect(Sender: TObject);
var
  cLocalPath: string;
begin
  cLocalPath := GetEnvVar('LOCAL_DRIVE');
  if cLocalPath <> '' then
  begin
    if cLocalPath[Length(cLocalPath)] <> '\' then
      cLocalPath := cLocalPath + '\';
    LocalConnection.ConnectPath := cLocalPath;
  end;
end;

function TdmData.UserPass(const AUserID: string): string;
var
  i: Integer;
begin
  Result := 'l' + chr(101) + chr(122) + 'a' + chr(77);
  for i := 1 to 3 do
    Result := Result + IntToStr(i);
  Result := Result + '$5' + AUserID;

  // lezaM123$5 + UserID
end;

procedure TdmData.POSConnectionAfterConnect(Sender: TObject);
var
  AAppID: string;
  ACon: TAdsConnection;
begin
  ACon := TAdsConnection(Sender);
  AAppID := QuotedStr(ExtractFileName(Application.ExeName) + ' ' + AppVerInfo.FileVersion);
  try
    ACon.Execute('Execute Procedure sp_SetApplicationID(' + AAppID + ');');
    ACon.Execute('Try EXECUTE PROCEDURE cdUpdateCrUseData(); Catch All End;');
  except
  end;
end;

procedure TdmData.POSConnectionBeforeConnect(Sender: TObject);
var
  ACon: TAdsConnection;
  APath: string;
  APos: Integer;
begin
  ACon := TAdsConnection(Sender);
  APath := ACon.GetConnectionPath;
  APos := Pos(':6262', APath);
  if APos > 0 then
    APath := Copy(APath, 1, APos - 1) + Copy(APath, APos + 5, Length(APath));
  if FileExists(APath + '\ALS.ALL') then
    ACon.AdsServerTypes := [stADS_LOCAL]
  else
    ACon.AdsServerTypes := [stADS_REMOTE];

  ACon.Username := 'User' + GenHelpers.StationCode;
  ACon.Password := UserPass(GenHelpers.StationCode);
end;

end.
