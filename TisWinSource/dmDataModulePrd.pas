unit dmDataModulePrd;

interface

uses
  SysUtils, Classes, DB, adsdata, adstable, adsfunc;

type
  TMarkUp = (muMarkup, muProfitModg);

  TdmDataPrd = class(TDataModule)
    tbPrdmstr: TAdsTable;
    tbPrdmstrList: TAdsTable;
    procedure DataModuleCreate(Sender: TObject);
    procedure tbPrdmstrAfterOpen(DataSet: TDataSet);
  private
  public
    procedure tbPrdmstrDataSetBeforePost(DataSet: TDataSet);
  end;

{$IFNDEF SANDBOX}

function dmDataPrd: TdmDataPrd;
{$ENDIF}

implementation

uses DataHelpersUnit, Variants, Forms, {$IFNDEF SANDBOX}dmDataModule{$ELSE}ServerMethodsUnit1{$ENDIF};

{$R *.dfm}
{$IFNDEF SANDBOX}

var
  FdmDataPrd: TdmDataPrd;

function dmDataPrd: TdmDataPrd;
begin
  if FdmDataPrd = nil then
    FdmDataPrd := TdmDataPrd.Create(Application);
  Result := FdmDataPrd;
end;
{$ENDIF}

procedure TdmDataPrd.DataModuleCreate(Sender: TObject);
begin
  tbPrdmstr.AdsConnection := dmData.POSConnection;
  tbPrdmstrList.AdsConnection := dmData.POSConnection;
end;

procedure TdmDataPrd.tbPrdmstrAfterOpen(DataSet: TDataSet);
begin
  DataSet.BeforePost := tbPrdmstrDataSetBeforePost;
end;

procedure TdmDataPrd.tbPrdmstrDataSetBeforePost(DataSet: TDataSet);
var
  AField: TField;
begin
  AField := DataSet.FindField('Cost');
  if not VarIsLikeNull(AField.OldValue) then
    DataSet.FieldByName('CstLast').AsCurrency := AField.OldValue;

  if not VarIsLikeNull(AField.OldValue) and (AField.Value <> AField.OldValue) then
  begin
    DataSet.FieldByName('CstLast').AsCurrency := AField.OldValue;

    if AField.Value > DataSet.FieldByName('CstMst').AsCurrency then
      DataSet.FieldByName('CstMst').AsCurrency := AField.Value;
    if AField.Value < DataSet.FieldByName('CstLeast').AsCurrency then
      DataSet.FieldByName('CstLeast').AsCurrency := AField.Value;

    DataSet.FieldByName('CstLastC').AsCurrency := AField.OldValue;
  end;
  if (AField.Value <> AField.OldValue) and (AField.Value <> 0) then
    DataSet.FieldByName('CstChenge').AsDateTime := Date;

  AField := DataSet.FindField('Price');
  if (not VarIsLikeNull(AField.OldValue)) and (AField.Value <> AField.OldValue) then
    DataSet.FieldByName('LstPrice').AsCurrency := AField.OldValue;
  if (AField.Value <> AField.OldValue) and (AField.Value <> 0) then
    DataSet.FieldByName('PrChenge').AsDateTime := Date;
end;

end.
