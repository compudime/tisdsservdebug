unit VendInvHelpersUnit;

interface

uses Classes, DB, AdsData, AdsTable, System.UITypes, Winapi.Windows, GeneralHelpersUnit, System.JSON
{$IFNDEF NONGUI}, VendInvHelpersGUnit{$ENDIF};

type
{$IFNDEF NONGUI}
  TVendInvHelpersGH = class(TVendInvHelpersG)
  end;
{$ENDIF}

  TInvValidationMode = (ivmField, ivmRowSilent, ivmRow);
  TInvTransType = (ittDebit, ittCredit);
  TItemByCaseUnit = (ibcuDefault, ibcuCase, ibcuUnit, ibcuPromptEach);

  TAiItem = class
  private
    FDecimalPos: Integer;
    FAiLength: Integer;
    FIndicator: string;
    FDescription: string;
    FDataType: string;
    FValue: string;
    FHasDecimal: Boolean;
    FIndicatorLength: Integer;
    FValuePos: Integer;

    procedure SetValue(const Value: string);
    procedure SetIndicator(const Value: string);
    procedure SetHasDecimal(const Value: Boolean);
  public
    property Indicator: string read FIndicator write SetIndicator;
    property IndicatorLength: Integer read FIndicatorLength;
    property Description: string read FDescription write FDescription;
    property AiLength: Integer read FAiLength write FAiLength;
    property DataType: string read FDataType write FDataType;
    property HasDecimal: Boolean read FHasDecimal write SetHasDecimal;
    property DecimalPos: Integer read FDecimalPos write FDecimalPos;
    property ValuePos: Integer read FValuePos;
    property Value: string read FValue write SetValue;
  end;

  TAiItemList = class(TStringList) // class to hold pointer to each of TAiItem
  private
    FIndicatorsFound: Integer;
    function GetPItem(Index: Integer): TAiItem;
    function AddAiItem(const AIndicator, ADescription, ADataType: string; AAiLength: Integer;
      AHasDecimal: Boolean): TAiItem;
    procedure ClearAllValues;
    procedure FixCustomCodes(var ACode: string);
  public
    property PItem[Index: Integer]: TAiItem read GetPItem; default;
    property IndicatorsFound: Integer read FIndicatorsFound;

    constructor Create;
    destructor Destroy; override;
    function ItemByAI(const AAI: string): TAiItem;
    procedure ProcessBarCode(ACode: string);
  end;

  TVendInvHelpers = class
  private
    FAmountSummary: Currency;
    FCountSummary: Currency;
    FOrigCostSummary: Currency;
    FDiscountSummary: Currency;
    FPrintAmountSummary: Currency;
    FDataPosted: Boolean;
    FDelID: string;
    FVendID: string;
    FInternalDetailDataChange: Boolean;
    FNextDelLine: Integer;
    FInternalFieldChange: Boolean;
    FVendEmailAddress: string;
    FUseUpcharge1: Boolean;
    FUseUpcharge2: Boolean;
    FUseUpcharge3: Boolean;
    FUseUpcharge4: Boolean;
    FUseUpcharge5: Boolean;
    UpchargeLineFactor: array [1 .. 5] of Double;
    FInvTransType: TInvTransType;
    FNoLstVend: Boolean;

    FRecCountAlertSave: Integer;
    FRecCountLastSave: Integer;
    FOnDetailSummaryChange: TNotifyEvent;
    FOnRecCountAlertSave: TNotifyEvent;
    FOnPrdCostChange: TFieldNotifyEvent;
    FOnPrdPriceChange: TFieldNotifyEvent;
    FCalcVendInvItemCostParamList: TStrings;
    FCalcVendInvItemCostEventList: TStrings;
    FOnInvoiceCostChange: TFieldNotifyEvent;
    FOnInvoiceUpcChange: TFieldNotifyEvent;
    FUpdateUpcCodeDefaults: Boolean;
    FOnItemCodeRelationCancel: TFieldNotifyEvent;
    FPrdmstrFieldList: TStringList;

    FPrdmstrNoEditFieldList: TStringList;
    FOnInvoicePrdmstrChange: TFieldNotifyEvent;
    FDetailDeleteList: TStringList;
    FForceVendInvNum: Boolean;
    FCostThisDelvOnly: Boolean;
    FBillDueDays: Integer;
    FInvValidationMode: TInvValidationMode;
    FOnInvTransTypeChange: TNotifyEvent;
    FOpenedInvTransType: TInvTransType;
    FItemByCaseUnit: TItemByCaseUnit;
    PrdUpdateList: TStringList;
    FLatestInvoiceDate: TDate;
    FInvoiceDateChanged: Boolean;
    FInternalNoThisDlvOnly: Boolean;
    FqInvoicePrdmstr2: TAdsQuery;
    FInternalNoUpdateToPrdmstr: Boolean;
    FInternalUpdateDiscount: Boolean;

    procedure StartNewInvoice;
    procedure InvoiceToVendHist;
    procedure PostInventoryChanges(InvoicingNow: Boolean = False);
    procedure CalcRowAmount(DS: TDataSet);
    procedure CalcRowPrintAmount(DS: TDataSet);
    procedure UpdateCostLevelValue;
    procedure UpdateCostFlagValue;
    procedure SetInternalFieldChange(const Value: Boolean);
    procedure SetVendID(const Value: string);

    procedure LoadCalcVendInvItemCostValues;
    procedure LoadCalcVendInvItemCostEvents;

    procedure qInvoiceHeadAfterPost(DataSet: TDataSet);
    procedure qInvoiceHeaderVenInvNoChange(Sender: TField);
    procedure qInvoiceHeaderDiscountChange(Sender: TField);
    procedure qInvoiceHeaderDateChange(Sender: TField);
    procedure qInvoiceHeaderEnhancedDiscountChange(Sender: TField);
    procedure qInvoiceHeaderPrintDiscountChange(Sender: TField);

    procedure qInvoiceDetailAfterDelete(DataSet: TDataSet);
    procedure qInvoiceDetailAfterInsert(DataSet: TDataSet);
    procedure qInvoiceDetailAfterPost(DataSet: TDataSet);
    procedure qInvoiceDetailBeforeDelete(DataSet: TDataSet);
    procedure qInvoiceDetailBeforeInsert(DataSet: TDataSet);

    procedure qInvoiceDetailCostChange(Sender: TField);
    procedure qInvoiceDetailOrigCostChange(Sender: TField);
    procedure qInvoiceDetailUnitCostChange(Sender: TField);
    procedure qInvoiceDetailUPCC_Change(Sender: TField);
    procedure qInvoiceDetailUpcCodeChange(Sender: TField);
    procedure qInvoiceDetailQtyFilledPriceChange(Sender: TField);
    procedure qInvoiceDetailGeneralChange(Sender: TField);
    procedure qInvoiceDetailPrdmstrChange(Sender: TField);
    procedure qInvoiceDetailPrintCostChange(Sender: TField);

    procedure qInvoiceChangesToPrdmstr(Sender: TField);
    procedure qInvoiceCostChangeToPrdmstr;
    procedure qInvoiceCostChangeToPrdmstrDiscount;

    procedure qInvoiceHDCalcVendInvItemCost(Sender: TField);
    procedure qInvoiceDTCalcVendInvItemCost(Sender: TField);

    procedure DoUpdateUpcCodeDefaults(Sender: TField; AVenCodeLookup: Boolean);

    function PrdmstrFieldString(ANoAlias: Boolean): string;
    function PostFlagModeRecord: Boolean;
    procedure DefaultUpchargeValues;
    procedure LoadUpchargeLineFactor;
    procedure DetailAmntDelAbsSwitch;
    procedure DetailAllUpdated;
    procedure SetInvTransType(const Value: TInvTransType);
    function LastCostForProduct(AUpcCode, ACurDelID: string; ANetCost: Boolean; AUPC: Integer): Currency;
    procedure DoUpdateLastCostPrdInfo;
    procedure VendItemValues_GetSet(var AUPC: Integer; var ACost: Currency; ANoSet: Boolean = True);

    function PrintDiscountLineFactor: Double;
    procedure DoEmailVendInvReport(const AVendID, ADelID, ADelIDList, ASQLFilter, ALineID, AEmailRecipients: string);
{$IFNDEF NONGUI}
    function VIGH: TVendInvHelpersGH;
{$ENDIF}
    // GUI
    procedure PrdmstrFieldOnChangeAssign;
    procedure qInvoiceDetailVenItemCodeChange(Sender: TField);
  protected
    FInternalDetailFieldChange: Boolean;
    FUpchargeMulti: Boolean;
    UpchargePercentOnly: Boolean;
    FUpchargeCalcNeeded: Boolean;
    FInternalPrdmstrDataChange: Boolean;

    property RecCountLastSave: Integer read FRecCountLastSave write FRecCountLastSave;
    property OnPrdCostChange: TFieldNotifyEvent read FOnPrdCostChange write FOnPrdCostChange;
    property OnPrdPriceChange: TFieldNotifyEvent read FOnPrdPriceChange write FOnPrdPriceChange;
    property InternalFieldChange: Boolean read FInternalFieldChange write SetInternalFieldChange;
    property UpchargeCalcNeeded: Boolean read FUpchargeCalcNeeded write FUpchargeCalcNeeded;
    property OpenedInvTransType: TInvTransType read FOpenedInvTransType write FOpenedInvTransType;
    property LatestInvoiceDate: TDate read FLatestInvoiceDate write FLatestInvoiceDate;
    property InvoiceDateChanged: Boolean read FInvoiceDateChanged write FInvoiceDateChanged;
    property InternalNoThisDlvOnly: Boolean read FInternalNoThisDlvOnly write FInternalNoThisDlvOnly;
    property InternalUpdateDiscount: Boolean read FInternalUpdateDiscount write FInternalUpdateDiscount;
    property NextDelLine: Integer read FNextDelLine;

    procedure CreateQueries;
    procedure qInvoiceBeforePost(DataSet: TDataSet);
    procedure DisableQueryControls;
    procedure EnableQueryControls;
  public
    qInvoiceHead: TAdsQuery;
    qInvoiceDetail: TAdsQuery;
    qInvoicePrdmstr: TAdsQuery;
    function qInvoicePrdmstr2: TAdsQuery;

    property DataPosted: Boolean read FDataPosted write FDataPosted;
    property VendID: string read FVendID write SetVendID;
    property DelID: string read FDelID write FDelID;
    property AmountSummary: Currency read FAmountSummary;
    property CountSummary: Currency read FCountSummary;
    property OrigCostSummary: Currency read FOrigCostSummary;
    property DiscountSummary: Currency read FDiscountSummary;
    property PrintAmountSummary: Currency read FPrintAmountSummary;
    property OnDetailSummaryChange: TNotifyEvent read FOnDetailSummaryChange write FOnDetailSummaryChange;
    property OnRecCountAlertSave: TNotifyEvent read FOnRecCountAlertSave write FOnRecCountAlertSave;
    property RecCountAlertSave: Integer read FRecCountAlertSave;
    property OnInvoiceCostChange: TFieldNotifyEvent read FOnInvoiceCostChange write FOnInvoiceCostChange;
    property OnInvoiceUpcChange: TFieldNotifyEvent read FOnInvoiceUpcChange write FOnInvoiceUpcChange;
    property OnInvoicePrdmstrChange: TFieldNotifyEvent read FOnInvoicePrdmstrChange write FOnInvoicePrdmstrChange;
    property UpdateUpcCodeDefaults: Boolean read FUpdateUpcCodeDefaults write FUpdateUpcCodeDefaults;
    property OnItemCodeRelationCancel: TFieldNotifyEvent read FOnItemCodeRelationCancel write FOnItemCodeRelationCancel;
    property VendEmailAddress: string read FVendEmailAddress;
    property ForceVendInvNum: Boolean read FForceVendInvNum;
    property CostThisDelvOnly: Boolean read FCostThisDelvOnly;
    property InvValidationMode: TInvValidationMode read FInvValidationMode write FInvValidationMode;
    property UseUpcharge1: Boolean read FUseUpcharge1;
    property UseUpcharge2: Boolean read FUseUpcharge2;
    property UseUpcharge3: Boolean read FUseUpcharge3;
    property UseUpcharge4: Boolean read FUseUpcharge4;
    property UseUpcharge5: Boolean read FUseUpcharge5;
    property InvTransType: TInvTransType read FInvTransType write SetInvTransType;
    property OnInvTransTypeChange: TNotifyEvent read FOnInvTransTypeChange write FOnInvTransTypeChange;
    property ItemByCaseUnit: TItemByCaseUnit read FItemByCaseUnit write FItemByCaseUnit;
    property InternalNoUpdateToPrdmstr: Boolean read FInternalNoUpdateToPrdmstr write FInternalNoUpdateToPrdmstr;
    property NoLstVend: Boolean read FNoLstVend;

    procedure CancelQueries;
    procedure OpenQueries;
    procedure PostQueries;
    procedure PostEditRecords;

    function qInvoicePrdmstrMoveToCode(ACode: string; AReload: Boolean = False): Boolean;
    function qInvoicePrdmstr2MoveToCode(ACode: string): Boolean;
    function DeleteInvoiceData: Boolean;
    procedure InsertLine;
    procedure CalcAmountSummary;
    procedure SwitchLines(AForward: Boolean);
    function PrdmstrFieldList: TStringList;
    procedure ValidateInvoiceRow;
    procedure CalculateEnhancedDiscountsAll;
    function CalculateEnhancedDiscountsRow(AOrigCost: Currency; const AMode: Word = vkAdd): Currency;
    function CalculatePrintDiscountsRow(AOrigCost: Currency; ALineFactor: Double; const AMode: Word = vkAdd): Currency;
    procedure DoAddItemsFrom_TempDelItems(AImportCost: Boolean);
    function NewVendorInvoiceWithAttach(const AVendID, AVendInvoiceNum, ADocFile, ADocDescription: string;
      var AError: string): Integer;
    procedure DoGenerateVendorCredits(const AFilterStr, ALineIdList: string; var ACreatedInvList: string);
    procedure NewVendorInvoice(JSONRequest, JSONResponse: TJSONObject);

    constructor Create;
    destructor Destroy; override;
  end;

{$IFNDEF SANDBOX}
function VendInvHelpers: TVendInvHelpers;
{$ENDIF}

const
  EmptyPrdCode = '__No_Such_Product_Code';

implementation

uses SysUtils, dmDataModule, DataHelpersUnit, VendHelpersIUnit, PrdHelpersIUnit, Utils, dmDataModulePrd, Variants,
  IniFiles, DateUtils, StrUtils, System.Math, System.Generics.Collections, Forms, GuiNonGuiHelpersUnit,
  ProjHelpersIUnit{$IFDEF SANDBOX}, ServerMethodsUnit1{$ENDIF};

{$IFNDEF SANDBOX}
var
  __VendInvHelpers: TVendInvHelpers;

function VendInvHelpers: TVendInvHelpers;
begin
  if not Assigned(__VendInvHelpers) then
    __VendInvHelpers := TVendInvHelpers.Create;
  Result := __VendInvHelpers;
end;
{$ENDIF}

{$I PasRight.inc}

{ TAiItemList }
function TAiItemList.AddAiItem(const AIndicator, ADescription, ADataType: string; AAiLength: Integer;
  AHasDecimal: Boolean): TAiItem;
begin
  if Assigned(ItemByAI(AIndicator)) then
    raise Exception.CreateFmt('Error creating AI Item %s, Indicator already in list.', [AIndicator]);

  Result := TAiItem.Create;
  with Result do
  begin
    Indicator := AIndicator;
    Description := ADescription;
    DataType := ADataType;
    AiLength := AAiLength;
    HasDecimal := AHasDecimal;
  end;
  AddObject(AIndicator, Result);
end;

procedure TAiItemList.ClearAllValues;
var
  x: Integer;
begin
  for x := 0 to Pred(Count) do
  begin
    TAiItem(Objects[x]).Value := '';
    TAiItem(Objects[x]).DecimalPos := -1;
  end;
end;

constructor TAiItemList.Create;
var
  AList: TAiItemList;
begin
  AList := inherited Create;
  AList.AddAiItem('01', 'SCC-14', 'String', 14, False);
  AList.AddAiItem('11', 'Production Date', 'Date', 6, False);
  AList.AddAiItem('13', 'Packaging Date', 'Date', 6, False);
  AList.AddAiItem('15', 'Sell By Date', 'Date', 6, False);
  AList.AddAiItem('17', 'Expiration Date', 'Date', 6, False);
  AList.AddAiItem('21', 'Serial Number', 'String', 20, False);
  AList.AddAiItem('320', 'Net Weight', 'Numeric', 6, True);
end;

destructor TAiItemList.Destroy;
  procedure FreeAiItems;
  var
    x: Integer;
  begin
    for x := 0 to Pred(Count) do
      Objects[x].Free;
  end;

begin
  FreeAiItems;
  inherited;
end;

procedure TAiItemList.FixCustomCodes(var ACode: string);
begin
  if Copy(ACode, 1, 2) = '99' then // Canadian
    ACode := '3200' + Copy(ACode, 9, 4) + '01' + Copy(ACode, 13, 4) + '11' + Copy(ACode, 17, 6);
end;

function TAiItemList.GetPItem(Index: Integer): TAiItem;
begin
  Result := TAiItem(Objects[Index]);
end;

function TAiItemList.ItemByAI(const AAI: string): TAiItem;
var
  Index: Integer;
begin
  Result := nil;
  Index := IndexOf(AAI);
  if Index >= 0 then
    Result := TAiItem(Objects[Index]);
end;

procedure TAiItemList.ProcessBarCode(ACode: string);
var
  AOrigCode: string;
  x: Integer;
  AItem: TAiItem;
  AFoundIndicator: Boolean;
begin
  ClearAllValues;

  if Copy(ACode, 1, 3) = ']C1' then
    ACode := Copy(ACode, 4, 500);
  AOrigCode := ACode;
  FIndicatorsFound := 0;

  FixCustomCodes(ACode);

  while ACode <> '' do
  begin
    AFoundIndicator := False;
    for x := 0 to Pred(Count) do
    begin
      AItem := TAiItem(Objects[x]);
      if AItem.Indicator = Copy(ACode, 1, AItem.IndicatorLength) then
      begin
        AFoundIndicator := True;
        Inc(FIndicatorsFound);
        if AItem.HasDecimal then
          AItem.DecimalPos := StrToInt(Copy(ACode, AItem.IndicatorLength + 1, 1));
        AItem.Value := Copy(ACode, AItem.ValuePos, AItem.AiLength);
        ACode := Copy(ACode, AItem.ValuePos + AItem.AiLength, Length(ACode));
      end;
    end;
    if not AFoundIndicator then
      ACode := '';
  end;
end;

{ TAiItem }

procedure TAiItem.SetHasDecimal(const Value: Boolean);
begin
  if FHasDecimal = Value then
    Exit;

  FHasDecimal := Value;

  if FHasDecimal then
    FValuePos := FIndicatorLength + 2
  else
    FValuePos := FValuePos + 1;
end;

procedure TAiItem.SetIndicator(const Value: string);
begin
  FIndicator := Value;
  FIndicatorLength := Length(FIndicator);

  if FHasDecimal then
    FValuePos := FIndicatorLength + 2
  else
    FValuePos := FIndicatorLength + 1;
end;

procedure TAiItem.SetValue(const Value: string);
var
  AYear, AMonth, ADay: Word;
begin
  FValue := Value;
  if FValue = '' then
    Exit;

  if HasDecimal and (DecimalPos > 0) then
    Insert('.', FValue, (AiLength + 1) - DecimalPos); // +1 for 0 based Index
  if SameText(FDataType, 'Date') then
  begin
    AYear := StrToInt(Copy(FValue, 1, 2));
    AMonth := StrToInt(Copy(FValue, 3, 2));
    ADay := StrToInt(Copy(FValue, 5, 2));
    if AYear >= 51 then
      AYear := 1900 + AYear
    else
      AYear := 2000 + AYear;
    if IsValidDate(AYear, AMonth, ADay) then
      FValue := DateToStr(EncodeDate(AYear, AMonth, ADay))
    else if IsValidDate(AYear, ADay, AMonth) then
      FValue := DateToStr(EncodeDate(AYear, ADay, AMonth))
    else
      FValue := '';
  end;
end;

{$IFNDEF NONGUI}

function TVendInvHelpers.VIGH: TVendInvHelpersGH;
begin
  Result := TVendInvHelpersGH(VendInvHelpersG);
end;
{$ENDIF}

{ TVendInvHelpers }
procedure TVendInvHelpers.DetailAllUpdated;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  // If Type change within invoice __Updated needs to be flagged as well
  AQuery.SQL.Text := 'Update #InvoiceDT_Edit Set __Updated=True;';
  AQuery.ExecSQL;
end;

procedure TVendInvHelpers.DetailAmntDelAbsSwitch;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Update #InvoiceDT_Edit Set AmntDel=AmntDel*-1;';
  AQuery.ExecSQL;
end;

procedure TVendInvHelpers.DisableQueryControls;
begin
  if Assigned(qInvoicePrdmstr) then
  begin
    qInvoiceHead.DisableControls;
    qInvoiceDetail.DisableControls;
    qInvoicePrdmstr.DisableControls;
  end;
  if Assigned(FqInvoicePrdmstr2) then
    FqInvoicePrdmstr2.DisableControls;
end;

procedure TVendInvHelpers.CalcAmountSummary;
var
  AQuery: TAdsQuery;
const
  SumQuery =
    'Select Sum(Amount) Amount, Sum(AmntDel) AmntDel, Sum(AmntDel*OrigCst) OrigCst, Sum(PrintAmount) PrintAmount From #InvoiceDT_Edit;';
begin
  if qInvoiceDetail.ControlsDisabled then
    Exit;

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := SumQuery;
  AQuery.Open;
  FAmountSummary := AQuery.FieldByName('Amount').AsCurrency * IIF(InvTransType = ittCredit, -1, 1);
  FCountSummary := AQuery.FieldByName('AmntDel').AsCurrency;
  FOrigCostSummary := AQuery.FieldByName('OrigCst').AsCurrency * IIF(InvTransType = ittCredit, -1, 1);
  FDiscountSummary := AQuery.FieldByName('Amount').AsCurrency - AQuery.FieldByName('OrigCst').AsCurrency;
  FPrintAmountSummary := AQuery.FieldByName('PrintAmount').AsCurrency * IIF(InvTransType = ittCredit, -1, 1);
end;

procedure TVendInvHelpers.CalcRowAmount(DS: TDataSet);
begin
  if not(DS.State in [dsEdit, dsInsert]) then
    raise Exception.Create('Can''t "CalcRowAmount" when not in Insert/Edit mode.');

  DS.FieldByName('Amount').AsCurrency := DS.FieldByName('AmntDel').AsCurrency * DS.FieldByName('Cost').AsCurrency;
end;

procedure TVendInvHelpers.CalcRowPrintAmount(DS: TDataSet);
var
  ALineFactor: Double;
begin
  if not(DS.State in [dsEdit, dsInsert]) then
    raise Exception.Create('Can''t "CalcRowPrintAmount" when not in Insert/Edit mode.');

  ALineFactor := PrintDiscountLineFactor;
  DS.FieldByName('PrintNet').AsCurrency := CalculatePrintDiscountsRow(qInvoiceDetail.FieldByName('PrintCost')
    .AsCurrency, ALineFactor);
  DS.FieldByName('PrintAmount').AsCurrency := DS.FieldByName('PrintNet').AsCurrency * DS.FieldByName('AmntDel')
    .AsCurrency;
end;

procedure TVendInvHelpers.CalculateEnhancedDiscountsAll;
var
  APos: TBookmark;
  ACalcCost: Double;
  AOldUpchargePercentOnly: Boolean;
begin
  if InternalFieldChange or FInternalDetailFieldChange or InternalUpdateDiscount then
    Exit;

  if not FUpchargeMulti then
    Exit;

  if qInvoiceDetail.Active then
    qInvoiceDetail.CheckBrowseMode;
  CalcAmountSummary;
  LoadUpchargeLineFactor;
  AOldUpchargePercentOnly := UpchargePercentOnly;
  UpchargePercentOnly := True;

  // AppActions.ShowHourGlassCursor;
  APos := qInvoiceDetail.GetBookmark;
  // InternalFieldChange := True;
  FInternalDetailFieldChange := True;
  InternalUpdateDiscount := True;
  DisableQueryControls;
  try
    qInvoiceDetail.First;
    while not qInvoiceDetail.Eof do
    begin
      ACalcCost := CalculateEnhancedDiscountsRow(qInvoiceDetail.FieldByName('OrigCst').AsCurrency);
      if qInvoiceDetail.FieldByName('Cost').AsCurrency <> ACalcCost then
      begin
        DataHelpers.MakeEdtMode(qInvoiceDetail);
        qInvoiceDetailOrigCostChange(qInvoiceDetail.FieldByName('OrigCst'));
      end;
      qInvoiceDetail.Next;
    end;
    qInvoiceDetail.GotoBookmark(APos);
  finally
    FInternalDetailFieldChange := False;
    InternalUpdateDiscount := False;
    // InternalFieldChange := False;
    EnableQueryControls;
    qInvoiceDetail.FreeBookmark(APos);
    // AppActions.HideHourGlassCursor;
    UpchargePercentOnly := AOldUpchargePercentOnly;
  end;
  UpchargeCalcNeeded := False;
  CalcAmountSummary;
  ACalcCost := CalculateEnhancedDiscountsRow(FOrigCostSummary);
  DataHelpers.MakeEdtMode(qInvoiceHead);
  qInvoiceHead.FieldByName('Adjust').AsCurrency := ACalcCost - AmountSummary;
  if Assigned(FOnDetailSummaryChange) then
    FOnDetailSummaryChange(nil);
end;

function TVendInvHelpers.CalculateEnhancedDiscountsRow(AOrigCost: Currency; const AMode: Word): Currency;
var
  I: Integer;
begin
  Result := AOrigCost;
  for I := 1 to 5 do
    if AMode <> vkSubtract then
      Result := Result + (AOrigCost * UpchargeLineFactor[I])
    else
      Result := Result + ((AOrigCost / (1 + UpchargeLineFactor[I])) - AOrigCost);
end;

function TVendInvHelpers.CalculatePrintDiscountsRow(AOrigCost: Currency; ALineFactor: Double; const AMode: Word)
  : Currency;
begin
  Result := AOrigCost;
  if AMode <> vkSubtract then
    Result := Result + (AOrigCost * ALineFactor)
  else
    Result := Result + ((AOrigCost / (1 + ALineFactor)) - AOrigCost);
end;

procedure TVendInvHelpers.CancelQueries;
begin
  PostEditRecords;
  DataHelpers.RunSimpleQuery('Delete From #InvoiceDT_Edit; Delete From #InvoiceHD_Edit; Delete From #InvoicePRD_Edit;');
  FDetailDeleteList.Clear;
  FDataPosted := True;
  GuiNonGuiHelpers.Status2 := '';
end;

constructor TVendInvHelpers.Create;
var
  ADiscCaption: string;
begin
  PrdUpdateList := TStringList.Create;
  PrdUpdateList.Sorted := True;
  PrdUpdateList.Duplicates := dupIgnore;
  CreateQueries;
  FRecCountAlertSave := ProgPref('VendInvAutoSave');
  FDetailDeleteList := TStringList.Create;
  FForceVendInvNum := ProgPref('ForceVendInvNum');
  ADiscCaption := ProgPref('VendInvUpcharge1Caption');
  if ADiscCaption = '' then
    GenHelpers.ProgPrefStore('VendInvUpcharge1Caption', GlobalSetName, 'Discount / Upcharge');

  ADiscCaption := ProgPref('VendInvPrintUpcharge1Caption');
  if ADiscCaption = '' then
    GenHelpers.ProgPrefStore('VendInvPrintUpcharge1Caption', GlobalSetName, 'Print Discount / Upcharge');

  FUpchargeMulti := GenHelpers.IsModuleRegistered(MODULE_EDIT_VEND_INV_B);
  if FUpchargeMulti then
  begin
    FUseUpcharge1 := True;
    FUseUpcharge2 := ProgPref('VendInvUpcharge2Caption') <> '';
    FUseUpcharge3 := ProgPref('VendInvUpcharge3Caption') <> '';
    FUseUpcharge4 := ProgPref('VendInvUpcharge4Caption') <> '';
    FUseUpcharge5 := ProgPref('VendInvUpcharge5Caption') <> '';
  end;
  FBillDueDays := ProgPref('VendInvBillDueDays');
end;

procedure TVendInvHelpers.CreateQueries;
const
  HeadCreate = 'Select d.*, False __Updated Into #InvoiceHD_Edit From Delvry d Where DelID='''';';
  DetailCreate = 'Select d.*, Cast(0 AS SQL_MONEY) Prd_UnitCost, Cast(0 AS SQL_MONEY) Prd_OldUnitCost, '
    + 'Cast(0 AS SQL_NUMERIC(20,6)) __PrcntOfTotal, False __Updated Into #InvoiceDT_Edit From DelItm d Where DelID='''';';
  DetailCreateWPrd = 'Select d.*, %s ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) Prd_PriceUp, Cast(0 AS SQL_NUMERIC(12,2)) Prd_PriceGin, ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) Prd_SaleUp, Cast(0 AS SQL_NUMERIC(12,2)) Prd_SaleGin, ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) Prd_OnHand, Cast(0 AS SQL_MONEY) Prd_UnitCost, ' +
    'Cast(0 AS SQL_MONEY) Prd_OldUnitCost, Cast(0 AS SQL_NUMERIC(20,6)) __PrcntOfTotal, ' +
    'False __Updated Into #InvoiceDT_Edit From DelItm d ' +
    'Left Outer Join Prdmstr p ON (Upper(p.Code)=d.UpcCode) Where DelID='''';';
  PrdmstrCreate = 'Select * From Prdmstr Where Upper(Code)=:UpcCode;';
  { When editing PrdmstrCreate2 SQL, we also need to look at the Insert Statement in OpenQueries }
  InvenCreate = 'Select e.LineID, e.UpcCode, e.AmntDel EQty, o.AmntDel OQty, ' +
    'e.Upcc EUpcc, o.Upcc OUpcc Into #VendInvcInvAdjust ' +
    'From #InvoiceDT_Edit e Left Outer Join DelItm o On e.DelID=o.DelID ' + 'Where e.LineID=''''; ';
  InvenLocCreate = 'Select eh.InvLocation, e.LineID, e.UpcCode, e.AmntDel Qty, e.Upcc Upcc ' +
    'Into #VendInvcLocInvAdjust From #InvoiceHD_Edit eh, #InvoiceDT_Edit e Where e.LineID=''''; ';
var
  AQuery: TAdsQuery;
  APrdList: string;
begin
  AQuery := DataHelpers.HelperQuery;

  if not dmDataPrd.tbPrdmstr.Active then
    dmDataPrd.tbPrdmstr.Open;
  if dmDataPrd.tbPrdmstr.FindField('Price') <> nil then
    OnPrdPriceChange := dmDataPrd.tbPrdmstr.FieldByName('Price').OnChange;
  if dmDataPrd.tbPrdmstr.FindField('Cost') <> nil then
    OnPrdCostChange := dmDataPrd.tbPrdmstr.FieldByName('Cost').OnChange;

  qInvoiceHead := dmData.CreateQueryObject(Application);
  qInvoiceDetail := dmData.CreateQueryObject(Application);
  qInvoicePrdmstr := dmData.CreateQueryObject(Application);

  qInvoiceHead.RequestLive := True;
  qInvoiceHead.SQL.Text := HeadCreate;
  qInvoiceHead.Open;

  qInvoiceDetail.RequestLive := True;
  // qInvoiceDetail.Sequenced := True;
  if PrdmstrFieldList.Count = 0 then
    qInvoiceDetail.SQL.Text := DetailCreate
  else
  begin
    APrdList := PrdmstrFieldString(False);
    qInvoiceDetail.SQL.Text := Format(DetailCreateWPrd, [APrdList]);
  end;
  qInvoiceDetail.Open;

  qInvoicePrdmstr.RequestLive := True;
  qInvoicePrdmstr.SQL.Text := PrdmstrCreate;
  qInvoicePrdmstr.Prepare;
  qInvoicePrdmstr.ParamByName('UpcCode').AsString := EmptyPrdCode;
  qInvoicePrdmstr.Open;

  qInvoicePrdmstr2MoveToCode(EmptyPrdCode);

  AQuery.SQL.Text := InvenCreate;
  VendHelpersI.UpdateVendBalanceEtcPrepare(AQuery);
  AQuery.ExecSQL;

  AQuery.SQL.Text := InvenLocCreate;
  AQuery.ExecSQL;

  qInvoiceHead.AfterPost := qInvoiceHeadAfterPost;
  qInvoiceDetail.AfterInsert := qInvoiceDetailAfterInsert;
  qInvoiceDetail.BeforeInsert := qInvoiceDetailBeforeInsert;
  qInvoiceDetail.AfterPost := qInvoiceDetailAfterPost;
  qInvoiceDetail.AfterDelete := qInvoiceDetailAfterDelete;
  qInvoiceDetail.BeforeDelete := qInvoiceDetailBeforeDelete;

  qInvoiceHead.BeforePost := qInvoiceBeforePost;
  qInvoiceDetail.BeforePost := qInvoiceBeforePost;
  qInvoicePrdmstr.BeforePost := dmDataPrd.tbPrdmstrDataSetBeforePost;

  LoadCalcVendInvItemCostValues;

  if ProgPref('DebugInventoryChanges') then
  begin
    AQuery.SQL.Clear;
    AQuery.SQL.Add('Try Create Table TrkInv (');
    AQuery.SQL.Add('RecordedOn TimeStamp, WSName CIChar( 50 ), DelID Char( 14 ), UpcCode Char( 12 ), ');
    AQuery.SQL.Add
      ('OnHandPre Numeric( 9 ,2 ), OnHandChange Numeric( 9 ,2 ), OnHandPost Numeric( 9 ,2 )); Catch All End;');
    AQuery.ExecSQL;
  end;
end;

procedure TVendInvHelpers.DefaultUpchargeValues;
var
  I: Integer;
  ASuf: string;
  AOldPosted: Boolean;
  AOldState: TDataSetState;
begin
  AOldPosted := FDataPosted;
  AOldState := qInvoiceHead.State;
  DataHelpers.MakeEdtMode(qInvoiceHead);
  for I := 1 to 5 do
  begin
    ASuf := Trim(IntToStr(I));
    if qInvoiceHead.FieldByName('UpDown' + ASuf).IsNull then
    begin
      if (I = 1) and (qInvoiceHead.FieldByName('Discount').AsCurrency <> 0) and
        (qInvoiceHead.FieldByName('UpAmnt1').AsCurrency = 0) then
      begin
        qInvoiceHead.FieldByName('CalcMethod1').AsInteger := 2;
        qInvoiceHead.FieldByName('UpDown1').AsInteger := IIF(qInvoiceHead.FieldByName('Discount').AsCurrency < 0, 1, 2);
        qInvoiceHead.FieldByName('UpAmnt1').AsCurrency := Abs(qInvoiceHead.FieldByName('Discount').AsCurrency);
      end
      else if ProgPref('VendInvUpcharge' + ASuf + 'UpDown') = 1 then
        qInvoiceHead.FieldByName('UpDown' + ASuf).AsInteger := 1
      else
        qInvoiceHead.FieldByName('UpDown' + ASuf).AsInteger := 2;
    end;
    if qInvoiceHead.FieldByName('CalcMethod' + ASuf).IsNull then
    begin
      if ProgPref('VendInvUpcharge' + ASuf + 'CalcMethod') = 1 then
        qInvoiceHead.FieldByName('CalcMethod' + ASuf).AsInteger := 1
      else
        qInvoiceHead.FieldByName('CalcMethod' + ASuf).AsInteger := 2;
    end;

    if qInvoiceHead.FieldByName('PrintUpDown1').IsNull then
      qInvoiceHead.FieldByName('PrintUpDown1').AsInteger := 2;

    if qInvoiceHead.FieldByName('PrintCalcMethod1').IsNull then
      qInvoiceHead.FieldByName('PrintCalcMethod1').AsInteger := 2;
  end;
  if not(AOldState in [dsEdit, dsInsert]) then
    qInvoiceHead.CheckBrowseMode;
  FDataPosted := AOldPosted;
end;

function TVendInvHelpers.DeleteInvoiceData: Boolean;
const
  DeleteSQL =
    'Delete From DelItm Where DelID=:DelID; Delete From Delvry Where DelID=:DelID; Delete From VHist Where DelID=:DelID; ';
var
  AQuery: TAdsQuery;
begin
  Result := True;
  PostEditRecords;

  AQuery := DataHelpers.HelperQuery;

  DisableQueryControls;
  try
    qInvoiceDetail.First;
    while not qInvoiceDetail.Eof do
    begin
      if (qInvoiceDetail.FieldByName('UpcCode').AsString <> '') then
        PrdUpdateList.Add(qInvoiceDetail.FieldByName('UpcCode').AsString);
      qInvoiceDetail.Next;
    end;
  finally
    EnableQueryControls;
  end;

  AQuery.AdsConnection.BeginTransaction;
  try
    if DelID <> '' then
    begin
      GenHelpers.RecordAction('VENDINV DELETE', 'Delete Vendor Invoice', 'Delvry', 'DelID', 0, 0, DelID, VendID, DelID);
      DataHelpers.RunSimpleQuery('Update #InvoiceDT_Edit Set AmntDel=0;'); // Clear Inventory
      // DataHelpers.RunSimpleQuery('Update #InvoiceDT_Edit Set Qty=0, PO=0;'); //Clear Inventory
      PostInventoryChanges;
    end;
    CancelQueries;

    if DelID <> '' then
    begin
      AQuery.SQL.Text := DeleteSQL;
      AQuery.Prepare;
      AQuery.ParamByName('DelID').AsString := DelID;
      AQuery.ExecSQL;
      VendHelpersI.UpdateVendBalanceEtc(VendID);
      InvoiceDateChanged := False;
      DoUpdateLastCostPrdInfo;
    end;
  except
    on E: Exception do
    begin
      AQuery.AdsConnection.Rollback;
      if DelID <> '' then
        OpenQueries
      else
        FDataPosted := True;
      raise;
    end;
  end;

  AQuery.AdsConnection.Commit;
  FDataPosted := True;
end;

destructor TVendInvHelpers.Destroy;
begin
  if Assigned(FPrdmstrFieldList) then
    FPrdmstrFieldList.Free;
  if Assigned(FPrdmstrNoEditFieldList) then
    FPrdmstrNoEditFieldList.Free;
  if Assigned(FCalcVendInvItemCostParamList) then
    FCalcVendInvItemCostParamList.Free;
  if Assigned(FCalcVendInvItemCostEventList) then
    FCalcVendInvItemCostEventList.Free;
  if Assigned(FDetailDeleteList) then
    FDetailDeleteList.Free;
  PrdUpdateList.Free;
  inherited Destroy;
end;

function TVendInvHelpers.PrintDiscountLineFactor: Double;
begin
  if not ProgPref('VendInvUsePrintCost') then
    Result := 0
  else
  begin
    Result := qInvoiceHead.FieldByName('PrintUpAmnt1').AsFloat;
    if Result > 0 then
    begin
      if qInvoiceHead.FieldByName('PrintUpDown1').AsInteger = 2 then // 2=Down
        Result := Result * -1;
      if qInvoiceHead.FieldByName('PrintCalcMethod1').AsInteger <> 2 then // 2=%
        Result := 0
      else // NOT SUPPORTED
        Result := Result / 100;
    end;
  end;
end;

procedure TVendInvHelpers.InvoiceToVendHist;
var
  AOrderTotal: Currency;
  ADiscTotal: Currency;
  AGrossTotal: Currency;
  AQuery: TAdsQuery;
begin
  AOrderTotal := AmountSummary + qInvoiceHead.FieldByName('Adjust').AsCurrency + qInvoiceHead.FieldByName('Adjustment')
    .AsCurrency;
  ADiscTotal := DiscountSummary;
  AGrossTotal := FOrigCostSummary;
  { TODO : calc tax }
  VendHelpersI.BillInvoice(VendID, DelID, AOrderTotal, qInvoiceHead.FieldByName('Date').AsDateTime);

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  AQuery.SQL.Add(Format('Update #InvoiceHD_Edit Set __Updated=True, BillTotal=%f, DiscAmnt=%f, GrossTotal=%f;',
    [AOrderTotal, ADiscTotal, AGrossTotal]));
  AQuery.SQL.Add
    ('Update #InvoiceHD_Edit Set FlagsCount=(Select Count(*) From #InvoiceDT_Edit Where IfNull(PostFlag,0)>0);');
  AQuery.ExecSQL;
end;

function TVendInvHelpers.LastCostForProduct(AUpcCode, ACurDelID: string; ANetCost: Boolean; AUPC: Integer): Currency;
var
  AQuery: TAdsQuery;
const
  { test version 1.5.0.1 }
  ASql = 'Select Top 1 h.Date, d.Cost, d.OrigCst, d.UPCC From DelItm d, Delvry h ' +
    'Where d.UpcCode=%s And d.DelID<>%s And h.Type=0 And d.AmntDel>=0 And d.UPCC>0 And h.DelID=d.DelID ' +
    'Order By h.Date Desc;';
  { original = ASql = 'Select Top 1 h.Date, d.Cost, d.OrigCst, d.UPCC From DelItm d Inner Join Delvry h On h.DelID=d.DelID ' +
    'Where d.DelID<>%s And d.UpcCode=%s And h.Type=0 And d.AmntDel>=0 And d.UPCC>0 Order By h.Date Desc;'; }
  { planned = ASql = 'Try Drop Table #LstCstTmp1; Catch All End; ' +
    'Select d.DelID, d.Cost, d.OrigCst, d.UPCC Into #Temp1 From DelItm d ' +
    'Where d.UpcCode=%s And d.DelID<>%s And d.AmntDel>=0 And d.UPCC>0; ' +
    'Select Top 1 d.Cost, d.OrigCst, d.UPCC From Delvry h, #LstCstTmp1 d ' +
    'Where h.DelID IN (Select d2.DelID From #LstCstTmp1 d2) And h.DelID=d.DelID And h.Type=0 ' +
    'Order By h.Date Desc;'; }
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format(ASql, [QuotedStr(AUpcCode), QuotedStr(ACurDelID)]);
  AQuery.Open;

  Result := 0;
  if not AQuery.IsEmpty then
  begin
    if AUPC = 0 then
      AUPC := AQuery.FieldByName('UPCC').AsInteger;
    if ANetCost then
      Result := AQuery.FieldByName('Cost').AsCurrency
    else
      Result := AQuery.FieldByName('OrigCst').AsCurrency;

    if (Result <> 0) and (AQuery.FieldByName('UPCC').AsInteger <> AUPC) then
      Result := (Result / AQuery.FieldByName('UPCC').AsInteger) * AUPC;
  end;
end;

procedure TVendInvHelpers.LoadCalcVendInvItemCostEvents;
var
  x, APos: Integer;
  ASourceQuery: TAdsQuery;
  ASourceField: string;
begin
  if not Assigned(FCalcVendInvItemCostEventList) then
    Exit;

  for x := 0 to FCalcVendInvItemCostEventList.Count - 1 do
  begin
    APos := Pos('=', FCalcVendInvItemCostEventList[x]);
    if APos > 0 then
    begin
      if SameText(Copy(FCalcVendInvItemCostEventList[x], 1, APos - 1), 'Detail') then
        ASourceQuery := qInvoiceDetail
      else if SameText(Copy(FCalcVendInvItemCostEventList[x], 1, APos - 1), 'Header') then
        ASourceQuery := qInvoiceHead
      else
        ASourceQuery := nil;

      if Assigned(ASourceQuery) then
        ASourceField := Copy(FCalcVendInvItemCostEventList[x], APos + 1, 100)
      else
        ASourceField := '';

      if Assigned(ASourceQuery) and (ASourceField <> '') and (ASourceQuery.FindField(ASourceField) <> nil) then
      begin
        if ASourceQuery = qInvoiceHead then
          ASourceQuery.FieldByName(ASourceField).OnChange := qInvoiceHDCalcVendInvItemCost
        else if ASourceQuery = qInvoiceDetail then
          ASourceQuery.FieldByName(ASourceField).OnChange := qInvoiceDTCalcVendInvItemCost;
      end;
    end;
  end;
end;

procedure TVendInvHelpers.LoadCalcVendInvItemCostValues;
var
  AQuery: TAdsQuery;
  AIniFile: TMemIniFile;
  AStrings: TStringList;
  AStream: TStream;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select * from System.Functions Where Name=''CalcVendInvItemCost'';';
  AQuery.Open;
  if AQuery.IsEmpty then
    Exit;

  AStream := TMemoryStream.Create;
  AIniFile := TMemIniFile.Create('');
  AStrings := TStringList.Create;
  try
    FCalcVendInvItemCostParamList := TStringList.Create;
    FCalcVendInvItemCostEventList := TStringList.Create;

    TMemoField(AQuery.FieldByName('Comment')).SaveToStream(AStream);
    AStream.Position := 0;
    AStrings.LoadFromStream(AStream);
    AIniFile.SetStrings(AStrings);
    AIniFile.ReadSectionValues('Events', FCalcVendInvItemCostEventList);
    AIniFile.ReadSectionValues('Parameters', FCalcVendInvItemCostParamList);
  finally
    AStream.Free;
    AStrings.Free;
    AIniFile.Free;
  end;
end;

procedure TVendInvHelpers.LoadUpchargeLineFactor;
var
  I: Integer;
  function LineFactor(ALevel: Integer): Double;
  var
    ACont: Boolean;
    ASuf: string;
  begin
    Result := 0;
    ACont := False;
    case ALevel of
      1:
        ACont := FUseUpcharge1;
      2:
        ACont := FUseUpcharge2;
      3:
        ACont := FUseUpcharge3;
      4:
        ACont := FUseUpcharge4;
      5:
        ACont := FUseUpcharge5;
    end;

    if not ACont then
      Exit;

    ASuf := Trim(IntToStr(ALevel));
    Result := qInvoiceHead.FieldByName('UpAmnt' + ASuf).AsFloat;
    if Result <= 0 then
    begin
      Result := 0;
      Exit;
    end;

    if qInvoiceHead.FieldByName('UpDown' + ASuf).AsInteger = 2 then // 2=Down
      Result := Result * -1;

    if qInvoiceHead.FieldByName('CalcMethod' + ASuf).AsInteger <> 2 then // 2=%
    begin
      UpchargePercentOnly := False;
      if FOrigCostSummary <> 0 then
        Result := Result / FOrigCostSummary; // Calculate Percentage by
    end
    else
      Result := Result / 100;
  end;

begin
  UpchargePercentOnly := True;
  // if FOrigCostSummary <> 0 then
  for I := 1 to 5 do
    UpchargeLineFactor[I] := LineFactor(I)
    { else
      for I := 1 to 5 do
      UpchargeLineFactor[I] := 0; }
end;

procedure TVendInvHelpers.NewVendorInvoice(JSONRequest, JSONResponse: TJSONObject);
var
  AVendorName, AVendID, ARemark: string;
  ADate: TDate;
  AType: Integer;
  ANoLstVend, AAutoEmail: Boolean;

  AJsonA: TJSONArray;
  AQuery: TAdsQuery;
  AEnumP: TJSONPairEnumerator;
  AEnumA: TJsonArrayEnumerator;
  AField: TField;
  APair: TJSONPair;
begin
  JSONRequest.TryGetValue<string>('VendorName', AVendorName);
  JSONRequest.TryGetValue<Boolean>('NoLstVend', ANoLstVend);
  JSONRequest.TryGetValue<Boolean>('AutoEmailToVendor', AAutoEmail);
  JSONRequest.TryGetValue<TDate>('Date', ADate);
  JSONRequest.TryGetValue<Integer>('Type', AType);
  JSONRequest.TryGetValue<TJSONArray>('Items', AJsonA);

  if (AJsonA.Count = 0) or AVendorName.IsEmpty then
  begin
    JSONResponse.AddPair('Error', 'NewVendorInvoice required parameter missing');
    Exit;
  end;

  try
    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := Format('Select VendID From VenMstr Where Name=%s;', [QuotedStr(AVendorName)]);
    AQuery.Open;
    if AQuery.Eof then
    begin
      VendHelpersI.AddNewVendor(AVendorName, ANoLstVend);
      AQuery.Close;
      AQuery.SQL.Text := Format('Select VendID From VenMstr Where Name=%s;', [QuotedStr(AVendorName)]);
      AQuery.Open;
    end;
    AVendID := AQuery.Fields[0].AsString;

    // AQuery.AdsConnection.BeginTransaction;
    try
      ARemark := 'Inventory Transfer ' + DateTimeToStr(Now);
      VendID := AVendID;
      DelID := '';
      OpenQueries;
      InvTransType := TInvTransType(AType);
      ItemByCaseUnit := ibcuCase;
      InvValidationMode := ivmRowSilent;
      DataHelpers.MakeEdtMode(qInvoiceHead);
      qInvoiceHead.FieldByName('Remark').AsString := ARemark;

      AEnumA := AJsonA.GetEnumerator;
      while AEnumA.MoveNext do
      begin
        if qInvoiceDetail.State <> dsInsert then
          qInvoiceDetail.Insert;
        AEnumP := (AEnumA.GetCurrent as TJSONObject).GetEnumerator;
        while AEnumP.MoveNext do
        begin
          APair := AEnumP.Current;
          AField := qInvoiceDetail.FindField(APair.JsonString.Value);
          if Assigned(AField) and (APair.JsonValue.Value <> '') then
            AField.AsString := APair.JsonValue.Value;
        end;
        qInvoiceDetail.Post;
      end;
      PostQueries;
      JSONResponse.AddPair('Status', 'Success');
      JSONResponse.AddPair('VendID', VendID);
      JSONResponse.AddPair('DelID', DelID);
      OpenQueries;
      JSONResponse.AddPair('DelNo', qInvoiceHead.FieldByName('DelNo').AsString);
      if AAutoEmail and (VendEmailAddress <> '') then
        try
          DoEmailVendInvReport(VendID, VendInvHelpers.DelID, '', '', '', VendEmailAddress);
        except
          on E: Exception do
            JSONResponse.AddPair('EmailError', Format('NewVendorInvoice %s', [E.Message]));
        end;
      CancelQueries;
    except
      on E: Exception do
      begin
        // AQuery.AdsConnection.Rollback;
        raise;
      end;
    end;

  except
    on E: Exception do
      JSONResponse.AddPair('Error', Format('NewVendorInvoice %s', [E.Message]));
  end;
end;

function TVendInvHelpers.NewVendorInvoiceWithAttach(const AVendID, AVendInvoiceNum, ADocFile, ADocDescription: string;
  var AError: string): Integer;
begin
  AError := '';
  Result := 0;
  if not VendHelpersI.FindVendorByID(nil, AVendID) then
  begin
    AError := 'Vendor ID not valid';
    Exit;
  end;

  try
    CancelQueries;

    DelID := '';
    VendID := AVendID;
    OpenQueries;
    InternalFieldChange := True;
    try
      qInvoiceHead.Edit;
      qInvoiceHead.FieldByName('VenInvNo').AsString := AVendInvoiceNum;
      qInvoiceHead.FieldByName('EnteredBy').AsString := 'Scanner';
      qInvoiceHead.Post;
    finally
      InternalFieldChange := False;
    end;
    PostQueries;

    if DelID <> '' then
    begin
      OpenQueries;
      Result := qInvoiceHead.FieldByName('Delno').AsInteger;
      CancelQueries;
      if ADocFile <> '' then
        ProjectHelpersI.AddAttachDocs('VendInvoice', DelID, ADocFile, ADocDescription);
    end;
  except
    on E: Exception do
      AError := E.Message;
  end;
end;

procedure TVendInvHelpers.OpenQueries;
const
  HeadOpen = 'Insert Into #InvoiceHD_Edit Select d.*, False From Delvry d Where DelID=:DelID; ' +
    'Select * From #InvoiceHD_Edit;';
  DetailOpen = 'Insert Into #InvoiceDT_Edit Select d.*, 0, 0, 0, False From DelItm d Where DelID=:DelID; ' +
    'Update #InvoiceDT_Edit Set Prd_UnitCost=IIF(IfNull(Cost,0)=0,0,Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)), ' +
    'Prd_OldUnitCost=Round(IIF(IfNull(OldCost,0)=0,0,OldCost/IIF(IfNull(OldUPC,0)=0,1,OldUpc)),2); ' +
    'Select * From #InvoiceDT_Edit Order By DelLine;';
  DetailOpenWPrd = 'Insert Into #InvoiceDT_Edit Select d.*, %s 0, 0, 0, 0, 0, 0, 0, 0, False ' +
    'From DelItm d Left Outer Join Prdmstr p ON (Upper(p.Code)=d.UpcCode) Where DelID=:DelID; ' +
    'Update #InvoiceDT_Edit Set ' +
    'Prd_PriceUp=IIF(IfNull(Cost,0)=0,0,((Prd_Price-(Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)))/' +
    '(Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)))*100), ' + 'Prd_PriceGin=IIF(IfNull(Cost,0)=0,0,IIF(IfNull(Prd_Price,0)=0,0,' +
    '(1-((Cost/IIF(IfNull(UPCC,0)=0,1,Upcc))/Prd_Price))*100)), ' +
    'Prd_SaleUp=IIF(IfNull(Cost,0)=0,0,((Prd_Sale-(Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)))/' +
    '(Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)))*100), ' + 'Prd_SaleGin=IIF(IfNull(Cost,0)=0,0,IIF(IfNull(Prd_Sale,0)=0,0,' +
    '(1-((Cost/IIF(IfNull(UPCC,0)=0,1,Upcc))/Prd_Sale))*100)), ' +
    'Prd_UnitCost=IIF(IfNull(Cost,0)=0,0,Cost/IIF(IfNull(UPCC,0)=0,1,Upcc)), ' +
    'Prd_OldUnitCost=Round(IIF(IfNull(OldCost,0)=0,0,OldCost/IIF(IfNull(OldUPC,0)=0,1,OldUpc)),2); ' +
    'Update p Set Prd_OnHand=o.Amount From #InvoiceDT_Edit p, Onhand o ' + 'Where p.UpcCode=Upper(o.UpcCode); ' +
    'Select * From #InvoiceDT_Edit Order By DelLine;';
  // DetailOpen = 'Drop Table #InvoiceDT_Edit; Select * Into #InvoiceDT_Edit From DelItm Where DelID=:DelID Order By LineID; Select * From #InvoiceDT_Edit;';
var
  APrdList: string;
begin
  // AppActions.ShowHourGlassCursor;
  // qInvoiceDetail.DisableControls;   //Before enabling test start new invoice
  try
    CancelQueries;

    qInvoicePrdmstr.Close;
    qInvoicePrdmstr.ParamByName('UpcCode').AsString := EmptyPrdCode;
    qInvoicePrdmstr.Open;

    qInvoicePrdmstr2MoveToCode(EmptyPrdCode);

    qInvoiceHead.SQL.Text := HeadOpen;
    qInvoiceHead.Prepare;
    qInvoiceHead.Params[0].AsString := DelID;
    qInvoiceHead.Open;

    if not qInvoiceHead.IsEmpty then
    begin
      InvTransType := TInvTransType(qInvoiceHead.FieldByName('Type').AsInteger);
      OpenedInvTransType := InvTransType;
    end
    else
      FInvTransType := ittDebit; // use variable not property, for new invoice

    if PrdmstrFieldList.Count = 0 then
      qInvoiceDetail.SQL.Text := DetailOpen
    else
    begin
      APrdList := PrdmstrFieldString(False);
      qInvoiceDetail.SQL.Text := Format(DetailOpenWPrd, [APrdList]);
    end;
    qInvoiceDetail.Prepare;
    qInvoiceDetail.Params[0].AsString := DelID;
    qInvoiceDetail.Open;

    if InvTransType = ittCredit then
    begin
      DetailAmntDelAbsSwitch;
      qInvoiceDetail.Refresh;
    end;

    qInvoiceHead.FieldByName('VenInvNo').OnChange := qInvoiceHeaderVenInvNoChange;
    qInvoiceHead.FieldByName('Date').OnChange := qInvoiceHeaderDateChange;
    qInvoiceHead.FieldByName('BillDue').OnChange := qInvoiceHeaderDateChange;

    if not FUpchargeMulti then
      qInvoiceHead.FieldByName('Discount').OnChange := qInvoiceHeaderDiscountChange
    else
    begin
      if DelID <> '' then
        DefaultUpchargeValues;
      qInvoiceHead.FieldByName('UpAmnt1').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpDown1').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('CalcMethod1').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpAmnt2').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpDown2').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('CalcMethod2').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpAmnt3').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpDown3').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('CalcMethod3').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpAmnt4').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpDown4').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('CalcMethod4').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpAmnt5').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('UpDown5').OnChange := qInvoiceHeaderEnhancedDiscountChange;
      qInvoiceHead.FieldByName('CalcMethod5').OnChange := qInvoiceHeaderEnhancedDiscountChange;

      qInvoiceHead.FieldByName('PrintUpAmnt1').OnChange := qInvoiceHeaderPrintDiscountChange;
      qInvoiceHead.FieldByName('PrintUpDown1').OnChange := qInvoiceHeaderPrintDiscountChange;
      qInvoiceHead.FieldByName('PrintCalcMethod1').OnChange := qInvoiceHeaderPrintDiscountChange;
    end;

    qInvoiceDetail.FieldByName('UpcCode').OnChange := qInvoiceDetailUpcCodeChange;
    qInvoiceDetail.FieldByName('VenItmCde').OnChange := qInvoiceDetailVenItemCodeChange;
    qInvoiceDetail.FieldByName('UPCC').OnChange := qInvoiceDetailUPCC_Change;
    qInvoiceDetail.FieldByName('Cost').OnChange := qInvoiceDetailCostChange;
    qInvoiceDetail.FieldByName('OrigCst').OnChange := qInvoiceDetailOrigCostChange;
    qInvoiceDetail.FieldByName('AmntDel').OnChange := qInvoiceDetailQtyFilledPriceChange;
    qInvoiceDetail.FieldByName('CostFlag').OnChange := qInvoiceDetailGeneralChange;
    qInvoiceDetail.FieldByName('Prd_UnitCost').OnChange := qInvoiceDetailUnitCostChange;
    qInvoiceDetail.FieldByName('PrintCost').OnChange := qInvoiceDetailPrintCostChange;

    PrdmstrFieldOnChangeAssign;

    LoadCalcVendInvItemCostEvents;

    if VendID = '' then
      VendID := qInvoiceHead.FieldByName('VendID').AsString;

    if (DelID = '') and (VendID <> '') then
      StartNewInvoice
    else if qInvoiceHead.FieldByName('Status').IsNull then
    begin
      DataHelpers.MakeEdtMode(qInvoiceHead);
      qInvoiceHead.FieldByName('Status').AsInteger := 0;
      qInvoiceHead.CheckBrowseMode;
    end;

    if qInvoiceDetail.IsEmpty and ((DelID <> '') or (VendID <> '')) then
      qInvoiceDetail.Insert;

    CalcAmountSummary;
    FDataPosted := True; // We have to prompt user to save if clicking Close Window
    RecCountLastSave := qInvoiceDetail.RecordCount;
    ItemByCaseUnit := ibcuDefault;
    PrdUpdateList.Clear;

    LatestInvoiceDate := qInvoiceHead.FieldByName('Date').AsDateTime;
    InvoiceDateChanged := False;
  finally
    // qInvoiceDetail.EnableControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TVendInvHelpers.PostEditRecords;
begin
  if qInvoiceHead.Active then
    qInvoiceHead.CheckBrowseMode;
  if qInvoiceDetail.Active then
    qInvoiceDetail.CheckBrowseMode;
  if qInvoicePrdmstr.Active then
    qInvoicePrdmstr.CheckBrowseMode;
end;

function TVendInvHelpers.PostFlagModeRecord: Boolean;
var
  AState: TDataSetState;
begin
  Result := False;
  if InvValidationMode = ivmRowSilent then
  begin
    AState := qInvoiceDetail.State;
    DataHelpers.MakeEdtMode(qInvoiceDetail);
    qInvoiceDetail.FieldByName('PostFlag').AsInteger := qInvoiceDetail.FieldByName('PostFlag').AsInteger + 1;
    if qInvoiceDetail.State <> AState then
      qInvoiceDetail.Post;
    Result := True;
  end;
end;

procedure TVendInvHelpers.PostInventoryChanges(InvoicingNow: Boolean);
const
  InvenPostPre = 'Delete From #VendInvcInvAdjust; ' +
    'Insert Into #VendInvcInvAdjust Select e.LineID, e.UpcCode, IIF(IfNull(e.PostFlag,0)>0, 0, e.AmntDel) EQty, ' +
    'IIF(IfNull(o.PostFlag,0)>0, 0, o.AmntDel) OQty, e.UPCC EUpcc, o.UPCC OUpcc  ' +
    'From #InvoiceDT_Edit e Left Outer Join DelItm o On e.DelID=o.DelID ' +
    'And e.LineID=o.LineID And e.UpcCode=o.UpcCode; ' +
    'Insert Into #VendInvcInvAdjust Select LineID, UpcCode, 0, o.AmntDel, 0, o.Upcc ' + 'From DelItm o ' +
    'Where o.DelID=:DelID And IfNull(o.PostFlag,0)=0 And Not LineID IN (Select LineID From #VendInvcInvAdjust ' +
    'Where Not LineID Is Null); ' + 'Insert Into #VendInvcInvAdjust Select o.LineID, o.UpcCode, 0, o.AmntDel, ' +
    '0, o.Upcc From DelItm o, #InvoiceDT_Edit e ' +
    'Where o.DelID=:DelID And e.LineID=o.LineID And e.UpcCode<>o.UpcCode And IfNull(o.PostFlag,0)=0; ' +
    'Update #VendInvcInvAdjust Set EQty=0 Where EQty Is Null; ' +
    'Update #VendInvcInvAdjust Set OQty=0 Where OQty Is Null; ' +
    'Update #VendInvcInvAdjust Set EUpcc=1 Where EUpcc Is Null or EUpcc=0; ' +
    'Update #VendInvcInvAdjust Set OUpcc=1 Where OUpcc Is Null or OUpcc=0; ' +
    'Delete From #VendInvcInvAdjust Where EQty=0 And OQty=0; ';

  InvenPost = 'Insert Into %s Select UpcCode, Sum((EQty*EUpcc)-(OQty*OUpcc)) From #VendInvcInvAdjust Group By 1; ';
var
  AQuery: TAdsQuery;
  InvUpdTable, InvLocUpdTable, AOldLoc, ANewLoc: string;
  ADebugInv, ANewInv: Boolean;
  ATimeStamp: string;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Select o.IgnoreInven OIgnore, e.IgnoreInven NIgnore, o.InvLocation OLoc, e.InvLocation NLoc ' +
    'From #InvoiceHD_Edit e Left Outer Join Delvry o On e.DelID=o.DelID;';
  AQuery.Open;
  if AQuery.FieldByName('NIgnore').AsBoolean then
    Exit;
  ANewInv := AQuery.FieldByName('OIgnore').AsBoolean;
  AOldLoc := AQuery.FieldByName('OLoc').AsString;
  ANewLoc := AQuery.FieldByName('NLoc').AsString;
  AQuery.Close;

  ADebugInv := ProgPref('DebugInventoryChanges');
  InvUpdTable := PrdHelpersI.InventoryUpdateTableName;
  InvLocUpdTable := PrdHelpersI.InventoryLocUpdateTableName;

  AQuery.SQL.Clear;
  AQuery.SQL.Add(InvenPostPre);
  if ANewInv then
    AQuery.SQL.Add('Update #VendInvcInvAdjust Set OQty=0;');
  AQuery.SQL.Add(Format(InvenPost, [InvUpdTable]));
  AQuery.Prepare;
  AQuery.ParamByName('DelID').AsString := DelID;
  AQuery.ExecSQL;

  if SameText(AOldLoc, ANewLoc) then
  begin
    AQuery.SQL.Clear;
    AQuery.SQL.Add(Format('Insert Into %s Select UpcCode, %s, Amount', [InvLocUpdTable, QuotedStr(ANewLoc)]));
    AQuery.SQL.Add(Format('From %s', [InvUpdTable]));
    AQuery.ExecSQL;
  end
  else
  begin
    AQuery.SQL.Clear;

    if (AOldLoc <> '') and (not ANewInv) then
    begin
      AQuery.SQL.Add(Format('Insert Into %s Select UpcCode, %s,', [InvLocUpdTable, QuotedStr(AOldLoc)]));
      AQuery.SQL.Add('(AmntDel*IIF(IfNull(Upcc,0)=0, 1, Upcc))*-1');
      AQuery.SQL.Add('From Delitm Where DelID=:DelID And IfNull(PostFlag,0)=0;');
    end;

    if ANewLoc <> '' then
    begin
      AQuery.SQL.Add(Format('Insert Into %s Select UpcCode, %s,', [InvLocUpdTable, QuotedStr(ANewLoc)]));
      AQuery.SQL.Add('(AmntDel*IIF(IfNull(Upcc,0)=0, 1, Upcc))');
      AQuery.SQL.Add('From #InvoiceDT_Edit Where IfNull(PostFlag,0)=0;');
    end;

    AQuery.SQL.Add(Format('Delete From %s Where Amount=0;', [InvLocUpdTable]));
    AQuery.Prepare;
    if AQuery.Params.FindParam('DelID') <> nil then
      AQuery.ParamByName('DelID').AsString := DelID;
    AQuery.ExecSQL;
  end;

  if ADebugInv then
  begin
    ATimeStamp := FormatDateTime('mm/dd/yyyy hh:mm:ss', Now);
    AQuery.SQL.Clear;

    AQuery.SQL.Add('Insert Into TrkInv Select :TimeStamp, :WS, :DelID, UpcCode, 0, 0, 0');
    AQuery.SQL.Add(Format('From %s;', [InvUpdTable]));

    AQuery.SQL.Add('Update ti Set OnHandPre=Amount From TrkInv ti, OnHand o');
    AQuery.SQL.Add('Where Upper(o.UpcCode)=Upper(ti.UpcCode)');
    AQuery.SQL.Add('And ti.RecordedOn=:TimeStamp And ti.WSName=:WS And ti.DelID=:DelID;');

    AQuery.SQL.Add(Format('Update ti Set OnHandChange=Amount From TrkInv ti, %s o', [InvUpdTable]));
    AQuery.SQL.Add('Where Upper(o.UpcCode)=Upper(ti.UpcCode)');
    AQuery.SQL.Add('And ti.RecordedOn=:TimeStamp And ti.WSName=:WS And ti.DelID=:DelID;');

    AQuery.Prepare;
    AQuery.ParamByName('TimeStamp').AsString := ATimeStamp;
    AQuery.ParamByName('WS').AsString := GetOSComputerName;
    AQuery.ParamByName('DelID').AsString := DelID;
    AQuery.ExecSQL;
  end;

  PrdHelpersI.DoUpdateInventory;

  if ADebugInv then
  begin
    AQuery.SQL.Clear;
    AQuery.SQL.Add('Update ti Set OnHandPost=Amount From TrkInv ti, OnHand o');
    AQuery.SQL.Add('Where Upper(o.UpcCode)=Upper(ti.UpcCode)');
    AQuery.SQL.Add('And ti.RecordedOn=:TimeStamp And ti.WSName=:WS And ti.DelID=:DelID;');

    AQuery.Prepare;
    AQuery.ParamByName('TimeStamp').AsString := ATimeStamp;
    AQuery.ParamByName('WS').AsString := GetOSComputerName;
    AQuery.ParamByName('DelID').AsString := DelID;
    AQuery.ExecSQL;
  end;
end;

procedure TVendInvHelpers.PostQueries;
const
  NewDelvryPrePost = 'Update #InvoiceHD_Edit Set DelID=:DelID, DelNo=%d; Update #InvoiceDT_Edit Set DelID=:DelID; ';
  // DetailPost = 'Delete From DelItm Where DelID=:DelID And Not LineID In (Select LineID From #InvoiceDT_Edit);';
  DetailPost = 'Delete From DelItm Where DelID=:DelID And Not LineID Is Null And LineID In (%s);';
  // PrdPost = 'Delete From #InvoicePRD_Edit Where Not Code In (Select UpcCode From #InvoiceDT_Edit);';
  VndRtrnPost = 'Update r Set VendID=s.VendID, DelID=s.DelID, DelLineID=s.LineID, DtTmReturned=Now() ' +
    'From VndRtrns r, #InvoiceDT_Edit s ' +
    'Where r.LineID=s.SourceLineID And s.SourceTable=''VndRtrns'' And s.__Updated And r.DelID Is Null;';
var
  AQuery: TAdsQuery;
  PreviousTransaction: Boolean;
  DetailInsertSQL, HdUpdateSQL, DtUpdateSQL, HeaderInsertSQL: string;
  APrdSkipList: string;
  iDelNo, I: Integer;
  oldDelID: string;
begin
  if UpchargeCalcNeeded then
    CalculateEnhancedDiscountsAll;

  RecCountLastSave := qInvoiceDetail.RecordCount;
  PostEditRecords;

  AQuery := DataHelpers.HelperQuery;

  // AppActions.ShowHourGlassCursor;
  PrdHelpersI; // Calling PrdHelpers here so that the Create of PrdHelpers takes place before transaction
  PreviousTransaction := AQuery.AdsConnection.TransactionActive;
  DisableQueryControls;

  // prevent exception when in invoice for very long time and temp table #TmpUpdVend is dropped
  if not PreviousTransaction then
    VendHelpersI.UpdateVendBalanceEtcPrepare(AQuery);

  try
    oldDelID := DelID;
    { TODO : Product Insert SQL, when adding new items thru invoice }
    APrdSkipList := '__Updated;CreatedOn;';
    HeaderInsertSQL := DataHelpers.SQLInsertAllFieldsExcept('#InvoiceHD_Edit', 'Delvry', APrdSkipList, '');

    APrdSkipList := 'DelID;__Updated;Paid;CreatedOn;';
    HdUpdateSQL := DataHelpers.SQLUpdateAllFieldsExcept('#InvoiceHD_Edit', 'Delvry', APrdSkipList,
      'Where t.DelID=:DelID And s.DelID=t.DelID And s.__Updated=True');

    APrdSkipList := PrdmstrFieldString(True);
    APrdSkipList := ReplaceStr(APrdSkipList, ', ', ';') + 'Prd_UnitCost;Prd_OldUnitCost;__PrcntOfTotal;__Updated;';
    DetailInsertSQL := DataHelpers.SQLInsertAllFieldsExcept('#InvoiceDT_Edit', 'DelItm', APrdSkipList,
      'Where Not LineID IN (Select LineID From DelItm Where DelID=:DelID) ' +
      'And (UpcCode<>'''' Or VenItmCde<>'''' Or Descrip<>'''' Or Cost<>0)');

    APrdSkipList := APrdSkipList + '__PrcntOfTotal;DelID;LineID;';
    APrdSkipList := APrdSkipList;
    DtUpdateSQL := DataHelpers.SQLUpdateAllFieldsExcept('#InvoiceDT_Edit', 'DelItm', APrdSkipList,
      'Where t.DelID=:DelID And s.DelID=t.DelID And s.LineID=t.LineID And s.__Updated=True');

    if not PreviousTransaction then
      AQuery.AdsConnection.BeginTransaction;
    try
      if InvTransType <> OpenedInvTransType then
        DetailAllUpdated;
      if InvTransType = ittCredit then
        DetailAmntDelAbsSwitch;
      if DelID <> '' then
      begin
        InvoiceToVendHist;
        PostInventoryChanges; // post Inventory before posting detail info so we have old and new values

        AQuery.SQL.Clear;
        AQuery.SQL.Add(HdUpdateSQL);
        AQuery.SQL.Add(DtUpdateSQL);
      end
      else
      begin
        DelID := GenHelpers.GuidIdString;
        iDelNo := dmData.GetIncNum('Delvry', 'DelNO');

        InvoiceToVendHist;
        PostInventoryChanges; // post Inventory before posting detail info so we have old and new values

        AQuery.SQL.Clear;
        AQuery.SQL.Add(Format(NewDelvryPrePost, [iDelNo]));
        AQuery.SQL.Add(HeaderInsertSQL);
      end;

      // AQuery.SQL.Add(PrdPost);
      if FDetailDeleteList.Count > 0 then
      begin
        APrdSkipList := '';
        for I := 0 to FDetailDeleteList.Count - 1 do
          APrdSkipList := APrdSkipList + QuotedStr(FDetailDeleteList[I]) + IfThen(I < (FDetailDeleteList.Count - 1),
            ', ', '');

        APrdSkipList := Format(DetailPost, [APrdSkipList]);
        AQuery.SQL.Add(APrdSkipList);
      end;
      AQuery.SQL.Add(DetailInsertSQL);
      AQuery.SQL.Add(VndRtrnPost);
      // AQuery.SQL.Add(PrdUpdateSQL);
      // AQuery.SQL.Add(PrdInsertSQL);
      AQuery.Prepare;
      AQuery.Params[0].AsString := DelID;
      AQuery.ExecSQL;

      FDataPosted := True;
    except
      if not PreviousTransaction then
        AQuery.AdsConnection.Rollback;
      DelID := oldDelID;
      raise;
    end;
    if not PreviousTransaction then
      AQuery.AdsConnection.Commit;
    DoUpdateLastCostPrdInfo;
    GuiNonGuiHelpers.Status2 := 'Saved ' + FormatDateTime('hh:nn:ss', Now);
  finally
    EnableQueryControls;
    // AppActions.HideHourGlassCursor;
  end;
end;

function TVendInvHelpers.PrdmstrFieldList: TStringList;
var
  AQuery: TAdsQuery;
  AField: string;
begin
  if not Assigned(FPrdmstrFieldList) then
  begin
    FPrdmstrFieldList := TStringList.Create;
    FPrdmstrFieldList.Sorted := True;
    FPrdmstrFieldList.Duplicates := dupIgnore;

    FPrdmstrNoEditFieldList := TStringList.Create;
    FPrdmstrNoEditFieldList.Sorted := True;
    FPrdmstrNoEditFieldList.Duplicates := dupIgnore;

    AQuery := DataHelpers.HelperQuery;
    AQuery.SQL.Text := Format('Select * From GridData Where [Form]=%s And [Grid]=%s And Active Order By [Position]',
      [QuotedStr('frmModuleEditVendorInvoice'), QuotedStr('TableView1')]);
    AQuery.Open;
    while not AQuery.Eof do
    begin
      AField := AQuery.FieldByName('Field').AsString;
      if StartsStr('Prd_', AField) then
        FPrdmstrFieldList.Add(AField);
      AQuery.Next;
    end;

    if (FPrdmstrFieldList.Count <= 2) and (FPrdmstrFieldList.IndexOf('Prd_UnitCost') > -1) and
      (FPrdmstrFieldList.IndexOf('Prd_OldUnitCost') > -1) then
      FPrdmstrFieldList.Clear;

    FPrdmstrFieldList.Sort;
    FPrdmstrFieldList.CaseSensitive := False;
    if FPrdmstrFieldList.Count > 0 then
    begin
      FPrdmstrFieldList.Add('Prd_Price');
      FPrdmstrFieldList.Add('Prd_PriceUp');
      FPrdmstrFieldList.Add('Prd_PriceGin');
      FPrdmstrFieldList.Add('Prd_Sale');
      FPrdmstrFieldList.Add('Prd_SaleUp');
      FPrdmstrFieldList.Add('Prd_SaleGin');
      FPrdmstrFieldList.Add('Prd_OnHand');

      FPrdmstrNoEditFieldList.Add('Prd_Price');
      FPrdmstrNoEditFieldList.Add('Prd_PriceUp');
      FPrdmstrNoEditFieldList.Add('Prd_PriceGin');
      FPrdmstrNoEditFieldList.Add('Prd_Sale');
      FPrdmstrNoEditFieldList.Add('Prd_SaleUp');
      FPrdmstrNoEditFieldList.Add('Prd_SaleGin');
      FPrdmstrNoEditFieldList.Add('Prd_OnHand');
      FPrdmstrNoEditFieldList.Add('Prd_UnitCost');
      FPrdmstrNoEditFieldList.Add('Prd_OldUnitCost');
    end;
  end;

  Result := FPrdmstrFieldList;
end;

procedure TVendInvHelpers.PrdmstrFieldOnChangeAssign;
var
  AList: TStringList;
  I: Integer;
begin
  AList := PrdmstrFieldList;
  for I := 0 to AList.Count - 1 do
    if FPrdmstrNoEditFieldList.IndexOf(AList[I]) = -1 then
      qInvoiceDetail.FieldByName(AList[I]).OnChange := qInvoiceDetailPrdmstrChange;
end;

function TVendInvHelpers.PrdmstrFieldString(ANoAlias: Boolean): string;
var
  AList: TStringList;
  I: Integer;
begin
  AList := PrdmstrFieldList;
  Result := '';
  for I := 0 to AList.Count - 1 do
    if ANoAlias then
      Result := Result + AList[I] + ', '
    else if not(SameText(AList[I], 'Prd_PriceUp') or SameText(AList[I], 'Prd_PriceGin') or
      SameText(AList[I], 'Prd_OnHand') or SameText(AList[I], 'Prd_UnitCost') or SameText(AList[I], 'Prd_SaleUp') or
      SameText(AList[I], 'Prd_SaleGin') or SameText(AList[I], 'Prd_OldUnitCost')) then
      Result := Result + 'p.' + Copy(AList[I], 5, Length(AList[I])) + ' ' + AList[I] + ', ';
end;

procedure TVendInvHelpers.qInvoiceBeforePost(DataSet: TDataSet);
begin
  if ((DataSet = qInvoicePrdmstr) or (DataSet = qInvoiceDetail)) and FInternalPrdmstrDataChange then
    Exit;

  DataSet.FieldByName('__Updated').AsBoolean := True;
  FDataPosted := False;
end;

procedure TVendInvHelpers.qInvoiceDTCalcVendInvItemCost(Sender: TField);
var
  AExp: string;
  AQuery: TAdsQuery;
  x, APos: Integer;
  ASourceQuery: TAdsQuery;
  ASourceField, ASourceType: string;
begin
  if not Assigned(FCalcVendInvItemCostParamList) then
    Exit;

  AExp := 'Select CalcVendInvItemCost(';

  for x := 0 to FCalcVendInvItemCostParamList.Count - 1 do
  begin
    APos := Pos('=', FCalcVendInvItemCostParamList[x]);
    if APos > 0 then
    begin
      if SameText(Copy(FCalcVendInvItemCostParamList[x], 1, APos - 1), 'Detail') then
        ASourceQuery := qInvoiceDetail
      else if SameText(Copy(FCalcVendInvItemCostParamList[x], 1, APos - 1), 'Header') then
        ASourceQuery := qInvoiceHead
      else
        ASourceQuery := nil;

      if Assigned(ASourceQuery) then
        ASourceField := Copy(FCalcVendInvItemCostParamList[x], APos + 1, 100)
      else
        ASourceField := '';

      ASourceType := 'C';
      if (ASourceField <> '') and (Pos(',', ASourceField) > 0) then
      begin
        APos := Pos(',', ASourceField);
        ASourceType := Copy(ASourceField, APos + 1, 1);
        ASourceField := Copy(ASourceField, 1, APos - 1);
      end;

      if Assigned(ASourceQuery) and (ASourceField <> '') and (ASourceQuery.FindField(ASourceField) <> nil) then
      begin
        if ASourceType = 'N' then
          AExp := AExp + FloatToStr(ASourceQuery.FieldByName(ASourceField).AsFloat) + ', '
        else
          AExp := AExp + ASourceQuery.FieldByName(ASourceField).AsString + ', ';
      end;
    end;
  end;

  if Copy(AExp, Length(AExp) - 1, 2) = ', ' then
    Delete(AExp, Length(AExp) - 1, 2);

  AExp := AExp + ') FROM system.iota;';
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := AExp;
  AQuery.Open;
  if not(AQuery.IsEmpty or qInvoiceDetail.IsEmpty) then
  begin
    DataHelpers.MakeEdtMode(qInvoiceDetail);
    qInvoiceDetail.FieldByName('OrigCst').AsCurrency := AQuery.Fields[0].AsCurrency;
  end;
end;

procedure TVendInvHelpers.qInvoiceChangesToPrdmstr(Sender: TField);
var
  AState: TDataSetState;
  PrdCost, InvCost, NewPrdCost: Currency;
  PrdUPC, InvUPC: Integer;
begin
  if (qInvoiceDetail.FieldByName('UpcCode').AsString = '') or
    (qInvoiceDetail.FieldByName('UpcCode').AsString = EmptyPrdCode) then
    Exit; // Product not entered

  if (qInvoiceDetail.FieldByName('AmntDel').AsInteger < 0) or (InvTransType = ittCredit) then
    Exit; // Vendor Return, we do not record those to Prdmstr

  if not qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString) then
    Exit; // Product not on file

  AState := qInvoicePrdmstr.State;
  if not Assigned(Sender) then
  begin
    qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('UPCC'));
    qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('Cost'));
    qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('CostFlag'));
  end
  else if SameText(Sender.FieldName, 'UPCC') and (qInvoicePrdmstr.FieldByName('UPC').AsInteger <> Sender.AsInteger) and
    (Sender.AsInteger >= 1) then
  begin
    if not PostFlagModeRecord then
    begin
      DataHelpers.MakeEdtMode(qInvoicePrdmstr);
      qInvoicePrdmstr.FieldByName('UPC').AsInteger := Sender.AsInteger;
    end;
  end
  else if SameText(Sender.FieldName, 'Cost') and (InvValidationMode <> ivmRowSilent) and
    (not ProgPref('VendInvNoPrdCost')) then
  begin
    PrdCost := qInvoicePrdmstr.FieldByName('Cost').AsCurrency;
    InvCost := Sender.AsCurrency;
    PrdUPC := qInvoicePrdmstr.FieldByName('UPC').AsInteger;
    InvUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    if PrdUPC = 0 then
      PrdUPC := 1;
    if (InvCost > 0) and (InvUPC > 0) and (InvCost <> PrdCost) and (PrdUPC <> InvUPC) then
      NewPrdCost := (InvCost / InvUPC) * PrdUPC
    else
      NewPrdCost := InvCost;
    if (NewPrdCost > 0) and (qInvoicePrdmstr.FieldByName('Cost').AsCurrency <> NewPrdCost) then
    begin
      DataHelpers.MakeEdtMode(qInvoicePrdmstr);
      if qInvoicePrdmstr.FieldByName('CstLastC').AsCurrency <> qInvoicePrdmstr.FieldByName('Cost').AsCurrency then
      begin
        qInvoicePrdmstr.FieldByName('CstLastC').AsCurrency := qInvoicePrdmstr.FieldByName('Cost').AsCurrency;
        // qInvoicePrdmstr.FieldByName('CstChenge').AsDateTime := qInvoiceHead.FieldByName('Date').AsDateTime;
        qInvoicePrdmstr.FieldByName('CstChenge').AsDateTime := Date;
      end;
      qInvoicePrdmstr.FieldByName('Cost').AsCurrency := NewPrdCost;
      PrdUpdateList.Add(qInvoiceDetail.FieldByName('UpcCode').AsString);
    end;
  end
  else if SameText(Sender.FieldName, 'CostFlag') and
    (qInvoicePrdmstr.FieldByName('CostFlag').AsString <> Sender.AsString) and (InvValidationMode <> ivmRowSilent) and
    (not ProgPref('VendInvNoPrdCost')) then
  begin
    DataHelpers.MakeEdtMode(qInvoicePrdmstr);
    qInvoicePrdmstr.FieldByName('CostFlag').AsString := Sender.AsString;
  end
  else if (SameText(Sender.FieldName, 'UpcCode') or SameText(Sender.FieldName, 'AmntDel')) and
    (qInvoiceDetail.FieldByName('AmntDel').AsInteger > 0) and (InvValidationMode <> ivmRowSilent) and
    (qInvoicePrdmstr.FieldByName('DelDate').AsDateTime <= qInvoiceHead.FieldByName('Date').AsDateTime) then
  begin
    if qInvoiceHead.FieldByName('Quick').AsBoolean then
      PrdUpdateList.Add(qInvoiceDetail.FieldByName('UpcCode').AsString)
    else
    begin
      DataHelpers.MakeEdtMode(qInvoicePrdmstr);
      qInvoicePrdmstr.FieldByName('LstDel').AsInteger := qInvoiceDetail.FieldByName('AmntDel').AsInteger;
      qInvoicePrdmstr.FieldByName('DelDate').AsDateTime := qInvoiceHead.FieldByName('Date').AsDateTime;
      if not NoLstVend then
        qInvoicePrdmstr.FieldByName('LstVend').AsString := VendID;
    end;
  end;

  if (qInvoicePrdmstr.State = dsEdit) and (AState <> qInvoicePrdmstr.State) then
    qInvoicePrdmstr.Post;
end;

procedure TVendInvHelpers.qInvoiceCostChangeToPrdmstr;
var
  PrdCost, InvCost, InvOrigCost, VenCost, NewPrdCost, NewVenCost: Currency;
  VenUPC, PrdUPC, InvUPC, VenUPCX, ASelected: Integer;
  bChangePrdCost: Boolean;
  AStringList: TStringList;
  AModalResult: TModalResult;
begin
  if (qInvoiceDetail.FieldByName('AmntDel').AsInteger < 0) or (InvTransType = ittCredit) or InternalNoUpdateToPrdmstr or
    ProgPref('VendInvNoPrdCost') then
    Exit; // Vendor Return, we do not record those to Prdmstr

  if InternalUpdateDiscount then
  begin
    qInvoiceCostChangeToPrdmstrDiscount;
    Exit;
  end;

  bChangePrdCost := True;
  if (not InternalNoThisDlvOnly) and qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString) then
  begin
    InvCost := qInvoiceDetail.FieldByName('Cost').AsCurrency;
    InvOrigCost := qInvoiceDetail.FieldByName('OrigCst').AsCurrency;
    PrdCost := qInvoicePrdmstr.FieldByName('Cost').AsCurrency;
    PrdUPC := qInvoicePrdmstr.FieldByName('UPC').AsInteger;
    InvUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    VendItemValues_GetSet(VenUPC, VenCost);

    if PrdUPC = 0 then
      PrdUPC := 1;
    VenUPCX := VenUPC;
    if VenUPCX = 0 then
      VenUPCX := PrdUPC;

    if (InvCost > 0) and (InvUPC > 0) and (InvCost <> PrdCost) and (PrdUPC <> InvUPC) then
      NewPrdCost := (InvCost / InvUPC) * PrdUPC
    else
      NewPrdCost := InvCost;
    if (InvOrigCost > 0) and (InvUPC > 0) and (InvOrigCost <> VenCost) and (VenUPCX <> InvUPC) then
      NewVenCost := (InvOrigCost / InvUPC) * VenUPCX
    else
      NewVenCost := InvOrigCost;

    if ((InvCost > 0) or (InvOrigCost > 0)) and ((PrdCost <> NewPrdCost) or (VenCost <> NewVenCost)) then
    begin
      if (InvValidationMode = ivmRowSilent) and ((PrdCost = NewPrdCost) and (VenCost <> NewVenCost)) then
        // if we're checking Cost only because of NewVenCost that matches PrdCost, we ignore it for ivmRowSilent
      else if PostFlagModeRecord then
        bChangePrdCost := False
      else
      begin
        if not ProgPref('CostThisDlvOnly') then
          ASelected := 2
        else
          ASelected := -1;
        if PrdCost = 0 then
        begin
          if VenCost = 0 then
            ASelected := 2
          else
            ASelected := 0;
        end;
        if ASelected = -1 then
        begin
{$IFDEF NONGUI}
          ASelected := 2;
{$ELSE}
          ASelected := VIGH.PromptCostToPrdmstr(PrdCost, VenCost, NewPrdCost, NewVenCost);
{$ENDIF}
        end;
        bChangePrdCost := False;
        if InvUPC = 0 then
          InvUPC := PrdUPC
        else
          InvUPC := 0; // DO NOT CHANGE CURRENT VEND-UPC
        case ASelected of
          0:
            bChangePrdCost := True;
          1:
            begin
              FCostThisDelvOnly := True;
              VendItemValues_GetSet(InvUPC, NewVenCost, False);
            end;
          2:
            begin
              bChangePrdCost := True;
              VendItemValues_GetSet(InvUPC, NewVenCost, False);
            end;
          3:
            FCostThisDelvOnly := True;
          -1:
            if not VarIsLikeNull(qInvoiceDetail.FieldByName('Cost').OldValue) then
              qInvoiceDetail.FieldByName('Cost').Value := qInvoiceDetail.FieldByName('Cost').OldValue;
        end;
        if ASelected >= 1 then
          qInvoiceDetail.FieldByName('CostOpt').AsInteger := ASelected + 1;
      end;
    end;
  end;
  if bChangePrdCost then
    qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('Cost'));
end;

procedure TVendInvHelpers.qInvoiceCostChangeToPrdmstrDiscount;
begin
  if qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString) and
    (qInvoiceDetail.FieldByName('CostOpt').AsInteger in [0, 1, 3]) then
    qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('Cost'));
end;

procedure TVendInvHelpers.qInvoiceDetailAfterDelete(DataSet: TDataSet);
begin
  CalcAmountSummary;
  if FUpchargeMulti and (not UpchargePercentOnly) then
    UpchargeCalcNeeded := True;
  FDataPosted := False;
  DataHelpers.RedoLineNumbers(qInvoiceDetail, 'DelLine');
end;

procedure TVendInvHelpers.qInvoiceDetailAfterInsert(DataSet: TDataSet);
begin
  if DelID <> '' then // else leave null
    DataSet.FieldByName('DelID').AsString := DelID;
  DataSet.FieldByName('VendID').AsString := VendID;
  DataSet.FieldByName('DelLine').AsInteger := FNextDelLine;
  DataSet.FieldByName('LineID').AsString := GenHelpers.GuidIdString;

  { TODO : Move these Default Values to database level, when creating temp table. }
  if not ProgPref('DefVendInvQuanZero') then
    DataSet.FieldByName('AmntDel').AsCurrency := 1
  else
    DataSet.FieldByName('AmntDel').AsCurrency := 0;
  // DataSet.FieldByName('BO').AsCurrency := 0;
  // DataSet.FieldByName('PO').AsCurrency := 0;
end;

procedure TVendInvHelpers.qInvoiceDetailAfterPost(DataSet: TDataSet);
begin
  CalcAmountSummary;
  if FUpchargeMulti and (not UpchargePercentOnly) then
    UpchargeCalcNeeded := True;
  if Assigned(FOnRecCountAlertSave) and (RecCountAlertSave > 0) and
    ((qInvoiceDetail.RecordCount - RecCountLastSave) > RecCountAlertSave) then
  begin
    FOnRecCountAlertSave(DataSet);
    RecCountLastSave := qInvoiceDetail.RecordCount;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailBeforeDelete(DataSet: TDataSet);
begin
  FDetailDeleteList.Add(DataSet.FieldByName('LineID').AsString);
  if DataSet.FieldByName('UpcCode').AsString <> '' then
    PrdUpdateList.Add(DataSet.FieldByName('UpcCode').AsString);
end;

procedure TVendInvHelpers.qInvoiceDetailBeforeInsert(DataSet: TDataSet);
begin
  DataSet.Last;
  if (not DataSet.IsEmpty) and DataSet.FieldByName('UpcCode').IsNull and DataSet.FieldByName('VenItmCde').IsNull and
    DataSet.FieldByName('Descrip').IsNull and DataSet.FieldByName('Cost').IsNull
  { and (DataSet.FieldByName('AmntDel').AsCurrency = 0) } then
    SysUtils.Abort;
  FNextDelLine := DataSet.FieldByName('DelLine').AsInteger + 1;
end;

procedure TVendInvHelpers.qInvoiceDetailCostChange(Sender: TField);
var
  AUPC: Integer;
begin
  if InternalFieldChange then
    Exit;

  InternalFieldChange := True;
  try
    FCostThisDelvOnly := False;
    if not FUpchargeMulti then
      qInvoiceDetail.FieldByName('OrigCst').AsCurrency :=
        VendHelpersI.AmountBeforeDiscount(qInvoiceDetail.FieldByName('Cost').AsCurrency,
        qInvoiceHead.FieldByName('Discount').AsCurrency)
    else if UpchargePercentOnly then
      qInvoiceDetail.FieldByName('OrigCst').AsCurrency :=
        CalculateEnhancedDiscountsRow(qInvoiceDetail.FieldByName('Cost').AsCurrency, vkSubtract)
    else { only if something entered in discount fields, else take COST here --also need to adjust for different U/P/C
      for returns we didn't yet adjust UPC and...
    }
    begin
      qInvoiceDetail.FieldByName('OrigCst').AsCurrency := qInvoiceDetail.FieldByName('Cost').AsCurrency;
      UpchargeCalcNeeded := True;
    end;

    AUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    if AUPC = 0 then
      AUPC := 1;
    qInvoiceDetail.FieldByName('Prd_UnitCost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency / AUPC;

    CalcRowAmount(qInvoiceDetail);

    qInvoiceCostChangeToPrdmstr;

    UpdateCostLevelValue;
    UpdateCostFlagValue;

    if Assigned(FOnInvoiceCostChange) then
      FOnInvoiceCostChange(Sender);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailGeneralChange(Sender: TField);
begin
  { This procedure should be assigned to all fields that need to update just Prdmstr }
  if InternalFieldChange then
    Exit;

  InternalFieldChange := True;
  try
    qInvoiceChangesToPrdmstr(Sender);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailOrigCostChange(Sender: TField);
var
  AUPC: Integer;
begin
  if InternalFieldChange then
    Exit;

  InternalFieldChange := True;
  try
    if not FUpchargeMulti then
      qInvoiceDetail.FieldByName('Cost').AsCurrency := VendHelpersI.AmountAfterDiscount
        (qInvoiceDetail.FieldByName('OrigCst').AsCurrency, qInvoiceHead.FieldByName('Discount').AsCurrency)
    else if UpchargePercentOnly then
      qInvoiceDetail.FieldByName('Cost').AsCurrency :=
        CalculateEnhancedDiscountsRow(qInvoiceDetail.FieldByName('OrigCst').AsCurrency)
    else
    begin
      qInvoiceDetail.FieldByName('Cost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency;
      UpchargeCalcNeeded := True;
    end;

    CalcRowAmount(qInvoiceDetail);

    AUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    if AUPC = 0 then
      AUPC := 1;
    qInvoiceDetail.FieldByName('Prd_UnitCost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency / AUPC;

    qInvoiceCostChangeToPrdmstr;

    UpdateCostLevelValue;
    if not FInternalDetailFieldChange then
      UpdateCostFlagValue;

    if Assigned(FOnInvoiceCostChange) then
      FOnInvoiceCostChange(Sender);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailPrdmstrChange(Sender: TField);
var
  DS: TDataSet;
  ACode, AFieldName: string;
begin
  if InternalFieldChange then
    Exit;

  if FInternalDetailDataChange then
    Exit;

  FInternalDetailDataChange := True;
  try
    if Assigned(OnInvoicePrdmstrChange) then
      OnInvoicePrdmstrChange(Sender);
    DS := Sender.DataSet;
    ACode := DS.FieldByName('UpcCode').AsString;
    if (ACode <> '') and PrdHelpersI.FindBarcode(dmDataPrd.tbPrdmstr, ACode) then
    begin
      AFieldName := Copy(Sender.FieldName, 5, Length(Sender.FieldName));
      dmDataPrd.tbPrdmstr.Edit;
      dmDataPrd.tbPrdmstr.FieldByName(AFieldName).Value := Sender.Value;
      dmDataPrd.tbPrdmstr.Post;
    end
    else
      Sender.Value := Sender.OldValue;
  finally
    FInternalDetailDataChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailPrintCostChange(Sender: TField);
begin
  if InternalFieldChange then
    Exit;

  InternalFieldChange := True;
  try
    CalcRowPrintAmount(qInvoiceDetail);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailQtyFilledPriceChange(Sender: TField);
var
  DS: TDataSet;
  AMode: TModalResult;
  ACostByUnit: Boolean;
begin
  if FInternalDetailDataChange then
    Exit;

  FInternalDetailDataChange := True;
  try
    DS := Sender.DataSet;

    if DS.FieldByName('AmntDel').IsNull then
      DS.FieldByName('AmntDel').AsCurrency := 0;

    if SameText(Sender.FieldName, 'AmntDel') and (DS.FieldByName('AmntDel').AsCurrency <= 0) and
      (DS.FieldByName('UpcCode').AsString <> '') then
      PrdUpdateList.Add(DS.FieldByName('UpcCode').AsString);

    if DS.FieldByName('UPCC').IsNull or (DS.FieldByName('UPCC').AsInteger = 0) then
      DS.FieldByName('UPCC').AsInteger := 1;

    if (InvTransType = ittCredit) and (ItemByCaseUnit = ibcuDefault) and SameText(Sender.FieldName, 'AmntDel') and
      (DS.FieldByName('AmntDel').AsCurrency > 0.0000) and
      ((DS.FieldByName('Cost').AsCurrency > 0) or (DS.FieldByName('UPCC').AsInteger > 1)) and
      (DS.FieldByName('UpcCode').AsString <> '') then
{$IFDEF NONGUI}
      ItemByCaseUnit := ibcuCase;
{$ELSE}
      VIGH.VendCreditQuanMode;
{$ENDIF}
    ACostByUnit := (InvTransType = ittCredit) and (ItemByCaseUnit = ibcuUnit) and
      (DS.FieldByName('UPCC').AsInteger > 1);

{$IFNDEF NONGUI}
    ACostByUnit := VIGH.PromptCostByUnit(DS, Sender);
{$ENDIF}
    if ACostByUnit then
    begin
      DS.FieldByName('OrigCst').AsCurrency := DS.FieldByName('OrigCst').AsCurrency / DS.FieldByName('UPCC').AsInteger;
      DS.FieldByName('UPCC').AsInteger := 1;
    end;

    CalcRowAmount(DS);
    qInvoiceChangesToPrdmstr(Sender);
    if ProgPref('VendInvUsePrintCost') then
      CalcRowPrintAmount(DS);
  finally
    FInternalDetailDataChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailUnitCostChange(Sender: TField);
var
  AUPC: Integer;
begin
  if InternalFieldChange then
    Exit;

  InternalFieldChange := True;
  try
    AUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    if AUPC = 0 then
      AUPC := 1;
    qInvoiceDetail.FieldByName('OrigCst').AsCurrency := qInvoiceDetail.FieldByName('Prd_UnitCost').AsCurrency * AUPC;
    if not FUpchargeMulti then
      qInvoiceDetail.FieldByName('Cost').AsCurrency := VendHelpersI.AmountAfterDiscount
        (qInvoiceDetail.FieldByName('OrigCst').AsCurrency, qInvoiceHead.FieldByName('Discount').AsCurrency)
    else if UpchargePercentOnly then
      qInvoiceDetail.FieldByName('Cost').AsCurrency :=
        CalculateEnhancedDiscountsRow(qInvoiceDetail.FieldByName('OrigCst').AsCurrency)
    else
    begin
      qInvoiceDetail.FieldByName('Cost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency;
      UpchargeCalcNeeded := True;
    end;
    CalcRowAmount(qInvoiceDetail);

    qInvoiceCostChangeToPrdmstr;

    UpdateCostLevelValue;
    if not FInternalDetailFieldChange then
      UpdateCostFlagValue;

    if Assigned(FOnInvoiceCostChange) then
      FOnInvoiceCostChange(Sender);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailUpcCodeChange(Sender: TField);
var
  ACode: string;
  bItemExists, AExit: Boolean;
  APrice, AWeight: string;
begin
  if (not VarIsLikeNull(Sender.OldValue)) and (Sender.OldValue <> '') then
    PrdUpdateList.Add(Sender.OldValue);

  if qInvoiceDetail.FieldByName('UpcCode').AsString = '' then
    Exit;

  if InternalFieldChange then
  begin
    if UpdateUpcCodeDefaults then
      DoUpdateUpcCodeDefaults(Sender, False);
    Exit;
  end;

  InternalFieldChange := True;
  try
    if not qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString) then
    begin
      // try diff find, edit code, call qInvoicePrdmstrMoveToCode again
      ACode := qInvoiceDetail.FieldByName('UpcCode').AsString;
      AWeight := '';
      bItemExists := PrdHelpersI.FindBarcode(nil, ACode, APrice, AWeight);
      if bItemExists then
      begin
        qInvoiceDetail.FieldByName('UpcCode').AsString := ACode;
        if AWeight <> '' then
          qInvoiceDetail.FieldByName('AmntDel').AsString := AWeight;
      end
      else if GenHelpers.IsModuleRegistered(MODULE_LIST_CARTON_CODES) then
      begin
        bItemExists := PrdHelpersI.FindUpcFromCartonCode(ACode);
        if bItemExists then
          qInvoiceDetail.FieldByName('UpcCode').AsString := ACode
      end;

{$IFNDEF NONGUI}
      if not bItemExists then
      begin
        bItemExists := VIGH.UpcCodeChangePrompts(ACode, AExit);
        if AExit then
          Exit;
      end;
{$ENDIF}
      if not bItemExists then
        qInvoiceDetail.FieldByName('UpcCode').AsString := '';
    end;

    DoUpdateUpcCodeDefaults(Sender, True);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailUPCC_Change(Sender: TField);
var
  PrdUPC, InvUPC, VenUPC: Integer;
  VenCost: Currency;
  ASelected: Integer;
  AModalResult: TModalResult;
begin
  if InternalFieldChange or InternalNoUpdateToPrdmstr then
    Exit;
  if (qInvoiceDetail.FieldByName('UpcCode').AsString = '') or
    (not qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString)) then
    Exit;
  if (qInvoiceDetail.FieldByName('AmntDel').AsInteger < 0) or (InvTransType = ittCredit) then
    Exit; // Vendor Return, we do not record those to Prdmstr

  InternalFieldChange := True;
  try
    PrdUPC := qInvoicePrdmstr.FieldByName('UPC').AsInteger;
    InvUPC := qInvoiceDetail.FieldByName('UPCC').AsInteger;
    VendItemValues_GetSet(VenUPC, VenCost);
    if (InvUPC <> 0) and ((InvUPC <> PrdUPC) or (InvUPC <> VenUPC)) and (not PostFlagModeRecord) then
    begin
      ASelected := -1;
      VenCost := 0;
      if PrdUPC = 0 then
      begin
        if VenUPC = 0 then
          ASelected := 2
        else
          ASelected := 0;
      end
      else if (InvUPC = PrdUPC) and (VenUPC = 0) then
        ASelected := 1;

      if ASelected = -1 then
      begin
{$IFDEF NONGUI}
        ASelected := 2;
{$ELSE}
        ASelected := VIGH.PromptUPCC_ToPrdmstr(PrdUPC, InvUPC, VenUPC);
{$ENDIF}
      end;
      case ASelected of
        0:
          qInvoiceChangesToPrdmstr(Sender);
        1:
          VendItemValues_GetSet(InvUPC, VenCost, False);
        2:
          begin
            qInvoiceChangesToPrdmstr(Sender);
            VendItemValues_GetSet(InvUPC, VenCost, False);
          end;
        -1:
          Sender.Value := Sender.OldValue;
      end;
      if (qInvoiceDetail.FieldByName('OrigCst').AsCurrency > 0) and (qInvoiceDetail.FieldByName('UPCC').AsInteger > 0)
      then
        qInvoiceDetail.FieldByName('Prd_UnitCost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency /
          qInvoiceDetail.FieldByName('UPCC').AsInteger;
      UpdateCostLevelValue;
      if (ASelected in [0, 2]) and Assigned(FOnInvoiceUpcChange) then
        FOnInvoiceUpcChange(Sender);

      { // Product does not have U/P/C or wants to change permenantly
        if (PrdUPC = 0) or (MessageDlg('Units Per Case for Product is set as ' + IntToStr(PrdUPC) + ',' + #13 + #10 +
        'Set Units Per Case to new value of ' + IntToStr(InvUPC) + '?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
        qInvoiceChangesToPrdmstr(Sender);
        if Assigned(FOnInvoiceUpcChange) then
        FOnInvoiceUpcChange(Sender);
        end; }
    end
    else
    begin
      if InvUPC = 0 then
        InvUPC := 1;
      qInvoiceDetail.FieldByName('Prd_UnitCost').AsCurrency := qInvoiceDetail.FieldByName('OrigCst').AsCurrency
        / InvUPC;
      UpdateCostLevelValue;
    end;
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceDetailVenItemCodeChange(Sender: TField);
var
  ACode: string;
begin
  if InternalFieldChange then
    Exit;
  if qInvoiceDetail.FieldByName('VenItmCde').AsString = '' then
    Exit;

  InternalFieldChange := True;
  try
    {$IFNDEF NONGUI}
    if VIGH.VenItemCodeChangePrompts then
      Exit;
    {$ENDIF}

    VendHelpersI.DoUpcVenCodeLookup(qInvoiceDetail, Sender, FInternalFieldChange);
  finally
    InternalFieldChange := False;
  end;
end;

procedure TVendInvHelpers.qInvoiceHeadAfterPost(DataSet: TDataSet);
begin
  FDataPosted := False;
end;

procedure TVendInvHelpers.qInvoiceHeaderDiscountChange(Sender: TField);
var
  APos: TBookmark;
begin
  // AppActions.ShowHourGlassCursor;
  APos := qInvoiceDetail.GetBookmark;
  FInternalDetailFieldChange := True;
  InternalUpdateDiscount := True;
  DisableQueryControls;
  try
    qInvoiceDetail.First;
    while not qInvoiceDetail.Eof do
    begin
      DataHelpers.MakeEdtMode(qInvoiceDetail);
      qInvoiceDetailOrigCostChange(qInvoiceDetail.FieldByName('OrigCst'));
      qInvoiceDetail.Next;
    end;
    qInvoiceDetail.GotoBookmark(APos);
  finally
    FInternalDetailFieldChange := False;
    InternalUpdateDiscount := False;
    EnableQueryControls;
    qInvoiceDetail.FreeBookmark(APos);
    // AppActions.HideHourGlassCursor;
  end;
  CalcAmountSummary;
  if Assigned(FOnDetailSummaryChange) then
    FOnDetailSummaryChange(Sender);
end;

procedure TVendInvHelpers.qInvoiceHeaderEnhancedDiscountChange(Sender: TField);
begin
  LoadUpchargeLineFactor;
  if UpchargePercentOnly then
    CalculateEnhancedDiscountsAll
  else
    UpchargeCalcNeeded := True;
end;

procedure TVendInvHelpers.qInvoiceHeaderPrintDiscountChange(Sender: TField);
var
  ALineFactor: Double;
  APos: TBookmark;
  ACalcCost: Double;
begin
  if FInternalDetailFieldChange or InternalUpdateDiscount then
    Exit;

  ALineFactor := PrintDiscountLineFactor;

  if qInvoiceDetail.Active then
    qInvoiceDetail.CheckBrowseMode;

  // AppActions.ShowHourGlassCursor;
  APos := qInvoiceDetail.GetBookmark;
  FInternalDetailFieldChange := True;
  InternalUpdateDiscount := True;
  DisableQueryControls;
  try
    qInvoiceDetail.First;
    while not qInvoiceDetail.Eof do
    begin
      ACalcCost := CalculatePrintDiscountsRow(qInvoiceDetail.FieldByName('PrintCost').AsCurrency, ALineFactor);
      if qInvoiceDetail.FieldByName('PrintNet').AsCurrency <> ACalcCost then
      begin
        DataHelpers.MakeEdtMode(qInvoiceDetail);
        qInvoiceDetail.FieldByName('PrintNet').AsCurrency := ACalcCost;
        qInvoiceDetailPrintCostChange(qInvoiceDetail.FieldByName('PrintCost'));
      end;
      qInvoiceDetail.Next;
    end;
    qInvoiceDetail.GotoBookmark(APos);
  finally
    FInternalDetailFieldChange := False;
    InternalUpdateDiscount := False;
    EnableQueryControls;
    qInvoiceDetail.FreeBookmark(APos);
    // AppActions.HideHourGlassCursor;
  end;
end;

function TVendInvHelpers.qInvoicePrdmstr2: TAdsQuery;
const
  PrdmstrCreate2 = 'Select d.*, False __Updated, ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) PriceUp, Cast(0 AS SQL_NUMERIC(12,2)) PriceGin, ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) SaleUp, Cast(0 AS SQL_NUMERIC(12,2)) SaleGin, ' +
    'Cast(0 AS SQL_NUMERIC(12,2)) OnHand, ' + 'Cast(0 AS SQL_NUMERIC(12,2)) UnitCost, ' +
    'Cast(0 AS SQL_INTEGER) SaleStatus ' + 'Into #InvoicePRD_Edit From Prdmstr d Where Upper(Code)=:UpcCode;';
begin
  if not Assigned(FqInvoicePrdmstr2) then
  begin
    FqInvoicePrdmstr2 := dmData.CreateQueryObject(Application);
    FqInvoicePrdmstr2.RequestLive := True;
    FqInvoicePrdmstr2.SQL.Text := PrdmstrCreate2;
    FqInvoicePrdmstr2.Prepare;
    FqInvoicePrdmstr2.ParamByName('UpcCode').AsString := EmptyPrdCode;
    FqInvoicePrdmstr2.Open;

    FqInvoicePrdmstr2.Close;
    FqInvoicePrdmstr2.SQL.Clear;
    FqInvoicePrdmstr2.SQL.Add('Delete From #InvoicePRD_Edit;');
    FqInvoicePrdmstr2.SQL.Add('Insert Into #InvoicePRD_Edit Select d.*, False, 0, 0, 0, 0, 0, 0, -1');
    FqInvoicePrdmstr2.SQL.Add('From Prdmstr d Where Upper(Code)=:UpcCode;');
    FqInvoicePrdmstr2.SQL.Add('Update p Set OnHand=o.Amount From #InvoicePRD_Edit p, Onhand o');
    FqInvoicePrdmstr2.SQL.Add('Where Upper(p.Code)=:UpcCode And Upper(p.Code)=Upper(o.UpcCode);');
    FqInvoicePrdmstr2.SQL.Add('Select * From #InvoicePRD_Edit;');
    FqInvoicePrdmstr2.Prepare;
    FqInvoicePrdmstr2.ParamByName('UpcCode').AsString := EmptyPrdCode;
    FqInvoicePrdmstr2.Open;
  end;
  Result := FqInvoicePrdmstr2;
end;

function TVendInvHelpers.qInvoicePrdmstr2MoveToCode(ACode: string): Boolean;
begin
  Result := ((ACode <> '') and (ACode <> EmptyPrdCode)) or (not Assigned(FqInvoicePrdmstr2));
  if not Result then
    Exit;

  qInvoicePrdmstr2.Close;
  qInvoicePrdmstr2.ParamByName('UpcCode').AsString := ACode;
  qInvoicePrdmstr2.Open;
  Result := SameText(qInvoicePrdmstr2.FieldByName('Code').AsString, ACode);
{$IFNDEF NONGUI}
  if Result then
    VIGH.UpdatePrdMarkupMargin(qInvoicePrdmstr2, '');
{$ENDIF}
end;

function TVendInvHelpers.qInvoicePrdmstrMoveToCode(ACode: string; AReload: Boolean): Boolean;
begin
  Result := (ACode <> '') and (ACode <> EmptyPrdCode);
  if not Result then
    Exit;
  if (not AReload) and qInvoicePrdmstr.Active and SameText(qInvoicePrdmstr.FieldByName('Code').AsString, ACode) then
    Exit;

  qInvoicePrdmstr.Close;
  qInvoicePrdmstr.ParamByName('UpcCode').AsString := ACode;
  qInvoicePrdmstr.Open;
  qInvoicePrdmstr.FieldByName('Price').OnChange := OnPrdPriceChange;
  qInvoicePrdmstr.FieldByName('Cost').OnChange := OnPrdCostChange;
  Result := SameText(qInvoicePrdmstr.FieldByName('Code').AsString, ACode);
end;

procedure TVendInvHelpers.StartNewInvoice;
var
  AQuery: TAdsQuery;
begin
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := Format('Select FOB, Discount, TermID From VenMstr Where VendID=''%s''', [VendID]);
  AQuery.Open;

  qInvoiceHead.Insert;
  qInvoiceHead.FieldByName('VendID').AsString := VendID;
  qInvoiceHead.FieldByName('Type').AsInteger := 0;
  qInvoiceHead.FieldByName('Date').AsDateTime := Date;
  qInvoiceHead.FieldByName('EnteredBy').AsString := GuiNonGuiHelpers.UserName;
  qInvoiceHead.FieldByName('FOB').AsString := AQuery.FieldByName('FOB').AsString;
  qInvoiceHead.FieldByName('Paid').AsCurrency := 0;
  qInvoiceHead.FieldByName('TermID').AsString := AQuery.FieldByName('TermID').AsString;
  qInvoiceHead.FieldByName('Discount').AsCurrency := AQuery.FieldByName('Discount').AsCurrency;
  qInvoiceHead.FieldByName('Quick').AsBoolean := False;
  qInvoiceHead.FieldByName('InvLocation').AsString := EnvSetting('OnHandLoc').AsString;
  qInvoiceHead.FieldByName('Status').AsInteger := 0;
  InvTransType := ittDebit;
  DefaultUpchargeValues;
  qInvoiceHead.Post;

  FDataPosted := False;
end;

procedure TVendInvHelpers.SwitchLines(AForward: Boolean);
var
  AQuery: TAdsQuery;
  DataSet: TDataSet;
  ALine1, ALine2: Integer;
begin
  DataSet := qInvoiceDetail;

  if DataSet.IsEmpty or (DataSet.FieldByName('UpcCode').IsNull and DataSet.FieldByName('VenItmCde').IsNull and
    DataSet.FieldByName('Descrip').IsNull and DataSet.FieldByName('Cost').IsNull) then
    Exit;

  DataSet.CheckBrowseMode;
  ALine1 := DataSet.FieldByName('DelLine').AsInteger;
  if AForward then
  begin
    DataSet.Next;
    if DataSet.Eof then
      Exit;
  end
  else
  begin
    DataSet.Prior;
    if DataSet.Bof then
      Exit;
  end;
  ALine2 := DataSet.FieldByName('DelLine').AsInteger;

  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;

  AQuery.SQL.Add('Update #InvoiceDT_Edit Set DelLine=-1, __Updated=True Where DelLine=:Line2;');
  AQuery.SQL.Add('Update #InvoiceDT_Edit Set DelLine=:Line2, __Updated=True');
  AQuery.SQL.Add('Where DelLine=:Line1;');
  AQuery.SQL.Add('Update #InvoiceDT_Edit Set DelLine=:Line1, __Updated=True Where DelLine=-1;');

  AQuery.Prepare;
  AQuery.ParamByName('Line1').AsInteger := ALine1;
  AQuery.ParamByName('Line2').AsInteger := ALine2;
  AQuery.ExecSQL;

  DataPosted := False;

  DataSet.Refresh;
end;

procedure TVendInvHelpers.UpdateCostFlagValue;
var
  AOption: Integer;
begin
  if not(qInvoiceDetail.State in [dsEdit, dsInsert]) then
    Exit;

  if (qInvoiceDetail.FieldByName('CostFlag').AsString = '') or
    (qInvoiceDetail.FieldByName('CostFlag').AsString <> qInvoiceDetail.FieldByName('OldFlag').AsString) or
    (qInvoiceDetail.FieldByName('Cost').AsCurrency = qInvoiceDetail.FieldByName('OldCost').AsCurrency) then
    Exit;

  AOption := ProgPref('ClearCostFlagOptions');
  case AOption of
    0:
{$IFNDEF NONGUI}
      if VIGH.PromptClearPrevCostFlag then
{$ENDIF}
        qInvoiceDetail.FieldByName('CostFlag').AsString := '';
    2:
      qInvoiceDetail.FieldByName('CostFlag').AsString := '';
  end;
end;

procedure TVendInvHelpers.UpdateCostLevelValue;
var
  AUnitCost: Currency;
  AUpcc: Integer;
begin
  if not(qInvoiceDetail.State in [dsEdit, dsInsert]) then
    Exit;

  // Prd_UnitCost is calculated by OrigCst so it can't be used here
  AUpcc := qInvoiceDetail.FieldByName('UPCC').AsInteger;
  if AUpcc <= 0 then
    AUpcc := 1;
  AUnitCost := RoundTo(qInvoiceDetail.FieldByName('Cost').AsCurrency / AUpcc, -2);

  if qInvoiceDetail.FieldByName('Cost').AsCurrency = 0 then
    qInvoiceDetail.FieldByName('CostLevel').AsInteger := 0
  else if (qInvoiceDetail.FieldByName('Prd_OldUnitCost').AsCurrency <> 0) and (AUnitCost <> 0) then
  begin
    if AUnitCost > qInvoiceDetail.FieldByName('Prd_OldUnitCost').AsCurrency then
      qInvoiceDetail.FieldByName('CostLevel').AsInteger := 1
    else if AUnitCost < qInvoiceDetail.FieldByName('Prd_OldUnitCost').AsCurrency then
      qInvoiceDetail.FieldByName('CostLevel').AsInteger := -1
    else
      qInvoiceDetail.FieldByName('CostLevel').AsInteger := 0;
  end
  else if qInvoiceDetail.FieldByName('Cost').AsCurrency > qInvoiceDetail.FieldByName('OldCost').AsCurrency then
    qInvoiceDetail.FieldByName('CostLevel').AsInteger := 1
  else if qInvoiceDetail.FieldByName('Cost').AsCurrency < qInvoiceDetail.FieldByName('OldCost').AsCurrency then
    qInvoiceDetail.FieldByName('CostLevel').AsInteger := -1
  else
    qInvoiceDetail.FieldByName('CostLevel').AsInteger := 0;
end;

procedure TVendInvHelpers.ValidateInvoiceRow;
begin
  qInvoiceDetail.Edit;
  qInvoiceDetailUPCC_Change(qInvoiceDetail.FieldByName('UPCC'));
  qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('AmntDel'));
  qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('Cost'));
  qInvoiceChangesToPrdmstr(qInvoiceDetail.FieldByName('CostFlag'));
  qInvoiceDetail.FieldByName('PostFlag').AsInteger := 0;
  qInvoiceDetail.Post;
end;

procedure TVendInvHelpers.VendItemValues_GetSet(var AUPC: Integer; var ACost: Currency; ANoSet: Boolean);
var
  AQuery: TAdsQuery;
  AVenItmC, AUpcCode: string;
begin
  AVenItmC := qInvoiceDetail.FieldByName('VenItmCde').AsString;
  AUpcCode := qInvoiceDetail.FieldByName('UpcCode').AsString;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;
  if (not ANoSet) and ((AUPC > 0) or (ACost > 0)) then
  begin
    AQuery.SQL.Add('Update VenItm Set');
    if AUPC > 0 then
    begin
      AQuery.SQL.Add(Format('UPC=%d', [AUPC]));
      if ACost > 0 then
        AQuery.SQL.Add(',');
    end;
    if ACost > 0 then
      AQuery.SQL.Add(Format('Cost=%f', [ACost]));
    AQuery.SQL.Add('Where VendID=:VendID And VenItmC=:VenItmC;');
  end;
  AQuery.SQL.Add('Select UPC, Cost From VenItm Where VendID=:VendID And VenItmC=:VenItmC');
  if AUpcCode <> '' then
    AQuery.SQL.Add('And UpcCode=:UpcCode');
  AQuery.SQL.Add(';');
  AQuery.Prepare;
  AQuery.ParamByName('VendID').AsString := VendID;
  AQuery.ParamByName('VenItmC').AsString := AVenItmC;
  if AUpcCode <> '' then
    AQuery.ParamByName('UpcCode').AsString := AUpcCode;
  AQuery.Open;
  if not AQuery.IsEmpty then
  begin
    AUPC := AQuery.Fields[0].AsInteger;
    ACost := AQuery.Fields[1].AsCurrency;
  end;
end;

procedure TVendInvHelpers.qInvoiceHDCalcVendInvItemCost(Sender: TField);
var
  APos: TBookmark;
begin
  // AppActions.ShowHourGlassCursor;
  APos := qInvoiceDetail.GetBookmark;
  DisableQueryControls;
  try
    qInvoiceDetail.First;
    while not qInvoiceDetail.Eof do
    begin
      qInvoiceDTCalcVendInvItemCost(Sender);
      qInvoiceDetail.Next;
    end;
    qInvoiceDetail.GotoBookmark(APos);
  finally
    EnableQueryControls;
    qInvoiceDetail.FreeBookmark(APos);
    // AppActions.HideHourGlassCursor;
  end;
end;

procedure TVendInvHelpers.qInvoiceHeaderVenInvNoChange(Sender: TField);
begin
  {$IFNDEF NONGUI}
  VIGH.qInvoiceHeaderVenInvNoChange(Sender);
  {$ENDIF}
end;

procedure TVendInvHelpers.InsertLine;
var
  AQuery: TAdsQuery;
  DataSet: TDataSet;
  ANewLine: Integer;
begin
  DataSet := qInvoiceDetail;

  if DataSet.IsEmpty or (DataSet.FieldByName('UpcCode').IsNull and DataSet.FieldByName('VenItmCde').IsNull and
    DataSet.FieldByName('Descrip').IsNull and DataSet.FieldByName('Cost').IsNull) then
    Exit;

  DataSet.CheckBrowseMode;

  ANewLine := DataSet.FieldByName('DelLine').AsInteger;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Text := 'Update #InvoiceDT_Edit Set DelLine=DelLine+1, __Updated=True Where DelLine>=:CurLine;';
  AQuery.Prepare;
  AQuery.ParamByName('CurLine').AsInteger := ANewLine;
  AQuery.ExecSQL;

  DataSet.Insert;
  DataSet.FieldByName('DelLine').AsInteger := ANewLine;
  DataSet.Post;
  DataSet.Refresh;
end;

procedure TVendInvHelpers.SetInternalFieldChange(const Value: Boolean);
begin
  FInternalFieldChange := Value;
  UpdateUpcCodeDefaults := not Value;
end;

procedure TVendInvHelpers.SetInvTransType(const Value: TInvTransType);
begin
  if not(Assigned(qInvoiceHead) and qInvoiceHead.Active and (not qInvoiceHead.IsEmpty)) then
    Exit;
  FInvTransType := Value;
  if qInvoiceHead.FieldByName('Type').AsInteger <> Integer(Value) then
  begin
    DataHelpers.MakeEdtMode(qInvoiceHead);
    qInvoiceHead.FieldByName('Type').AsInteger := Integer(Value);
    CalcAmountSummary;
    if Assigned(OnDetailSummaryChange) then
      OnDetailSummaryChange(nil);
  end;

  if Assigned(OnInvTransTypeChange) then
    OnInvTransTypeChange(nil);
end;

procedure TVendInvHelpers.SetVendID(const Value: string);
var
  AQuery: TAdsQuery;
begin
  if FVendID = Value then
    Exit;

  FVendID := Value;
  AQuery := VendHelpersI.VendLookupRecord(FVendID);
  if not AQuery.IsEmpty then
  begin
    FVendEmailAddress := AQuery.FieldByName('EMail').AsString;
    FNoLstVend := AQuery.FieldByName('NoLstVend').AsBoolean;
  end;
end;

procedure TVendInvHelpers.DoAddItemsFrom_TempDelItems(AImportCost: Boolean);
var
  AQuery: TAdsQuery;
  ADataSet: TDataSet;
  bFirst: Boolean;
  AField: string;
  x: Integer;
  AOnInvoiceCostChange: TFieldNotifyEvent;
  AOnInvoiceUpcChange: TFieldNotifyEvent;
  AOnDetailSummaryChange, AOnRecCountAlertSave: TNotifyEvent;
  AOldInvValidationMode: TInvValidationMode;
begin
  // AppActions.ShowHourGlassCursor;

  AQuery := dmData.CreateQueryObject(nil);
  ADataSet := qInvoiceDetail;

  AOnInvoiceCostChange := OnInvoiceCostChange;
  AOnInvoiceUpcChange := OnInvoiceUpcChange;
  AOnDetailSummaryChange := OnDetailSummaryChange;
  AOnRecCountAlertSave := OnRecCountAlertSave;
  OnInvoiceCostChange := nil;
  OnInvoiceUpcChange := nil;
  OnDetailSummaryChange := nil;
  OnRecCountAlertSave := nil;

  bFirst := True;
  AOldInvValidationMode := InvValidationMode;
  InvValidationMode := ivmRowSilent;
  ADataSet.DisableControls;
  try
    AQuery.SQL.Clear;
    AQuery.SQL.Add(AField);
    AQuery.SQL.Add('Select * From #TempDelItems1 Where [Selected]=True Order By IfNull([Sort], 999);');
    AQuery.Open;

    ADataSet.Last;
    bFirst := (ADataSet.FieldByName('UpcCode').AsString = ''); // if empty we don't Insert first
    while not AQuery.Eof do
    begin
      if not bFirst then
        ADataSet.Insert
      else if not(ADataSet.State in [dsInsert, dsEdit]) then
      begin
        ADataSet.Edit;
        bFirst := False;
      end;
      for x := 0 to Pred(ADataSet.FieldCount) do
      begin
        AField := ADataSet.Fields[x].FieldName;
        if (not AImportCost) and (SameText(AField, 'Cost') or SameText(AField, 'OrigCst')) then
          Continue;
        if (AQuery.FindField(AField) <> nil) and (AQuery.FieldByName(AField).AsString <> '') then
          ADataSet.FieldByName(AField).Value := AQuery.FieldByName(AField).Value;
      end;
      ADataSet.Post;
      if ADataSet.FieldByName('PostFlag').AsInteger = 0 then
      begin
        InvValidationMode := ivmRow;
        ValidateInvoiceRow;
        InvValidationMode := ivmRowSilent;
      end;
      AQuery.Next;
    end;
  finally
    ADataSet.EnableControls;
    FreeAndNil(AQuery);
    InvValidationMode := AOldInvValidationMode;
    OnInvoiceCostChange := AOnInvoiceCostChange;
    OnInvoiceUpcChange := AOnInvoiceUpcChange;
    OnDetailSummaryChange := AOnDetailSummaryChange;
    OnRecCountAlertSave := AOnRecCountAlertSave;
    // AppActions.HideHourGlassCursor;

    if (not bFirst) and Assigned(ADataSet.AfterPost) then
      ADataSet.AfterPost(ADataSet);
  end;
end;

procedure TVendInvHelpers.DoEmailVendInvReport(const AVendID, ADelID, ADelIDList, ASQLFilter, ALineID,
  AEmailRecipients: string);
begin
end;

procedure TVendInvHelpers.DoGenerateVendorCredits(const AFilterStr, ALineIdList: string; var ACreatedInvList: string);
var
  AQuery: TAdsQuery;
  AProcessedVendList, ARemark: string;
begin
  AProcessedVendList := '';
  AQuery := dmData.CreateQueryObject(nil);
  // AQuery.AdsConnection.BeginTransaction;
  try
    ARemark := 'Vendor Return ' + DateTimeToStr(Now);
    VendID := '';
    AQuery.SQL.Clear;
    AQuery.SQL.Add('Select * From VndRtrns Where VendID<>'''' And DelID Is Null And Quan>0');
    if AFilterStr <> '' then
      AQuery.SQL.Add('And (' + AFilterStr + ')');

    if ALineIdList <> '' then
      AQuery.SQL.Add('And (LineID IN (' + ALineIdList + '))');

    AQuery.SQL.Add('Order By VendID, DtTmCreated;');
    AQuery.Open;
    AQuery.First;
    if AQuery.IsEmpty then
      Exit;
    repeat
      if not SameText(AQuery.FieldByName('VendID').AsString, VendID) then
      begin
        VendID := AQuery.FieldByName('VendID').AsString;

        DelID := '';

        OpenQueries;
        InvTransType := ittCredit;
        ItemByCaseUnit := ibcuUnit;
        DataHelpers.MakeEdtMode(qInvoiceHead);
        qInvoiceHead.FieldByName('Remark').AsString := ARemark;
      end;
      if qInvoiceDetail.State <> dsInsert then
        qInvoiceDetail.Insert;
      qInvoiceDetail.FieldByName('UpcCode').AsString := AQuery.FieldByName('UpcCode').AsString;
      qInvoiceDetail.FieldByName('AmntDel').AsFloat := AQuery.FieldByName('Quan').AsInteger;
      qInvoiceDetail.FieldByName('SourceTable').AsString := 'VndRtrns';
      qInvoiceDetail.FieldByName('SourceLineID').AsString := AQuery.FieldByName('LineID').AsString;
      qInvoiceDetail.FieldByName('UPCC').AsInteger := 1;
      qInvoiceDetail.Post;

      AQuery.Next;

      if (not SameText(AQuery.FieldByName('VendID').AsString, VendID)) and (VendID <> '') then
      begin
        AProcessedVendList := AProcessedVendList + QuotedStr(VendID) + ',';
        PostQueries;
        ACreatedInvList := ACreatedInvList + QuotedStr(DelID) + ',';
      end;
    until AQuery.Eof;

    if VendID <> '' then
    begin
      AProcessedVendList := AProcessedVendList + QuotedStr(VendID) + ',';
      PostQueries;
      ACreatedInvList := ACreatedInvList + QuotedStr(DelID) + ',';
    end;

    if ACreatedInvList[Length(ACreatedInvList)] = ',' then
      Delete(ACreatedInvList, Length(ACreatedInvList), 1);
  except
    on E: Exception do
    begin
      // AQuery.AdsConnection.Rollback;
      raise;
    end;
  end;
  // AQuery.AdsConnection.Commit;
end;

procedure TVendInvHelpers.DoUpdateLastCostPrdInfo;
var
  AQuery: TAdsQuery;
  I: Integer;
begin
  // AppActions.ShowHourGlassCursor;
  AQuery := DataHelpers.HelperQuery;
  AQuery.SQL.Clear;

  for I := 0 to PrdUpdateList.Count - 1 do
    AQuery.SQL.Add(Format('Execute Procedure cdUpdateLastCostPrdInfo(%s, %s);', [QuotedStr(PrdUpdateList[I]),
      QuotedStr(FormatDateTime('yyyy-mm-dd', LatestInvoiceDate))]));
  if InvoiceDateChanged and qInvoiceDetail.Active then
  begin
    DisableQueryControls;
    try
      qInvoiceDetail.First;
      while not qInvoiceDetail.Eof do
      begin
        if (qInvoiceDetail.FieldByName('UpcCode').AsString <> '') and
          (PrdUpdateList.IndexOf(qInvoiceDetail.FieldByName('UpcCode').AsString) = -1) then
          AQuery.SQL.Add(Format('Execute Procedure cdUpdateLastCostPrdInfo(%s, %s);',
            [QuotedStr(qInvoiceDetail.FieldByName('UpcCode').AsString),
            QuotedStr(FormatDateTime('yyyy-mm-dd', LatestInvoiceDate))]));
        qInvoiceDetail.Next;
      end;
    finally
      EnableQueryControls;
    end;
  end;

  if AQuery.SQL.Count > 0 then
    AQuery.ExecSQL;

  // AppActions.HideHourGlassCursor;
end;

procedure TVendInvHelpers.DoUpdateUpcCodeDefaults(Sender: TField; AVenCodeLookup: Boolean);
var
  ANewCost: Double;
  APriceSuffix: string;
  AOldInternalFieldChange: Boolean;
  AFieldName: string;
  I: Integer;
begin
  if qInvoicePrdmstrMoveToCode(qInvoiceDetail.FieldByName('UpcCode').AsString) then
  begin
    qInvoiceDetail.FieldByName('Descrip').AsString := qInvoicePrdmstr.FieldByName('Desc').AsString;
    qInvoiceDetail.FieldByName('UPCC').AsInteger := qInvoicePrdmstr.FieldByName('UPC').AsInteger;
    qInvoiceDetail.FieldByName('CostFlag').AsString := qInvoicePrdmstr.FieldByName('CostFlag').AsString;
    qInvoiceDetail.FieldByName('OldFlag').AsString := qInvoicePrdmstr.FieldByName('CostFlag').AsString;

    APriceSuffix := qInvoiceHead.FieldByName('FOB').AsString;
    if SameText(APriceSuffix, 'LA') then
      APriceSuffix := '2'
    else if SameText(APriceSuffix, 'Factory') then
      APriceSuffix := '3'
    else
      APriceSuffix := '';
    qInvoiceDetail.FieldByName('OldCost').AsCurrency := qInvoicePrdmstr.FieldByName('Cost' + APriceSuffix).AsCurrency;
    qInvoiceDetail.FieldByName('OldUPC').AsInteger := qInvoicePrdmstr.FieldByName('UPC').AsInteger;
    if (qInvoiceDetail.FieldByName('OldCost').AsCurrency <> 0) and (qInvoiceDetail.FieldByName('OldUPC').AsInteger <> 0)
    then
      qInvoiceDetail.FieldByName('Prd_OldUnitCost').AsCurrency :=
        RoundTo(qInvoiceDetail.FieldByName('OldCost').AsCurrency / qInvoiceDetail.FieldByName('OldUPC').AsInteger, -2);

    for I := 0 to qInvoiceDetail.Fields.Count - 1 do
      if StartsText('Prd_', qInvoiceDetail.Fields[I].FieldName) then
      begin
        AFieldName := Copy(qInvoiceDetail.Fields[I].FieldName, 5, Length(qInvoiceDetail.Fields[I].FieldName));
        if qInvoicePrdmstr.FindField(AFieldName) <> nil then
          try
            qInvoiceDetail.Fields[I].Value := qInvoicePrdmstr.FieldByName(AFieldName).Value;
          except
          end;
      end;

    AOldInternalFieldChange := InternalFieldChange;
    InternalFieldChange := False; // Changing it to False so that Cost gets updated properly
    InternalNoThisDlvOnly := True;

    ANewCost := 0;
    if FUpchargeMulti and (not ProgPref('VendInvNoPrdCost')) then
      ANewCost := LastCostForProduct(qInvoiceDetail.FieldByName('UpcCode').AsString, DelID, False,
        qInvoiceDetail.FieldByName('UPCC').AsInteger);

    if ANewCost = 0 then
      ANewCost := VendHelpersI.AmountBeforeDiscount(qInvoicePrdmstr.FieldByName('Cost' + APriceSuffix).AsCurrency,
        qInvoiceHead.FieldByName('Discount').AsCurrency);

    qInvoiceDetail.FieldByName('OrigCst').AsCurrency := ANewCost;

    InternalFieldChange := AOldInternalFieldChange;
    InternalNoThisDlvOnly := False;

    if AVenCodeLookup then
      VendHelpersI.DoUpcVenCodeLookup(qInvoiceDetail, Sender, FInternalFieldChange);
    qInvoiceChangesToPrdmstr(Sender);
  end;
end;

procedure TVendInvHelpers.EnableQueryControls;
begin
  if Assigned(qInvoicePrdmstr) then
  begin
    qInvoiceHead.EnableControls;
    qInvoiceDetail.EnableControls;
    qInvoicePrdmstr.EnableControls;
  end;
  if Assigned(FqInvoicePrdmstr2) then
    FqInvoicePrdmstr2.EnableControls;
end;

procedure TVendInvHelpers.qInvoiceHeaderDateChange(Sender: TField);
begin
  if not Assigned(Sender) then
    Exit;
  if FInternalFieldChange then
    Exit;

  FInternalFieldChange := True;
  try
    if SameText(Sender.FieldName, 'Date') then
    begin
      InvoiceDateChanged := True;
      if (qInvoiceHead.FieldByName('Date').AsDateTime > LatestInvoiceDate) then
        LatestInvoiceDate := qInvoiceHead.FieldByName('Date').AsDateTime;
    end;

    if (not SameText(Sender.FieldName, 'BillDue')) and (qInvoiceHead.FieldByName('BillDue').IsNull or
      // VarIsLikeNull(Sender.OldValue) or
      (qInvoiceHead.FieldByName('BillDue').AsDateTime = (Sender.OldValue + FBillDueDays))) then
    begin
      qInvoiceHead.FieldByName('BillDue').AsDateTime := Sender.AsDateTime + FBillDueDays;
    end;
    if qInvoiceHead.State = dsEdit then
      qInvoiceHead.Post;
  finally
    FInternalFieldChange := False;
  end;
end;

end.
