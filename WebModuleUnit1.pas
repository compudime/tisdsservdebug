unit WebModuleUnit1;

interface

uses
  System.SysUtils, System.Classes, Web.HTTPApp, Datasnap.DSHTTPCommon, Datasnap.DSHTTPWebBroker, Datasnap.DSServer,
  Web.WebFileDispatcher, Web.HTTPProd, Datasnap.DSAuth, Datasnap.DSProxyDispatcher, Datasnap.DSProxyJavaAndroid,
  Datasnap.DSProxyJavaBlackBerry, Datasnap.DSProxyObjectiveCiOS, Datasnap.DSProxyCsharpSilverlight,
  Datasnap.DSProxyFreePascal_iOS, Datasnap.DSProxyJavaScript, IPPeerServer, Datasnap.DSMetadata,
  Datasnap.DSServerMetadata, Datasnap.DSClientMetadata, Datasnap.DSCommonServer, Datasnap.DSHTTP,
  Datasnap.DSProxyDelphiRest, System.IniFiles, System.JSON, Data.DBXCommon;

type
  TWebModule1 = class(TWebModule)
    DSHTTPWebDispatcher1: TDSHTTPWebDispatcher;
    DSServer1: TDSServer;
    DSServerClass1: TDSServerClass;
    ServerFunctionInvoker: TPageProducer;
    ReverseString: TPageProducer;
    WebFileDispatcher1: TWebFileDispatcher;
    DSProxyGenerator1: TDSProxyGenerator;
    DSServerMetaDataProvider1: TDSServerMetaDataProvider;
    DSProxyDispatcher1: TDSProxyDispatcher;
    DSAuthenticationManager1: TDSAuthenticationManager;
    WebDispatcherApiV1: TDSHTTPWebDispatcher;
    procedure DSServerClass1GetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
    procedure ServerFunctionInvokerHTMLTag(Sender: TObject; Tag: TTag; const TagString: string; TagParams: TStrings;
      var ReplaceText: string);
    procedure WebModuleDefaultAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
      var Handled: Boolean);
    procedure WebFileDispatcher1BeforeDispatch(Sender: TObject; const AFileName: string; Request: TWebRequest;
      Response: TWebResponse; var Handled: Boolean);
    procedure WebModuleCreate(Sender: TObject);
    procedure DSAuthenticationManager1UserAuthenticate(Sender: TObject; const Protocol, Context, User, Password: string;
      var valid: Boolean; UserRoles: TStrings);
    procedure DSAuthenticationManager1UserAuthorize(Sender: TObject; AuthorizeEventObject: TDSAuthorizeEventObject;
      var valid: Boolean);
    procedure DSHTTPWebDispatcher1FormatResult(Sender: TObject; var ResultVal: TJSONValue; const Command: TDBXCommand;
      var Handled: Boolean);
  private
    { Private declarations }
    FServerFunctionInvokerAction: TWebActionItem;
    function AllowServerFunctionInvoker: Boolean;
  public
    { Public declarations }
  end;

var
  WebModuleClass: TComponentClass = TWebModule1;

implementation

{$R *.dfm}

uses ServerMethodsUnit1, Web.WebReq;

procedure TWebModule1.DSAuthenticationManager1UserAuthenticate(Sender: TObject;
  const Protocol, Context, User, Password: string; var valid: Boolean; UserRoles: TStrings);
begin
  valid := SameText(User, 'CompuDime1') and SameText(Password, User);
  if (not valid) and SameText(User, 'WebOrder') and SameText(Password, User) and SameText(Context, 'api/') then
  begin
    valid := True;
    UserRoles.Clear;
    UserRoles.Add('PostWebOrder');
  end
  else if (not valid) and SameText(User, 'Scanovator') and SameText(Password, 'Chaim') and SameText(Context, 'api/')
  then
  begin
    valid := False;
    UserRoles.Clear;
    //UserRoles.Add('UpdDlvStatus');
  end;
end;

procedure TWebModule1.DSAuthenticationManager1UserAuthorize(Sender: TObject;
  AuthorizeEventObject: TDSAuthorizeEventObject; var valid: Boolean);
begin
  //
end;

procedure TWebModule1.DSHTTPWebDispatcher1FormatResult(Sender: TObject; var ResultVal: TJSONValue;
  const Command: TDBXCommand; var Handled: Boolean);
var
  Aux: TJSONValue;
begin
  //https://stackoverflow.com/questions/4824063/delphi-datasnap-framework-adding-stuff-to-json-message
  if Command.Text.EndsWith('DoDataCmd') or Command.Text.EndsWith('UpdDlvStatus') or
    Command.Text.EndsWith('AddBatchItem') or Command.Text.EndsWith('NewVendorInvoice') or
    Command.Text.EndsWith('PrintLabelRm') or Command.Text.EndsWith('PrdGlobalChanges') or
    Command.Text.EndsWith('PostPrintJob') or Command.Text.EndsWith('IsCustomerInDB') or
    Command.Text.EndsWith('AddCustomerToDB') or Command.Text.EndsWith('AddUpdateCustomer') or
    Command.Text.EndsWith('CustomerCardPayment') or Command.Text.EndsWith('ProductInfo') or
    Command.Text.EndsWith('AddLineBusterBatch') or Command.Text.EndsWith('LocationInventoryTransfer') or
    Command.Text.EndsWith('CustAcntAvailLineBuster') or Command.Text.EndsWith('LineBusterClear') then
  begin
    // remove [] element
    Aux := ResultVal;
    ResultVal := TJSONArray(Aux).Items[0];
    TJSONArray(Aux).Remove(0);
    Aux.Free;
    // we do not need "result" tag
    Handled := True;
  end;
end;

procedure TWebModule1.DSServerClass1GetClass(DSServerClass: TDSServerClass; var PersistentClass: TPersistentClass);
begin
  PersistentClass := ServerMethodsUnit1.TServerMethods1;
end;

procedure TWebModule1.ServerFunctionInvokerHTMLTag(Sender: TObject; Tag: TTag; const TagString: string;
  TagParams: TStrings; var ReplaceText: string);
begin
  if SameText(TagString, 'urlpath') then
    ReplaceText := string(Request.InternalScriptName)
  else if SameText(TagString, 'port') then
    ReplaceText := IntToStr(Request.ServerPort)
  else if SameText(TagString, 'host') then
    ReplaceText := string(Request.Host)
  else if SameText(TagString, 'classname') then
    ReplaceText := ServerMethodsUnit1.TServerMethods1.ClassName
  else if SameText(TagString, 'loginrequired') then
    if DSHTTPWebDispatcher1.AuthenticationManager <> nil then
      ReplaceText := 'true'
    else
      ReplaceText := 'false'
  else if SameText(TagString, 'serverfunctionsjs') then
    ReplaceText := string(Request.InternalScriptName) + '/js/serverfunctions.js'
  else if SameText(TagString, 'servertime') then
    ReplaceText := DateTimeToStr(Now)
  else if SameText(TagString, 'serverfunctioninvoker') then
    if AllowServerFunctionInvoker then
      ReplaceText := '<div><a href="' + string(Request.InternalScriptName) +
        '/ServerFunctionInvoker" target="_blank">Server Functions</a></div>'
    else
      ReplaceText := '';
end;

procedure TWebModule1.WebModuleDefaultAction(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
begin
  if (Request.InternalPathInfo = '') or (Request.InternalPathInfo = '/') then
    Response.Content := ReverseString.Content
  else
    Response.SendRedirect(Request.InternalScriptName + '/');
end;

procedure TWebModule1.WebModuleBeforeDispatch(Sender: TObject; Request: TWebRequest; Response: TWebResponse;
  var Handled: Boolean);
begin
  if FServerFunctionInvokerAction <> nil then
    FServerFunctionInvokerAction.Enabled := AllowServerFunctionInvoker;
end;

function TWebModule1.AllowServerFunctionInvoker: Boolean;
begin
  Result := (Request.RemoteAddr = '127.0.0.1') or (Request.RemoteAddr = '0:0:0:0:0:0:0:1') or
    (Request.RemoteAddr = '::1');
end;

procedure TWebModule1.WebFileDispatcher1BeforeDispatch(Sender: TObject; const AFileName: string; Request: TWebRequest;
  Response: TWebResponse; var Handled: Boolean);
var
  D1, D2: TDateTime;
begin
  Handled := False;
  if SameFileName(ExtractFileName(AFileName), 'serverfunctions.js') then
    if not FileExists(AFileName) or (FileAge(AFileName, D1) and FileAge(WebApplicationFileName, D2) and (D1 < D2)) then
    begin
      DSProxyGenerator1.TargetDirectory := ExtractFilePath(AFileName);
      DSProxyGenerator1.TargetUnitName := ExtractFileName(AFileName);
      DSProxyGenerator1.Write;
    end;
end;

procedure TWebModule1.WebModuleCreate(Sender: TObject);
var
  IniFile: string;
begin
  FServerFunctionInvokerAction := ActionByName('ServerFunctionInvokerAction');
  IniFile := ChangeFileExt(ParamStr(0), '.ini');
  with TIniFile.Create(IniFile) do
    try
      if ReadBool('Settings', 'UseAuthentication', False) then
        DSHTTPWebDispatcher1.AuthenticationManager := DSAuthenticationManager1;
    finally
      Free;
    end;
end;

initialization

finalization

Web.WebReq.FreeWebModules;

end.
